import 'package:flutter/widgets.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_sheet_localization/flutter_sheet_localization.dart';
part 'localizations.g.dart';
// https://docs.google.com/spreadsheets/d/1boWadXBxz9uExM97LqUQBMjZvXOaoX8pHXVzFAg8Kok/edit?usp=sharing
// 1NK-L9tzupc6IFYBePGNTl5qYoyidZszKXnUlU7DcbhY
@SheetLocalization("1boWadXBxz9uExM97LqUQBMjZvXOaoX8pHXVzFAg8Kok","0") // <- See 1. to get DOCID and SHEETID
class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) =>
      AppLocalizations.languages.containsKey(locale);
  @override
  Future<AppLocalizations> load(Locale locale) =>
      SynchronousFuture<AppLocalizations>(AppLocalizations(locale));
  @override
  bool shouldReload(AppLocalizationsDelegate old) => false;

}
