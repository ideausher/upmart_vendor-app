import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:geolocator/geolocator.dart';
import 'package:upmart_driver/modules/auth/auth_bloc/auth_bloc.dart';
import 'package:upmart_driver/modules/auth/screens/initialization/page/initialization_page.dart';
import 'package:upmart_driver/modules/common/app_config/app_config.dart';
import 'package:upmart_driver/modules/common/base_api/base_api.dart';
import 'package:upmart_driver/modules/common/utils/firebase_messaging_utils.dart';
import 'package:upmart_driver/routes.dart';
import 'localizations.dart';
import 'modules/auth/api/sign_in/model/auth_response_model.dart';
import 'modules/common/app_bloc_utilities/bloc_helpers/bloc_provider.dart';
import 'modules/common/utils/fetch_prefs_utils.dart';
import 'modules/common/utils/network_connectivity_utils.dart';
import 'modules/current_location_updator/api/model/location_request_model.dart';
import 'modules/current_location_updator/manager/current_location_manager.dart';

Future<void> main() async {
  final GlobalKey<NavigatorState> navigatorKey =
      new GlobalKey<NavigatorState>();

  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  var _appBloc = AuthBloc(isLoading: true);
  var configuredApp = new AppConfig(
    appName: "Upmart Driver",
    // apiBaseUrl: "http://iphoneapps.co.in:9074/limitless/limitless/public/api/",
    apiBaseUrl: "http://iphoneapps.co.in:9074/upmart/public/api/",
    // apiBaseUrl: "https://loukylimitless.com/lms/public/api/",
    termsUrl: "http://iphoneapps.co.in:9074/upmart/public/terms-and-policy",
    baseApi: BaseApi(navigatorKey),
    child: BlocProvider<AuthBloc>(
      bloc: _appBloc,
      child: MyApp(
        defaultLocale: Locale("en"),
        appBloc: _appBloc,
        navigatorKey: navigatorKey,
      ),
    ),
  );

  HttpOverrides.global = MyHttpOverrides();
  runApp(configuredApp);
}

class MyApp extends StatefulWidget {
  var defaultLocale = Locale("en");
  var appBloc;
  final GlobalKey<NavigatorState> navigatorKey;

  @override
  _MyAppState createState() => _MyAppState();

  MyApp({this.defaultLocale, this.appBloc, this.navigatorKey});
}

class _MyAppState extends State<MyApp> implements LocationChangeCallBack {
  BuildContext _context;

  @override
  void initState() {
    super.initState();
    FirebaseMessagingUtils.firebaseMessagingUtils.initFirebaseMessaging();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      CurrentLocationManger.locationMangerInstance.initLocationManager();
    });

    CurrentLocationManger.locationMangerInstance.addLocationCallback(this);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    CurrentLocationManger.locationMangerInstance.removeLocationCallback(this);
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    return MaterialApp(
      routes: Routes.routes(),
      navigatorKey: widget.navigatorKey,
      debugShowCheckedModeBanner: false,
      locale: widget.defaultLocale,
      // <- Current locale
      localizationsDelegates: [
        const AppLocalizationsDelegate(), // <- Your custom delegate
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: AppLocalizations.languages.keys.toList(),
      title: 'Upmart Driver',
      theme: AppConfig.of(_context).themeData,
      home: InitializationPage(widget.appBloc),
    );
  }

  @override
  Future<void> onLocationChanged({Position currentLocation}) async {
    print("locationchanged --- main");
    LocationRequestModel locationRequestModel = LocationRequestModel(
      latitude: currentLocation?.latitude,
      longitude: currentLocation?.longitude,
      //time: currentLocation?.speedAccuracy,
      speedAccuracy: currentLocation?.speedAccuracy,
      speed: currentLocation?.speed,
      altitude: currentLocation?.altitude,
      heading: currentLocation?.heading,
      accuracy: currentLocation?.accuracy,
    );

    AuthResponseModel _authResponseModel =
        await FetchPrefsUtils.fetchPrefsUtilsInstance.getAuthResponseModel();

    if (_authResponseModel != null && _authResponseModel?.userData != null) {
      NetworkConnectionUtils.networkConnectionUtilsInstance
          .getConnectivityStatus(context, showNetworkDialog: true)
          .then((onValue) {
        if (onValue) {
          callLocationApi(
            _context,
            locationRequestModel,
          );
        }
      });
    }
  }
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context) {
    return super.createHttpClient(context)..maxConnectionsPerHost = 50;
  }
}
