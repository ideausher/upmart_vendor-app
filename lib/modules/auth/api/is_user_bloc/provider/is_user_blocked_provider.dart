import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/auth/api/is_user_bloc/model/is_user_blocked_request_model.dart';
import '../../../../common/app_config/app_config.dart';

class IsUserBlockedProvider
{
  Future<dynamic> isUserBlockApiCall({
    BuildContext context,
    UserBlocRequestModel isUserBlockedRequestModel
  }) async {
    var isBlock = "isblock";

    var result = await AppConfig
        .of(context)
        .baseApi
        .postRequest(
        isBlock,
        context,
        data:isUserBlockedRequestModelToJson(isUserBlockedRequestModel)

    );

    return result;
  }


  Future<dynamic> getProfileApiCall({
    BuildContext context,
    UserBlocRequestModel isUserBlockedRequestModel
  }) async {
    var getProfile = "get-profile";

    var result = await AppConfig
        .of(context)
        .baseApi
        .getRequest(
        getProfile,
        context,
       );



    return result;
  }
}
