// To parse this JSON data, do
//
//     final loginWithOtpRequestModel = loginWithOtpRequestModelFromJson(jsonString);

import 'dart:convert';

LoginWithOtpRequestModel loginWithOtpRequestModelFromJson(String str) => LoginWithOtpRequestModel.fromJson(json.decode(str));

String loginWithOtpRequestModelToJson(LoginWithOtpRequestModel data) => json.encode(data.toJson());

class LoginWithOtpRequestModel {
  LoginWithOtpRequestModel({
    this.type,
    this.phoneNumber,
    this.countryCode,
    this.token,
    this.registration,
    this.userType,
    this.countryIsoCode
  });

  String type;
  String phoneNumber;
  String countryCode;
  String token;
  int registration;
  int userType;
  String countryIsoCode;

  factory LoginWithOtpRequestModel.fromJson(Map<String, dynamic> json) => LoginWithOtpRequestModel(
    type: json["type"] == null ? null : json["type"],
    phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
    countryCode: json["country_code"] == null ? null : json["country_code"],
    token: json["token"] == null ? null : json["token"],
    registration: json["registration"] == null ? null : json["registration"],
    userType: json["user_type"] == null ? null : json["user_type"],
    countryIsoCode: json["country_iso_code"] == null ? null : json["country_iso_code"],
  );

  Map<String, dynamic> toJson() => {
    "type": type == null ? null : type,
    "phone_number": phoneNumber == null ? null : phoneNumber,
    "country_code": countryCode == null ? null : countryCode,
    "token": token == null ? null : token,
    "registration": registration == null ? null : registration,
    "user_type": userType == null ? null : userType,
    "country_iso_code": countryIsoCode == null ? null : countryIsoCode,
  };
}
