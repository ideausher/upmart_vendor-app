import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/auth/api/login_with_otp/model/login_with_otp_request_model.dart';
import '../../../../common/app_config/app_config.dart';

class LoginWithOtpProvider {
  Future<dynamic> loginWithOtpApiCall({
    BuildContext context,
    LoginWithOtpRequestModel loginWithOtpRequestModel
  }) async {
    var loginWithOtp = "login";

    var result = await AppConfig
        .of(context)
        .baseApi
        .postRequest(
        loginWithOtp,
        context,
        data:loginWithOtpRequestModelToJson(loginWithOtpRequestModel)

    );

    return result;
  }
}
