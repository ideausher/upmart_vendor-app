// To parse this JSON data, do
//
//     final authResponseModel = authResponseModelFromJson(jsonString);

import 'dart:convert';

AuthResponseModel authResponseModelFromJson(String str) => AuthResponseModel.fromMap(json.decode(str));

String authResponseModelToJson(AuthResponseModel data) => json.encode(data.toMap());

class AuthResponseModel {
  AuthResponseModel({
    this.userData,
    this.message,
    this.statusCode,
  });

  UserData userData;
  String message;
  int statusCode;

  factory AuthResponseModel.fromMap(Map<String, dynamic> json) => AuthResponseModel(
    userData: json["data"] == null ? null : UserData.fromMap(json["data"]),
    message: json["message"] == null ? null : json["message"],
    statusCode: json["status_code"] == null ? null : json["status_code"],
  );

  Map<String, dynamic> toMap() => {
    "data": userData == null ? null : userData.toMap(),
    "message": message == null ? null : message,
    "status_code": statusCode == null ? null : statusCode,
  };
}

class UserData {
  UserData({
    this.id,
    this.title,
    this.name,
    this.email,
    this.phoneNumber,
    this.countryIsoCode,
    this.countryCode,
    this.verified,
    this.userType,
    this.profilePicture,
    this.address,
    this.accessToken,
    this.availablity,
  });

  int id;
  String title;
  String name;
  String email;
  String phoneNumber;
  String countryIsoCode;
  String countryCode;
  int verified;
  int userType;
  String profilePicture;
  List<Address> address;
  String accessToken;
  int availablity;

  factory UserData.fromMap(Map<String, dynamic> json) => UserData(
    id: json["id"] == null ? null : json["id"],
    title: json["title"] == null ? null : json["title"],
    name: json["name"] == null ? null : json["name"],
    email: json["email"] == null ? null : json["email"],
    phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
    countryIsoCode: json["country_iso_code"] == null ? null : json["country_iso_code"],
    countryCode: json["country_code"] == null ? null : json["country_code"],
    verified: json["verified"] == null ? null : json["verified"],
    userType: json["user_type"] == null ? null : json["user_type"],
    profilePicture: json["profile_picture"],
    address: json["address"] == null ? null : List<Address>.from(json["address"].map((x) => Address.fromMap(x))),
    availablity: json["availablity"] == null ? null : json["availablity"],
    accessToken: json["access_token"] == null ? null : json["access_token"],
  );

  Map<String, dynamic> toMap() => {
    "id": id == null ? null : id,
    "title": title == null ? null : title,
    "name": name == null ? null : name,
    "email": email == null ? null : email,
    "phone_number": phoneNumber == null ? null : phoneNumber,
    "country_iso_code": countryIsoCode == null ? null : countryIsoCode,
    "country_code": countryCode == null ? null : countryCode,
    "verified": verified == null ? null : verified,
    "user_type": userType == null ? null : userType,
    "profile_picture": profilePicture,
    "address": address == null ? null : List<dynamic>.from(address.map((x) => x.toMap())),
    "access_token": accessToken == null ? null : accessToken,
    "availablity": availablity == null ? null : availablity,
  };
}

class Address {
  Address({
    this.addressId,
    this.primary,
    this.city,
    this.country,
    this.addressType,
    this.formattedAddress,
    this.additionalInfo,
    this.latitude,
    this.longitude,
    this.pincode
  });

  int addressId;
  int primary;
  String city;
  String country;
  int addressType;
  String formattedAddress;
  String additionalInfo;
  double latitude;
  double longitude;
  String pincode;

  factory Address.fromMap(Map<String, dynamic> json) => Address(
    addressId: json["id"] == null ? null : json["id"],
    pincode: json["pincode"] == null ? null : json["pincode"],
    primary: json["Primary-address"] == null ? null : json["Primary-address"],
    city: json["city"] == null ? null : json["city"],
    country: json["country"] == null ? null : json["country"],
    addressType: json["address_type"] == null ? null : json["address_type"],
    formattedAddress: json["formatted_address"] == null ? null : json["formatted_address"],
    additionalInfo: json["additional_info"] == null ? null : json["additional_info"],
    latitude: json["latitude"] == null ? null : json["latitude"].toDouble(),
    longitude: json["longitude"] == null ? null : json["longitude"].toDouble(),
  );

  Map<String, dynamic> toMap() => {
    "id": addressId == null ? null : addressId,
    "Primary-address": primary == null ? null : primary,
    "city": city == null ? null : city,
    "country": country == null ? null : country,
    "address_type": addressType == null ? null : addressType,
    "formatted_address": formattedAddress == null ? null : formattedAddress,
    "additional_info": additionalInfo == null ? null : additionalInfo,
    "latitude": latitude == null ? null : latitude,
    "longitude": longitude == null ? null : longitude,
    "pincode": pincode == null ? null : pincode,
  };
}
