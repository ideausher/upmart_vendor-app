// To parse this JSON data, do
//
//     final enterDetailsRequestModel = enterDetailsRequestModelFromJson(jsonString);

import 'dart:convert';

EnterDetailsRequestModel enterDetailsRequestModelFromJson(String str) => EnterDetailsRequestModel.fromJson(json.decode(str));

String enterDetailsRequestModelToJson(EnterDetailsRequestModel data) => json.encode(data.toJson());

class EnterDetailsRequestModel {
  EnterDetailsRequestModel({
    this.name,
    this.countryCode,
    this.countryIsoCode,
    this.title
  });

  String name;
  String countryCode;
  String countryIsoCode;
  String title;

  factory EnterDetailsRequestModel.fromJson(Map<String, dynamic> json) => EnterDetailsRequestModel(
    name: json["name"] == null ? null : json["name"],
    countryCode: json["country_code"] == null ? null : json["country_code"],
    countryIsoCode: json["country_iso_code"] == null ? null : json["country_iso_code"],
    title: json["title"] == null ? null : json["title"],
  );

  Map<String, dynamic> toJson() => {
    "name": name == null ? null : name,
    "country_code": countryCode == null ? null : countryCode,
    "country_iso_code": countryIsoCode == null ? null : countryIsoCode,
    "title": title == null ? null : title,
  };
}
