// To parse this JSON data, do
//
//     final enterDetailsResponseModel = enterDetailsResponseModelFromJson(jsonString);

import 'dart:convert';

EnterDetailsResponseModel enterDetailsResponseModelFromJson(String str) => EnterDetailsResponseModel.fromMap(json.decode(str));

String enterDetailsResponseModelToJson(EnterDetailsResponseModel data) => json.encode(data.toJson());

class EnterDetailsResponseModel {
  EnterDetailsResponseModel({
    this.data,
    this.message,
    this.statusCode,
  });

  Data data;
  String message;
  int statusCode;

  factory EnterDetailsResponseModel.fromMap(Map<String, dynamic> json) => EnterDetailsResponseModel(
    data: json["data"] == null ? null : Data.fromMap(json["data"]),
    message: json["message"] == null ? null : json["message"],
    statusCode: json["status_code"] == null ? null : json["status_code"],
  );

  Map<String, dynamic> toJson() => {
    "data": data == null ? null : data.toJson(),
    "message": message == null ? null : message,
    "status_code": statusCode == null ? null : statusCode,
  };
}

class Data {
  Data({
    this.id,
    this.title,
    this.name,
    this.email,
    this.phoneNumber,
    this.countryIsoCode,
    this.countryCode,
    this.verified,
    this.userType,
    this.profilePicture,
    this.userAddress,
  });

  int id;
  String name;
  String title;
  String email;
  String phoneNumber;
  String countryIsoCode;
  String countryCode;
  int verified;
  int userType;
  String profilePicture;
  List<UserAddress> userAddress;

  factory Data.fromMap(Map<String, dynamic> json) => Data(
    id: json["id"] == null ? null : json["id"],
    title: json["title"] == null ? null : json["title"],
    name: json["name"] == null ? null : json["name"],
    email: json["email"] == null ? null : json["email"],
    phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
    countryIsoCode: json["country_iso_code"] == null ? null : json["country_iso_code"],
    countryCode: json["country_code"] == null ? null : json["country_code"],
    verified: json["verified"] == null ? null : json["verified"],
    userType: json["user_type"] == null ? null : json["user_type"],
    profilePicture: json["profile_picture"],
    userAddress:  json["address"] == null ? null : List<UserAddress>.from(json["address"].map((x) => UserAddress.fromMap(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "title": title == null ? null : title,
    "email": email == null ? null : email,
    "phone_number": phoneNumber == null ? null : phoneNumber,
    "country_iso_code": countryIsoCode == null ? null : countryIsoCode,
    "country_code": countryCode == null ? null : countryCode,
    "verified": verified == null ? null : verified,
    "user_type": userType == null ? null : userType,
    "profile_picture": profilePicture,
    "address": userAddress == null ? null : List<dynamic>.from(userAddress.map((x) => x.toMap())),
  };
}


class UserAddress {
  UserAddress({
    this.addressId,
    this.primary,
    this.city,
    this.country,
    this.addressType,
    this.formattedAddress,
    this.additionalInfo,
    this.latitude,
    this.longitude,
    this.pincode
  });

  int addressId;
  int primary;
  String city;
  String pincode;
  String country;
  int addressType;
  String formattedAddress;
  String additionalInfo;
  double latitude;
  double longitude;

  factory UserAddress.fromMap(Map<String, dynamic> json) => UserAddress(
    addressId: json["address_id"] == null ? null : json["address_id"],
    primary: json["primary"] == null ? null : json["primary"],
    city: json["city"] == null ? null : json["city"],
    pincode: json["pincode"] == null ? null : json["pincode"],
    country: json["country"] == null ? null : json["country"],
    addressType: json["address_type"] == null ? null : json["address_type"],
    formattedAddress: json["formatted_address"] == null ? null : json["formatted_address"],
    additionalInfo: json["additional_info"] == null ? null : json["additional_info"],
    latitude: json["latitude"] == null ? null : json["latitude"].toDouble(),
    longitude: json["longitude"] == null ? null : json["longitude"].toDouble(),
  );

  Map<String, dynamic> toMap() => {
    "address_id": addressId == null ? null : addressId,
    "primary": primary == null ? null : primary,
    "city": city == null ? null : city,
    "pincode": pincode == null ? null : pincode,
    "country": country == null ? null : country,
    "address_type": addressType == null ? null : addressType,
    "formatted_address": formattedAddress == null ? null : formattedAddress,
    "additional_info": additionalInfo == null ? null : additionalInfo,
    "latitude": latitude == null ? null : latitude,
    "longitude": longitude == null ? null : longitude,
  };
}