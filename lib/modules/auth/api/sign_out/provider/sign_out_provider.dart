import 'package:flutter/material.dart';
import '../../../../common/app_config/app_config.dart';

class SignOutProvider {
  Future<dynamic> signOutApiCall({BuildContext context}) async {
    var path = "logout";

    var result = await AppConfig.of(context).baseApi.postRequest(
      path,
      context,
    );

    return result;
  }
}
