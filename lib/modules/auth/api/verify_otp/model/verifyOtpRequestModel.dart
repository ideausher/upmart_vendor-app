// To parse this JSON data, do
//
//     final verifyOtpRequestModel = verifyOtpRequestModelFromJson(jsonString);

import 'dart:convert';

VerifyOtpRequestModel verifyOtpRequestModelFromJson(String str) => VerifyOtpRequestModel.fromMap(json.decode(str));

String verifyOtpRequestModelToJson(VerifyOtpRequestModel data) => json.encode(data.toMap());

class VerifyOtpRequestModel {
  String otp;

  VerifyOtpRequestModel({
    this.otp,
  });

  factory VerifyOtpRequestModel.fromMap(Map<String, dynamic> json) => VerifyOtpRequestModel(
    otp: json["otp"] == null ? null : json["otp"],
  );

  Map<String, dynamic> toMap() => {
    "otp": otp == null ? null : otp,
  };
}
