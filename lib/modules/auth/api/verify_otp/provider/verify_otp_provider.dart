import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/auth/api/sign_up/model/send_otp_request_model.dart';
import '../../../../auth/api/verify_otp/model/verify_otp_request_model.dart';
import '../../../../common/app_config/app_config.dart';

class VerifyOtpProvider {
  Future<dynamic> verifyOtpApiCall({BuildContext context, VerifyOtpRequestModel verifyOtpRequestModel}) async {
    var confirmOtp = "verify-otp";

    var result = await AppConfig.of(context).baseApi.postRequest(
      confirmOtp,
      context,
      data:verifyOtpRequestModelToJson(verifyOtpRequestModel),
    );

    return result;
  }



}
