import 'package:flutter/cupertino.dart';
import 'package:upmart_driver/modules/auth/api/is_user_bloc/model/is_user_blocked_response_model.dart';
import 'package:upmart_driver/modules/common/model/common_response_model.dart';
import 'package:upmart_driver/modules/common/model/update_ui_data_model.dart';
import 'package:upmart_driver/modules/force_update/model/force_update_response_model.dart';
import '../../../modules/auth/api/sign_in/model/auth_response_model.dart';
import '../../../modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';

class AuthState extends BlocState {
  final bool isLoading; // used to show loader
  final AuthResponseModel authResponseModel;
  final BuildContext context;
  final String message, dropDownValue;
   UpdateUiDataModel updateUiDataModel;
  final CommonResponseModel commonResponseModel;
  final IsUserBlockedResponseModel isUserBlockedResponseModel;
  final  bool isUserBlock,isResend;
  final ForceUpdateResponseModel forceUpdateResponseModel;
  final bool needToUpdate;


  AuthState({this.isLoading,
    this.authResponseModel,
    this.context,
    this.message,
    this.dropDownValue,
    this.isResend,
    this.updateUiDataModel,
    this.commonResponseModel,
    this.isUserBlock,
    this.isUserBlockedResponseModel,
    this.forceUpdateResponseModel,
    this.needToUpdate
  })
      : super(isLoading);

  // not authenticated
  factory AuthState.initiating({bool isLoading}) {
    return AuthState(
      isLoading: isLoading,
    );
  }

  factory AuthState.updateUi({bool isLoading,
    AuthResponseModel authResponseModel,
    BuildContext context,
    String message,
    UpdateUiDataModel updateUiDataModel,
    CommonResponseModel commonResponseModel,
    IsUserBlockedResponseModel isUserBlockedResponseModel,
    bool isUserBlock,
    bool isResend,
    ForceUpdateResponseModel forceUpdateResponseModel,
    bool needToUpdate,
  }) {
    return AuthState(
        isLoading: isLoading,
        context: context,
        authResponseModel: authResponseModel,
        message: message,
        updateUiDataModel: updateUiDataModel,
        commonResponseModel: commonResponseModel,
       isUserBlock: isUserBlock,
       isResend: isResend,
       isUserBlockedResponseModel: isUserBlockedResponseModel,
      forceUpdateResponseModel: forceUpdateResponseModel,
      needToUpdate: needToUpdate,

    );
  }
}
