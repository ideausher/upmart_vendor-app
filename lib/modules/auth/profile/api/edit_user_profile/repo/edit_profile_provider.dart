import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/auth/api/sign_in/model/enter_details_request_model.dart';
import 'package:upmart_driver/modules/auth/profile/model/check_user_request_model.dart';
import '../../../../../../modules/auth/profile/api/edit_user_profile/model/edit_email_phone_request_model.dart';
import '../../../../../../modules/common/app_config/app_config.dart';

class EditUserProfileProvider {
  // upload profile pic
  Future<dynamic> uploadProfileImage({BuildContext context, File file}) async {
    FormData formData = FormData.fromMap({
      "image": await MultipartFile.fromFile(file.path)
    });
    var path = "upload-image";
    var result = await AppConfig
        .of(context)
        .baseApi
        .postRequest(path, context, data: formData);
    return result;
  }

  //edit user name
  Future<dynamic> editUserName(
      {BuildContext context, EnterDetailsRequestModel enterDetailsRequestModel}) async {
    var path = "update-profile";
    var result = await AppConfig
        .of(context)
        .baseApi
        .postRequest(path, context,
        data: enterDetailsRequestModelToJson(enterDetailsRequestModel));

    return result;
  }

  //edit user phone number and email
  Future<dynamic> editUserEmailPhoneNumber(
      {BuildContext context, EditEmailPhoneRequestModel editEmailPhoneRequest}) async {
    var path = "v1/updateUserCredSendOtp";
    var result = await AppConfig
        .of(context)
        .baseApi
        .postRequest(path, context, data: editEmailPhoneRequest);

    return result;
  }


  //check user exist or not with phone or email
  Future<dynamic> checkUserExistance(
      {BuildContext context, CheckUserRequestModel checkUserRequestModel}) async {
    var path = "check-user";
    var data;
    var result;
    /*f(checkUserRequestModel?.phoneNumber!=null)
      {
        data={
         "phone_number":checkUserRequestModel?.phoneNumber,
          "country_code":checkUserRequestModel?.countryCode

        };
      }
    else
      {
        data={"email":checkUserRequestModel?.email};
      }*/
    if (checkUserRequestModel?.phoneNumber != null) {
       result = await AppConfig
          .of(context)
          .baseApi
          .postRequest(path, context,
          data: checkUserRequestModelToJsonForPhone(checkUserRequestModel));
    }
    else {
      result = await AppConfig
          .of(context)
          .baseApi
          .postRequest(path, context,
          data:{"email":checkUserRequestModel?.email});
    }

    return result;
  }
}
