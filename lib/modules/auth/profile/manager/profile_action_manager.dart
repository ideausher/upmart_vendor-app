import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_country_picker/country.dart';
import 'package:upmart_driver/modules/auth/api/sign_up/model/send_otp_request_model.dart';
import 'package:upmart_driver/modules/auth/auth_bloc/auth_state.dart';
import 'package:upmart_driver/modules/auth/manager/auth_manager.dart';
import 'package:upmart_driver/modules/auth/profile/enums/setting_enums.dart';
import 'package:upmart_driver/modules/auth/profile/model/check_user_request_model.dart';
import 'package:upmart_driver/modules/common/app_config/app_config.dart';
import 'package:upmart_driver/modules/common/model/common_response_model.dart';
import 'package:upmart_driver/modules/common/utils/launcher_utils.dart';
import 'package:upmart_driver/modules/vehicle_documents/constants/string_constant.dart';
import 'package:upmart_driver/modules/vehicle_documents/registration_routes.dart';
import '../../../../modules/auth/profile/api/edit_profile_verify_otp/model/edit_profile_verify_otp_request_model.dart';
import '../../../../modules/auth/profile/api/edit_user_profile/model/edit_email_phone_request_model.dart';
import '../../../../modules/auth/api/sign_in/model/auth_response_model.dart';
import '../../../../modules/auth/auth_bloc/auth_bloc.dart';
import '../../../../modules/auth/auth_bloc/auth_event.dart';
import '../../../../modules/auth/enums/auth_enums.dart';
import '../../../../modules/auth/profile/bloc/edit_profile_verify_otp/edit_profile_verify_otp_bloc.dart';
import '../../../../modules/auth/profile/bloc/edit_profile_verify_otp/edit_profile_verify_otp_event.dart';
import '../../../../modules/auth/profile/bloc/edit_profile_verify_otp/edit_profile_verify_otp_state.dart';
import '../../../../modules/auth/profile/bloc/profile_bloc/profile_bloc.dart';
import '../../../../modules/auth/profile/bloc/profile_bloc/profile_event.dart';
import '../../../../modules/auth/profile/bloc/profile_bloc/profile_state.dart';
import '../../../../modules/auth/profile/model/common_pass_data_model.dart';
import '../../../../modules/auth/validator/auth_validator.dart';
import '../../../../modules/common/enum/enums.dart';
import '../../../../modules/common/utils/common_utils.dart';
import '../../../../modules/common/utils/dialog_snackbar_utils.dart';
import '../../../../modules/common/utils/navigator_utils.dart';
import '../../../../modules/common/utils/network_connectivity_utils.dart';
import '../../../../modules/file_picker/dialog/single_file_picker_dialog.dart';
import '../../../../modules/file_picker/enums/file_picker_enums.dart';
import '../../../../modules/file_picker/model/file_picker_model.dart';
import '../../../../localizations.dart';
import '../../../../routes.dart';
import '../../auth_routes.dart';

class ProfileActionManager {
  AuthManager _authManager = new AuthManager();

  // used to set data on controller
  setUserData(
      {AuthResponseModel authResponseModel,
      TextEditingController nameController,
      TextEditingController emailController,
      TextEditingController phoneController,
      AuthState authState}) {
    if (authResponseModel != null) {
      nameController.text =
          (nameController != null && nameController?.text?.isNotEmpty)
              ? nameController.text
              : authResponseModel.userData.name;
      emailController.text = authResponseModel.userData.email;
      phoneController.text = authResponseModel.userData.phoneNumber;
    }
  }

  // for state change in the profile screen
  actionOnProfileStateChange(
      {AuthBloc authBloc,
      ProfileState profileState,
      TextEditingController nameController,
      bool enableProfileEditing,
      BuildContext context,
      ScaffoldState scaffoldState,
      ProfileBloc profileBloc}) {
    // need to update data in the auth bloc
    if (profileState?.message?.isNotEmpty == true) {
      DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
          context: context,
          scaffoldState: scaffoldState,
          message: profileState?.message);
      if (profileState?.status == ApiStatus.Updated) {
        nameController.text = profileState?.authResponseModel?.userData?.name;
        // update auth bloc
        authBloc.emitEvent(UpdateBlocEvent(
            isLoading: false,
            context: context,
            authResponseModel: profileState?.authResponseModel));
      }
      profileBloc.emitEvent(UpdateEvent(
          isLoading: false,
          context: context,
          authResponseModel: profileState?.authResponseModel,
          enableProfileEditing: profileState?.enableProfileEditing));
    }
  }

  // for action perform on change profile image
  onProfileImageUpdate(
      {BuildContext context,
      ProfileBloc profileBloc,
      ProfileState profileState,
      AuthResponseModel authResponseModel,
      ScaffoldState scaffoldState}) {
    SingleFilePickerDialog.singleFilePickerDialogInstance.showFilePickerDialog(
        filePickerModel: FilePickerModel(
          pickerType: FilePickerTypeEnum.PickerTypeImage,
          pickFrom: FilePickFromEnum.PickFromBoth,
        ),
        context: context,
        fileData: (value) {
          var _image = File(value);
          if (_image != null) {
            NetworkConnectionUtils.networkConnectionUtilsInstance
                .getConnectivityStatus(context, showNetworkDialog: true)
                .then((onValue) {
              if (onValue) {
                // hide keyboard
                CommonUtils.commonUtilsInstance.hideKeyboard(context: context);
                // emit edit profile event
                profileBloc.emitEvent(EditUserImage(
                  isLoading: true,
                  enableProfileEditing: profileState?.enableProfileEditing,
                  authResponseModel: authResponseModel,
                  context: context,
                  userProfileImage: _image,
                ));
              }
            });
          }
        });
  }

  actionOnContinueButton({
    String name,
    String title,
    ProfileBloc profileBloc,
    AuthResponseModel authResponseModel,
    BuildContext context,
    bool enableProfileEditing,
    ScaffoldState scaffoldState,
  }) {
    String result;
    // validate name is empty
    // check name field is empty or not
    result = AuthValidator.authValidatorInstance.enterDetailsScreen(
      context: context,
      name: name,
    );

    if (result.isNotEmpty) {
      if (AuthValidator.authValidatorInstance.checkValidName(value: name) ==
          true) result = result;
    }
    if (result.isEmpty) {
      // connection check
      NetworkConnectionUtils.networkConnectionUtilsInstance
          .getConnectivityStatus(context, showNetworkDialog: true)
          .then((onValue) {
        if (onValue) {
          // hide keyboard
          CommonUtils.commonUtilsInstance.hideKeyboard(context: context);
          // emit sign up event
          profileBloc.emitEvent(
            EditUserNameEvent(
              authResponseModel: authResponseModel,
              isLoading: true,
              context: context,
              title: title,
              userName:
                  AuthValidator.authValidatorInstance.trimValue(value: name),
              enableProfileEditing: enableProfileEditing,
            ),
          );
        }
      });
    } else {
      // if error
      DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
          context: context, scaffoldState: scaffoldState, message: result);
    }
  }

  // for state change in the edit email phone screen state change
  actionOnEditPhoneEmailStateChange(
      {ProfileState profileState,
      TextEditingController phoneController,
      TextEditingController emailController,
      int tagCameFrom,
      AuthBloc authBloc,
      ProfileBloc profileBloc,
      AuthState authState,
      BuildContext context,
      ScaffoldState scaffoldState,
      Country country}) {
    if (profileState?.isLoading == false) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        if (profileState?.commonResponseModel != null &&
            profileState?.commonResponseModel?.status ==
                ApiStatus.Success.value) {
          profileBloc?.emitEvent(UpdateEvent(
              enableProfileEditing: profileState?.enableProfileEditing,
              authResponseModel: authState?.authResponseModel,
              commonResponseModel: new CommonResponseModel(),
              context: context));

          if (profileState?.isSendOtp == true) {
            String value = (tagCameFrom == VerifyOtpFrom.PhoneNumber.value)
                ? phoneController.text.toString()
                : emailController.text.toString();
            NavigatorUtils.navigatorUtilsInstance.navigatorPopAndPushNamed(
                context, AuthRoutes.PROFILE_EDIT_VERIFY_OTP_SCREEN_ROOT,
                dataToBeSend: CommonPassDataModel(tagCameFrom, value, country,
                    sendOtpRequestModel: profileState?.sendOtpRequestModel));
          } else {
            DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                context: context,
                scaffoldState: scaffoldState,
                message: profileState?.message);
            print("signUp==> ${profileState?.message}");
          }
        } else if (profileState?.commonResponseModel != null &&
            profileState?.commonResponseModel?.status ==
                ApiStatus.NotFound.value) {
          profileBloc?.emitEvent(UpdateEvent(
              enableProfileEditing: profileState?.enableProfileEditing,
              authResponseModel: authState?.authResponseModel,
              commonResponseModel: new CommonResponseModel(),
              context: context));

          //if user does not exist call send otp API
          _callsendOtpApi(
              context,
              scaffoldState,
              authBloc,
              authState,
              phoneController.text,
              country,
              profileBloc,
              tagCameFrom,
              emailController.text);
        } else if (profileState?.message?.toString()?.trim()?.isNotEmpty ==
            true) {
          DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
              context: context,
              scaffoldState: scaffoldState,
              message: profileState?.message);
          print("signUp==> ${profileState?.message}");
        }
      });
    }
  }

  // for perform action on the send otp button in edit email phone screen
  actionOnSendOtpPressedOnEditPhoneEmailScreen({
    int tagCameFrom,
    TextEditingController phoneController,
    AuthResponseModel authResponseModel,
    TextEditingController emailController,
    BuildContext context,
    ScaffoldState scaffoldState,
    String countryCode,
    String diallingCode,
    ProfileBloc profileBloc,
    Country country,
  }) {
    if (tagCameFrom == VerifyOtpFrom.PhoneNumber.value) {
      String result;
      // validate phone is empty
      result = AuthValidator.authValidatorInstance.validateSendOtpScreen(
          phoneNumber: phoneController?.text,
          context: context,
          country:
              Country.IN /*signUpDataModel?.selectedCountry ?? Country.IN*/,
          isConditionAgreed: true);

      if (result.isNotEmpty) {
        DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
          context: context,
          scaffoldState: scaffoldState,
          message: result,
        );
      } else {
        NetworkConnectionUtils.networkConnectionUtilsInstance
            .getConnectivityStatus(context, showNetworkDialog: true)
            .then(
          (onValue) async {
            if (onValue) {
              // create request
              EditEmailPhoneRequestModel _editEmailPhoneRequest =
                  new EditEmailPhoneRequestModel(
                type: VerifyOtpFrom.PhoneNumber.value,
                countryIsoCode: countryCode,
                countryCode: diallingCode,
                country: country,
                value: AuthValidator.authValidatorInstance
                    .trimValue(value: phoneController.text),
              );
              // hide keyboard
              CommonUtils.commonUtilsInstance.hideKeyboard(context: context);
              // emit event
              profileBloc.emitEvent(EditUserEmailPhoneEvent(
                  isLoading: true,
                  context: context,
                  authResponseModel: authResponseModel,
                  checkUserRequestModel: new CheckUserRequestModel(
                    phoneNumber: phoneController?.text,
                    countryCode: "+${diallingCode}",
                  ),
                  editEmailPhoneRequest: _editEmailPhoneRequest));
            }
          },
        );
      }
    } else {
      var result;
      // validate email is empty
      if (AuthValidator.authValidatorInstance
              .checkEmptyField(value: emailController?.text) ==
          true)
        result = AppLocalizations.of(context).common.error.pleaseEnterEmail;

      if (result == null) {
        if (AuthValidator.authValidatorInstance
                .checkValidEmail(value: emailController?.text) ==
            false)
          result =
              AppLocalizations.of(context).editProfile.error.enterValidEmail;
      }
      if (result == null) {
        NetworkConnectionUtils.networkConnectionUtilsInstance
            .getConnectivityStatus(context, showNetworkDialog: true)
            .then(
          (onValue) async {
            if (onValue) {
              EditEmailPhoneRequestModel _editEmailPhoneRequest =
                  new EditEmailPhoneRequestModel(
                      type: VerifyOtpFrom.Email.value,
                      countryCode: countryCode,
                      diallingCode: diallingCode,
                      country: country,
                      value: emailController.text.toString());
              // hide keyboard
              CommonUtils.commonUtilsInstance.hideKeyboard(context: context);
              // emit event
              profileBloc.emitEvent(EditUserEmailPhoneEvent(
                  isLoading: true,
                  context: context,
                  checkUserRequestModel:
                      new CheckUserRequestModel(email: emailController.text),
                  authResponseModel: authResponseModel,
                  editEmailPhoneRequest: _editEmailPhoneRequest));
            }
          },
        );
      } else {
        DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
          context: context,
          scaffoldState: scaffoldState,
          message: result,
        );
      }
    }
  }

  // for verify otp
  editProfileVerifyOtpCall(
      {BuildContext context,
      ScaffoldState scaffoldState,
      EditProfileVerifyOtpBloc editProfileVerifyOtpBloc,
      String otp,
      AuthResponseModel authResponseModel,
      CommonPassDataModel dataModel}) {
    // check otp
    var result = AuthValidator.authValidatorInstance.validateVerifyOtpScreen(
      value: otp,
      context: context,
    );
    // if no error
    if (result.isEmpty) {
      NetworkConnectionUtils.networkConnectionUtilsInstance
          .getConnectivityStatus(context, showNetworkDialog: true)
          .then((onValue) {
        if (onValue) {
          // hide keyboard
          CommonUtils.commonUtilsInstance.hideKeyboard(context: context);
          editProfileVerifyOtpBloc.emitEvent(
            ProfileEditVerifyOtpEvent(
              isLoading: true,
              context: context,
              authResponseModel: authResponseModel,
              verifyOtpRequestModel: EditProfileVerifyOtpRequestModel(
                  otp:
                      AuthValidator.authValidatorInstance.trimValue(value: otp),
                  phoneNumber: getData(dataModel),
                  email: getEmail(dataModel),
                  action: "0",
                  countryCode: getCountryCode(dataModel),
                  countryIsoCode: getCountryIsoCode(dataModel),
                  type: LoginWithPhoneOrEmail?.phone?.toString()),
            ),
          );
        }
      });
    } else {
      // show error
      DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
          context: context, scaffoldState: scaffoldState, message: result);
    }
  }

  // for resend otp
  editProfileResendOtpCall({
    BuildContext context,
    ScaffoldState scaffoldState,
    SendOtpRequestModel sendOtpRequestModel,
    EditProfileVerifyOtpBloc editProfileVerifyOtpBloc,
    AuthResponseModel authResponseModel,
  }) {
    // check internet connection
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) {
      if (onValue) {
        // hide keyboard
        CommonUtils.commonUtilsInstance.hideKeyboard(context: context);
        // bloc update
        editProfileVerifyOtpBloc.emitEvent(
          ProfileEditResendOtpEvent(
              isLoading: true,
              context: context,
              authResponseModel: authResponseModel,
              sendOtpRequestModel: sendOtpRequestModel),
        );
      }
    });
  }

  // for state change in the profile screen
  actionOnEditProfileVerifyOtpStateChange({
    AuthBloc authBloc,
    EditProfileVerifyOtpState editProfileVerifyOtpState,
    bool enableProfileEditing,
    TextEditingController otpController,
    BuildContext context,
    ScaffoldState scaffoldState,
  }) {
    // need to update data in the auth bloc
    if (editProfileVerifyOtpState?.isLoading == false) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        if (editProfileVerifyOtpState?.status == ApiStatus.Updated) {
          if (editProfileVerifyOtpState?.authResponseModel != null &&
              editProfileVerifyOtpState?.authResponseModel?.statusCode ==
                  ApiStatus.Success.value) {
            if (editProfileVerifyOtpState?.status == ApiStatus.Updated) {
              authBloc.emitEvent(UpdateBlocEvent(
                  isLoading: false,
                  context: context,
                  authResponseModel:
                      editProfileVerifyOtpState?.authResponseModel));
              NavigatorUtils.navigatorUtilsInstance.navigatorPopUntil(
                  context, AuthRoutes.EDIT_PROFILE_SCREEN_ROOT);
            }
          }
        } else if (editProfileVerifyOtpState?.commonResponseModel != null &&
            editProfileVerifyOtpState?.commonResponseModel?.status ==
                ApiStatus.Success.value) {
          print('otp resend');
          otpController.clear();
          DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
              context: context,
              scaffoldState: scaffoldState,
              message: editProfileVerifyOtpState?.commonResponseModel?.message);
        } else {
          if (editProfileVerifyOtpState?.message?.isNotEmpty == true) {
            // if error
            DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                context: context,
                scaffoldState: scaffoldState,
                message: editProfileVerifyOtpState?.message);
          }
        }
      });
    }
  }

  //this action will be performed when user updates its country code
  actionOnUpdateCountryCodeStateChanged(
      {ProfileBloc profileBloc,
      Country selectedCountry,
      AuthResponseModel authResponseModel,
      BuildContext context,
      ScaffoldState scaffoldState}) {
    profileBloc?.emitEvent(UpdateUserCountryEvent(
      context: context,
      selectedCountry: selectedCountry,
      authResponseModel: authResponseModel,
      isLoading: false,
    ));
  }

  void _callsendOtpApi(
      BuildContext context,
      ScaffoldState scaffoldState,
      AuthBloc authBloc,
      AuthState authState,
      String phoneNumber,
      Country selectedCountry,
      ProfileBloc profileBloc,
      int tagCameFrom,
      String email) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) {
      if (onValue) {
        // hide keyboard
        CommonUtils.commonUtilsInstance.hideKeyboard(context: context);
        // emit sign up event
        profileBloc.emitEvent(SendProfileOtpEvent(
          isLoading: true,
          context: context,
          updateUiDataModel: authState?.updateUiDataModel,
          authResponseModel: authState?.authResponseModel,
          sendOtpRequestModel: getRequestModelBasedOnTag(
              tagCameFrom, phoneNumber, email, selectedCountry),
        ));
      }
    });
  }

  //get email base on the value of tag i.e for verifying otp on email or phone
  getEmail(CommonPassDataModel dataModel) {
    if (dataModel?.tag == VerifyOtpFrom.Email.value) {
      return "${dataModel?.value}";
    } else {
      return "";
    }
  }

  //get country code for if verifying otp for phone number
  getCountryCode(CommonPassDataModel dataModel) {
    if (dataModel?.tag == VerifyOtpFrom.PhoneNumber.value) {
      return "+${dataModel?.country?.dialingCode}";
    } else {
      return "";
    }
  }

  getCountryIsoCode(CommonPassDataModel dataModel) {
    if (dataModel?.tag == VerifyOtpFrom.PhoneNumber.value) {
      return "${dataModel?.country?.isoCode}";
    } else {
      return "";
    }
  }

  //here creating request model for sending otp for i.e verify phone number or email
  getRequestModelBasedOnTag(int tagCameFrom, String phoneNumber, String email,
      Country selectedCountry) {
    if (tagCameFrom == VerifyOtpFrom.PhoneNumber.value) {
      return SendOtpRequestModel(
          type: LoginWithPhoneOrEmail?.phone?.value.toString(),
          countryIsoCode: selectedCountry?.isoCode,
          phoneNumber:
              AuthValidator.authValidatorInstance.trimValue(value: phoneNumber),
          countryCode: "+${selectedCountry?.dialingCode}");
    } else {
      return SendOtpRequestModel(
          type: LoginWithPhoneOrEmail?.email?.value.toString(), email: email);
    }
  }

  getData(CommonPassDataModel dataModel) {
    if (dataModel?.tag == VerifyOtpFrom.PhoneNumber.value) {
      return "${dataModel?.value}";
    } else {
      return "";
    }
  }

  actionOnVerifyOtpStateChange(
      AuthState _authState,
      var _loggedOut,
      BuildContext context,
      EditProfileVerifyOtpState editProfileVerifyOtpState) {
    if (_authState.isLoading == false) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        if (_authState?.authResponseModel == null &&
            _authState.isLoading == false &&
            _loggedOut == false) {
          _loggedOut = true;
          NavigatorUtils.navigatorUtilsInstance
              .navigatorClearStack(context, AuthRoutes.SEND_OTP_SCREEN);
        }
      });
    }
  }

  //Action on Profile Item Click
  void actionOnProfileItemClick(
      {int index,
      AuthState authState,
      BuildContext context,
      ScaffoldState scaffoldState,
      AuthBloc authBloc}) {
    if (index == ProfileItemEnum.MyEarnings.value) {
      NavigatorUtils.navigatorUtilsInstance.navigatorPushedName(
          context, RegistrationRoutes.MY_EARNING_PAGE,
          dataToBeSend: PROFILE);
    } else if (index == ProfileItemEnum.HelpAndSupport.value) {
      NavigatorUtils.navigatorUtilsInstance
          .navigatorPushedName(context, Routes.QUERY_LISTING_PAGE);
    } else if (index == ProfileItemEnum.MyDocuments.value) {
      NavigatorUtils.navigatorUtilsInstance.navigatorPushedName(
          context, RegistrationRoutes.DRIVER_DETAILS,
          dataToBeSend: PROFILE);
    } else if (index == ProfileItemEnum.TermsAndPolicy.value) {
      LauncherUtils.launcherUtilsInstance
          .launchInBrowser(url: AppConfig.of(context).termsUrl);
    } else if (index == ProfileItemEnum.LogOut.value) {
      return _authManager?.signOutDialog(
          context: context,
          authState: authState,
          scaffoldState: scaffoldState,
          authBloc: authBloc);
    }
  }
}
