// To parse this JSON data, do
//
//     final editProfileResponseModel = editProfileResponseModelFromJson(jsonString);

import 'dart:convert';

EditProfileResponseModel editProfileResponseModelFromJson(String str) => EditProfileResponseModel.fromMap(json.decode(str));

String editProfileResponseModelToJson(EditProfileResponseModel data) => json.encode(data.toJson());

class EditProfileResponseModel {
  EditProfileResponseModel({
    this.data,
    this.message,
    this.statusCode,
  });

  Data data;
  String message;
  int statusCode;

  factory EditProfileResponseModel.fromMap(Map<String, dynamic> json) => EditProfileResponseModel(
    data: json["data"] == null ? null : Data.fromMap(json["data"]),
    message: json["message"] == null ? null : json["message"],
    statusCode: json["status_code"] == null ? null : json["status_code"],
  );

  Map<String, dynamic> toJson() => {
    "data": data == null ? null : data.toJson(),
    "message": message == null ? null : message,
    "status_code": statusCode == null ? null : statusCode,
  };
}

class Data {
  Data({
    this.image,
  });

  String image;

  factory Data.fromMap(Map<String, dynamic> json) => Data(
    image: json["profile_picture"] == null ? null : json["profile_picture"],
  );

  Map<String, dynamic> toJson() => {
    "profile_picture": image == null ? null : image,
  };
}
