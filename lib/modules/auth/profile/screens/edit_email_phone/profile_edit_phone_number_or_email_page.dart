import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_country_picker/flutter_country_picker.dart';
import 'package:upmart_driver/modules/auth/constants/image_constant.dart';
import 'package:upmart_driver/modules/auth/validator/auth_validator.dart';
import 'package:upmart_driver/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:upmart_driver/modules/common/common_widget/custom_raised_gradient_button.dart';
import 'package:upmart_driver/modules/common/constants/color_constants.dart';
import 'package:upmart_driver/modules/common/theme/app_themes.dart';
import '../../../../../modules/common/utils/common_utils.dart';
import '../../../../../modules/auth/auth_bloc/auth_bloc.dart';
import '../../../../../modules/auth/auth_bloc/auth_state.dart';
import '../../../../../modules/auth/enums/auth_enums.dart';
import '../../../../../modules/auth/profile/bloc/profile_bloc/profile_bloc.dart';
import '../../../../../modules/auth/profile/bloc/profile_bloc/profile_state.dart';
import '../../../../../modules/auth/profile/manager/profile_action_manager.dart';
import '../../../../../modules/auth/profile/model/common_pass_data_model.dart';
import '../../../../../modules/common/app_bloc_utilities/bloc_helpers/bloc_provider.dart';
import '../../../../../modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import '../../../../../modules/common/app_config/app_config.dart';
import '../../../../../modules/common/constants/dimens_constants.dart';
import '../../../../../localizations.dart';

class ProfileEditPhoneNumberOrEmailPage extends StatefulWidget {
  BuildContext context;

  ProfileEditPhoneNumberOrEmailPage({
    this.context,
  });

  @override
  _ProfileEditPhoneNumberOrEmailPageState createState() =>
      _ProfileEditPhoneNumberOrEmailPageState();
}

class _ProfileEditPhoneNumberOrEmailPageState
    extends State<ProfileEditPhoneNumberOrEmailPage> {
  //Declaration of County Picker Var
  Country _selectedCountry = Country.IN;

  //Declaration of scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  //Declaration of TextFormField Controllers
  final TextEditingController _phoneController = new TextEditingController();

  final TextEditingController _emailController = new TextEditingController();

  //Declaration of Bloc and state
  var _profileBloc = ProfileBloc();
  ProfileState _profileState;

  //Declaration of App bloc
  var _authBloc = AuthBloc(); // bloc
  AuthState _authState;

  BuildContext _context;

  ProfileActionManager _profileActionManager = ProfileActionManager();
  CommonPassDataModel _data;
  String userEmail = "";
  String userPhoneNumber = "";

  @override
  void initState() {
    super.initState();
    _authBloc = BlocProvider.of<AuthBloc>(widget.context);
    _data = ModalRoute.of(widget.context).settings.arguments;
  }

  @override
  void dispose() {
    super.dispose();
    _emailController.dispose();
    _phoneController.dispose();
    _profileBloc.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocEventStateBuilder<AuthState>(
      bloc: _authBloc,
      builder: (BuildContext context, AuthState authState) {
        _context = context;
        if (_authState != authState) {
          _authState = authState;
          if (_authState?.authResponseModel?.userData != null) {
            _selectedCountry = Country.findByIsoCode(
                authState?.authResponseModel?.userData?.countryIsoCode);
          }

          (_data?.tag == VerifyOtpFrom.PhoneNumber.value)
              ? _phoneController?.text = _data?.value
              : _emailController?.text = _data?.value;
        }
        return SafeArea(
          bottom: false,
          top: false,
          child: Scaffold(
            key: _scaffoldKey,
            appBar: _showAppBar(),
            body: BlocEventStateBuilder<ProfileState>(
              bloc: _profileBloc,
              builder: (BuildContext context, ProfileState profileState) {
                if (profileState != null && _profileState != profileState) {
                  _profileState = profileState;
                  _profileActionManager.actionOnEditPhoneEmailStateChange(
                      phoneController: _phoneController,
                      emailController: _emailController,
                      authState: _authState,
                      profileBloc: _profileBloc,
                      scaffoldState: _scaffoldKey?.currentState,
                      context: _context,
                      authBloc: _authBloc,
                      profileState: _profileState,
                      tagCameFrom: _data?.tag,
                      country: _selectedCountry);
                }
                return ModalProgressHUD(
                    inAsyncCall: profileState.isLoading,
                    child: SingleChildScrollView(
                      padding: EdgeInsets.all(SIZE_20),
                      child: Wrap(
                        alignment: WrapAlignment.center,
                        runSpacing: SIZE_30,
                        children: <Widget>[
                          //Title
                          Text(
                            (_data?.tag == VerifyOtpFrom.PhoneNumber.value)
                                ? AppLocalizations.of(_context)
                                    .editPhoneEmail
                                    .text
                                    .verifyNumber
                                : AppLocalizations.of(_context)
                                    .editPhoneEmail
                                    .text
                                    .verifyEmail,
                            style: AppConfig.of(_context)
                                .themeData
                                .textTheme
                                .headline1,
                          ),

                          Visibility(
                            visible:
                                (_data?.tag == VerifyOtpFrom.PhoneNumber.value),
                            child: _showCountryCodePickerAndPhoneNumber(),
                            replacement: _emailTextFormField(),
                          ),

                          //show button view
                          _showButtonView()
                        ],
                      ),
                    ));
              },
            ),
          ),
        );
      },
    );
  }

  // used to create a  phone country picker  text field form
  Widget _showCountryCodePickerAndPhoneNumber() {
    return Container(
      padding: EdgeInsets.only(left: SIZE_5, right: SIZE_5),
      margin: EdgeInsets.only(top: SIZE_15),
      decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          border: Border.all(color: COLOR_PRIMARY, width: SIZE_1),
          borderRadius: BorderRadius.circular(SIZE_30)),
      child: Row(
        children: <Widget>[
          // Padding(
          //   padding: const EdgeInsets.all(SIZE_5),
          //   child: CountryPicker(
          //     dense: false,
          //     showFlag: true,
          //     //displays flag, true by default
          //     showDialingCode: false,
          //     //displays dialing code, false by default
          //     showName: false,
          //     //displays country name, true by default
          //     showCurrency: false,
          //     //eg. 'British pound'
          //     showCurrencyISO: false,
          //     //eg. 'GBP'
          //     onChanged: (Country country) {
          //       _selectedCountry = country;
          //       _profileActionManager?.actionOnUpdateCountryCodeStateChanged(
          //           profileBloc: _profileBloc,
          //           selectedCountry: country,
          //           authResponseModel: _authState?.authResponseModel,
          //           context: _context,
          //           scaffoldState: _scaffoldKey?.currentState);
          //     },
          //     selectedCountry: _selectedCountry ?? Country.IN,
          //   ),
          // ),
          Expanded(
            child: _phoneTextFormField(),
          )
        ],
      ),
    );
  }

  //this method is used ti show send otp button
  Widget _showButtonView() {
    return Align(
      alignment: Alignment.center,
      child: Container(
        margin: EdgeInsets.only(top: SIZE_50, bottom: SIZE_20),
        height: CommonUtils.commonUtilsInstance.getPercentageSize(
            context: _context, percentage: SIZE_6, ofWidth: false),
        width: CommonUtils.commonUtilsInstance.getPercentageSize(
            context: _context, percentage: SIZE_50, ofWidth: true),
        child: RaisedGradientButton(
          radious: SIZE_30,
          gradient: LinearGradient(
            begin: Alignment.bottomCenter,
            end: Alignment.topCenter,
            colors: <Color>[COLOR_PRIMARY, COLOR_ACCENT],
          ),
          onPressed: () {
            _profileActionManager.actionOnSendOtpPressedOnEditPhoneEmailScreen(
              tagCameFrom: _data?.tag,
              authResponseModel: _authState?.authResponseModel,
              context: _context,
              countryCode: _selectedCountry?.isoCode,
              diallingCode: _selectedCountry?.dialingCode,
              scaffoldState: _scaffoldKey?.currentState,
              emailController: _emailController,
              phoneController: _phoneController,
              profileBloc: _profileBloc,
              country: _selectedCountry,
              /*  signUpDataModel: _signUpDataModel*/
            );
          },
          child: Text(
            AppLocalizations.of(_context).editPhoneEmail.button.sendOtp,
            textAlign: TextAlign.center,
            style: textStyleSize14WithWhiteColor,
          ),
        ),
      ),
    );
  }

  // used to create a  phone text field form
  Widget _phoneTextFormField() {
    return Row(
      children: <Widget>[
        // Padding(
        //   padding: const EdgeInsets.only(bottom: SIZE_2),
        //   child: Text(
        //     '(+${_selectedCountry?.dialingCode ?? ""}) ',
        //     style: AppConfig.of(_context).themeData.textTheme.headline2,
        //   ),
        // ),
        Expanded(
          child: _textFieldForm(
            keyboardType: TextInputType.phone,
            //ned to change the name in localization
            hint: AppLocalizations.of(_context).editPhoneEmail.hint.sendOtp,
            elevation: ELEVATION_0,
            isInputFormater: true,
            controller: _phoneController,
          ),
        ),
      ],
    );
  }

  // used to create a  email text field form
  Widget _emailTextFormField() {
    return Container(
      padding: EdgeInsets.only(left: SIZE_5, right: SIZE_5),
      decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          border: Border.all(color: COLOR_BORDER_GREY, width: SIZE_1),
          borderRadius: BorderRadius.circular(SIZE_10)),
      child: Row(
        children: <Widget>[
          SizedBox(
            width: SIZE_10,
          ),
          Image.asset(
            EMAIL_ICON,
            height: SIZE_20,
            width: SIZE_20,
            color: COLOR_ACCENT,
            alignment: Alignment.center,
          ),
          SizedBox(
            width: SIZE_10,
          ),
          Expanded(
            child: _textFieldForm(
                keyboardType: TextInputType.text,
                isInputFormater: false,
                hint: AppLocalizations.of(_context).editProfile.hint.enterEmail,
                controller: _emailController,
                elevation: ELEVATION_05),
          ),
        ],
      ),
    );
  }

  //method to return text form field
  Widget _textFieldForm(
      {TextEditingController controller,
      TextInputType keyboardType,
      IconData icon,
      String hint,
      bool isInputFormater,
      double elevation,
      FormFieldValidator<String> validator}) {
    return TextFormField(
      validator: validator,
      inputFormatters: (isInputFormater)
          ? <TextInputFormatter>[
              WhitelistingTextInputFormatter.digitsOnly,
              LengthLimitingTextInputFormatter(14)
            ]
          : null,
      style: AppConfig.of(_context).themeData.textTheme.headline2,
      controller: controller,
      keyboardType: keyboardType,
      onFieldSubmitted: _showEmailTick,
      onChanged: (value) {
        userEmail = value;
        userPhoneNumber = value;
        if (_data?.tag == VerifyOtpFrom.Email.value) {
          if (AuthValidator.authValidatorInstance
                  .checkValidEmail(value: value) !=
              false) {
            _showEmailTick(value);
          } else {
            _showEmailTick(value);
          }
        } else {
          if (value.length >= MaxLength.MinPhoneLength.value) {
            _showEmailTick(value);
          } else {
            _showEmailTick(value);
          }
        }
      },
      decoration: InputDecoration(
        suffixIcon: (_data?.tag == VerifyOtpFrom.Email.value)
            ? ((userEmail.isNotEmpty &&
                    AuthValidator.authValidatorInstance
                            .checkValidEmail(value: userEmail) !=
                        false)
                ? Icon(Icons.check, color: COLOR_PRIMARY, size: SIZE_20)
                : null)
            : ((userPhoneNumber != null &&
                    userPhoneNumber?.length >= MaxLength.MinPhoneLength.value)
                ? Icon(Icons.check, color: COLOR_PRIMARY, size: SIZE_20)
                : null),
        hintText: hint,
        contentPadding: EdgeInsets.only(left: SIZE_10),
        border: OutlineInputBorder(borderSide: BorderSide.none),
      )
      /* decoration: InputDecoration(
        contentPadding: EdgeInsets.all(SIZE_0),
        prefixIcon: (icon != null)
            ? Icon(icon,
            color: AppConfig
                .of(_context)
                .themeData
                .accentColor,
            size: SIZE_20)
            : null,
        hintText: hint,
        border: OutlineInputBorder(borderSide: BorderSide.none),
      )*/
      ,
    );
  }

  //this method is used to return the show app bar
  Widget _showAppBar() {
    return CommonUtils.commonUtilsInstance.getAppBar(
        context: _context,
        elevation: ELEVATION_0,
        centerTitle: true,
        defaultLeadingIcon: Icons.arrow_back_ios,
        defaultLeadingIconColor: COLOR_BLACK,
        appBarTitleStyle:
            AppConfig.of(_context).themeData.primaryTextTheme.bodyText2,
        backGroundColor: Colors.transparent);
  }

  void _showEmailTick(String email) {
    _profileActionManager?.actionOnUpdateCountryCodeStateChanged(
        profileBloc: _profileBloc,
        selectedCountry: _selectedCountry,
        authResponseModel: _authState?.authResponseModel,
        context: _context,
        scaffoldState: _scaffoldKey?.currentState);
  }
}
