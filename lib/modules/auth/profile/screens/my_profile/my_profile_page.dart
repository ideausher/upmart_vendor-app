import 'dart:io';

import 'package:flutter/material.dart';
import 'package:upmart_driver/localizations.dart';
import 'package:upmart_driver/modules/auth/auth_bloc/auth_bloc.dart';
import 'package:upmart_driver/modules/auth/auth_bloc/auth_state.dart';
import 'package:upmart_driver/modules/auth/auth_routes.dart';
import 'package:upmart_driver/modules/auth/constants/image_constant.dart';
import 'package:upmart_driver/modules/auth/manager/auth_manager.dart';
import 'package:upmart_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_provider.dart';
import 'package:upmart_driver/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:upmart_driver/modules/common/app_config/app_config.dart';
import 'package:upmart_driver/modules/common/constants/color_constants.dart';
import 'package:upmart_driver/modules/common/constants/dimens_constants.dart';
import 'package:upmart_driver/modules/common/theme/app_themes.dart';
import 'package:upmart_driver/modules/common/utils/common_utils.dart';
import 'package:upmart_driver/modules/common/utils/image_utils.dart';
import 'package:upmart_driver/modules/common/utils/navigator_utils.dart';
import 'package:upmart_driver/modules/common/common_widget/async_call_parent_widget.dart';

import 'package:upmart_driver/modules/vehicle_documents/constants/string_constant.dart';
import 'package:upmart_driver/modules/vehicle_documents/registration_routes.dart';

class MyProfilePage extends StatefulWidget {
  BuildContext context;

  MyProfilePage(this.context);

  @override
  _UserProfilePageState createState() => _UserProfilePageState();
}

class _UserProfilePageState extends State<MyProfilePage> {
  //Declaration of scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  AuthState _authState;
  AuthBloc _authBloc;
  BuildContext _context;

  @override
  void initState() {
    super.initState();
    _authBloc = BlocProvider.of<AuthBloc>(widget.context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocEventStateBuilder<AuthState>(
      bloc: _authBloc,
      builder: (BuildContext context, AuthState authState) {
        _context = context;
        if (_authState != authState) {
          _authState = authState;
        }
        return ModalProgressHUD(
            inAsyncCall: _authState?.isLoading ?? false,
            child: (_authState?.authResponseModel != null)
                ? SafeArea(
                    bottom: false,
                    top: false,
                    child: Scaffold(
                      appBar: _showAppBar(),
                      key: _scaffoldKey,
                      body: Padding(
                        padding: const EdgeInsets.only(
                            left: SIZE_25, right: SIZE_25, bottom: SIZE_10),
                        child: Wrap(
                          runSpacing: SIZE_15,
                          children: <Widget>[
                            _showUserProfileImage(),

                            SizedBox(height: SIZE_5),
                            _showPersonalInfoTitle(),
                            InkWell(
                              onTap: (){
                                NavigatorUtils.navigatorUtilsInstance.navigatorPushedName(
                                    context, RegistrationRoutes.DRIVER_DETAILS,
                                    dataToBeSend: PROFILE);
                              },
                              child: _showCommonWidget(
                                  title: AppLocalizations.of(_context)
                                      .common
                                      .text
                                      .userInfoname,
                                  value:
                                      '${_authState?.authResponseModel?.userData?.name ?? ""}'),
                            ),
                            InkWell(
                              onTap: (){

                              },
                              child: _showCommonWidget(
                                  title: AppLocalizations.of(_context)
                                      .common
                                      .text
                                      .phoneNumber,
                                  value:
                                      '(${_authState?.authResponseModel?.userData?.countryCode}) ${_authState?.authResponseModel?.userData?.phoneNumber ?? ""}'),
                            ),
                            InkWell(
                              onTap: (){

                              },
                              child: _showCommonWidget(
                                  title: AppLocalizations.of(_context)
                                      .common
                                      .text
                                      .email,
                                  value:
                                      '${_authState?.authResponseModel?.userData?.email ?? ""}'),
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                : const SizedBox());
      },
    );
  }

  //this method is used to return the show app bar
  Widget _showAppBar() {
    return CommonUtils.commonUtilsInstance.getAppBar(
        context: _context,
        elevation: ELEVATION_0,
        centerTitle: true,
        defaultLeadingIcon: Icons.arrow_back_ios,
        defaultLeadingIconColor: COLOR_BLACK,
        appBarTitleStyle:
            AppConfig.of(_context).themeData.primaryTextTheme.bodyText2,
        backGroundColor: Colors.transparent);
  }

  //this method is used to return a user  profile image widget
  Widget _showUserProfileImage() {
    return Align(
      alignment: Alignment.center,
      child: Column(
        children: <Widget>[
          Container(
            width: CommonUtils.commonUtilsInstance
                .getPercentageSize(context: _context, percentage: SIZE_20),
            height: CommonUtils.commonUtilsInstance
                .getPercentageSize(context: _context, percentage: SIZE_20),
            child: (_authState?.authResponseModel?.userData?.profilePicture !=
                        null &&
                    _authState?.authResponseModel?.userData?.profilePicture
                            ?.isNotEmpty ==
                        true)
                ? ImageUtils.imageUtilsInstance.showCacheNetworkImage(
                    context: _context,
                    showProgressBarInPlaceHolder: true,
                    placeHolderImage: DEFAULT_PROFILE_ICON,
                    // todo later change to profile icon
                    height: double.infinity,
                    width: double.infinity,
                    url: _authState
                            ?.authResponseModel?.userData?.profilePicture ??
                        "",
                  )
                : CircleAvatar(
                    backgroundColor: Colors.white,
                    radius: SIZE_30,
                    backgroundImage: AssetImage(DEFAULT_PROFILE_ICON)),
          ),
          _showUserName(),
          _showUserPhoneNumber()

        ],
      ),
    );
  }

  //this method will show the user mobile number
  Widget _showUserPhoneNumber() {
    return Text(
        ' (${_authState?.authResponseModel?.userData?.countryCode}) ${_authState?.authResponseModel?.userData?.phoneNumber ?? ""}',
        style: textStyleSize12GREY);
  }

  //this method is used to show user name
  Widget _showUserName() {
    return Text(_authState?.authResponseModel?.userData?.name ?? "",
        style: textStyleSizeBlackColor);
  }

  //this method will show the user mobile number
  Widget _showPersonalInfoTitle() {
    return Text(
      AppLocalizations.of(_context).editProfile.text.personalInfo,
      style: AppConfig.of(_context).themeData.textTheme.headline3,
    );
  }

  //this method is used to show user phone number widget
  Widget _showCommonWidget({String title, String value}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Expanded(
          flex: 35,
          child: Text(title,
              style: textStyleSize14LightBlackColor),
        ),
        Expanded(
          flex: 65,
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 80,
                child: Text(
                  value,
                  style: textStyleSize14LightGreyColor,
                  textAlign: TextAlign.end,
                ),
              ),
              Expanded(
                flex: 10,
                child: Icon(
                  Icons.arrow_forward_ios,
                  size: SIZE_16,
                  color: COLOR_LIGHT_GREY,
                ),
              )
            ],
          ),
        )
      ],
    );
  }



  //method to return spave between widgets

}
