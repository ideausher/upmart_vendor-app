import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:upmart_driver/modules/auth/api/sign_in/model/auth_response_model.dart';
import 'package:upmart_driver/modules/auth/auth_bloc/auth_bloc.dart';
import 'package:upmart_driver/modules/auth/auth_bloc/auth_state.dart';
import 'package:upmart_driver/modules/auth/auth_routes.dart';
import 'package:upmart_driver/modules/auth/constants/image_constant.dart';
import 'package:upmart_driver/modules/auth/manager/auth_manager.dart';
import 'package:upmart_driver/modules/auth/profile/enums/setting_enums.dart';
import 'package:upmart_driver/modules/auth/profile/manager/profile_action_manager.dart';
import 'package:upmart_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_provider.dart';
import 'package:upmart_driver/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:upmart_driver/modules/common/app_config/app_config.dart';
import 'package:upmart_driver/modules/common/constants/color_constants.dart';
import 'package:upmart_driver/modules/common/constants/constants.dart';
import 'package:upmart_driver/modules/common/constants/dimens_constants.dart';
import 'package:upmart_driver/modules/common/theme/app_themes.dart';
import 'package:upmart_driver/modules/common/utils/common_utils.dart';
import 'package:upmart_driver/modules/common/utils/image_utils.dart';
import 'package:upmart_driver/modules/common/utils/navigator_utils.dart';
import 'package:upmart_driver/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:upmart_driver/modules/notification/widget/notification_unread_count_widget.dart';
import '../../../../localizations.dart';

class ProfilePage extends StatefulWidget {
  BuildContext context;
  AuthResponseModel authResponseModel;
  Function backCallBack;

  ProfilePage(this.context, {this.authResponseModel, this.backCallBack});

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  //Declaration of scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  AuthState _authState;
  AuthBloc _authBloc;
  AuthManager _authManager;

  //Managers
  ProfileActionManager _profileActionManager = new ProfileActionManager();

  //Build Context
  BuildContext _context;

  @override
  void initState() {
    _authBloc = BlocProvider.of<AuthBloc>(widget.context);
    _authManager = AuthManager();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    return SafeArea(
      bottom: true,
      top: true,
      child: Scaffold(
        key: _scaffoldKey,
        appBar: _showAppBar(),
        body: BlocEventStateBuilder<AuthState>(
          bloc: _authBloc,
          builder: (BuildContext context, AuthState authState) {
            _context = context;
            if (_authState != authState) {
              _authState = authState;

              _authManager.actionOnStateUpdate(
                  context: _context,
                  authBloc: _authBloc,
                  authState: _authState,
                  scaffoldState: _scaffoldKey.currentState);
            }
            return ModalProgressHUD(
                inAsyncCall: _authState?.isLoading ?? false,
                child: (_authState?.authResponseModel != null)
                    ? Column(
                        children: <Widget>[
                          _showDivider(),
                          _showUserProfileImage(),
                          _showUserName(),
                          _showUserPhoneNumber(),
                          _showEditProfileButton(),
                          SizedBox(
                            height: CommonUtils.commonUtilsInstance
                                .getPercentageSize(
                                    context: _context,
                                    percentage: SIZE_2,
                                    ofWidth: false),
                          ),
                          Expanded(
                            child: _showListView(),
                          )
                        ],
                      )
                    : const SizedBox());
          },
        ),
      ),
    );
  }

  //this method is used to return the show app bar
  Widget _showAppBar() {
    return CommonUtils.commonUtilsInstance.getAppBar(
        context: _context,
        elevation: ELEVATION_0,
        defaultLeadIconPressed: () {
          NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
        },
        popScreenOnTapOfLeadingIcon: false,
        centerTitle: true,
        appBarTitle: AppLocalizations.of(context).profile.text.profile,
        defaultLeadingIconColor: Colors.black,
        actionWidgets: [
          Padding(
            padding: const EdgeInsets.only(right: SIZE_16, top: SIZE_16),
            child: NotificationReadCountWidget(
              context: context,
            ),
          )
        ],
        appBarTitleStyle:
            AppConfig.of(_context).themeData.primaryTextTheme.headline3,
        backGroundColor: Colors.transparent);
  }

  //this method is used to show listview
  Widget _showListView() {
    return ListView.builder(
        itemCount: TOTAL_MENU_COUNT,
        itemBuilder: (_context, index) {
          return Padding(
              padding: const EdgeInsets.only(
                  left: SIZE_14, right: SIZE_30, top: SIZE_6, bottom: SIZE_6),
              child: InkWell(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onTap: () {
                  _profileActionManager?.actionOnProfileItemClick(
                      index: index,
                      scaffoldState: _scaffoldKey.currentState,
                      context: _context,
                      authState: _authState,
                      authBloc: _authBloc);
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Image.asset(
                      _getIcons(index),
                      height: CommonUtils.commonUtilsInstance.getPercentageSize(
                          context: _context,
                          percentage: SIZE_6,
                          ofWidth: false),
                      width: CommonUtils.commonUtilsInstance.getPercentageSize(
                          context: _context, percentage: SIZE_6, ofWidth: true),
                    ),
                    SizedBox(
                      width: SIZE_10,
                    ),
                    Expanded(
                      child: ((index == TOTAL_MENU_COUNT - 1))
                          ? Text(
                              _getTitle(index),
                              style: textStyleSize14BlackColor,
                            )
                          : Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  _getTitle(index),
                                  style: textStyleSize14BlackColor,
                                ),
                                Text(
                                  _getSubTitle(index),
                                  style: textStyleSize12GREY,
                                ),
                              ],
                            ),
                    ),
                    (index == TOTAL_MENU_COUNT - 1)
                        ? const SizedBox()
                        : Image.asset(
                            ARROW_RIGHT_ICON,
                            color: COLOR_ACCENT,
                            height: SIZE_12,
                            width: SIZE_12,
                          )
                  ],
                ),
              ));
        });
  }

  //this method is used to return icons of setting list
  String _getIcons(int index) {
    if (index == ProfileItemEnum.MyEarnings.value) {
      return MY_EARNING_ICON;
    } else if (index == ProfileItemEnum.HelpAndSupport.value) {
      return HELP_AND_SUPPORT_ICON;
    } else if (index == ProfileItemEnum.MyDocuments.value) {
      return MY_DOCUMENT_ICON;
    } else if (index == ProfileItemEnum.TermsAndPolicy.value) {
      return TERMS_AND_POLICY_ICON;
    } else if (index == ProfileItemEnum.LogOut.value) {
      return LOGOUT_ICON;
    }
  }

  //this method is used to return title
  String _getTitle(int index) {
    if (index == ProfileItemEnum.MyEarnings.value) {
      return AppLocalizations.of(_context).profile.text.myEarning;
    } else if (index == ProfileItemEnum.HelpAndSupport.value) {
      return AppLocalizations.of(_context).profile.text.helpAndSupport;
    } else if (index == ProfileItemEnum.MyDocuments.value) {
      return AppLocalizations.of(_context).profile.text.myDocuments;
      ;
    } else if (index == ProfileItemEnum.TermsAndPolicy.value) {
      return AppLocalizations.of(_context).profile.text.termsAndPolicies;
    } else if (index == ProfileItemEnum.LogOut.value) {
      return AppLocalizations.of(_context).profile.text.logout;
    }
  }

  //this method is used to return title
  String _getSubTitle(int index) {
    if (index == ProfileItemEnum.MyEarnings.value) {
      return AppLocalizations.of(_context).profile.text.subtitleEarning;
    } else if (index == ProfileItemEnum.HelpAndSupport.value) {
      return AppLocalizations.of(_context).profile.text.subtitleHelpAndSupport;
    } else if (index == ProfileItemEnum.MyDocuments.value) {
      return AppLocalizations.of(_context).profile.text.myDocuments;
    } else if (index == ProfileItemEnum.TermsAndPolicy.value) {
      return AppLocalizations.of(_context).profile.text.termsAndPolicies;
    } else if (index == ProfileItemEnum.LogOut.value) {
      return "";
    }
  }

  //this method is used to return a user  profile image widget
  Widget _showUserProfileImage() {
    return Container(
      margin: EdgeInsets.only(top: SIZE_10),
      height: SIZE_80,
      width: SIZE_80,
      child: (_authState?.authResponseModel?.userData?.profilePicture != null &&
              _authState?.authResponseModel?.userData?.profilePicture
                      ?.isNotEmpty ==
                  true)
          ? ImageUtils.imageUtilsInstance.showCacheNetworkImage(
              context: _context,
              fit: BoxFit.contain,
              showProgressBarInPlaceHolder: true,
              placeHolderImage: DEFAULT_PROFILE_ICON,
              url:
                  _authState?.authResponseModel?.userData?.profilePicture ?? "",
            )
          : CircleAvatar(
              backgroundColor: Colors.white,
              radius: SIZE_30,
              backgroundImage: AssetImage(DEFAULT_PROFILE_ICON)),
    );
  }

  //this method is used to show user name
  Widget _showUserName() {
    return InkWell(
      onTap: () {
        NavigatorUtils.navigatorUtilsInstance
            .navigatorPushedName(_context, AuthRoutes.EDIT_PROFILE_SCREEN_ROOT);
      },
      child: Text(_authState?.authResponseModel?.userData?.name ?? "",
          style: AppConfig.of(_context).themeData.primaryTextTheme.headline5),
    );
  }

  //this method will show the user mobile number
  Widget _showUserPhoneNumber() {
    return Text(
        ' +(${_authState?.authResponseModel?.userData?.countryCode.replaceAll("+", "")}) ${_authState?.authResponseModel?.userData?.phoneNumber ?? ""}',
        style: textStyleSize12GREY);
  }

  //method to show horizontal divider
  Widget _showDivider() {
    return Divider(
      color: COLOR_GREY,
      thickness: SIZE_1,
    );
  }

  //method used to show edit profile button
  Widget _showEditProfileButton() {
    return InkWell(
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      onTap: () {
        NavigatorUtils.navigatorUtilsInstance
            .navigatorPushedName(_context, AuthRoutes.EDIT_PROFILE_SCREEN_ROOT);
      },
      child: Container(
        width: CommonUtils.commonUtilsInstance.getPercentageSize(
            context: _context, percentage: SIZE_20, ofWidth: false),
        margin: EdgeInsets.only(top: SIZE_12),
        padding: EdgeInsets.only(
            left: SIZE_15, right: SIZE_15, top: SIZE_6, bottom: SIZE_6),
        decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            border: Border.all(color: COLOR_PRIMARY, width: SIZE_1_5),
            borderRadius: BorderRadius.circular(SIZE_30)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(
              EDIT_ICON,
              height: SIZE_16,
              width: SIZE_14,
              color: COLOR_PRIMARY,
            ),
            SizedBox(
              width: SIZE_5,
            ),
            Text(
              AppLocalizations.of(context).profile.text.editProfile,
              style:
                  AppConfig.of(_context).themeData.primaryTextTheme.headline2,
            ),
          ],
        ),
      ),
    );
  }
}
