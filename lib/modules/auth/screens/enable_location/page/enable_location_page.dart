import 'package:flutter/material.dart';
import 'package:upmart_driver/localizations.dart';
import 'package:upmart_driver/modules/auth/constants/image_constant.dart';
import 'package:upmart_driver/modules/auth/manager/enable_location_manager.dart';
import 'package:upmart_driver/modules/auth/screens/enable_location/location_bloc/enable_location_bloc.dart';
import 'package:upmart_driver/modules/auth/screens/enable_location/location_bloc/enable_location_state.dart';
import 'package:upmart_driver/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:upmart_driver/modules/common/app_config/app_config.dart';
import 'package:upmart_driver/modules/common/common_widget/custom_raised_gradient_button.dart';
import 'package:upmart_driver/modules/common/constants/color_constants.dart';
import 'package:upmart_driver/modules/common/constants/dimens_constants.dart';
import 'package:upmart_driver/modules/common/theme/app_themes.dart';
import 'package:upmart_driver/modules/common/utils/common_utils.dart';
import 'package:upmart_driver/modules/common/common_widget/async_call_parent_widget.dart';
import '../../../../../routes.dart';

class EnableLocationPage extends StatefulWidget {
  BuildContext context;

  EnableLocationPage(this.context);

  @override
  _EnableLocationPageState createState() => _EnableLocationPageState();
}

class _EnableLocationPageState extends State<EnableLocationPage> {
  BuildContext _context;

  //bloc variables
  EnableCurrentLocationBloc _enableLocationBloc;
  EnableCurrentLocationState _enableLocationState;

  //class variables
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  //managers
  EnableLocationManager _enableLocationManager;

  @override
  void initState() {
    super.initState();
    _enableLocationBloc = EnableCurrentLocationBloc();
    _enableLocationManager = EnableLocationManager(
        context: widget?.context,
        enableCurrentLocationBloc: _enableLocationBloc,
        scaffoldState: _scaffoldKey?.currentState);
  }

  @override
  void dispose() {
    super.dispose();
    _enableLocationBloc?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    return BlocEventStateBuilder<EnableCurrentLocationState>(
      bloc: _enableLocationBloc,
      builder: (BuildContext context,
          EnableCurrentLocationState enbaleLocationState) {
        _context = context;
        _enableLocationManager.context = context;
        if (enbaleLocationState != null &&
            _enableLocationState != enbaleLocationState) {
          _enableLocationState = enbaleLocationState;
          _enableLocationManager?.actionOnInitializationScreenStateChange(
              enableCurrentLocationState: _enableLocationState,
              scaffoldState: _scaffoldKey.currentState);
        }
        return ModalProgressHUD(
            inAsyncCall: enbaleLocationState?.isLoading ?? false,
            child: SafeArea(
              bottom: false,
              top: false,
              child: Scaffold(
                key: _scaffoldKey,
                body: Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    Image.asset(
                      ENABLE_LOCATION_LOGO,
                      height: CommonUtils.commonUtilsInstance.getPercentageSize(
                          context: context,
                          percentage: SIZE_28,
                          ofWidth: false),
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          height: CommonUtils.commonUtilsInstance.getPercentageSize(
                              context: _context, ofWidth: false, percentage: SIZE_15),
                        ),
                        _showLocationWidget(),
                        _showAllowButton()
                      ],
                    )
                  ],
                ),
              ),
            ));
      },
    );
  }

  //method to show allow button
  Widget _showAllowButton() {
    return Container(
      height: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _context, percentage: SIZE_6, ofWidth: false),
      width: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _context, percentage: SIZE_70, ofWidth: true),
      child: RaisedGradientButton(
        radious: SIZE_30,
        gradient: LinearGradient(
          begin: Alignment.bottomCenter,
          end: Alignment.topCenter,
          colors: <Color>[COLOR_PRIMARY, COLOR_ACCENT],
        ),
        onPressed: () {
          _enableUserCurrentLocation();
        },
        child: Text(
          "Allow",
          textAlign: TextAlign.center,
          style: textStyleSize14WithWhiteColor,
        ),
      ),
    );
  }

  //method to show enable location title text
  Widget _showEnableLocationText() {
    return Text(AppLocalizations.of(_context).enableLocation.text.locationTitle,
        style: AppConfig.of(_context).themeData.textTheme.headline6);
  }

  //method to show subtitle text of enable location text
  Widget _showSubTitleText() {
    return Text(
      AppLocalizations.of(_context).enableLocation.text.giveAccess,
      style: AppConfig.of(_context).themeData.textTheme.bodyText1,
    );
  }

  //method to return enable location text widget
  Widget _showLocationWidget() {
    return Container(
      width: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _context, percentage: SIZE_100, ofWidth: true),
      height: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _context, percentage: SIZE_40, ofWidth: false),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          _showEnableLocationText(),
          _showSubTitleText(),
        ],
      ),
    );
  }

  //method to show user current location
  void _enableUserCurrentLocation() {
    _enableLocationManager?.callEnableLocationEvent();
  }
}
