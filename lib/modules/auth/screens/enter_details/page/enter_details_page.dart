import 'package:flutter/material.dart';
import 'package:flutter_country_picker/country.dart';
import 'package:flutter_country_picker/flutter_country_picker.dart';
import 'package:upmart_driver/modules/auth/constants/image_constant.dart';
import 'package:upmart_driver/modules/common/common_widget/custom_raised_gradient_button.dart';
import 'package:upmart_driver/modules/common/constants/color_constants.dart';
import 'package:upmart_driver/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:upmart_driver/modules/common/model/update_ui_data_model.dart';
import 'package:upmart_driver/modules/common/theme/app_themes.dart';
import '../../../../../localizations.dart';
import '../../../../../modules/common/utils/common_utils.dart';
import '../../../../../modules/auth/enums/auth_enums.dart';
import '../../../../../modules/auth/manager/auth_manager.dart';
import '../../../../auth/auth_bloc/auth_bloc.dart';
import '../../../../auth/auth_bloc/auth_state.dart';
import '../../../../common/app_bloc_utilities/bloc_helpers/bloc_provider.dart';
import '../../../../common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import '../../../../common/app_config/app_config.dart';
import '../../../../common/constants/dimens_constants.dart';

class EnterDetailsPage extends StatefulWidget {
  BuildContext _context;

  EnterDetailsPage(this._context);

  @override
  _EnterDetailsPageState createState() => _EnterDetailsPageState();
}

class _EnterDetailsPageState extends State<EnterDetailsPage> {
  //Declaration of auth bloc
  var _authBloc;
  AuthState _authState;
  BuildContext _context;

  //Declaration of scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  // Declaration of edit text controller
  final TextEditingController _phoneNumberController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();

  //Class variables
  AuthManager _authManager = AuthManager();
  Country _selectedCountry = Country.IN;
  UpdateUiDataModel updateUiDataModel;
  String userName = "";

  //Dropdown item
  List<int> _dropdownValues = [
    UserGender.Mrs.value,
    UserGender.Mr.value,
    UserGender.Ms.value
  ];

  @override
  Future<void> initState() {
    super.initState();
    updateUiDataModel = UpdateUiDataModel();
    _authBloc = BlocProvider.of<AuthBloc>(widget._context);
  }

  @override
  void dispose() {
    super.dispose();
    _phoneNumberController.dispose();
    _nameController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _context = context;

    return WillPopScope(
      onWillPop: () {
        return _authManager?.signOutDialog(
            context: _context,
            authState: _authState,
            scaffoldState: _scaffoldKey.currentState,
            authBloc: _authBloc);
      },
      child: SafeArea(
        bottom: false,
        top: false,
        child: Scaffold(
          key: _scaffoldKey,
          body: BlocEventStateBuilder<AuthState>(
            bloc: _authBloc,
            builder: (BuildContext context, AuthState authState) {
              _context = context;
              if (authState != null && _authState != authState) {
                _authState = authState;
                if (authState?.authResponseModel?.userData != null) {
                  _selectedCountry = Country.findByIsoCode(authState
                          ?.authResponseModel?.userData?.countryIsoCode ??
                      authState?.updateUiDataModel?.selectedCountry?.isoCode);
                }

                _authManager.actionOnEnterDetailsStateChanged(
                    context: context,
                    authBloc: _authBloc,
                    authState: authState,
                    phoneController: _phoneNumberController,
                    updateUiDataModel: authState?.updateUiDataModel,
                    scaffoldState: _scaffoldKey?.currentState,
                    selectedCountry: _selectedCountry);
              }
              return ModalProgressHUD(
                inAsyncCall: authState?.isLoading ?? false,
                child: Stack(
                  children: [
                    Container(
                      decoration: new BoxDecoration(
                        image: DecorationImage(
                          image: new AssetImage(AUTH_BACK),
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    SingleChildScrollView(
                      padding: EdgeInsets.all(SIZE_25),
                      child: Wrap(
                        alignment: WrapAlignment.center,
                        children: <Widget>[
                          _showLoginLogo(),
                          // _showLoginTitle(),
                          // _showLoginSubTitle(),

                          _getCardView(),
                        ],
                      ),
                    ),
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  // used to create a top text view
  Widget _showLoginLogo() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: SIZE_20),
          child: Container(
              margin: EdgeInsets.only(top: SIZE_20),
              width: MediaQuery.of(_context).size.width,
              height: CommonUtils.commonUtilsInstance.getPercentageSize(
                  context: _context, ofWidth: false, percentage: SIZE_30),
              child: Image.asset(
                SIGN_UP_LOGO,
              )),
        ),
        SizedBox(
          height: CommonUtils.commonUtilsInstance.getPercentageSize(
              context: _context, ofWidth: false, percentage: SIZE_3),
        )
      ],
    );
  }

  // used to create a  phone text field form
  Widget _phoneTextFormField() {
    return Row(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(bottom: SIZE_2),
          child: Row(
            children: <Widget>[
              Text(
                '(${_authState?.authResponseModel?.userData?.countryCode ?? ""}) ',
                style: AppConfig.of(_context).themeData.textTheme.headline2,
              ),
            ],
          ),
        ),
        Expanded(
          child: _textFieldForm(
            keyboardType: TextInputType.phone,
            elevation: ELEVATION_0,
            enabled: false,
            showTickIcon: true,
            controller: _phoneNumberController,
          ),
        ),
      ],
    );
  }

  // used to create a  phone country picker  text field form
  Widget _showCountryCodePickerAndPhoneNumber() {
    return Visibility(
      visible: false,
      child: Container(
        margin: EdgeInsets.only(top: SIZE_12),
        padding: EdgeInsets.only(right: SIZE_5, left: SIZE_10),
        decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            border: Border.all(color: COLOR_BORDER_GREY, width: SIZE_1),
            borderRadius: BorderRadius.circular(SIZE_10)),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 22,
              child: CountryPicker(
                  dense: false,
                  showFlag: true,
                  // isShowDialog: false,
                  //displays flag, true by default
                  showDialingCode: false,
                  //displays dialing code, false by default
                  showName: false,
                  //displays country name, true by default
                  showCurrency: false,
                  //eg. 'British pound'
                  showCurrencyISO: false,
                  //eg. 'GBP'
                  onChanged: null,
                  selectedCountry: _selectedCountry ?? Country?.IN),
            ),
            Expanded(
              flex: 5,
              child: Container(
                margin: EdgeInsets.only(right: SIZE_5),
                child: Image.asset(DIVIDER_ICON),
                height: CommonUtils.commonUtilsInstance.getPercentageSize(
                    context: _context, ofWidth: false, percentage: SIZE_5),
              ),
            ),
            Expanded(
              flex: 77,
              child: _phoneTextFormField(),
            )
          ],
        ),
      ),
    );
  }

  //method to show continue button
  Widget _continueButton() {
    return Container(
      margin: EdgeInsets.only(top: SIZE_20),
      height: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _context, percentage: SIZE_6, ofWidth: false),
      width: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _context, percentage: SIZE_80, ofWidth: true),
      child: RaisedGradientButton(
        radious: SIZE_30,
        gradient: LinearGradient(
          begin: Alignment.bottomCenter,
          end: Alignment.topCenter,
          colors: <Color>[COLOR_PRIMARY, COLOR_ACCENT],
        ),
        onPressed: () {
          _validateInput();
        },
        child: Text(
          "Continue",
          textAlign: TextAlign.center,
          style: textStyleSize14WithWhiteColor,
        ),
      ),
    );
  }

  //method to return textform field
  Widget _textFieldForm(
      {TextEditingController controller,
      TextInputType keyboardType,
      IconData icon,
      String hint,
      double elevation,
      bool enabled,
      bool showTickIcon = false,
      FormFieldValidator<String> validator}) {
    return Material(
      color: Colors.white,
      borderRadius: BorderRadius.circular(SIZE_20),
      elevation: elevation,
      child: TextFormField(
        validator: validator,
        onFieldSubmitted: _updateUserName,
        textCapitalization: TextCapitalization.words,
        onChanged: (name) {
          userName = name;
          if (name.length >= 3) {
            _updateUserName(name);
          } else {
            _updateUserName(name);
          }
        },
        style: AppConfig.of(_context).themeData.textTheme.headline2,
        controller: controller,
        enabled: enabled ?? true,
        keyboardType: keyboardType,
        decoration: InputDecoration(
          prefixIcon: Image.asset(USER_ICON,scale: SIZE_3,),
          suffixIcon: (userName.isNotEmpty && userName?.length >= 3)
              ? Icon(Icons.check, color: COLOR_PRIMARY, size: SIZE_20)
              : null,
          hintText: hint,
          contentPadding: EdgeInsets.all(SIZE_0),
          border: OutlineInputBorder(borderSide: BorderSide.none),
        ),
        /*decoration: InputDecoration(
          contentPadding: EdgeInsets.all(SIZE_0),
          suffixIcon: (showTickIcon)
              ? (_authState?.authResponseModel?.userData?.phoneNumber
              ?.isNotEmpty ==
              true)
              ? Icon(Icons.check, color: COLOR_PRIMARY, size: SIZE_20)
              : null
              : null,
          hintText: hint,
          border: OutlineInputBorder(borderSide: BorderSide.none),
        )*/
      ),
    );
  }

  //method to check validation of phone field
  void _validateInput() {
    _authManager.enterDetailsUpdate(
      scaffoldState: _scaffoldKey?.currentState,
      context: _context,
      authBloc: _authBloc,
      updateUiDataModel: _authState?.updateUiDataModel,
      authResponseModel: _authState?.authResponseModel,
      name: _nameController?.text,
    );
  }

  //method to show login title
  Widget _showLoginTitle() {
    return Padding(
      padding: const EdgeInsets.only(top: SIZE_24),
      child: Text(
        AppLocalizations.of(_context).enterDetails.text.enterDetails,
        textAlign: TextAlign.center,
        style: AppConfig.of(_context).themeData.textTheme.headline1,
      ),
    );
  }

  //method to show login screen subtitle
  Widget _showLoginSubTitle() {
    return Text(
      AppLocalizations.of(_context)
          .enterDetails
          .text
          .enterDetailToProceedFurther,
      textAlign: TextAlign.center,
      style: AppConfig.of(_context).themeData.textTheme.headline2,
    );
  }

  //method to return text for filed of name
  Widget _showNameTextField() {
    return Container(
      margin: EdgeInsets.only(top: SIZE_16),
      padding: EdgeInsets.only(left: SIZE_5, right: SIZE_5),
      decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          border: Border.all(color: COLOR_PRIMARY, width: SIZE_1),
          borderRadius: BorderRadius.circular(SIZE_30)),
      child:
          _nameTextFormField() /*Row(
        children: <Widget>[
          Expanded(
            flex: 22,
            child: DropdownButton<int>(
              value: getTitleValue(
                  _authState?.authResponseModel?.userData?.title) ??
                  UserGender?.Mrs?.value, */ /* _authState?.updateUiDataModel?.dropDownValue ??
                  UserGender.Mrs.value*/ /*
              icon: Icon(
                Icons.arrow_drop_down,
                size: SIZE_22,
                color: Color(0xFF303030),
              ),
              iconSize: SIZE_24,
              elevation: 16,
              underline: Container(
                height: SIZE_2,
                color: Colors.transparent,
              ),
              style: AppConfig
                  .of(_context)
                  .themeData
                  .textTheme
                  .headline2,
              onChanged: (int newValue) {
                // _authState?.updateUiDataModel?.dropDownValue = newValue;
                _authState?.authResponseModel?.userData?.title =
                    _showGenderValue(newValue);
                _authManager?.updateUI(
                    context: _context,
                    authBloc: _authBloc,
                    authState: _authState,
                    updateUiDataModel: _authState?.updateUiDataModel);
              },
              items: _dropdownValues.map<DropdownMenuItem<int>>((int value) {
                return DropdownMenuItem<int>(
                  value: value,
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: SIZE_3),
                    child: Text(
                      _showGenderValue(value),
                      textAlign: TextAlign.center,
                    ),
                  ),
                );
              }).toList(),
            ),
          ),
          Expanded(
            flex: 5,
            child: Container(
              margin: EdgeInsets.only(right: SIZE_5),
              child: Image.asset(DIVIDER_ICON),
              height: CommonUtils.commonUtilsInstance.getPercentageSize(
                  context: _context, ofWidth: false, percentage: SIZE_5),
            ),
          ),
          Expanded(
            flex: 77,
            child: _nameTextFormField(),
          )
        ],
      )*/
      ,
    );
  }

  //enter name field
  Widget _nameTextFormField() {
    return _textFieldForm(
      keyboardType: TextInputType.text,
      hint: "Username",
      elevation: ELEVATION_0,
      icon: null,
      controller: _nameController,
    );
  }

  //this method will give the space between the widgets
  Widget _showSpace() {
    return SizedBox(
      height: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _context, ofWidth: false, percentage: SIZE_24),
    );
  }

  // getTitleValue(String title) {
  //   switch (title) {
  //     case "Ms.":
  //       return 2;
  //       break;
  //     case "Mrs.":
  //       return 1;
  //       break;
  //     case "Mr.":
  //       return 0;
  //       break;
  //   }
  // }

  void _updateUserName(String userName) {
    _authManager?.updateUI(
        context: _context,
        authBloc: _authBloc,
        authState: _authState,
        updateUiDataModel: _authState?.updateUiDataModel);
  }

  //Get card view
  _getCardView() {
    return Container(
      height: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _context, ofWidth: false, percentage: SIZE_53),
      padding: EdgeInsets.all(SIZE_20),
      decoration: new BoxDecoration(
        borderRadius: new BorderRadius.all(Radius.circular(SIZE_30)),
        shape: BoxShape.rectangle,
        color: Colors.white,
        boxShadow: [BoxShadow(color: Colors.white70, blurRadius: 6)],
      ),
      child: Wrap(
        runSpacing: SIZE_10,
        alignment: WrapAlignment.center,
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Sign In / Sign Up",
              style:
                  AppConfig.of(_context).themeData.primaryTextTheme.subtitle2,
            ),
          ),
          _showNameTextField(),
          _showCountryCodePickerAndPhoneNumber(),
          _continueButton(),
        ],
      ),
    );
  }
}
