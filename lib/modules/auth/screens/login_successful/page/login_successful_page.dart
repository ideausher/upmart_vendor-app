import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:upmart_driver/modules/common/theme/app_themes.dart';
import 'package:upmart_driver/modules/vehicle_documents/constants/string_constant.dart';
import 'package:upmart_driver/modules/vehicle_documents/registration_routes.dart';
import 'package:upmart_driver/modules/auth/auth_routes.dart';
import 'package:upmart_driver/modules/auth/constants/image_constant.dart';
import 'package:upmart_driver/modules/auth/enums/auth_enums.dart';
import 'package:upmart_driver/modules/common/app_config/app_config.dart';
import 'package:upmart_driver/modules/common/constants/dimens_constants.dart';
import 'package:upmart_driver/modules/common/enum/enums.dart';
import 'package:upmart_driver/modules/common/utils/common_utils.dart';
import 'package:upmart_driver/modules/common/utils/navigator_utils.dart';
import 'package:upmart_driver/modules/common/utils/shared_prefs_utils.dart';
import 'package:upmart_driver/modules/current_location_updator/manager/current_location_manager.dart';
import 'package:upmart_driver/modules/common/model/user_current_location_model.dart';

import '../../../../../localizations.dart';

class LogInSuccessFullScreen extends StatefulWidget {
  BuildContext _context;

  LogInSuccessFullScreen(this._context);

  @override
  _LogInSuccessFullScreenState createState() => _LogInSuccessFullScreenState();
}

class _LogInSuccessFullScreenState extends State<LogInSuccessFullScreen> {
  bool _serviceEnabled;
  Position _myLocation;
  CurrentLocation _currentLocation;
  BuildContext context;

  //class variables
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    //here starting the timer of 2 sec after this navigate to next screen
    Future.delayed(Duration(seconds: 2), () async {
      _checkLocationPermissionStatus();
    });
    context = widget._context;
  }

  @override
  Widget build(BuildContext context) {
    context = context;
    return SafeArea(
      bottom: false,
      top: false,
      child: Scaffold(
        backgroundColor: Colors.white,
        key: _scaffoldKey,
        body: Stack(
          children: [
            Container(
              decoration: new BoxDecoration(
                image: DecorationImage(
                  image: new AssetImage(AUTH_BACK),
                  fit: BoxFit.fill,
                ),
              ),
            ),
            SingleChildScrollView(
              padding: EdgeInsets.all(SIZE_25),
              child: Wrap(
                alignment: WrapAlignment.center,
                children: <Widget>[
                  _showLoginLogo(),
                  _getCardView(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  //this method will check the location permission status if enables navigate to home else navigate to enable location screen
  void _checkLocationPermissionStatus() async {
    _serviceEnabled = await CurrentLocationManger.locationMangerInstance
        .serviceEnabledOrNot();
    if (!_serviceEnabled) {
      NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
          widget?._context, AuthRoutes.ENABLE_LOCATION_ROOT);
    } else {
      //if location permission is allowed only then get user location else show grant location meassage
      LocationPermission permissionStatus = await CurrentLocationManger
          .locationMangerInstance
          .permissionEnabled();

      if (permissionStatus == LocationPermission.always ||
          permissionStatus == LocationPermission.whileInUse) {
        _getLocationAndFindAddress();
      }
      //if user denies the permission navigate to location screen
      else {
        NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
            widget?._context, AuthRoutes.ENABLE_LOCATION_ROOT);
      }
    }
  }

  //method to get location and find address
  void _getLocationAndFindAddress() async {
    _myLocation = await CurrentLocationManger.locationMangerInstance
        .getCurrentLocationData();
    _currentLocation = await CurrentLocationManger?.locationMangerInstance
        ?.getAddressUsingLocation(widget?._context, _myLocation);
    print('current location is ${_currentLocation?.currentAddress}');
    await SharedPrefUtils.sharedPrefUtilsInstance
        .saveLocationObject(_currentLocation, PrefsEnum.UserLocationData.value);
    NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
        context, RegistrationRoutes.COMPLETE_PROFILE,
        dataToBeSend: HOME);
    /*  NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(
        widget?._context, AuthRoutes.HOME_SCREEN, */ /*dataToBeSend: _currentLocation*/ /*);*/
  }

  // used to create a top text view
  Widget _showLoginLogo() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: SIZE_20),
          child: Container(
              margin: EdgeInsets.only(top: SIZE_20),
              width: MediaQuery.of(context).size.width,
              height: CommonUtils.commonUtilsInstance.getPercentageSize(
                  context: context, ofWidth: false, percentage: SIZE_30),
              child: Image.asset(
                SIGN_UP_LOGO,
              )),
        ),
        SizedBox(
          height: CommonUtils.commonUtilsInstance.getPercentageSize(
              context: context, ofWidth: false, percentage: SIZE_3),
        )
      ],
    );
  }

  //Get card view
  _getCardView() {
    return Container(
      height: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: context, ofWidth: false, percentage: SIZE_53),
      padding: EdgeInsets.all(SIZE_20),
      decoration: new BoxDecoration(
        borderRadius: new BorderRadius.all(Radius.circular(SIZE_30)),
        shape: BoxShape.rectangle,
        color: Colors.white,
        boxShadow: [BoxShadow(color: Colors.white70, blurRadius: 6)],
      ),
      child: Column(
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Sign In / Sign Up",
              style: AppConfig.of(context).themeData.primaryTextTheme.subtitle2,
            ),
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(LOGIN_SUCCESSFUL_LOGO,
                    width: CommonUtils.commonUtilsInstance.getPercentageSize(
                        context: context, percentage: SIZE_20, ofWidth: true)),
                Text(
                  "Successful",
                  style: textStyleSize24PrimaryColor,
                )
              ],
            ),
          ),
          SizedBox(
            height: CommonUtils.commonUtilsInstance.getPercentageSize(
                context: context, ofWidth: false, percentage: SIZE_3),
          )
        ],
      ),
    );
  }
}
