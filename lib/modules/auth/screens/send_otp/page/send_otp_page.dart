import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_country_picker/flutter_country_picker.dart';
import 'package:upmart_driver/modules/auth/constants/image_constant.dart';
import 'package:upmart_driver/modules/auth/enums/auth_enums.dart';
import 'package:upmart_driver/modules/common/common_widget/custom_raised_gradient_button.dart';
import 'package:upmart_driver/modules/common/constants/color_constants.dart';
import 'package:upmart_driver/modules/common/theme/app_themes.dart';
import 'package:upmart_driver/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:upmart_driver/modules/common/model/update_ui_data_model.dart';
import 'package:upmart_driver/modules/common/utils/launcher_utils.dart';
import '../../../../../localizations.dart';
import '../../../../../modules/common/utils/common_utils.dart';
import '../../../../../modules/auth/manager/auth_manager.dart';
import '../../../../common/app_bloc_utilities/bloc_helpers/bloc_provider.dart';
import '../../../../common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import '../../../../common/app_config/app_config.dart';
import '../../../../common/constants/dimens_constants.dart';
import '../../../auth_bloc/auth_bloc.dart';
import '../../../auth_bloc/auth_state.dart';

class SendOtpPage extends StatefulWidget {
  BuildContext context;

  SendOtpPage(this.context);

  @override
  _SendOtpPageState createState() => _SendOtpPageState();
}

class _SendOtpPageState extends State<SendOtpPage> {
  //Declaration of auth bloc
  var _authBloc;
  AuthState _authState;
  BuildContext _context;

  //Declaration of scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  // Declaration text editing controller
  final TextEditingController _phoneController = TextEditingController();

  //Declaration of manager
  AuthManager _authManager = AuthManager();

  //Declaration of UI updation model
  UpdateUiDataModel _updateUiDataModel;

  @override
  void initState() {
    super.initState();
    _updateUiDataModel = new UpdateUiDataModel();
    _authBloc = BlocProvider.of<AuthBloc>(widget.context);
    _authManager.actionOnInit(
        context: widget.context,
        authBloc: _authBloc,
        updateUiDataModel: _updateUiDataModel);
  }

  @override
  void dispose() {
    super.dispose();
    _phoneController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    return Scaffold(
      key: _scaffoldKey,
      body: BlocEventStateBuilder<AuthState>(
        bloc: _authBloc,
        builder: (BuildContext context, AuthState authState) {
          _context = context;
          if (authState != null && _authState != authState) {
            _authState = authState;
            _authManager.actionSendOtpStateChange(
                context: context,
                scaffoldState: _scaffoldKey?.currentState,
                phoneController: _phoneController,
                authBloc: _authBloc,
                authState: _authState,
                updateUiDataModel: _updateUiDataModel);
          }
          return ModalProgressHUD(
            inAsyncCall: authState?.isLoading ?? false,
            child: SafeArea(
              bottom: false,
              top: false,
              child: Stack(
                children: [
                  Container(
                    decoration: new BoxDecoration(
                      image: DecorationImage(
                        image: new AssetImage(AUTH_BACK),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  SingleChildScrollView(
                    padding: EdgeInsets.all(SIZE_24),
                    child: Wrap(
                      runSpacing: SIZE_1,
                      alignment: WrapAlignment.center,
                      children: <Widget>[
                        _showLogInLogo(),
                        _getCardView(),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  // used to create a top text view
  Widget _showLogInLogo() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: SIZE_20),
          child: Container(
              margin: EdgeInsets.only(top: SIZE_20),
              width: MediaQuery.of(_context).size.width,
              height: CommonUtils.commonUtilsInstance.getPercentageSize(
                  context: _context, ofWidth: false, percentage: SIZE_30),
              child: Image.asset(
                SIGN_UP_LOGO,
              )),
        ),
        SizedBox(
          height: CommonUtils.commonUtilsInstance.getPercentageSize(
              context: _context, ofWidth: false, percentage: SIZE_3),
        )
      ],
    );
  }

  //used to create a form
  Widget _form() {
    return Form(
      child: _showCountryCodePickerAndPhoneNumber(),
    );
  }

  // used to create a  phone text field form
  Widget _phoneTextFormField() {
    return Row(
      children: <Widget>[
        // Padding(
        //   padding: const EdgeInsets.only(bottom: SIZE_2),
        //   child: Text(
        //     '+ ${_updateUiDataModel?.selectedCountry?.dialingCode}' ??
        //         Country?.IN?.dialingCode + " ",
        //     style: AppConfig.of(_context).themeData.textTheme.headline2,
        //   ),
        // ),
        Expanded(
          child: _textFieldForm(
            keyboardType: TextInputType.number,
            hint: AppLocalizations.of(_context).sendOtp.hint.phoneNumber,
            elevation: ELEVATION_0,
            controller: _phoneController,
          ),
        )
      ],
    );
  }

  // used to create a  phone country picker  text field form
  Widget _showCountryCodePickerAndPhoneNumber() {
    return Container(
      padding: EdgeInsets.only(left: SIZE_5, right: SIZE_5),
      margin: EdgeInsets.only(top: SIZE_15),
      decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          border: Border.all(color: COLOR_PRIMARY, width: SIZE_1),
          borderRadius: BorderRadius.circular(SIZE_30)),
      child: Row(
        children: <Widget>[
          // Padding(
          //   padding: const EdgeInsets.all(SIZE_5),
          //   child: CountryPicker(
          //     dense: false,
          //     showFlag: true,
          //     //displays flag, true by default
          //     showDialingCode: false,
          //     //displays dialing code, false by default
          //     showName: false,
          //     //displays country name, true by default
          //     showCurrency: false,
          //     //eg. 'British pound'
          //     showCurrencyISO: false,
          //     //eg. 'GBP'
          //     onChanged: (Country country) {
          //       _updateUiDataModel?.selectedCountry = country;
          //       _updateUiDataModel?.phoneNumber =
          //           _phoneController.text.toString();
          //       _authManager?.updateUI(
          //           context: _context,
          //           authBloc: _authBloc,
          //           authState: _authState,
          //           updateUiDataModel: _updateUiDataModel);
          //       ;
          //     },
          //     selectedCountry:
          //         _updateUiDataModel?.selectedCountry ?? Country.IN,
          //   ),
          // ),
          // Container(
          //   margin: EdgeInsets.only(right: SIZE_5),
          //   child: Image.asset(DIVIDER_ICON),
          //   height: CommonUtils.commonUtilsInstance.getPercentageSize(
          //       context: _context, ofWidth: false, percentage: SIZE_5),
          // ),
          Expanded(
            child: _phoneTextFormField(),
          )
        ],
      ),
    );
  }

  //method to show  proceed button
  Widget _continueButton() {
    return Container(
      margin: EdgeInsets.only(top: SIZE_20),
      height: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _context, percentage: SIZE_6, ofWidth: false),
      width: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _context, percentage: SIZE_80, ofWidth: true),
      child: RaisedGradientButton(
        radious: SIZE_30,
        gradient: LinearGradient(
          begin: Alignment.bottomCenter,
          end: Alignment.topCenter,
          colors: <Color>[COLOR_PRIMARY, COLOR_ACCENT],
        ),
        onPressed: () {
          _validateSignUp();
        },
        child: Text(
          "Continue",
          textAlign: TextAlign.center,
          style: textStyleSize14WithWhiteColor,
        ),
      ),
    );
  }

  //method to return text form field
  Widget _textFieldForm(
      {TextEditingController controller,
      TextInputType keyboardType,
      bool showTickIcon,
      String hint,
      double elevation,
      FormFieldValidator<String> validator}) {
    return Material(
      borderRadius: BorderRadius.circular(SIZE_20),
      elevation: elevation,
      color: Colors.white,
      child: TextFormField(
        validator: validator,
        onFieldSubmitted: _showTickMark,
        onChanged: (phoneNumber) {
          if (phoneNumber.length >= MaxLength.MinPhoneLength.value) {
            _showTickMark(phoneNumber);
          } else {
            _showTickMark(phoneNumber);
          }
        },
        controller: controller,
        keyboardType: keyboardType,
        inputFormatters: <TextInputFormatter>[
          WhitelistingTextInputFormatter.digitsOnly,
          new LengthLimitingTextInputFormatter(14),
        ],
        style: AppConfig.of(_context).themeData.textTheme.headline2,
        decoration: InputDecoration(
          prefixIcon: Image.asset(
            USER_ICON,
            scale: SIZE_3,
          ),
          suffixIcon: (_updateUiDataModel?.phoneNumber != null &&
                  _updateUiDataModel?.phoneNumber?.length >=
                      MaxLength.MinPhoneLength.value)
              ? Icon(Icons.check, color: COLOR_PRIMARY, size: SIZE_20)
              : null,
          hintText: hint,
          contentPadding: EdgeInsets.only(left: SIZE_10),
          border: OutlineInputBorder(borderSide: BorderSide.none),
        ),
      ),
    );
  }

  //this method will show the tick mark icon when user hits done button while typing number
  void _showTickMark(String values) {
    _updateUiDataModel.phoneNumber = values;
    _authManager?.updateUI(
      context: _context,
      updateUiDataModel: _authState?.updateUiDataModel ?? _updateUiDataModel,
      authBloc: _authBloc,
      authState: _authState,
    );
  }

  //method to return accept terms and services widget
  Widget _acceptTermsAndConditionWidget() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Theme(
          data: Theme.of(_context).copyWith(
            unselectedWidgetColor: COLOR_BORDER_GREY,
          ),
          child: Padding(
            padding: const EdgeInsets.only(bottom: SIZE_12),
            child: Checkbox(
                value: _authState?.updateUiDataModel?.isTermsAccepted ?? false,
                activeColor: COLOR_PRIMARY,
                onChanged: (bool value) {
                  _updateUiDataModel?.isTermsAccepted = value;
                  _updateUiDataModel?.phoneNumber =
                      _phoneController?.text.toString();
                  _updateUiDataModel?.selectedCountry = Country.CA;
                  _authManager?.updateUI(
                      context: _context,
                      authState: _authState,
                      updateUiDataModel: _updateUiDataModel,
                      authBloc: _authBloc);
                }),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: SIZE_10, bottom: SIZE_10),
          child: Center(
            child: RichText(
              textAlign: TextAlign.start,
              text: TextSpan(
                children: <TextSpan>[
                  TextSpan(
                      text: AppLocalizations.of(_context).sendOtp.text.accepthe,
                      style:
                          AppConfig.of(_context).themeData.textTheme.subtitle2),
                  TextSpan(
                      text: AppLocalizations.of(_context)
                              .sendOtp
                              .text
                              .termsAndServices +
                          " ",
                      style: textStyleSize28WithPrimaryColor,
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          LauncherUtils.launcherUtilsInstance.launchInBrowser(
                              url: AppConfig.of(context).termsUrl);
                        }),
                  TextSpan(
                      text:
                          AppLocalizations.of(_context).sendOtp.text.limitLess,
                      style: textStyleSize28WithBlackColor)
                ],
              ),
              textScaleFactor: 0.5,
            ),
          ),
        ),
      ],
    );
  }

  //method to call validate for when proceed button is pressed
  void _validateSignUp() {
    _authManager.sendOtpCall(
      context: _context,
      authBloc: _authBloc,
      phoneNumber: _phoneController?.text,
      updateUiDataModel: _updateUiDataModel,
      // selectedCountry: _updateUiDataModel?.selectedCountry ?? Country.IN,
      selectedCountry: Country.CA,
      scaffoldState: _scaffoldKey?.currentState,
    );
  }

  //Get card view
  _getCardView() {
    return Container(
      height: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _context, ofWidth: false, percentage: SIZE_53),
      padding: EdgeInsets.all(SIZE_20),
      decoration: new BoxDecoration(
        borderRadius: new BorderRadius.all(Radius.circular(SIZE_30)),
        shape: BoxShape.rectangle,
        color: Colors.white,
        boxShadow: [BoxShadow(color: Colors.white70, blurRadius: 6)],
      ),
      child: Wrap(
        runSpacing: SIZE_10,
        alignment: WrapAlignment.center,
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Sign In / Sign Up",
              style:
                  AppConfig.of(_context).themeData.primaryTextTheme.subtitle2,
            ),
          ),
          _form(),
          // _acceptTermsAndConditionWidget(),
          _continueButton(),
        ],
      ),
    );
  }
}
