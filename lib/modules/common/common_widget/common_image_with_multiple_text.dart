import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:upmart_driver/modules/common/app_config/app_config.dart';
import 'package:upmart_driver/modules/common/constants/dimens_constants.dart';
import 'package:upmart_driver/modules/common/theme/app_themes.dart';
import 'package:upmart_driver/modules/common/utils/common_utils.dart';
import 'package:upmart_driver/modules/common/utils/image_utils.dart';

class CommonImageWithMultipleTextWidget extends StatefulWidget {
  String imageUrl;
  String titleText;
  String subTitleText;
  String messageText;
  String localImagePath;
  bool isSvgImage = true;
  BuildContext context;
  TextStyle textStyle;
  double imageHeight;
  double imageWidth;

  CommonImageWithMultipleTextWidget(
      {this.context,
      this.imageUrl,
      this.localImagePath,
      this.textStyle,
      this.isSvgImage = true,
      this.titleText,
      this.subTitleText,
      this.messageText,
      this.imageWidth,
      this.imageHeight}) {
    this.context = context;
    this.imageUrl = imageUrl;
    this.localImagePath = localImagePath;
    this.isSvgImage = isSvgImage;
    this.titleText = titleText;
    this.subTitleText = subTitleText;
    this.messageText = messageText;
    this.textStyle = textStyle;
    this.imageHeight = imageHeight;
    this.imageWidth = imageWidth;
  }

  @override
  _CommonImageWithMultipleTextWidgetState createState() =>
      _CommonImageWithMultipleTextWidgetState();
}

class _CommonImageWithMultipleTextWidgetState
    extends State<CommonImageWithMultipleTextWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        (widget.imageUrl?.isNotEmpty == true)
            ? ImageUtils.imageUtilsInstance.showCacheNetworkImage(
                context: context,
                showProgressBarInPlaceHolder: true,
                shape: BoxShape.circle,
                url: widget?.imageUrl ?? "",
                height: CommonUtils.commonUtilsInstance.getPercentageSize(
                    context: context,
                    percentage: widget?.imageHeight ?? SIZE_7,
                    ofWidth: false),
                width: CommonUtils.commonUtilsInstance.getPercentageSize(
                    context: context,
                    percentage: widget?.imageWidth ?? SIZE_7,
                    ofWidth: false))
            : widget.isSvgImage == true
                ? SvgPicture.asset(
                    widget?.localImagePath,
                    height: CommonUtils.commonUtilsInstance.getPercentageSize(
                        context: context,
                        percentage: widget?.imageHeight ?? SIZE_7,
                        ofWidth: false),
                    width: CommonUtils.commonUtilsInstance.getPercentageSize(
                        context: context,
                        percentage: widget?.imageWidth ?? SIZE_7,
                        ofWidth: true),
                  )
                : Image.asset(
                    widget.localImagePath,
                    height: CommonUtils.commonUtilsInstance.getPercentageSize(
                        context: context,
                        percentage: widget?.imageHeight ?? SIZE_7,
                        ofWidth: false),
                    width: CommonUtils.commonUtilsInstance.getPercentageSize(
                        context: context,
                        percentage: widget?.imageWidth ?? SIZE_7,
                        ofWidth: true),
                  ),
        SizedBox(
          height: SIZE_10,
        ),
        Text(
          widget?.titleText,
          style: AppConfig.of(context).themeData.primaryTextTheme.headline5,
        ),
        SizedBox(
          height: SIZE_10,
        ),
        Text(
          widget?.subTitleText,
          style: textStyleSize14WithBlackBoldColor,
        ),
        SizedBox(
          height: SIZE_2,
        ),
        Text(
          widget?.messageText,
          style: widget?.textStyle,
        )
      ],
    );
  }
}
