//class to provide border color to circular indicator
import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/common/constants/color_constants.dart';
import 'package:upmart_driver/modules/common/constants/dimens_constants.dart';

class IndicatorExample extends StatelessWidget {
  const IndicatorExample( {Key key, this.number,this.color}) : super(key: key);
   final String number;
   final Color color;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.fromBorderSide(
          BorderSide(
            color:color,
            width: SIZE_2,
          ),
        ),
      ),
    );
  }
}
