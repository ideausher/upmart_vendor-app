import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:upmart_driver/localizations.dart';
import 'package:upmart_driver/modules/auth/auth_bloc/auth_bloc.dart';
import 'package:upmart_driver/modules/auth/auth_bloc/auth_state.dart';
import 'package:upmart_driver/modules/auth/constants/image_constant.dart';
import 'package:upmart_driver/modules/auth/enums/auth_enums.dart';
import 'package:upmart_driver/modules/auth/manager/auth_manager.dart';
import 'package:upmart_driver/modules/common/app_config/app_config.dart';
import 'package:upmart_driver/modules/common/common_widget/custom_raised_gradient_button.dart';
import 'package:upmart_driver/modules/common/constants/color_constants.dart';
import 'package:upmart_driver/modules/common/constants/dimens_constants.dart';
import 'package:upmart_driver/modules/common/constants/font_constant.dart';
import 'package:upmart_driver/modules/common/theme/app_themes.dart';
import 'package:upmart_driver/modules/common/utils/common_utils.dart';
import 'package:upmart_driver/modules/common/model/update_ui_data_model.dart';

class VerifyOTPWidget extends StatefulWidget {
  String title;
  String subtitle;
  String userPhoneEmail;
  BuildContext context;
  AuthManager authManager;
  AuthState authState;
  AuthBloc authBloc;
  Function onResendOtpButtonPressed;
  Function onSendOtpButtonPressed;
  Function onChangeNumberPressed;
  bool isForEmail;
  bool isFromEmailEdit;
  TextEditingController verifyOtpController;
  TextEditingController phoneNumberController;

  VerifyOTPWidget(
      {this.context,
      this.title,
      this.subtitle,
      this.authManager,
      this.authState,
      this.authBloc,
      this.userPhoneEmail,
      this.isFromEmailEdit: false,
      this.onSendOtpButtonPressed,
      this.onResendOtpButtonPressed,
      this.isForEmail: false,
      this.verifyOtpController,
      this.phoneNumberController,
      this.onChangeNumberPressed});

  @override
  _VerifyOTPWidgetState createState() => _VerifyOTPWidgetState();
}

class _VerifyOTPWidgetState extends State<VerifyOTPWidget> {
  BuildContext _context;
  UpdateUiDataModel _updateUiDataModel;

  //Controllers for OTP
  StreamController<ErrorAnimationType> _errorController =
      StreamController<ErrorAnimationType>();
  int _endTime = DateTime.now()
      .add(Duration(minutes: Timer?.sec?.value))
      .millisecondsSinceEpoch;

  @override
  void initState() {
    super.initState();
    _updateUiDataModel = new UpdateUiDataModel();
  }

  @override
  void dispose() {
    super.dispose();
    _errorController.close();
    widget?.verifyOtpController?.dispose();
    widget?.phoneNumberController?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _context = widget?.context;
    _updateUiDataModel =
        widget?.authState?.updateUiDataModel ?? _updateUiDataModel;
    return SingleChildScrollView(
      padding: EdgeInsets.all(SIZE_18),
      child: Wrap(
        alignment: WrapAlignment.center,
        children: <Widget>[
          Visibility(
              visible: widget.isFromEmailEdit == false,
              child: _verifyOtpLogo()),
          // _verifyMobileNumberTitleView(),
          // _verifyMobileNumberSubtitleView(),

          widget.isFromEmailEdit == false
              ? _getCardView()
              : _getEditProfileCardView(),
        ],
      ),
    );
  }

  //Method to show subtitle text of verify otp screen
  Widget _verifyOtpLogo() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: SIZE_20),
          child: Container(
              margin: EdgeInsets.only(top: SIZE_20),
              width: MediaQuery.of(_context).size.width,
              height: CommonUtils.commonUtilsInstance.getPercentageSize(
                  context: _context, ofWidth: false, percentage: SIZE_30),
              child: Image.asset(
                SIGN_UP_LOGO,
              )),
        ),
        SizedBox(
          height: CommonUtils.commonUtilsInstance.getPercentageSize(
              context: _context, ofWidth: false, percentage: SIZE_3),
        )
      ],
    );
  }

  //This method will return a view to fill OTP
  Widget _enterOtpView() {
    return Container(
      padding: EdgeInsets.only(left: SIZE_5, right: SIZE_5),
      margin: EdgeInsets.only(top: SIZE_15),
      decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          border: Border.all(color: COLOR_PRIMARY, width: SIZE_1),
          borderRadius: BorderRadius.circular(SIZE_30)),
      child: Row(
        children: <Widget>[
          Expanded(
            child: _textFieldForm(
                keyboardType: TextInputType.number,
                hint: "Enter OTP",
                elevation: ELEVATION_0,
                controller: widget?.verifyOtpController,
                inputCharLenght: 4,
                showPrefixIcon: false,
                showSuffixIcon: false,
                showTickIcon: false,
                enabled: true),
          )
        ],
      ),
    );
    // return Padding(
    //   padding:
    //       const EdgeInsets.only(left: SIZE_30, right: SIZE_30, top: SIZE_22),
    //   child: PinCodeTextField(
    //     length: MaxLength.OTP.value,
    //     obsecureText: false,
    //     inputFormatters: <TextInputFormatter>[
    //       WhitelistingTextInputFormatter.digitsOnly
    //     ],
    //     textInputType: TextInputType.number,
    //     animationType: AnimationType.scale,
    //     mainAxisAlignment: MainAxisAlignment.spaceAround,
    //     pinTheme: PinTheme(
    //       shape: PinCodeFieldShape.box,
    //       borderRadius: BorderRadius.circular(SIZE_6),
    //       borderWidth: SIZE_1,
    //       fieldHeight: CommonUtils.commonUtilsInstance.getPercentageSize(
    //           context: _context, percentage: SIZE_6, ofWidth: false),
    //       activeColor: AppConfig.of(_context).themeData.primaryColor,
    //       inactiveColor: COLOR_LIGHT_GREY,
    //       inactiveFillColor: COLOR_OFF_WHITE,
    //       selectedFillColor: Colors.white,
    //       selectedColor: AppConfig.of(_context).themeData.primaryColor,
    //       fieldWidth: CommonUtils.commonUtilsInstance.getPercentageSize(
    //           context: _context, percentage: SIZE_12, ofWidth: true),
    //       activeFillColor: COLOR_OFF_WHITE,
    //     ),
    //     animationDuration: Duration(milliseconds: 300),
    //     enableActiveFill: true,
    //     textStyle: AppConfig.of(_context).themeData.textTheme.bodyText1,
    //     autoDisposeControllers: false,
    //     errorAnimationController: _errorController,
    //     controller: widget?.verifyOtpController,
    //     onCompleted: (v) {
    //       _updateUiDataModel?.otpLength =
    //           widget?.verifyOtpController?.text?.length;
    //       _callChangeButtonColorEvent();
    //       print('hii i am oncomplete');
    //     },
    //     onChanged: (value) {
    //       if (value.length <= MaxLength.OtpErrorLength.value) {
    //         _updateUiDataModel?.otpLength =
    //             widget?.verifyOtpController?.text?.length;
    //         _callChangeButtonColorEvent();
    //       }
    //     },
    //     beforeTextPaste: (text) {
    //       print("Allowing to paste $text");
    //       //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
    //       //but you can show anything you want here, like your pop up saying wrong paste format or etc
    //       return true;
    //     },
    //   ),
    // );
  }

  //Method to show continue Raised Button.
  Widget _continueButton() {
    return Container(
      margin: EdgeInsets.only(top: SIZE_20),
      height: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _context, percentage: SIZE_6, ofWidth: false),
      width: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _context, percentage: SIZE_80, ofWidth: true),
      child: RaisedGradientButton(
        radious: SIZE_30,
        gradient: LinearGradient(
          begin: Alignment.bottomCenter,
          end: Alignment.topCenter,
          colors: <Color>[COLOR_PRIMARY, COLOR_ACCENT],
        ),
        onPressed: () {
          _callOTPVerification(
            otp: widget?.verifyOtpController?.text.toString(),
          );
        },
        child: Text(
          "Continue",
          textAlign: TextAlign.center,
          style: textStyleSize14WithWhiteColor,
        ),
      ),
    );
  }

  //method to show verify mobile number title view
  Widget _verifyMobileNumberTitleView() {
    return Text(
      widget.title,
      style: AppConfig.of(_context).themeData.textTheme.headline1,
    );
  }

  //method to show verify mobile number subtitle view
  Widget _verifyMobileNumberSubtitleView() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Expanded(
          child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              children: <TextSpan>[
                TextSpan(
                    text: widget.subtitle,
                    style: AppConfig.of(_context)
                        .themeData
                        .primaryTextTheme
                        .bodyText1),
                TextSpan(
                    text: widget.userPhoneEmail ?? "",
                    style: AppConfig.of(_context)
                        .themeData
                        .primaryTextTheme
                        .caption),
              ],
            ),
            textScaleFactor: 0.5,
          ),
        )
      ],
    );
  }

  //method to show change number view
  Widget _showChangeNumberView() {
    return Padding(
      padding: const EdgeInsets.only(left: SIZE_12, right: SIZE_12),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          InkWell(
            onTap: () {
              _onChangeNumberPressed();
            },
            child: Text(
              (widget?.isFromEmailEdit == true)
                  ? ""
                  : AppLocalizations.of(_context).verifyOtp.text.changeNumber,
              style: AppConfig.of(_context).themeData.textTheme.headline3,
            ),
          ),
          Wrap(
            direction: Axis.vertical,
            children: <Widget>[
              InkWell(
                onTap: () {
                  if (widget?.authState?.updateUiDataModel?.resendButton ==
                      COLOR_PRIMARY) {
                    _callOnResendOtp();
                  }
                },
                child: Text(AppLocalizations.of(_context).verifyOtp.text.resend,
                    style: TextStyle(
                        color: widget
                                ?.authState?.updateUiDataModel?.resendButton ??
                            COLOR_BORDER_GREY,
                        fontSize: SIZE_16,
                        fontWeight: FontWeight.w600,
                        fontFamily: FONT_FAMILY_INTER)),
              ),
            ],
          )
        ],
      ),
    );
  }

  //this method will call the validate otp method
  void _callOTPVerification({String otp}) {
    widget?.onSendOtpButtonPressed(otp);
  }

  //this method is used to call resend otp method
  void _callOnResendOtp() {
    _endTime = DateTime.now()
        .add(Duration(minutes: Timer?.sec?.value))
        .millisecondsSinceEpoch;
    _updateUiDataModel.resendButton = COLOR_BORDER_GREY;
    widget?.authManager?.updateUI(
        context: _context,
        authBloc: widget?.authBloc,
        authState: widget?.authState,
        updateUiDataModel: _updateUiDataModel);
    widget?.onResendOtpButtonPressed();
  }

  void _onChangeNumberPressed() {
    widget?.onChangeNumberPressed();
  }

  //this method is used to change color when user fills otp
  void _callChangeButtonColorEvent() {
    widget?.authManager?.updateUI(
        context: _context,
        authBloc: widget?.authBloc,
        authState: widget?.authState,
        updateUiDataModel: _updateUiDataModel);
  }

  //method to show otp timer widget view
  Widget _showOTPTimerView() {
    return CountdownTimer(
      endTime: _endTime,
      widgetBuilder: (_, CurrentRemainingTime time) {
        return (time != null)
            ? Container(
                padding: const EdgeInsets.only(right: SIZE_12),
                alignment: Alignment.topRight,
                width: MediaQuery.of(context).size.width,
                child: Text(
                  '0${time?.min ?? "0"} : ${(time?.sec?.toString()?.length == Timer?.sec?.value) ? time?.sec : '0${time?.sec}' ?? "00:00"}',
                  style: AppConfig.of(_context).themeData.textTheme.headline4,
                ),
              )
            : Container(
                padding: const EdgeInsets.only(right: SIZE_12),
                width: MediaQuery.of(context).size.width,
                alignment: Alignment.topRight,
                child: Text(
                  "00:00",
                  style: AppConfig.of(_context).themeData.textTheme.headline4,
                  textAlign: TextAlign.end,
                ),
              );
      },
      emptyWidget: Text(
        "00:00",
        style: AppConfig.of(_context).themeData.textTheme.headline4,
        textAlign: TextAlign.center,
      ),
      onEnd: () {
        UpdateUiDataModel updateUiDataModel = _updateUiDataModel;
        updateUiDataModel.resendButton = COLOR_PRIMARY;
        widget?.authManager?.updateUI(
            context: _context,
            authBloc: widget?.authBloc,
            authState: widget?.authState,
            updateUiDataModel: updateUiDataModel);
      },
      textStyle: AppConfig.of(_context).themeData.textTheme.headline4,
    );
  }

  //Get card view
  _getCardView() {
    return Container(
      height: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _context, ofWidth: false, percentage: SIZE_53),
      padding: EdgeInsets.all(SIZE_20),
      decoration: new BoxDecoration(
        borderRadius: new BorderRadius.all(Radius.circular(SIZE_30)),
        shape: BoxShape.rectangle,
        color: Colors.white,
        boxShadow: [BoxShadow(color: Colors.white70, blurRadius: 6)],
      ),
      child: Wrap(
        runSpacing: SIZE_10,
        alignment: WrapAlignment.center,
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Sign In / Sign Up",
              style:
                  AppConfig.of(_context).themeData.primaryTextTheme.subtitle2,
            ),
          ),
          // _form(),
          _getPhoneTextField(),
          _enterOtpView(),
          // _showChangeNumberView(),
          // _showOTPTimerView(),
          _continueButton(),
        ],
      ),
    );
  }

  //To get the form
  _getPhoneTextField() {
    widget?.phoneNumberController?.text = widget?.userPhoneEmail ?? "";
    return Container(
      padding: EdgeInsets.only(left: SIZE_5, right: SIZE_5),
      margin: EdgeInsets.only(top: SIZE_15),
      decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          border: Border.all(color: COLOR_PRIMARY, width: SIZE_1),
          borderRadius: BorderRadius.circular(SIZE_30)),
      child: Row(
        children: <Widget>[
          Expanded(
            child: _textFieldForm(
                keyboardType: TextInputType.number,
                hint: AppLocalizations.of(_context).sendOtp.hint.phoneNumber,
                elevation: ELEVATION_0,
                controller: widget?.phoneNumberController,
                inputCharLenght: 14,
                showPrefixIcon: true,
                showSuffixIcon: false,
                showTickIcon: false,
                enabled: false),
          )
        ],
      ),
    );
  }

  //method to return text form field
  Widget _textFieldForm({
    TextEditingController controller,
    TextInputType keyboardType,
    bool showTickIcon,
    String hint,
    double elevation,
    FormFieldValidator<String> validator,
    int inputCharLenght,
    bool showSuffixIcon,
    bool showPrefixIcon,
    bool enabled,
  }) {
    return Material(
      borderRadius: BorderRadius.circular(SIZE_20),
      elevation: elevation,
      color: Colors.white,
      child: TextFormField(
        validator: validator,
        onChanged: (phoneNumber) {},
        enabled: enabled,
        controller: controller,
        keyboardType: keyboardType,
        inputFormatters: <TextInputFormatter>[
          WhitelistingTextInputFormatter.digitsOnly,
          new LengthLimitingTextInputFormatter(inputCharLenght),
        ],
        style: AppConfig.of(_context).themeData.textTheme.headline2,
        decoration: InputDecoration(
          prefixIcon: showPrefixIcon == true
              ? Image.asset(
                  USER_ICON,
                  scale: SIZE_3,
                )
              : null,
          suffixIcon: showSuffixIcon == false
              ? null
              : (_updateUiDataModel?.phoneNumber != null &&
                      _updateUiDataModel?.phoneNumber?.length >=
                          MaxLength.MinPhoneLength.value)
                  ? Icon(Icons.check, color: COLOR_PRIMARY, size: SIZE_20)
                  : null,
          hintText: hint,
          contentPadding: EdgeInsets.only(left: SIZE_10),
          border: OutlineInputBorder(borderSide: BorderSide.none),
        ),
      ),
    );
  }

  //Get edit profile card view
  _getEditProfileCardView() {
    return Wrap(
      runSpacing: SIZE_10,
      alignment: WrapAlignment.center,
      children: [
        _getPhoneTextField(),
        _enterOtpView(),
        _continueButton(),
      ],
    );
  }
}
