import 'package:flutter/material.dart';

const Color COLOR_PRIMARY = Color(0xFF13243F);
const Color COLOR_PRIMARY_DARK = Color(0xFF242A37);
const Color COLOR_ACCENT = Color(0xFFCE2834);

const Color COLOR_BORDER_GREY = Color(0xFFC0C0C0);
const Color COLOR_BLACK = Color(0xFF303030);
const Color COLOR_RED = Color(0xFFFF0000);
const Color COLOR_WHITE = Color(0xFFFFFFFF);
const Color COLOR_OFF_WHITE = Color(0xFFFAF8EF);
const Color COLOR_LIGHT_GREY = Color(0xFFBEC2CE);
const Color COLOR_LIGHT_GREEN = Color(0xFF00FFB0);
const Color COLOR_DARK_GREEN = Color(0xFF4BC39D);
const Color COLOR_GREY = Color(0xFFE8E8E8);
const Color LIGHT_PINK = Color(0xFFFFF6F6);

const Color COLOR_DARK_GREY = Color(0xFF666666);
const Color COLOR_GREY_LIGHT = Color(0xFF757575);
const Color COLOR_LIGHT_BLUE = Color(0xFFE5F2FF);
const Color COLOR_LIGHT_RED = Color(0xFFFFE5E5);
const Color COLOR_DARK_BLUE = Color(0xFF3086FF);
const Color COLOR_GREEN_COLOR = Color(0xFFEDFFE5);
const Color COLOR_MY_EARNING_BACK = Color(0xFF8e5eff);
const Color COLOR_BUTTON = Color(0xFFFFBE27);

const Color COLOR_GREY_DIVIDER = Color(0xFF707070);
const Color COLOR_TEXT_LABEL = Color(0xFFA6A6A6);
const Color COLOR_GREEN = Color(0xFF08C25E);

const Color COLOR_RATING = Color(0xFF6e6e6e);
const Color COLOR_LIGHT_PINK = Color(0xFF99FFF6F6);

const Color COLOR_CARD_GARDIENT_1 = Color(0xFFFF777A);
const Color COLOR_CARD_GARDIENT_2 = Color(0xFF6CBFFF);
const Color COLOR_BLUE_TEXT = Color(0xFF003965);
const Color COLOR_ORANGE = Color(0xFFFF8330);
const Color COLOR_ORANGE_LIGHT = Color(0xFFFFE5D3);
const Color COLOR_NOTIFICATION_BACK = Color(0xFFF4F4F4);
const Color COLOR_GREY_TEXT = Color(0xFFd0cdcd);


