enum ApiStatus { Success, Failure, Updated, NoChange, NotFound, Unauthorise }

extension ApiStatusExtension on ApiStatus {
  int get value {
    switch (this) {
      case ApiStatus.Success:
        return 200;
      case ApiStatus.Failure:
        return 0;
      case ApiStatus.Updated:
        return 1;
      case ApiStatus.NoChange:
        return 0;
      case ApiStatus.NotFound:
        return 404;
      case ApiStatus.Unauthorise:
        return 401;
      default:
        return null;
    }
  }
}

enum LanguageType { English, Portuguese }

extension LanguageTypeExtension on LanguageType {
  String get value {
    switch (this) {
      case LanguageType.English:
        return "English";
      case LanguageType.Portuguese:
        return "Portuguese";
      default:
        return "English";
    }
  }
}

enum ApiStatusParams { Status, Message, Success, Error,Data }

extension ApiStatusParamsExtension on ApiStatusParams {
  String get value {
    switch (this) {
      case ApiStatusParams.Success:
        return "Success";
      case ApiStatusParams.Status:
        return "status_code";
      case ApiStatusParams.Message:
        return "message";
      case ApiStatusParams.Data:
        return "data";
      default:
        return null;
    }
  }
}

enum PrefsEnum { UserProfileData, UserLocationData, UserVehicleData, FcmToken }

extension PrefsEnumExtension on PrefsEnum {
  String get value {
    switch (this) {
      case PrefsEnum.UserProfileData:
        return "UserProfileData";
        break;
      case PrefsEnum.UserLocationData:
        return "UserLocationData";
      case PrefsEnum.UserVehicleData:
        return "UserVehicleData";
      case PrefsEnum.FcmToken:
        return "FcmToken";
      default:
        return null;
    }
  }
}

enum NotificationType { chat,queryResponse }

extension NotificationTypeExtension on NotificationType {
  int get value {
    switch (this) {
      case NotificationType.chat:
        return 2;
      case NotificationType.queryResponse:
        return 16;
      default:
        return null;
    }
  }
}

enum AppConfiguration { Live, Staging }

extension AppConfigurationExtension on AppConfiguration {
  String get value {
    switch (this) {
      case AppConfiguration.Live:
        return "http://iphoneapps.co.in/baza_web/public/api/";
      case AppConfiguration.Staging:
        return "http://iphoneapps.co.in/baza_web/public/api/";
      default:
        return null;
    }
  }
}
