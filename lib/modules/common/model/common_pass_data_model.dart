import 'package:upmart_driver/modules/vehicle_documents/api/model/vehicle_registration_response_model.dart';

import '../screen_pop_callback.dart';

class CommonPassDataModel {
  VehicleRegistrationResponseModel vehicleRegistrationResponseModel;
  bool isComingFromDetails = false;
  ScreenPopCallBack screenPopCallBack;
  CommonPassDataModel({this.vehicleRegistrationResponseModel, this.isComingFromDetails,this.screenPopCallBack});
}