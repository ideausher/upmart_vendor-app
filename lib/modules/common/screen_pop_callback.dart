class ScreenPopBack {
  static ScreenPopBack _screenPopBack = ScreenPopBack();

  static ScreenPopBack get screenPopBack => _screenPopBack;

  List<ScreenPopCallBack> screenPopCallBacks = List<ScreenPopCallBack>();

  addCallback({ScreenPopCallBack screenPopCallBack}) {
    screenPopCallBacks.add(screenPopCallBack);
  }

  removeCallback({ScreenPopCallBack screenPopCallBack}) {
    screenPopCallBacks.remove(screenPopCallBack);
  }
}

class ScreenPopCallBack {
  onScreenPop({int callbackValue}) {}
}
