import 'dart:convert';
import 'dart:io';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:upmart_driver/modules/common/enum/enums.dart';
import 'package:upmart_driver/modules/common/utils/shared_prefs_utils.dart';

class FirebaseMessagingUtils {
  static FirebaseMessagingUtils _firebaseMessagingUtils =
      FirebaseMessagingUtils();

  static FirebaseMessagingUtils get firebaseMessagingUtils =>
      _firebaseMessagingUtils;

  List<PushReceived> pushCallbacks = List<PushReceived>();

  addCallback({PushReceived pushReceived}) {
    pushCallbacks.add(pushReceived);
  }

  removeCallback({PushReceived pushReceived}) {
    pushCallbacks.remove(pushReceived);
  }

  FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  Future<void> _onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    print("notification received");
// display a dialog with the notification details, tap ok to go to another page
  }

  initFirebaseMessaging() {
    int _id = 0;
    var initializationSettingsAndroid =
        AndroidInitializationSettings('mipmap/ic_launcher');
    var initializationSettingsIOS = IOSInitializationSettings(
        onDidReceiveLocalNotification: _onDidReceiveLocalNotification);

    var initializationSettings = InitializationSettings(
        android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
    _flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: (data) {
      print("onclick: $data");
    });

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        _id++;
        NotificationPushModel _notificationModel =
            NotificationPushModel.fromMap(message);
        print("_notificationModel: $_notificationModel");
        var androidPlatformChannelSpecifics = AndroidNotificationDetails(
            'your channel id', 'your channel name', 'your channel description',
            importance: Importance.max,
            priority: Priority.high,
            ticker: 'ticker',
            autoCancel: true);
        var iOSPlatformChannelSpecifics = IOSNotificationDetails();
        var platformChannelSpecifics = NotificationDetails(
            android: androidPlatformChannelSpecifics,
            iOS: iOSPlatformChannelSpecifics);

        if (_notificationModel?.data?.type != null) {
          // await _flutterLocalNotificationsPlugin.cancel(0);
        } else {
          await _flutterLocalNotificationsPlugin.show(
            _id,
            Platform.isAndroid
                ? _notificationModel?.notification?.title
                : _notificationModel?.aps?.alert?.title,
            Platform.isAndroid
                ? _notificationModel?.notification?.body
                : _notificationModel?.aps?.alert?.body,
            platformChannelSpecifics,
          );
        }

        pushCallbacks?.forEach((element) {
          element.onMessageReceived(notificationPushModel: _notificationModel);
        });
      },
      onBackgroundMessage:
          Platform.isAndroid ? myBackgroundMessageHandler : null,
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        pushCallbacks?.forEach((element) {
          element.onMessageReceived();
        });
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        pushCallbacks?.forEach((element) {
          element.onMessageReceived();
        });
      },
    );
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(
            sound: true, badge: true, alert: true, provisional: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
    _firebaseMessaging.getToken().then((String token) async {
      await SharedPrefUtils.sharedPrefUtilsInstance
          .setString(token, PrefsEnum.FcmToken.value);
      print("Settings token: $token");
    });
  }

  Future<String> getFcmToken() async {
    return await _firebaseMessaging.getToken();
  }
}

class PushReceived {
  onMessageReceived({NotificationPushModel notificationPushModel}) {}
}

Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) async {
  print("myBackgroundMessageHandler: $message");
  FirebaseMessagingUtils.firebaseMessagingUtils.pushCallbacks
      ?.forEach((element) {
    element.onMessageReceived();
  });

  // Or do other work.
}

// To parse this JSON data, do
//
//     final notificationPushModel = notificationPushModelFromJson(jsonString);

// To parse this JSON data, do
//
//     final notificationPushModel = notificationPushModelFromJson(jsonString);

NotificationPushModel notificationPushModelFromMap(String str) =>
    NotificationPushModel.fromMap(json.decode(str));

String notificationPushModelToMap(NotificationPushModel data) =>
    json.encode(data.toMap());

class NotificationPushModel {
  NotificationPushModel({
    this.notification,
    this.data,
    this.aps,
  });

  PushNotification notification;
  NotificatonData data;
  Aps aps;

  factory NotificationPushModel.fromMap(Map<dynamic, dynamic> json) =>
      NotificationPushModel(
        notification: json["notification"] == null
            ? null
            : PushNotification.fromMap(json["notification"]),
        data:
            json["data"] == null ? null : NotificatonData.fromMap(json["data"]),
        aps: json["aps"] == null ? null : Aps.fromJson(json["aps"]),
      );

  Map<dynamic, dynamic> toMap() => {
        "notification": notification == null ? null : notification.toMap(),
        "data": data == null ? null : data.toMap(),
        "aps": aps == null ? null : aps.toJson(),
      };
}

class NotificatonData {
  NotificatonData({
    this.priority,
    this.body,
    this.title,
    this.type,
  });

  String priority;
  String body;
  String title;
  var type;

  factory NotificatonData.fromMap(Map<dynamic, dynamic> json) =>
      NotificatonData(
        priority: json["priority"] == null ? null : json["priority"],
        body: json["body"] == null ? null : json["body"],
        title: json["title"] == null ? null : json["title"],
        type: json["type"] == null ? null : json["type"],
      );

  Map<dynamic, dynamic> toMap() => {
        "priority": priority == null ? null : priority,
        "body": body == null ? null : body,
        "title": title == null ? null : title,
        "type": type == null ? null : type,
      };
}

class PushNotification {
  PushNotification({
    this.title,
    this.body,
  });

  String title;
  String body;

  factory PushNotification.fromMap(Map<dynamic, dynamic> json) =>
      PushNotification(
        title: json["title"] == null ? null : json["title"],
        body: json["body"] == null ? null : json["body"],
      );

  Map<dynamic, dynamic> toMap() => {
        "title": title == null ? null : title,
        "body": body == null ? null : body,
      };
}

class Aps {
  Aps({
    this.alert,
  });

  Alert alert;

  factory Aps.fromJson(Map<dynamic, dynamic> json) => Aps(
        alert: json["alert"] == null ? null : Alert.fromJson(json["alert"]),
      );

  Map<String, dynamic> toJson() => {
        "alert": alert == null ? null : alert.toJson(),
      };
}

class Alert {
  Alert({
    this.title,
    this.body,
  });

  String title;
  String body;

  factory Alert.fromJson(Map<dynamic, dynamic> json) => Alert(
        title: json["title"] == null ? null : json["title"],
        body: json["body"] == null ? null : json["body"],
      );

  Map<String, dynamic> toJson() => {
        "title": title == null ? null : title,
        "body": body == null ? null : body,
      };
}

enum NotificationType {
  sendByAdmin,
  adminUserAction, //if user block by admin
  walletMoneyAdded,
  bookingNotification,
  referralUser,
  acceptBooking,
  rejectBooking,
  vendorOutForService,
  vendorStartedJob,
  vendorJobFinished,
  cancelBooking,
  vendorLocationChanged,
  vendorReached,
  chat,
}

extension NotificationTypeExtension on NotificationType {
  int get value {
    switch (this) {
      case NotificationType.sendByAdmin:
        return 0;

      case NotificationType.adminUserAction:
        return 1;

      case NotificationType.walletMoneyAdded:
        return 2;
      case NotificationType.bookingNotification:
        return 3;

      case NotificationType.referralUser:
        return 4;

      case NotificationType.acceptBooking:
        return 5;

      case NotificationType.rejectBooking:
        return 6;

      case NotificationType.vendorOutForService:
        return 7;

      case NotificationType.vendorStartedJob:
        return 8;

      case NotificationType.vendorJobFinished:
        return 9;

      case NotificationType.cancelBooking:
        return 10;

      case NotificationType.vendorLocationChanged:
        return 11;
      case NotificationType.vendorReached:
        return 12;
      case NotificationType.chat:
        return 25;

      default:
        return null;
    }
  }
}
