import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class NumberFormatUtils {
  static NumberFormatUtils _numberFormatUtils = NumberFormatUtils();

  static NumberFormatUtils get numberFormatUtilsInstance => _numberFormatUtils;

  // var _locale = Locale("en", "US");
  // var _stringLocale = "en_US";

  var _locale = Locale("en", "ca");
  var _stringLocale = "en-ca";

  // update locale
  void updateLocale({String languageCode, String countryCode}) {
    _locale = Locale(languageCode, countryCode);
    _stringLocale = "${languageCode}_${countryCode}";
  }

  // get currency symbol
  String getCurrencySymbol() {
    var _format = NumberFormat.simpleCurrency(locale: _locale.toString());
    return _format.currencySymbol;
  }

  // get currency name
  String getCurrencyName() {
    var _format = NumberFormat.simpleCurrency(locale: _locale.toString());
    return _format.currencyName;
  }

  // format price with symbol
  String formatPriceWithSymbol({num price}) {
    return NumberFormat.simpleCurrency(locale: _stringLocale).format(price);
  }

  //format price with currency name
  String formatPriceWithName({num price}) {
    return NumberFormat.currency(locale: _stringLocale).format(price);
  }
}
