import 'package:upmart_driver/localizations.dart';

import '../../../modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import '../../../modules/common/enum/enums.dart';
import '../../../modules/file_picker/api/repo/remove_document_api.dart';
import '../../../modules/file_picker/api/repo/upload_document_api.dart';
import '../../../modules/file_picker/bloc/upload_document_event.dart';
import '../../../modules/file_picker/bloc/upload_document_state.dart';
import '../../../modules/file_picker/model/file_picker_model.dart';
import '../../../modules/file_picker/model/file_picker_response_model.dart';

class UploadDocumentBloc extends BlocEventStateBase<UploadDocumentEvent, UploadDocumentState> {
  UploadDocumentBloc(
      {bool initializing = true,
      FilePickerModel filePickerIntentModel,
      FilePickerResponseModel filePickerResponseModel})
      : super(
            initialState: UploadDocumentState.initiating(
                filePickerIntentModel: filePickerIntentModel, filePickerResponseModel: filePickerResponseModel));

  @override
  Stream<UploadDocumentState> eventHandler(UploadDocumentEvent event, UploadDocumentState currentState) async* {
    // used for the call sign up api
    if (event is AddFileEvent) {
      String message = "";

      yield UploadDocumentState.onDocumentUpdate(
        isLoading: true,
        message: "",
        file: event.file,
        filePickerResponseModel: event.filePickerResponseModel,
        filePickerIntentModel: event.filePickerIntentModel,
      );

      if (event.filePickerIntentModel.uploadEnable == true) {
        // call api
        var result = await UploadDocumentApi().uploadDocumentApiCall(
          context: event.context,
          file: event.file,
          endPoint: event.filePickerIntentModel.endPointUpload,
        );

        if (result != null) {
          // check result status
          if (result[ApiStatusParams.Status.value]!=null && result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
            event.filePickerResponseModel.fileList.add(FileModel(
              localPath: event.file.path,
              fileId: result["fileId"],
              serverPath: result["serverPath"],
            ));

            yield UploadDocumentState.onDocumentAdded(
              isLoading: false,
              message: result[ApiStatusParams.Message.value],
              file: event.file,
              filePickerResponseModel: event.filePickerResponseModel,
              filePickerIntentModel: event.filePickerIntentModel,
            );
          }
          // failure case
          else {
            //for check user date updated or not
            yield UploadDocumentState.onDocumentAdded(
              isLoading: false,
              message: result["message"],
              filePickerResponseModel: event.filePickerResponseModel,
              file: event.file,
              filePickerIntentModel: event.filePickerIntentModel,
            );
          }
        } else {
          yield UploadDocumentState.onDocumentAdded(
            isLoading: false,
            message: AppLocalizations.of(event.context).common.error.somethingWentWrong,
            filePickerResponseModel: event.filePickerResponseModel,
            file: event.file,
            filePickerIntentModel: event.filePickerIntentModel,
          );
        }
      } else {
        event.filePickerResponseModel.fileList.add(FileModel(
          localPath: event.file.path,
          fileId: 0,
          serverPath: "",
        ));

        yield UploadDocumentState.onDocumentAdded(
          isLoading: false,
          message:AppLocalizations.of(event.context).common.text.fileAdded,
          file: event.file,
          filePickerResponseModel: event.filePickerResponseModel,
          filePickerIntentModel: event.filePickerIntentModel,
        );
      }
    }

    if (event is RemoveDocumentEvent) {
      if (event.filePickerIntentModel.uploadEnable == true) {
        String message = "";
        yield UploadDocumentState.onDocumentUpdate(
          isLoading: true,
          message: "",
          file: event.file,
          filePickerResponseModel: event.filePickerResponseModel,
          filePickerIntentModel: event.filePickerIntentModel,
        );

        num index = -1;
        for (var fileModel in event.filePickerResponseModel.fileList) {
          if (fileModel.localPath == event.file.path) {
            break;
          } else {
            index++;
          }
        }

        // call api
        var result = await RemoveDocumentApi().removeDocumentApiCall(
          context: event.context,
          fileId: event.filePickerResponseModel.fileList[index].fileId,
          //event.filePickerResponseModel?.userData?.uploadedDriverDocuments[0],
          endPoint: event.filePickerIntentModel.endPointUpload,
        );

        if (result != null) {
          // check result status
          if (result[ApiStatusParams.Status.value]!=null && result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
            event.filePickerResponseModel.fileList.removeAt(index);

            yield UploadDocumentState.onDocumentAdded(
              isLoading: false,
              message: result[ApiStatusParams.Message.value],
              file: event.file,
              filePickerResponseModel: event.filePickerResponseModel,
              filePickerIntentModel: event.filePickerIntentModel,
            );
          }
          // failure case
          else {
            yield UploadDocumentState.onDocumentAdded(
              isLoading: false,
              message: result["message"],
              filePickerResponseModel: event.filePickerResponseModel,
              file: event.file,
              filePickerIntentModel: event.filePickerIntentModel,
            );
          }
        } else {
          yield UploadDocumentState.onDocumentAdded(
            isLoading: false,
            message: AppLocalizations.of(event.context).common.error.somethingWentWrong,
            filePickerResponseModel: event.filePickerResponseModel,
            file: event.file,
            filePickerIntentModel: event.filePickerIntentModel,
          );
        }
      } else {
        num index = -1;
        for (var fileModel in event.filePickerResponseModel.fileList) {
          if (fileModel.localPath == event.file.path) {
            index++;
            break;
          } else {
            index++;
          }
        }
        event.filePickerResponseModel.fileList.removeAt(index);

        yield UploadDocumentState.onDocumentAdded(
          isLoading: false,
          message: AppLocalizations.of(event.context).common.text.imageRemoved,
          file: event.file,
          filePickerResponseModel: event.filePickerResponseModel,
          filePickerIntentModel: event.filePickerIntentModel,
        );
      }
    }
  }
}
