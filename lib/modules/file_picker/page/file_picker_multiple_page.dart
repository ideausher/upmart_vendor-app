import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:upmart_driver/localizations.dart';
import 'package:upmart_driver/modules/common/common_widget/async_call_parent_widget.dart';
import '../../../modules/common/utils/common_utils.dart';
import '../../../modules/common/app_config/app_config.dart';
import '../../../modules/common/utils/dialog_snackbar_utils.dart';
import '../../../modules/common/utils/navigator_utils.dart';
import '../../../modules/common/utils/network_connectivity_utils.dart';
import '../../../modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import '../../../modules/common/constants/color_constants.dart';
import '../../../modules/common/constants/dimens_constants.dart';
import '../../../modules/common/theme/app_themes.dart';
import '../../../modules/file_picker/bloc/upload_document_bloc.dart';
import '../../../modules/file_picker/bloc/upload_document_event.dart';
import '../../../modules/file_picker/bloc/upload_document_state.dart';
import '../../../modules/file_picker/constants/file_picker_images_constants.dart';
import '../../../modules/file_picker/enums/file_picker_enums.dart';
import '../../../modules/file_picker/manager/manager.dart';
import '../../../modules/file_picker/model/file_picker_model.dart';
import '../../../modules/file_picker/model/file_picker_response_model.dart';

class FilePickerMultiplePage extends StatefulWidget {
  BuildContext context;

  FilePickerMultiplePage(this.context);

  @override
  _FilePickerMultiplePageState createState() => _FilePickerMultiplePageState();
}

class _FilePickerMultiplePageState extends State<FilePickerMultiplePage> {
  //Declaration of image picker variables

  bool _cameraVisibility;
  bool _galleryVisibility;
  FilePickerModel filePickerModel;

//  Declaration of scaffold key
  final _scaffoldUploadDocumentKey = GlobalKey<ScaffoldState>();

  UploadDocumentBloc _uploadDocumentBloc; // bloc
  UploadDocumentState _uploadDocumentState; // state

  double _imageSize;
  BuildContext _context;

  @override
  void initState() {
    filePickerModel = ModalRoute.of(widget.context).settings.arguments;

    _uploadDocumentBloc = UploadDocumentBloc(
        filePickerIntentModel: filePickerModel,
        filePickerResponseModel: FilePickerResponseModel());

    _cameraVisibility =
        ((filePickerModel.pickFrom == FilePickFromEnum.PickFromBoth ||
                filePickerModel.pickFrom == FilePickFromEnum.PickFromCamera) &&
            filePickerModel.pickerType != FilePickerTypeEnum.PickerTypeDoc);

    _galleryVisibility =
        filePickerModel.pickFrom == FilePickFromEnum.PickFromBoth ||
            filePickerModel.pickFrom == FilePickFromEnum.PickFromGallery;

    super.initState();
  }

  @override
  void dispose() {
    _uploadDocumentBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    _imageSize = MediaQuery.of(context).size.width / 4;
    return BlocEventStateBuilder<UploadDocumentState>(
      bloc: _uploadDocumentBloc,
      builder: (BuildContext context, UploadDocumentState state) {
        if (state != null && _uploadDocumentState != state) {
          _uploadDocumentState = state;
          if (_uploadDocumentState?.message?.toString()?.trim()?.isNotEmpty ==
              true) {
            print("bb" + _uploadDocumentState?.message);
            WidgetsBinding.instance.addPostFrameCallback((_) {
              DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                  backgroundColor: AppConfig.of(context).themeData.accentColor,
                  context: context,
                  scaffoldState: _scaffoldUploadDocumentKey.currentState,
                  message: _uploadDocumentState?.message);
            });
          }
        }
        return ModalProgressHUD(
          inAsyncCall: _uploadDocumentState.isLoading,
          child:
              _getMainChild(context, _uploadDocumentState, _uploadDocumentBloc),
        );
      },
    );
  }

  Widget _getMainChild(
      BuildContext context,
      UploadDocumentState _uploadDocumentState,
      UploadDocumentBloc _uploadDocumentBloc) {
    return Scaffold(
      key: _scaffoldUploadDocumentKey,
      backgroundColor: Colors.grey,
      appBar: _getAppBarWidget(context),
      body: WillPopScope(
        onWillPop: () {
          return NavigatorUtils.navigatorUtilsInstance
              .navigatorPopScreen(context, dataToBeSend: null);
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Expanded(
              child: (_uploadDocumentState
                          ?.filePickerResponseModel?.fileList?.isNotEmpty ==
                      true)
                  ? _getGridWidget(
                      context, _uploadDocumentState, _uploadDocumentBloc)
                  : _getNoDataWidget(_uploadDocumentState),
            ),
            Container(
              padding: EdgeInsets.all(SIZE_10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(SIZE_10),
                  topRight: Radius.circular(SIZE_10),
                ),
                color: Colors.white,
              ),
              child: Row(
                children: <Widget>[
                  Visibility(
                    visible: _cameraVisibility,
                    child: Expanded(
                      child: showSelectGalleryButtonView(
                        buttonText: AppLocalizations.of(_context).filePicker.text.useCamera,
                        iconData: Icons.camera_alt,
                        radius: SIZE_10,
                        onPressed: () {
                          if ((_uploadDocumentState?.filePickerResponseModel
                                      ?.fileList?.length ??
                                  0) <
                              _uploadDocumentState
                                  ?.filePickerIntentModel?.maxFiles) {
                            _imageSelectorCamera(context, _uploadDocumentBloc,
                                _uploadDocumentState);
                          } else {
                            DialogSnackBarUtils.dialogSnackBarUtilsInstance
                                .showSnackbar(
                                    context: context,
                                    backgroundColor: AppConfig.of(context)
                                        .themeData
                                        .accentColor,
                                    scaffoldState:
                                        _scaffoldUploadDocumentKey.currentState,
                                    message:
                                    AppLocalizations.of(_context).filePicker.text.maxFiles /* AppLocalizations
                                    .of(context)
                                    .filepicker
                                    .error
                                    .maxSelect(value: "${_uploadDocumentState.filePickerIntentModel.maxFiles}")*/
                                    );
                          }
                        },
                        backgroundColor: COLOR_PRIMARY,
                        iconColor: Colors.white,
                      ),
                    ),
                  ),
                  Visibility(
                    visible: _cameraVisibility && _galleryVisibility,
                    child: SizedBox(
                      width: SIZE_10,
                    ),
                  ),
                  Visibility(
                    visible: _galleryVisibility,
                    child: Expanded(
                      child: showSelectGalleryButtonView(
                          buttonText: AppLocalizations.of(_context).filePicker.text.useGallery ,
                          iconData: Icons.perm_media,
                          radius: SIZE_10,
                          backgroundColor: COLOR_PRIMARY,
                          iconColor: Colors.white,
                          onPressed: () {
                            if ((_uploadDocumentState?.filePickerResponseModel
                                        ?.fileList?.length ??
                                    0) <
                                _uploadDocumentState
                                    ?.filePickerIntentModel?.maxFiles) {
                              if (_uploadDocumentState
                                      ?.filePickerIntentModel?.pickerType ==
                                  FilePickerTypeEnum.PickerTypeImage) {
                                _imageSelectorGallery(context,
                                    _uploadDocumentBloc, _uploadDocumentState);
                              } else {
                                selectFileFromGallery(context,
                                    _uploadDocumentBloc, _uploadDocumentState);
                              }
                            } else {
                              DialogSnackBarUtils.dialogSnackBarUtilsInstance
                                  .showSnackbar(
                                      backgroundColor: AppConfig.of(context)
                                          .themeData
                                          .accentColor,
                                      context: context,
                                      scaffoldState: _scaffoldUploadDocumentKey
                                          .currentState,
                                      message:
                                      AppLocalizations.of(_context).filePicker.text.maxFiles  /*AppLocalizations
                                      .of(context)
                                      .filepicker
                                      .error
                                      .maxSelect(value: "${_uploadDocumentState.filePickerIntentModel.maxFiles}")*/
                                      );
                            }
                          }),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  //Select file from gallery and upload
  selectFileFromGallery(
    BuildContext context,
    UploadDocumentBloc _uploadDocumentBloc,
    UploadDocumentState _uploadDocumentState,
  ) async {
    var types;
    if (_uploadDocumentState?.filePickerIntentModel?.docTypes?.isNotEmpty ==
        true) {
      types = _uploadDocumentState.filePickerIntentModel.docTypes;
    } else {
      types = List<String>();
      types.add(DocTypeEnum.Pdf.value);
      types.add(DocTypeEnum.Doc.value);
      types.add(DocTypeEnum.Jpg.value);
      types.add(DocTypeEnum.Jpeg.value);
      types.add(DocTypeEnum.Png.value);
    }

    File galleryFile = await FilePicker.getFile(
        type: FileType.custom, allowedExtensions: types);
    if (galleryFile != null) {
      if (_uploadDocumentState?.filePickerIntentModel?.uploadEnable == true) {
        NetworkConnectionUtils.networkConnectionUtilsInstance
            .getConnectivityStatus(context, showNetworkDialog: true)
            .then((onValue) {
          if (onValue) {
            _uploadDocumentState?.file = galleryFile;
            _uploadDocumentBloc.emitEvent(AddFileEvent(
                isLoading: true,
                context: context,
                file: _uploadDocumentState?.file,
                filePickerResponseModel:
                    _uploadDocumentState?.filePickerResponseModel,
                filePickerIntentModel:
                    _uploadDocumentState?.filePickerIntentModel));
          }
        });
      } else {
        _uploadDocumentState?.file = galleryFile;
        _uploadDocumentBloc.emitEvent(AddFileEvent(
            isLoading: true,
            context: context,
            file: _uploadDocumentState?.file,
            filePickerResponseModel:
                _uploadDocumentState?.filePickerResponseModel,
            filePickerIntentModel:
                _uploadDocumentState?.filePickerIntentModel));
      }
    }
  }

  //display image selected from gallery
  _imageSelectorGallery(
    BuildContext context,
    UploadDocumentBloc _uploadDocumentBloc,
    UploadDocumentState _uploadDocumentState,
  ) async {
    File galleryImage = await ImagePicker.pickImage(
      source: ImageSource.gallery,
    );
    print("You selected gallery image : " + galleryImage.path);
    _uploadDocumentState?.file = galleryImage;

    _cropImage(context, galleryImage, _uploadDocumentState);
  }

  //display image selected from camera
  _imageSelectorCamera(
      BuildContext context,
      UploadDocumentBloc _uploadDocumentBloc,
      UploadDocumentState _uploadDocumentState) async {
    File cameraImage = await ImagePicker.pickImage(
      source: ImageSource.camera,
    );

    _cropImage(context, cameraImage, _uploadDocumentState);
    print("You selected camera image : " + cameraImage.path);
  }

  //set custom image in case of pdf and jpg
  _getCustomImageView({bool isPdf}) {
    return Container(
      padding: EdgeInsets.all(SIZE_20),
      color: Colors.white,
      child: Image.asset(
        isPdf ? PDF_PREVIEW : DOC_PREVIEW,
        color: isPdf ? null : Colors.white,
      ),
    );
  }

  AppBar _getAppBarWidget(BuildContext context) {
    return CommonUtils.commonUtilsInstance.getAppBar(
        context: context,
        backGroundColor: Colors.grey,
        elevation: ELEVATION_0,
        actionWidgets: <Widget>[
          InkWell(
            onTap: () {
              _callDoneUploadingDialog(context, _uploadDocumentState, () {
                NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(
                    context,
                    dataToBeSend: _uploadDocumentState.filePickerResponseModel);
              });
            },
            child: Center(
                child: Padding(
              padding: const EdgeInsets.only(right: SIZE_20),
              child: Text(
                AppLocalizations.of(_context).filePicker.text.done,
              ),
            )),
          )
        ]);
  }

  Widget _getGridWidget(
      BuildContext context,
      UploadDocumentState _uploadDocumentState,
      UploadDocumentBloc _uploadDocumentBloc) {
    return GridView.count(
      crossAxisCount: 2,
      children: new List<Widget>.generate(
          _uploadDocumentState?.filePickerResponseModel?.fileList?.length,
          (index) {
        return Stack(
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(SIZE_10),
              child: Card(
                  elevation: ELEVATION_10,
                  child: Padding(
                    padding: const EdgeInsets.all(SIZE_5),
                    child: Center(
                      child: (_uploadDocumentState?.filePickerResponseModel
                                      ?.fileList[index]?.localPath
                                      ?.toLowerCase() ??
                                  "")
                              .endsWith(".pdf")
                          ? _getCustomImageView(isPdf: true)
                          : (((_uploadDocumentState?.filePickerResponseModel
                                              ?.fileList[index]?.localPath
                                              ?.toLowerCase() ??
                                          "")
                                      .endsWith(".doc")) ||
                                  ((_uploadDocumentState
                                              ?.filePickerResponseModel
                                              ?.fileList[index]
                                              ?.localPath
                                              ?.toLowerCase() ??
                                          "")
                                      .endsWith(".docx")))
                              ? _getCustomImageView(isPdf: false)
                              : (_uploadDocumentState
                                          ?.filePickerResponseModel
                                          ?.fileList[index]
                                          ?.serverPath
                                          ?.isNotEmpty ==
                                      true)
                                  ? CachedNetworkImage(
                                      height: double.infinity,
                                      width: double.infinity,
                                      imageUrl: _uploadDocumentState
                                              ?.filePickerResponseModel
                                              ?.fileList[index]
                                              ?.serverPath ??
                                          "",
                                      fit: BoxFit.cover,
                                      placeholder: (context, url) =>
                                          Image.asset(
                                        _uploadDocumentState
                                                ?.filePickerResponseModel
                                                ?.fileList[index]
                                                ?.localPath ??
                                            NO_PREVIEW,
                                      ),
                                      errorWidget: (context, url, error) =>
                                          Image.asset(
                                        _uploadDocumentState
                                                ?.filePickerResponseModel
                                                ?.fileList[index]
                                                ?.localPath ??
                                            NO_PREVIEW,
                                      ),
                                    )
                                  : Image.file(
                                      File(_uploadDocumentState
                                          ?.filePickerResponseModel
                                          ?.fileList[index]
                                          ?.localPath),
                                      height: double.infinity,
                                      width: double.infinity,
                                      fit: BoxFit.cover,
                                    ),
                    ),
                  )),
            ),
            Positioned(
              top: SIZE_7,
              right: SIZE_7,
              child: InkWell(
                highlightColor: Colors.transparent,
                splashColor: Colors.transparent,
                onTap: () {
                  print("tapped");
                  NetworkConnectionUtils.networkConnectionUtilsInstance
                      .getConnectivityStatus(context, showNetworkDialog: true)
                      .then((onValue) {
                    _uploadDocumentBloc.emitEvent(RemoveDocumentEvent(
                      isLoading: true,
                      context: context,
                      file: File(_uploadDocumentState
                          ?.filePickerResponseModel?.fileList[index].localPath),
                      filePickerResponseModel:
                          _uploadDocumentState?.filePickerResponseModel,
                      filePickerIntentModel:
                          _uploadDocumentState?.filePickerIntentModel,
                    ));
                  });
                },
                child: CircleAvatar(
                  radius: CommonUtils.commonUtilsInstance
                      .getPercentageSize(context: _context, percentage: SIZE_3),
                  backgroundColor: COLOR_PRIMARY,
                  child: Icon(
                    Icons.close,
                    color: Colors.white,
                    size: CommonUtils.commonUtilsInstance.getPercentageSize(
                        context: _context, percentage: SIZE_4),
                  ),
                ),
              ),
            ),
          ],
        );
      }),
    );
  }

  Widget _getNoDataWidget(UploadDocumentState _uploadDocumentState) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Image.asset(
          UPLOAD_DOCUMENT_ICON,
          height: _imageSize,
          width: _imageSize,
          fit: BoxFit.contain,
        ),
        SizedBox(
          height: SIZE_20,
        ),
        Text(
          (_uploadDocumentState?.filePickerIntentModel?.pickerType ==
                  FilePickerTypeEnum.PickerTypeDoc)
              ? AppLocalizations.of(_context).filePicker.text.uploadDocument
              : AppLocalizations.of(_context).filePicker.text.uploadImage,
          style: appTheme.textTheme.headline5,
        ),
      ],
    );
  }

  //method to call logout API
  void _callDoneUploadingDialog(BuildContext context,
      UploadDocumentState _uploadDocumentState, Function callBack) {
    if (_uploadDocumentState?.filePickerIntentModel?.uploadEnable == false) {
      callBack();
      return;
    }
    DialogSnackBarUtils.dialogSnackBarUtilsInstance.showAlertDialog(
        barrierDismissible: true,
        context: context,
        positiveButton:
        AppLocalizations.of(_context).common.text.ok /*AppLocalizations
            .of(context)
            .common
            .text
            .ok*/
        ,
        //AppLocalizations.of(context).common.text.ok,
        title: (_uploadDocumentState
                    ?.filePickerResponseModel?.fileList?.isNotEmpty ==
                true)
            ? AppLocalizations.of(_context).filePicker.text.uploadSuccessfully
            : AppLocalizations.of(_context).filePicker.text.error,
        onPositiveButtonTab: () {
          if (_uploadDocumentState
                  ?.filePickerResponseModel?.fileList?.isNotEmpty ==
              true) {
            // _callLogout(context: context, state: _uploadDocumentState);
            NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
            callBack();
          } else {
            NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
            callBack();
          }
        },
        subTitle: (_uploadDocumentState
                    ?.filePickerResponseModel?.fileList?.isNotEmpty ==
                true)
            ? ""
            : '${(_uploadDocumentState?.filePickerIntentModel?.uploadEnable == true) ? AppLocalizations.of(_context).filePicker.text.upload: AppLocalizations.of(_context).filePicker.text.select} ${(_uploadDocumentState?.filePickerIntentModel?.pickerType == FilePickerTypeEnum.PickerTypeDoc) ? AppLocalizations.of(_context).filePicker.text.document : AppLocalizations.of(_context).filePicker.text.image /*AppLocalizations
            .of(context)
            .filepicker
            .text
            .image*/
            }',
        centerImage: "");
  }

  _cropImage(BuildContext context, File file,
      UploadDocumentState _uploadDocumentState) async {
    File croppedFile =
        (_uploadDocumentState?.filePickerIntentModel?.cropEnable == true)
            ? await ImageCropper.cropImage(
                sourcePath: file.path,
                maxWidth: 512,
                maxHeight: 512,
              )
            : file;

    if (_uploadDocumentState?.filePickerIntentModel?.uploadEnable == true) {
      // check upload == true or not
      NetworkConnectionUtils.networkConnectionUtilsInstance
          .getConnectivityStatus(context, showNetworkDialog: true)
          .then((onValue) {
        if (onValue) {
          _uploadDocumentState?.file = croppedFile;
          _uploadDocumentBloc.emitEvent(AddFileEvent(
            isLoading: true,
            context: context,
            file: _uploadDocumentState?.file,
            filePickerResponseModel:
                _uploadDocumentState?.filePickerResponseModel,
            filePickerIntentModel: _uploadDocumentState?.filePickerIntentModel,
          ));
        }
      });
    } else {
      _uploadDocumentState?.file = croppedFile;
      _uploadDocumentBloc.emitEvent(AddFileEvent(
        isLoading: true,
        context: context,
        file: _uploadDocumentState?.file,
        filePickerResponseModel: _uploadDocumentState?.filePickerResponseModel,
        filePickerIntentModel: _uploadDocumentState?.filePickerIntentModel,
      ));
    }
  }
}
