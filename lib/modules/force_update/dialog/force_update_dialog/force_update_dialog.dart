import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/common/constants/color_constants.dart';
import 'package:upmart_driver/modules/common/constants/dimens_constants.dart';
import 'package:upmart_driver/modules/common/constants/font_constant.dart';
import 'package:upmart_driver/modules/common/theme/app_themes.dart';
import 'package:upmart_driver/modules/common/utils/navigator_utils.dart';

import 'package:url_launcher/url_launcher.dart';

class ForceUpdateDialog extends StatefulWidget {
  BuildContext context;
  String packageName;
  String versionNumber;
  String buildNumber;
  bool forcefullyUpdate;
  String message;
  String url;

  ForceUpdateDialog({
    @required this.url,
    this.context,
    this.packageName,
    this.buildNumber,
    this.versionNumber,
    this.forcefullyUpdate = false,
    this.message,
  });

  @override
  _ForceUpdateDialogState createState() => _ForceUpdateDialogState();
}

class _ForceUpdateDialogState extends State<ForceUpdateDialog> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {},
      child: Align(
        alignment: Alignment.center,
        child: Container(
          height: (widget.forcefullyUpdate == false) ? 240 : 200,
          padding: const EdgeInsets.all(SIZE_20),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              // used for the cross button
              Visibility(
                visible: widget.forcefullyUpdate == false,
                child: Align(
                  alignment: Alignment.topRight,
                  child: GestureDetector(
                    child: Icon(
                      Icons.cancel,
                      size: SIZE_30,
                      color: COLOR_PRIMARY,
                    ),
                    onTap: () {
                      NavigatorUtils.navigatorUtilsInstance
                          .navigatorPopScreen(context);
                    },
                  ),
                ),
              ),

              SizedBox(
                height: SIZE_20,
              ),
              Text(
                (widget.message?.toString()?.trim()?.isNotEmpty == true)
                    ? widget.message?.toString()?.trim()
                    : "You have a new update of Drive App, please update the app.",
                maxLines: 3,
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    color: Colors.grey,
                    fontSize: SIZE_18,
                    fontFamily: FONT_FAMILY_INTER,
                    fontWeight: FontWeight.normal,
                    decoration: TextDecoration.none),
              ),

              SizedBox(
                height: SIZE_20,
              ),

              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: (widget.forcefullyUpdate == false)
                    ? MainAxisAlignment.spaceAround
                    : MainAxisAlignment.center,
                children: [
                  Visibility(
                    visible: widget.forcefullyUpdate == false,
                    child: _getButton(
                      context: context,
                      btnText: "Cancel",
                      onTap: () {
                        NavigatorUtils.navigatorUtilsInstance
                            .navigatorPopScreen(context);
                      },
                      fontColor: Colors.red,
                    ),
                  ),
                  _getButton(
                    context: context,
                    btnText: (widget.forcefullyUpdate == false)
                        ? "Update"
                        : "Force Update",
                    onTap: () async {
                      if (await canLaunch(widget.url)) {
                        return await launch(
                          widget.url,
                          forceSafariVC: false,
                          forceWebView: false,
                        );
                      } else {
                        return false;
                      }
                    },
                    fontColor: Colors.red,
                  ),
                ],
              ),
            ],
          ),
          margin: EdgeInsets.all(SIZE_20),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(SIZE_20),
          ),
        ),
      ),
    );
  }

  // used to get the button view
  Widget _getButton(
      {String btnText, BuildContext context, Function onTap, Color fontColor}) {
    return RaisedButton(
      padding: EdgeInsets.all(SIZE_10),
      child: Text(
        btnText,
        style: textStyleSize16WithWhiteColor,
      ),
      onPressed: onTap,
    );
  }
}
