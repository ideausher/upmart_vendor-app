

import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/auth/constants/image_constant.dart';
import 'package:upmart_driver/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';

import 'package:upmart_driver/modules/common/app_config/app_config.dart';
import 'package:upmart_driver/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:upmart_driver/modules/common/common_widget/common_image_with_text.dart';
import 'package:upmart_driver/modules/common/constants/color_constants.dart';
import 'package:upmart_driver/modules/common/constants/dimens_constants.dart';
import 'package:upmart_driver/modules/common/theme/app_themes.dart';
import 'package:upmart_driver/modules/common/utils/common_utils.dart';
import 'package:upmart_driver/modules/common/utils/date_utils.dart';
import 'package:upmart_driver/modules/common/utils/firebase_messaging_utils.dart';
import 'package:upmart_driver/modules/common/utils/image_utils.dart';
import 'package:upmart_driver/modules/common/utils/navigator_utils.dart';
import 'package:upmart_driver/modules/help_and_support/queries/api/model/query_list_response_model.dart';
import 'package:upmart_driver/modules/help_and_support/queries/bloc/queries_listing_state.dart';
import 'package:upmart_driver/modules/help_and_support/queries/manager/queries_listing_action_managers.dart';
import 'package:upmart_driver/modules/orders/api/model/order_listing_response_model.dart';

import '../../../../localizations.dart';
import '../../../../routes.dart';

class QueriesListingPage extends StatefulWidget {
  BuildContext context;

  QueriesListingPage(this.context);

  @override
  _QueriesListingPageState createState() => _QueriesListingPageState();
}

class _QueriesListingPageState extends State<QueriesListingPage>
    implements PushReceived {
  //Bloc and state variable declaration
  QueriesListingManager _userAllQueriesActionManager = QueriesListingManager();

  //Declaration of scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  // initialise controllers
  ScrollController _scrollController;
  Order _order;

  @override
  void initState() {
    super.initState();
    if (ModalRoute.of(widget.context)?.settings?.arguments != null &&
        ModalRoute.of(widget.context)?.settings?.arguments is Order)
      _order = ModalRoute.of(widget.context).settings.arguments;
    _userAllQueriesActionManager.context = widget.context;
    _userAllQueriesActionManager.actionOnInit(order: _order);
    _scrollController = ScrollController();
    FirebaseMessagingUtils.firebaseMessagingUtils
        .addCallback(pushReceived: this);
    _scrollController.addListener(_scrollListener);
  }

  @override
  void dispose() {
    super.dispose();
    _userAllQueriesActionManager?.userAllQueriesBloc?.dispose();
    FirebaseMessagingUtils.firebaseMessagingUtils
        .removeCallback(pushReceived: this);
  }

  @override
  Widget build(BuildContext context) {
    _userAllQueriesActionManager.context = context;
    return Scaffold(
        appBar: _showAppBar(),
        key: _scaffoldKey,
        body: BlocEventStateBuilder<QueriesListingState>(
          bloc: _userAllQueriesActionManager?.userAllQueriesBloc,
          builder:
              (BuildContext context, QueriesListingState userAllQueriesState) {
            _userAllQueriesActionManager.context = context;
            if (userAllQueriesState != null &&
                _userAllQueriesActionManager.userAllQueriesState !=
                    userAllQueriesState) {
              _userAllQueriesActionManager.userAllQueriesState =
                  userAllQueriesState;
              _userAllQueriesActionManager
                  ?.actionTocallOnHelpAndSupportStateChange(
                currentState: _scaffoldKey?.currentState,
              );
            }
            print("${userAllQueriesState?.isLoading}");
            return ModalProgressHUD(
              inAsyncCall: userAllQueriesState?.isLoading ?? false,
              child: Padding(
                padding: const EdgeInsets.all(SIZE_12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Visibility(
                        visible: _userAllQueriesActionManager
                            ?.userAllQueriesState?.listQuery?.isNotEmpty ==
                            true,
                        child: _showRecentOrderTitleText()),
                    _showPreviousTicketsList(),
                    // used for the start new query
                    _showRaiseNewTicketList()
                  ],
                ),
              ),
            );
          },
        ));
  }

  //this method is used to return the show app bar
  Widget _showAppBar() {
    return CommonUtils.commonUtilsInstance.getAppBar(
        context: _userAllQueriesActionManager?.context,
        elevation: ELEVATION_0,
        defaultLeadIconPressed: () {
          NavigatorUtils.navigatorUtilsInstance
              .navigatorPopScreen(_userAllQueriesActionManager?.context);
        },
        popScreenOnTapOfLeadingIcon: false,
        centerTitle: true,
        appBarTitle: AppLocalizations.of(context)
            .helpAndSupport
            .appbar
            .title,
        defaultLeadingIcon: Icons.arrow_back,
        defaultLeadingIconColor: Colors.black,
        appBarTitleStyle: AppConfig.of(
          _userAllQueriesActionManager?.context,
        ).themeData.primaryTextTheme.headline3,
        backGroundColor: Colors.transparent);
  }

  Widget _showPreviousTicketsList() {
    return Expanded(
      child: (_userAllQueriesActionManager
          ?.userAllQueriesState?.listQuery?.isNotEmpty ==
          true)
          ? ListView.builder(
          key: PageStorageKey("list"),
          controller: _scrollController,
          itemCount: _userAllQueriesActionManager
              ?.userAllQueriesState?.listQuery?.length,
          shrinkWrap: true,
          itemBuilder: (context, index) {
            Query _query = _userAllQueriesActionManager
                ?.userAllQueriesState?.listQuery[index];
            return Card(
              elevation: ELEVATION_05,
              margin: EdgeInsets.all(SIZE_10),
              child: Padding(
                padding: const EdgeInsets.all(SIZE_10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    InkWell(
                      onTap: () {
                        _userAllQueriesActionManager
                            ?.actionToOpenDetailPage(order: _query?.order);
                      },
                      child: Wrap(
                        spacing: SIZE_10,
                        runSpacing: SIZE_10,
                        children: <Widget>[
                          _showTicketDetails(
                              ticketTitle: AppLocalizations.of(context)
                                  .helpAndSupport
                                  .text
                                  .ticketID,
                              ticketValue:
                              _query?.ticketId.toString() ?? "",
                              ticketTitleTextStyle:
                              textStyleSize14BlackColor,
                              ticketValuesTextStyle:
                              textStyleSize14WithGreyColor),
                          _showTicketDetails(
                              ticketTitle: AppLocalizations.of(context)
                                  .helpAndSupport
                                  .text
                                  .issueRaisedOn,
                              ticketValue:
                              (_query?.createdAt?.isNotEmpty == true)
                                  ? DateUtils.dateUtilsInstance
                                  .getDateAndTimeFormat(
                                  dateTime: _query?.createdAt)
                                  : "",
                              ticketTitleTextStyle:
                              textStyleSize14BlackColor,
                              ticketValuesTextStyle:
                              textStyleSize14WithGreyColor),
                          _showTicketDetails(
                              ticketTitle: AppLocalizations.of(context)
                                  .helpAndSupport
                                  .text
                                  .orderId,
                              ticketValue:
                              "#${_query?.order?.bookingCode?.toString() ?? "N/A"}",
                              ticketTitleTextStyle:
                              textStyleSize14BlackColor,
                              ticketValuesTextStyle:
                              textStyleSize14WithGreyColor),
                          _showTicketDetails(
                              ticketTitle: AppLocalizations.of(context)
                                  .helpAndSupport
                                  .text
                                  .orderDate,
                              ticketValue:
                              (_query?.order?.createdAt?.isNotEmpty ==
                                  true)
                                  ? DateUtils.dateUtilsInstance
                                  .getDateAndTimeFormat(
                                  dateTime:
                                  _query?.order?.createdAt)
                                  : "",
                              ticketTitleTextStyle:
                              textStyleSize14BlackColor,
                              ticketValuesTextStyle:
                              textStyleSize14WithGreyColor),
                          _showTicketDetails(
                              ticketTitle: AppLocalizations.of(context)
                                  .helpAndSupport
                                  .text
                                  .ticketStatus,
                              ticketValue: _query?.queryStatus == 0
                                  ? AppLocalizations.of(context)
                                  .helpAndSupport
                                  .text
                                  .pending
                                  : AppLocalizations.of(context)
                                  .helpAndSupport
                                  .text
                                  .closed,
                              ticketTitleTextStyle:
                              textStyleSize14BlackColor,
                              ticketValuesTextStyle:
                              _query?.queryStatus == 0
                                  ? textStyleSize15WithREDColor
                                  : textStyleSize15WithGreenColor),
                          _showTicketDetails(
                              ticketTitle: AppLocalizations.of(context)
                                  .helpAndSupport
                                  .text
                                  .query,
                              ticketValue: _query?.feedbackMessage ?? "",
                              isMessage: true,
                              ticketTitleTextStyle:
                              textStyleSize14BlackColor,
                              ticketValuesTextStyle:
                              textStyleSize12WithGrey),
                          Visibility(
                            visible:
                            _query?.adminReply?.trim()?.isNotEmpty ==
                                true,
                            child: _showTicketDetails(
                                ticketTitle: AppLocalizations.of(context)
                                    .helpAndSupport
                                    .text
                                    .response,
                                ticketValue: _query?.adminReply ?? "",
                                isMessage: true,
                                ticketTitleTextStyle:
                                textStyleSize14BlackColor,
                                ticketValuesTextStyle:
                                textStyleSize12WithGrey),
                          ),
                        ],
                      ),
                    ),
                    _showImages(_query)
                  ],
                ),
              ),
            );
          })
          : (_userAllQueriesActionManager?.userAllQueriesState?.isLoading ==
          false)
          ? Align(
        alignment: Alignment.center,
        child: CommonImageWithTextWidget(
          context: _userAllQueriesActionManager?.context,
          localImagePath: HELP_AND_SUPPORT,
          textToShow: AppLocalizations.of(context)
              .helpAndSupport
              .text
              .noQueriesFound,
        ),
      )
          : SizedBox(),
    );
  }

  //this method is used to show raise new ticket button
  Widget _showRaiseNewTicketList() {
    return Align(
      alignment: Alignment.center,
      child: Container(
        margin: EdgeInsets.only(top: SIZE_20),
        child: RaisedButton(
          color: Colors.white,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(SIZE_80),
              side: BorderSide(color: COLOR_BLUE_TEXT)),
          onPressed: () async {
            _userAllQueriesActionManager?.actionToRaiseQuery(order: _order);
          },
          child: Padding(
            padding: const EdgeInsets.all(SIZE_10),
            child: Text(
              AppLocalizations.of(context).helpAndSupport.button.raiseNewTicket,
              style: textStyleSize16WithBlueColor,
            ),
          ),
        ),
      ),
    );
  }

  //this method will return a widget will will show ticket info
  Widget _showTicketDetails({
    TextStyle ticketTitleTextStyle,
    TextStyle ticketValuesTextStyle,
    String ticketTitle,
    String ticketValue,
    bool isMessage = false,
  }) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        (isMessage == false)
            ? Text(
          ticketTitle,
          style: ticketTitleTextStyle != null
              ? ticketTitleTextStyle
              : AppConfig.of(_userAllQueriesActionManager.context)
              .themeData
              .primaryTextTheme
              .overline,
        )
            : Text(
          ticketTitle,
          style: ticketTitleTextStyle != null
              ? ticketTitleTextStyle
              : AppConfig.of(_userAllQueriesActionManager.context)
              .themeData
              .primaryTextTheme
              .overline,
        ),
        Expanded(
          child: Text(
            ticketValue ?? "",
            textAlign:
            (isMessage == true) ? TextAlign.justify : TextAlign.right,
            style: ticketValuesTextStyle != null
                ? ticketValuesTextStyle
                : textStyleSize15WithBlackColor,
          ),
        ),
      ],
    );
  }

  //method to show previous tickets title text
  Widget _showRecentOrderTitleText() {
    return Text(
      AppLocalizations.of(context).helpAndSupport.text.previousTicket,
      style: textStyleSize18WithBlueColor,
      textAlign: TextAlign.left,
    );
  }

  ///scroll listener
  void _scrollListener() {
    if (_scrollController.offset >=
        _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      _userAllQueriesActionManager.actionTogetUserQueriesList(
          order: _order,
          page:
          (_userAllQueriesActionManager?.userAllQueriesState?.page != null)
              ? _userAllQueriesActionManager.userAllQueriesState.page + 1
              : 1);
    }
    if (_scrollController.offset <=
        _scrollController.position.minScrollExtent &&
        !_scrollController.position.outOfRange) {}
  }

  @override
  onMessageReceived({NotificationPushModel notificationPushModel}) {
    _userAllQueriesActionManager.actionOnInit(order: _order);
  }

  Widget _showImages(Query query) {
    return (query?.images?.isNotEmpty == true)
        ? Container(
      height: CommonUtils?.commonUtilsInstance?.getPercentageSize(
          context: _userAllQueriesActionManager.context,
          ofWidth: false,
          percentage: SIZE_10),
      child: ListView.builder(
          itemCount: query?.images?.length,
          padding: EdgeInsets.all(SIZE_0),
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            return InkWell(
              onTap: () {
                ImageUtils.imageUtilsInstance.showFullScreenImage(
                    context: _userAllQueriesActionManager.context,
                    shape: BoxShape.rectangle,
                    image: query?.images[index]);
              },
              child: Padding(
                padding: const EdgeInsets.only(right: SIZE_10),
                child: ImageUtils.imageUtilsInstance
                    .showCacheNetworkImage(
                    context: context,
                    showProgressBarInPlaceHolder: true,
                    shape: BoxShape.rectangle,
                    url: query?.images[index] ?? "",
                    height: CommonUtils?.commonUtilsInstance
                        ?.getPercentageSize(
                        context:
                        _userAllQueriesActionManager.context,
                        ofWidth: false,
                        percentage: SIZE_10),
                    width: CommonUtils?.commonUtilsInstance
                        ?.getPercentageSize(
                        context:
                        _userAllQueriesActionManager.context,
                        ofWidth: false,
                        percentage: SIZE_10)),
              ),
            );
          }),
    )
        : const SizedBox();
  }
}
