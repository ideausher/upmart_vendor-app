// To parse this JSON data, do
//
//     final sendQueryRequestModel = sendQueryRequestModelFromJson(jsonString);

import 'dart:convert';

SendQueryRequestModel sendQueryRequestModelFromJson(String str) =>
    SendQueryRequestModel.fromJson(json.decode(str));

String sendQueryRequestModelToJson(SendQueryRequestModel data) =>
    json.encode(data.toJson());

class SendQueryRequestModel {
  SendQueryRequestModel({
    this.bookingId,
    this.feedbackTitle,
    this.feedbackDescription,
    this.images,
  });

  String bookingId;
  String feedbackTitle;
  String feedbackDescription;
  List<String> images;

  factory SendQueryRequestModel.fromJson(Map<String, dynamic> json) =>
      SendQueryRequestModel(
        bookingId: json["booking_id"] == null ? null : json["booking_id"],
        feedbackTitle:
            json["feedback_title"] == null ? null : json["feedback_title"],
        feedbackDescription: json["feedback_description"] == null
            ? null
            : json["feedback_description"],
        images: json["images"] == null
            ? null
            : List<String>.from(json["images"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "booking_id": bookingId == null ? null : bookingId,
        "feedback_title": feedbackTitle == null ? null : feedbackTitle,
        "feedback_description":
            feedbackDescription == null ? null : feedbackDescription,
        "images":
            images == null ? null : List<dynamic>.from(images.map((x) => x)),
      };
}
