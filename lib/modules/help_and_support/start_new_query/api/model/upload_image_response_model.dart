// To parse this JSON data, do
//
//     final editProfileResponseModel = editProfileResponseModelFromJson(jsonString);

import 'dart:convert';

UploadImageResponseModel editProfileResponseModelFromJson(String str) =>
    UploadImageResponseModel.fromMap(json.decode(str));

String editProfileResponseModelToJson(UploadImageResponseModel data) => json.encode(data.toJson());

class UploadImageResponseModel {
  UploadImageResponseModel({
    this.data,
    this.message,
    this.statusCode,
  });

  Data data;
  String message;
  int statusCode;

  factory UploadImageResponseModel.fromMap(Map<String, dynamic> json) => UploadImageResponseModel(
    data: json["data"] == null ? null : Data.fromMap(json["data"]),
    message: json["message"] == null ? null : json["message"],
    statusCode: json["status_code"] == null ? null : json["status_code"],
  );

  Map<String, dynamic> toJson() => {
    "data": data == null ? null : data.toJson(),
    "message": message == null ? null : message,
    "status_code": statusCode == null ? null : statusCode,
  };
}

class Data {
  Data({
    this.image,
    this.imagePath
  });

  String image;
  String imagePath;

  factory Data.fromMap(Map<String, dynamic> json) => Data(
    image: json["profile_picture"] == null ? null : json["profile_picture"],
    imagePath: json["image_path"] == null ? null : json["image_path"],
  );

  Map<String, dynamic> toJson() => {
    "profile_picture": image == null ? null : image,
    "image_path": imagePath == null ? null : imagePath,
  };
}
