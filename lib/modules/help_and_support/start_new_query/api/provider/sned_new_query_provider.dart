import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/common/app_config/app_config.dart';
import 'package:upmart_driver/modules/help_and_support/start_new_query/api/model/send_query_request_model.dart';


class SendNewQueryProvider {
  Future<dynamic> sendNewQueryApiCall({BuildContext context, SendQueryRequestModel sendQueryRequestModel}) async {
    var sendQuery = "support";

    var result =
    await AppConfig.of(context).baseApi.postRequest(sendQuery, context, data: sendQueryRequestModel?.toJson());

    return result;
  }
}
