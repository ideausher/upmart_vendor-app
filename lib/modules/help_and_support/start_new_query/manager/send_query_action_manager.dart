import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/auth/validator/auth_validator.dart';
import 'package:upmart_driver/modules/common/app_config/app_config.dart';
import 'package:upmart_driver/modules/common/enum/enums.dart';
import 'package:upmart_driver/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:upmart_driver/modules/common/utils/navigator_utils.dart';
import 'package:upmart_driver/modules/common/utils/network_connectivity_utils.dart';
import 'package:upmart_driver/modules/file_picker/dialog/single_file_picker_dialog.dart';
import 'package:upmart_driver/modules/file_picker/enums/file_picker_enums.dart';
import 'package:upmart_driver/modules/file_picker/model/file_picker_model.dart';
import 'package:upmart_driver/modules/help_and_support/start_new_query/bloc/send_query_bloc.dart';
import 'package:upmart_driver/modules/help_and_support/start_new_query/bloc/send_query_event.dart';
import 'package:upmart_driver/modules/help_and_support/start_new_query/bloc/send_query_state.dart';

import '../../../../localizations.dart';

class SendQueryActionManager {
  BuildContext context;
  SendQueryBloc sendQueryBloc ;
  SendQueryState sendQueryState;

  //method to call on home page State change method
  callOnSendQueryStateChange(
      {ScaffoldState currentState, TextEditingController queryController}) {
    WidgetsBinding.instance.addPostFrameCallback(
          (_) {
        if (sendQueryState?.isLoading == false) {
          if (sendQueryState?.message?.toString()?.trim()?.isNotEmpty == true) {
            DialogSnackBarUtils.dialogSnackBarUtilsInstance.showAlertDialog(
                context: context,
                title: "",
                subTitle: sendQueryState?.message,
                positiveButton: AppLocalizations.of(context).common.text.ok,
                onPositiveButtonTab: () {
                  NavigatorUtils.navigatorUtilsInstance
                      .navigatorPopScreen(context);
                  if (sendQueryState?.apiStatus == ApiStatus.Success) {
                    NavigatorUtils.navigatorUtilsInstance
                        .navigatorPopScreen(context);
                  }
                });
          }
        }
      },
    );
  }

  callSendQueryEvent(
      {ScaffoldState scaffoldState, TextEditingController messageController}) {
    String result = AuthValidator.authValidatorInstance.validateSendQuery(
        queryString: messageController?.text, context: context);
    // if no error
    if (result == null || result.isEmpty == true) {
      // connection check
      NetworkConnectionUtils.networkConnectionUtilsInstance
          .getConnectivityStatus(context, showNetworkDialog: true)
          .then((onValue) {
        if (onValue) {
          sendQueryBloc?.emitEvent(SubmitQueryEvent(
              isLoading: false,
              message: messageController?.text?.trim(),
              context: context,
              orderModel: sendQueryState?.orderModel,
              images: sendQueryState?.images));
        }
      });
    } else {
      // if error
      DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
          context: context, scaffoldState: scaffoldState, message: result);
    }
  }

  //Use to upload image
  void actionOnUploadImage({ScaffoldState currentState}) {
    SingleFilePickerDialog.singleFilePickerDialogInstance.showFilePickerDialog(
        filePickerModel: FilePickerModel(
          pickerType: FilePickerTypeEnum.PickerTypeImage,
          pickFrom: FilePickFromEnum.PickFromBoth,
        ),
        context: context,
        fileData: (value) {
          var _image = File(value);
          if (_image != null) {
            NetworkConnectionUtils.networkConnectionUtilsInstance
                .getConnectivityStatus(context, showNetworkDialog: true)
                .then((onValue) async {
              if (onValue) {
                sendQueryBloc?.emitEvent(UploadImageEvent(
                    context: context,
                    isLoading: true,
                    imageFile: _image,
                    orderModel: sendQueryState?.orderModel,
                    images: sendQueryState?.images?.isNotEmpty == true
                        ? sendQueryState?.images
                        : new List<String>()));
              }
            });
            // _imageFile = _image;
            // _callApi();
          }
        });
  }

  void actionOnDeleteImage({ScaffoldState currentState, int index}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) async {
      if (onValue) {
        sendQueryBloc?.emitEvent(DeleteImageEvent(
            context: context,
            isLoading: true,
            index: index,
            orderModel: sendQueryState?.orderModel,
            images: sendQueryState?.images));
      }
    });
  }

  //this method is used to show remove address dialog
  void openRemoveImageAlertDialog(
      {GlobalKey<ScaffoldState> key,
        BuildContext context,
        SendQueryActionManager sendQueryActionManager,
        int index}) {
    DialogSnackBarUtils.dialogSnackBarUtilsInstance.showAlertDialog(
        context: context,
        title: AppLocalizations.of(context).sendQuery.text.removeImage,
        negativeButton: AppLocalizations.of(context).common.text.no,
        positiveButton: AppLocalizations.of(context).common.text.yes,
        titleTextStyle: AppConfig.of(context).themeData.textTheme.headline4,
        buttonTextStyle: AppConfig.of(context).themeData.textTheme.headline4,
        subTitle: AppLocalizations.of(context).sendQuery.text.areYouSureYouWantToRemoveImage,
        onNegativeButtonTab: () {
          NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
        },
        onPositiveButtonTab: () async {
          NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
          sendQueryActionManager?.actionOnDeleteImage(
              currentState: key?.currentState, index: index);
        });
  }
}
