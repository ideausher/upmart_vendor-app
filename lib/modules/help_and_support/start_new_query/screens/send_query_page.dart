import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/auth/constants/image_constant.dart';
import 'package:upmart_driver/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:upmart_driver/modules/common/app_config/app_config.dart';
import 'package:upmart_driver/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:upmart_driver/modules/common/constants/color_constants.dart';
import 'package:upmart_driver/modules/common/constants/dimens_constants.dart';
import 'package:upmart_driver/modules/common/theme/app_themes.dart';
import 'package:upmart_driver/modules/common/utils/common_utils.dart';
import 'package:upmart_driver/modules/common/utils/image_utils.dart';
import 'package:upmart_driver/modules/help_and_support/start_new_query/bloc/send_query_bloc.dart';
import 'package:upmart_driver/modules/help_and_support/start_new_query/bloc/send_query_state.dart';
import 'package:upmart_driver/modules/help_and_support/start_new_query/manager/send_query_action_manager.dart';
import 'package:upmart_driver/modules/orders/api/model/order_listing_response_model.dart';

import '../../../../localizations.dart';

class SendQueryPage extends StatefulWidget {
  BuildContext context;

  SendQueryPage(this.context) {}

  @override
  _SendQueryPageState createState() => _SendQueryPageState();
}

class _SendQueryPageState extends State<SendQueryPage> {
  TextEditingController _queryController = new TextEditingController();
  TextEditingController _orderIdController = new TextEditingController();

  //managers
  SendQueryActionManager _sendQueryActionManager;

  Order _order;

  //Declaration of scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _order = ModalRoute.of(widget.context).settings.arguments;
    _orderIdController?.text = _order?.bookingCode?.toString() ?? "";
    _sendQueryActionManager = new SendQueryActionManager();
    _sendQueryActionManager.context = widget.context;
    _sendQueryActionManager.sendQueryBloc = new SendQueryBloc(order: _order);
  }

  @override
  void dispose() {
    super.dispose();
    _queryController.dispose();
    _sendQueryActionManager?.sendQueryBloc?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _sendQueryActionManager.context = context;
    return Scaffold(
        appBar: _showAppBar(),
        key: _scaffoldKey,
        body: BlocEventStateBuilder<SendQueryState>(
          bloc: _sendQueryActionManager?.sendQueryBloc,
          builder: (BuildContext context, SendQueryState sendQueryState) {
            _sendQueryActionManager.context = context;
            if (sendQueryState != null &&
                _sendQueryActionManager.sendQueryState != sendQueryState) {
              _sendQueryActionManager.sendQueryState = sendQueryState;
              _sendQueryActionManager?.callOnSendQueryStateChange(
                  currentState: _scaffoldKey?.currentState,
                  queryController: _queryController);
            }
            return ModalProgressHUD(
              inAsyncCall: sendQueryState?.isLoading ?? false,
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: SIZE_12, right: SIZE_12, top: SIZE_24),
                  child: Wrap(
                    spacing: SIZE_16,
                    runSpacing: SIZE_16,
                    children: <Widget>[
                      Text(
                        AppLocalizations.of(context)
                            .sendQuery
                            .text
                            .tellUsWhyDoYouNeedHelp,
                        style: textStyleSize14BlackColor,
                      ),
                      _showOrderId(),
                      _showDivider(),
                      _showCommentBox(),
                      _showAttachImage(),
                      _showSubmitButton()
                    ],
                  ),
                ),
              ),
            );
          },
        ));
  }

  //this method will return a app bar widget
  Widget _showAppBar() {
    return CommonUtils.commonUtilsInstance.getAppBar(
        context: _sendQueryActionManager.context,
        elevation: ELEVATION_0,
        appBarTitle: AppLocalizations.of(context).sendQuery.text.getInTouch,
        defaultLeadingIcon: Icons.arrow_back,
        defaultLeadingIconColor: COLOR_BLACK,
        appBarTitleStyle: AppConfig.of(_sendQueryActionManager.context)
            .themeData
            .primaryTextTheme
            .headline3,
        backGroundColor: Colors.transparent);
  }

  //this method is used to show the vehicle name
  Widget _showOrderId() {
    return _textFieldForm(
        controller: _orderIdController,
        isEnabled: false,
        context: _sendQueryActionManager.context,
        maxLines: 1);
  }

  //method to show divider
  Widget _showDivider() {
    return Divider(
      color: COLOR_BORDER_GREY,
    );
  }

  //method to show title text for entering the details of the issue user is having
  Widget _showRaiseQueryTitleText() {
    return Text(
      AppLocalizations.of(context).sendQuery.text.fillDetails,
      style: AppConfig.of(_sendQueryActionManager.context)
          .themeData
          .primaryTextTheme
          .overline,
    );
  }

  //this will show the commnt widget for typeing query
  Widget _showCommentBox() {
    return _textFieldForm(
        controller: _queryController,
        hint: AppLocalizations.of(context).sendQuery.text.description,
        isEnabled: true,
        context: _sendQueryActionManager.context,
        maxLines: 5);
  }

  // text field form
  Widget _textFieldForm(
      {TextEditingController controller,
      TextInputType keyboardType,
      String hint,
      FormFieldValidator<String> validator,
      int maxLines = 1,
      bool isEnabled = true,
      FocusNode currentFocusNode,
      FocusNode nextFocusNode,
      BuildContext context}) {
    return TextField(
      controller: controller,
      keyboardType: keyboardType,
      maxLines: maxLines,
      style: textStyleSize15WithLightGreyColor,
      focusNode: currentFocusNode,
      onSubmitted: (term) {
        currentFocusNode?.unfocus();
        if (nextFocusNode != null)
          FocusScope.of(context).requestFocus(nextFocusNode);
      },
      decoration: new InputDecoration(
        hintText: hint,
        filled: true,
        enabled: isEnabled,
        fillColor: Colors.white,
        contentPadding: EdgeInsets.all(SIZE_10),
        hintStyle: textStyleSize15WithLightGreyColor,
        enabledBorder: new OutlineInputBorder(
          borderRadius: const BorderRadius.all(
            const Radius.circular(SIZE_8),
          ),
          borderSide: new BorderSide(width: SIZE_0_5, color: COLOR_BORDER_GREY),
        ),
        disabledBorder: new OutlineInputBorder(
          borderRadius: const BorderRadius.all(
            const Radius.circular(SIZE_8),
          ),
          borderSide: new BorderSide(width: SIZE_0_5, color: COLOR_BORDER_GREY),
        ),
        focusedBorder: new OutlineInputBorder(
          borderRadius: const BorderRadius.all(
            const Radius.circular(SIZE_8),
          ),
          borderSide: new BorderSide(width: SIZE_0_5, color: COLOR_BORDER_GREY),
        ),
      ),
    );
  }

  //method to show submit button
  Widget _showSubmitButton() {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.only(bottom: SIZE_10),
      child: RaisedButton(
        color: COLOR_PRIMARY,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(SIZE_80),
            side: BorderSide(color: COLOR_PRIMARY)),
        onPressed: () {
          _sendQueryActionManager?.callSendQueryEvent(
              scaffoldState: _scaffoldKey?.currentState,
              messageController: _queryController);
        },
        child: Padding(
          padding: const EdgeInsets.all(SIZE_10),
          child: Text(
            AppLocalizations.of(context).sendQuery.button.submit,
            style: textStyleSize14WithWhiteColor,
          ),
        ),
      ),
    );
  }

  //Show attach image
  _showAttachImage() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(AppLocalizations.of(context).sendQuery.text.uploadPicture,
                style: textStyleSize14BlackColor),
            (_sendQueryActionManager?.sendQueryState?.images?.isNotEmpty ==
                    true)
                ? InkWell(
                    onTap: () {
                      _sendQueryActionManager?.actionOnUploadImage(
                          currentState: _scaffoldKey?.currentState);
                    },
                    child: Text(
                        "+${AppLocalizations.of(context).sendQuery.text.addImage}",
                        style: textStyleSize14BlackColor))
                : const SizedBox()
          ],
        ),
        SizedBox(
          height: SIZE_10,
        ),
        Visibility(
          visible: (_sendQueryActionManager?.sendQueryState?.images == null ||
                  _sendQueryActionManager?.sendQueryState?.images.isEmpty ==
                      true) ||
              (_sendQueryActionManager?.sendQueryState?.images?.isNotEmpty ==
                      true &&
                  _sendQueryActionManager?.sendQueryState?.images?.length < 4),
          child: InkWell(
            onTap: () {
              _sendQueryActionManager?.actionOnUploadImage(
                  currentState: _scaffoldKey?.currentState);
            },
            child: _showUploadImageView(),
          ),
        ),
        SizedBox(
          height: SIZE_10,
        ),
        Visibility(
          visible:
              _sendQueryActionManager?.sendQueryState?.images?.isNotEmpty ==
                  true,
          child: _getImagesList(),
        ),
      ],
    );
  }

  //Get Images List
  _getImagesList() {
    return Container(
      height: CommonUtils.commonUtilsInstance
          .getPercentageSize(context: context, percentage: SIZE_40),
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          padding: const EdgeInsets.all(SIZE_8),
          itemCount: _sendQueryActionManager?.sendQueryState?.images?.length,
          itemBuilder: (BuildContext context, int index) {
            return _getListItem(
                imageUrl:
                    _sendQueryActionManager?.sendQueryState?.images[index],
                index: index);
          }),
    );
  }

  //Use to get list item
  _getListItem({String imageUrl, int index}) {
    return Stack(
      children: <Widget>[
        InkWell(
            onTap: () {},
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(SIZE_10)),
                  border: Border.all(
                    width: 1.0,
                  )),
              height: CommonUtils.commonUtilsInstance
                  .getPercentageSize(context: context, percentage: SIZE_30),
              width: CommonUtils.commonUtilsInstance.getPercentageSize(
                  context: context, percentage: SIZE_30, ofWidth: true),
              margin: EdgeInsets.all(SIZE_8),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(SIZE_10),
                child: ImageUtils.imageUtilsInstance.showCacheNetworkImage(
                    url: imageUrl,
                    shape: BoxShape.rectangle,
                    context: context,
                    placeHolderImage: APP_ICON),
              ),
            )),
        Positioned(
          top: SIZE_1,
          right: SIZE_1,
          child: InkWell(
            onTap: () {},
            child: CircleAvatar(
              radius: CommonUtils.commonUtilsInstance
                  .getPercentageSize(context: context, percentage: SIZE_3),
              backgroundColor: COLOR_PRIMARY,
              child: InkWell(
                onTap: () {
                  _showRemoveImageAlert(index);
                },
                child: Icon(
                  Icons.delete,
                  color: Colors.white,
                  size: CommonUtils.commonUtilsInstance
                      .getPercentageSize(context: context, percentage: SIZE_4),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  //method to show upload image view
  Widget _showUploadImageView() {
    return (_sendQueryActionManager?.sendQueryState?.images?.isNotEmpty == true)
        ? const SizedBox()
        : DottedBorder(
            dashPattern: [10, 4],
            strokeWidth: 1,
            color: COLOR_BORDER_GREY,
            strokeCap: StrokeCap.round,
            child: InkWell(
              child: Container(
                alignment: Alignment.center,
                height: CommonUtils.commonUtilsInstance.getPercentageSize(
                    context: context, percentage: SIZE_20, ofWidth: false),
                width: CommonUtils.commonUtilsInstance.getPercentageSize(
                    context: context, percentage: SIZE_20, ofWidth: false),
                child: Image.asset(
                  UPLOAD_ICON,
                  height: SIZE_30,
                  width: SIZE_30,
                ),
              ),
            ),
          );
  }

  //this method is used for showing remove alert
  void _showRemoveImageAlert(int index) {
    _sendQueryActionManager?.openRemoveImageAlertDialog(
        key: _scaffoldKey,
        context: _sendQueryActionManager?.context,
        index: index,
        sendQueryActionManager: _sendQueryActionManager);
  }
}
