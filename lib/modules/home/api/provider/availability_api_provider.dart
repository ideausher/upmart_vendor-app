import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/common/app_config/app_config.dart';

class HomeApiProvider {
  Future<dynamic> changeDriverAvailability(
      {BuildContext context, int availability}) async {
    var getShops = "availability";

    var result = await AppConfig.of(context).baseApi.postRequest(
      getShops,
      context,
      data: {
        "availability": availability,
      } /*shopListRequestModelToJson(shopsListRequestModel)*/,
    );

    return result;
  }

}
