import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/auth/api/sign_in/model/auth_response_model.dart';
import 'package:upmart_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';

abstract class HomeEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final bool isAvailable;
  final AuthResponseModel authResponseModel;

  HomeEvent({
    this.isLoading: false,
    this.context,
    this.isAvailable: false,
    this.authResponseModel
  });
}

//this event is used for updating the home page UI data
class ChangeAvailabilityEvent extends HomeEvent {
  ChangeAvailabilityEvent({
    bool isLoading,
    BuildContext context,
    AuthResponseModel authResponseModel,
    bool isAvailable
  }) : super(
      isLoading: isLoading,
      context: context,
      isAvailable: isAvailable,
      authResponseModel: authResponseModel
  );
}



//this event is used for updating the home page UI data
class GetProfileEvent extends HomeEvent {
  GetProfileEvent({
    bool isLoading,
    BuildContext context,
    AuthResponseModel authResponseModel,
    bool isAvailable
  }) : super(
      isLoading: isLoading,
      context: context,
      isAvailable: isAvailable,
      authResponseModel: authResponseModel
  );
}


