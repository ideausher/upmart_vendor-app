import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/auth/auth_bloc/auth_bloc.dart';
import 'package:upmart_driver/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:upmart_driver/modules/common/utils/network_connectivity_utils.dart';
import 'package:upmart_driver/modules/force_update/manager/force_update_action_manager/force_update_action_manager.dart';
import 'package:upmart_driver/modules/home/bloc/home_bloc.dart';
import 'package:upmart_driver/modules/home/bloc/home_event.dart';
import 'package:upmart_driver/modules/home/bloc/home_state.dart';

class HomeActionManagers {
  BuildContext context;
  HomeState homeState;
  HomeBloc homeBloc = HomeBloc();
  AuthBloc authBloc;

  void actionOnInit() {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then(
      (onValue) {
        if (onValue) {
          homeBloc?.emitEvent(GetProfileEvent(
            isLoading: true,
            context: context,
            isAvailable: false,
          ));
        }
      },
    );
  }

  // for state  update
  actionOnHomeStateChanged({
    BuildContext context,
    ScaffoldState scaffoldState,
  }) {
    if (homeState?.isLoading == false) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        if (homeState?.message?.isNotEmpty == true) {
          DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
            context: context,
            scaffoldState: scaffoldState,
            message: homeState?.message,
          );
        }

        //To check new update available or not
        if (homeState?.forceUpdateResponseModel != null &&
            homeState?.forceUpdateResponseModel?.data != null &&
            homeState?.needToUpdate != null &&
            homeState?.needToUpdate == true) {
          ForceUpdateActionManager _forceUpdateActionManager =
          ForceUpdateActionManager();
          _forceUpdateActionManager.showForceUpdateDialog(
            context: context,
            forcefullyUpdate:
            (homeState?.forceUpdateResponseModel?.data?.forceUpdate == 1)
                ? true
                : false,
            message: homeState?.forceUpdateResponseModel?.data?.message,
          );
        }
      });
    }
  }

  //Change availability status
  void actionOnChangeAvailabilityStatus({bool availabilityStatus}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) {
      if (onValue) {
        // hide keyboard
        homeBloc?.emitEvent(ChangeAvailabilityEvent(
            isLoading: true,
            context: context,
            isAvailable: availabilityStatus,
            authResponseModel: homeState?.authResponseModel));
      }
    });
  }
}
