import 'package:animator/animator.dart';
import 'package:flutter/material.dart';
import 'package:upmart_driver/localizations.dart';
import 'package:upmart_driver/modules/auth/manager/auth_manager.dart';
import 'package:upmart_driver/modules/common/constants/dimens_constants.dart';
import 'package:upmart_driver/modules/common/theme/app_themes.dart';
import 'package:upmart_driver/modules/common/utils/common_utils.dart';
import 'package:upmart_driver/modules/intro/constant/image_constant.dart';

class IntroPage extends StatefulWidget {
  @override
  _IntroPageState createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {
  AuthManager _authManager = AuthManager();
  var _onceUiBuild = false;
  BuildContext _context;

  @override
  Widget build(BuildContext context) {
    _context = context;
    if (!_onceUiBuild) {
      _onceUiBuild = true;
      _authManager.actionOnIntroScreenStateChange(
        context: context,
      );
    }
    return Scaffold(
      body: Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.cover, image: AssetImage(INTRO_BACKGROUND))),
          ),
          Container(
              alignment: Alignment.topCenter,
              height: CommonUtils.commonUtilsInstance.getPercentageSize(
                  context: context, ofWidth: false, percentage: SIZE_100),
              width: CommonUtils.commonUtilsInstance.getPercentageSize(
                  context: context, ofWidth: true, percentage: SIZE_100),
              decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.fill, image: AssetImage(INTRO_GRADIENT)),
              ),
              child: Padding(
                padding: const EdgeInsets.only(top: SIZE_40),
                child: Column(
                  children: <Widget>[_showIntroLogo(), _showLimitLessLogo()],
                ),
              )),
        ],
      ),
    );
  }

  //this method will show intro logo
  Widget _showIntroLogo() {
    return Animator(
      tween: Tween<Offset>(begin: Offset(-1, 0), end: Offset(0.0, 0)),
      duration: Duration(seconds: 1),
      curve: Curves.decelerate,
      repeats: 1,
      cycles: 1,
      builder: (_, animatorState, __) => FractionalTranslation(
        translation: animatorState.value,
        child: Image.asset(
          INTRO_LOGO,
          height: CommonUtils.commonUtilsInstance.getPercentageSize(
              context: _context, ofWidth: false, percentage: SIZE_15),
          alignment: Alignment.bottomCenter,
          width: CommonUtils.commonUtilsInstance.getPercentageSize(
              context: _context, ofWidth: true, percentage: SIZE_30),
        ),
      ),
    );
  }

  //this method will show Limit less logo
  Widget _showLimitLessLogo() {
    return Expanded(
        child: Animator(
      tween: Tween<Offset>(begin: Offset(0, 0.5), end: Offset(0, 0)),
      duration: Duration(seconds: 1),
      repeats: 1,
      cycles: 1,
      builder: (_, animatorState, __) => FractionalTranslation(
        translation: animatorState.value,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(AppLocalizations.of(context).common.text.appTitle,
                style: textStyleSize30WithWhiteColor),
            Text(
              AppLocalizations.of(context).common.text.appSubtitle,
              style: textStyleSize16WithWhiteColor,
            )
          ],
        ),
      ),
    ));
  }
}
