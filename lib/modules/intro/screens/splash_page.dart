import 'package:animator/animator.dart';
import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/auth/manager/auth_manager.dart';
import 'package:upmart_driver/modules/common/constants/dimens_constants.dart';
import 'package:upmart_driver/modules/common/theme/app_themes.dart';
import 'package:upmart_driver/modules/common/utils/common_utils.dart';
import 'package:upmart_driver/modules/intro/constant/image_constant.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  AuthManager _authManager = AuthManager();

  var _onceUiBuild = false;
  bool isAnimationFinished = false;

  @override
  Widget build(BuildContext context) {
    if (!_onceUiBuild) {
      _onceUiBuild = true;
      _authManager.actionOnSplashScreenStateChange(
        context: context,
      );
    }

    return SafeArea(
      child: Scaffold(
        body: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
              image: AssetImage(SPLASH_IMAGE),
              fit: BoxFit.cover,
            )),
            height: double.infinity,
            width: double.infinity,
            alignment: Alignment.center,
            child: Column(
              children: [
                _showScooterLogo(context),
              ],
            )),
      ),
    );
  }

  Widget _showScooterLogo(BuildContext context) {
    return (isAnimationFinished == false)
        ? Animator(
            tween: Tween<Offset>(begin: Offset(-10, 0), end: Offset(0.0, 0)),
            duration: Duration(seconds: 5),
            curve: Curves.decelerate,
            repeats: 1,
            cycles: 1,
            endAnimationListener: (animation) {
              if (animation.controller.isCompleted == true) {
                isAnimationFinished = true;
                setState(() {});
              }
            },
            builder: (_, animatorState, __) => FractionalTranslation(
              translation: animatorState.value,
              child: Image.asset(
                SCOOTER_UP_LOGO,
                height: CommonUtils.commonUtilsInstance.getPercentageSize(
                    context: context, ofWidth: false, percentage: SIZE_20),
                alignment: Alignment.center,
                width: CommonUtils.commonUtilsInstance.getPercentageSize(
                    context: context, ofWidth: true, percentage: SIZE_30),
              ),
            ),
          )
        : Column(
            children: [
              Image.asset(
                SCOOTER_DOWN_LOGO,
                height: CommonUtils.commonUtilsInstance.getPercentageSize(
                    context: context, ofWidth: false, percentage: SIZE_20),
                alignment: Alignment.center,
                width: CommonUtils.commonUtilsInstance.getPercentageSize(
                    context: context, ofWidth: true, percentage: SIZE_30),
              ),
              _getText()
            ],
          );
  }

  Widget _getText() {
    return Animator(
      tween: Tween<Offset>(begin: Offset(0, 0.2), end: Offset(0.0, 0)),
      duration: Duration(seconds: 1),
      curve: Curves.easeIn,
      repeats: 1,
      cycles: 1,
      builder: (_, animatorState, __) => FractionalTranslation(
          translation: animatorState.value,
          child: Column(
            children: [
              Text(
                "upmart",
                style: textStyleSize28TabAccentColorAllertaFont,
              ),
              Text(
                "driver",
                style: textStyleSize24PrimaryColorAllertaFont,
              ),
            ],
          )),
    );
  }
}
