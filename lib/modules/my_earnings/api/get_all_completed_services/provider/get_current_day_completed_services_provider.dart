import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/common/app_config/app_config.dart';

class GetCurrentDayCompletedServicesProvider {
  Future<dynamic> getAllCompletedServices(
      {BuildContext context, String selectedDate}) async {
    var confirmOtp = "booking/deliveryboy/myearning";

    var result = await AppConfig.of(context)
        .baseApi
        .getRequest(confirmOtp, context, queryParameters: {
      "start_date": selectedDate,
      "end_date":selectedDate,
    });
    return result;
  }
}
