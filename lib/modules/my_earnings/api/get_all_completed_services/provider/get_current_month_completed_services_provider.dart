import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/common/app_config/app_config.dart';
import 'package:upmart_driver/modules/common/utils/date_utils.dart';


class GetCurrentMonthCompletedServicesProvider {
  Future<dynamic> getCurrentMonthCompletedServices(
      {BuildContext context, String selectedDate}) async {
    var confirmOtp = "booking/deliveryboy/myearning";

    DateTime _selectedDateTime =
        DateUtils.dateUtilsInstance.convertStringToDate(dateTime: selectedDate);

    // used to get the days in a month
    DateTime _nextMonth =
        new DateTime(_selectedDateTime.year, _selectedDateTime.month + 1, 0,);
    // create start date
    DateTime _startDateTime =
        new DateTime(_selectedDateTime.year, _selectedDateTime.month, 1);
    // create end date
    DateTime _endDateTime = new DateTime(
        _selectedDateTime.year, _selectedDateTime.month, _nextMonth.day);

    var result = await AppConfig.of(context)
        .baseApi
        .getRequest(confirmOtp, context, queryParameters: {
      "start_date": DateUtils.dateUtilsInstance
          .convertDateTimeToString(dateTime: _startDateTime),
      "end_date": DateUtils.dateUtilsInstance
          .convertDateTimeToString(dateTime: _endDateTime),
    });
    return result;
  }
}
