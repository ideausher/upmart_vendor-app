import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:upmart_driver/modules/orders/api/model/order_listing_response_model.dart';

class CurrentMonthEarningsState extends BlocState {
  final bool isLoading; // used to show loader
  final BuildContext context;
  final List<Order> orderList;
  final String message;
  final num currentMonthTotalEarningOfDay;
  final String monthName;

  CurrentMonthEarningsState({
    this.isLoading,
    this.context,
    this.orderList,
    this.message,
    this.currentMonthTotalEarningOfDay,
    this.monthName,
  }) : super(isLoading);

  factory CurrentMonthEarningsState.initiating({bool isLoading}) {
    return CurrentMonthEarningsState(
      isLoading: isLoading,
    );
  }

  factory CurrentMonthEarningsState.updateUi({
    bool isLoading,
    BuildContext context,
    List<Order> orderList,
    String message,
    num totalEarningOfDay,
    String monthName,
  }) {
    return CurrentMonthEarningsState(
      isLoading: isLoading,
      context: context,
      orderList: orderList,
      message: message,
      currentMonthTotalEarningOfDay: totalEarningOfDay,
      monthName: monthName,
    );
  }
}
