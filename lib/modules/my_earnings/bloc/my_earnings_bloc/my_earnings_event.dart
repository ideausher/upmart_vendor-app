import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';



abstract class MyEarningsEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final String selectedDate;

  MyEarningsEvent({
    this.isLoading: false,
    this.context,
    this.selectedDate,
  });
}

// for get completed services
class GetCompletedServicesListEvent extends MyEarningsEvent {
  GetCompletedServicesListEvent({
    BuildContext context,
    bool isLoading,
    String selectedDate,
  }) : super(
          context: context,
          isLoading: isLoading,
          selectedDate: selectedDate,
        );
}
