import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:upmart_driver/modules/orders/api/model/order_listing_response_model.dart';



class MyEarningsState extends BlocState {
  final bool isLoading; // used to show loader
  final BuildContext context;
  final List<Order> orderList;
  final String message;
  final num totalEarningOfDay;

  MyEarningsState({
    this.isLoading,
    this.context,
    this.orderList,
    this.message,
    this.totalEarningOfDay,
  }) : super(isLoading);

  factory MyEarningsState.initiating({bool isLoading}) {
    return MyEarningsState(
      isLoading: isLoading,
    );
  }

  factory MyEarningsState.updateUi({
    bool isLoading,
    BuildContext context,
    List<Order> orderList,
    String message,
    num totalEarningOfDay,
  }) {
    return MyEarningsState(
      isLoading: isLoading,
      context: context,
      orderList: orderList,
      message: message,
      totalEarningOfDay: totalEarningOfDay,
    );
  }
}
