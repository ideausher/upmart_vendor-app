import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/auth/auth_bloc/auth_bloc.dart';
import 'package:upmart_driver/modules/auth/auth_bloc/auth_state.dart';
import 'package:upmart_driver/modules/common/utils/date_utils.dart';
import 'package:upmart_driver/modules/common/utils/network_connectivity_utils.dart';
import 'package:upmart_driver/modules/my_earnings/bloc/current_month_credit_bloc/current_month_earnings_bloc.dart';
import 'package:upmart_driver/modules/my_earnings/bloc/current_month_credit_bloc/current_month_earnings_event.dart';
import 'package:upmart_driver/modules/my_earnings/bloc/current_month_credit_bloc/current_month_earnings_state.dart';
import 'package:upmart_driver/modules/my_earnings/bloc/my_earnings_bloc/my_earnings_bloc.dart';
import 'package:upmart_driver/modules/my_earnings/bloc/my_earnings_bloc/my_earnings_event.dart';
import 'package:upmart_driver/modules/my_earnings/bloc/my_earnings_bloc/my_earnings_state.dart';



class MyEarningsActionManager {
  BuildContext context;

  AuthBloc authBloc;
  AuthState authState;

  MyEarningsBloc myEarningsBloc;
  MyEarningsState myEarningsState;

  CurrentMonthEarningsBloc currentMonthEarningsBloc;
  CurrentMonthEarningsState currentMonthEarningsState;
  // used to perform the action on the init
  void actionOnInit({
    ScaffoldState scaffoldState,
    DateTime selectedDate,
  }) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then(
      (onValue) {
        if (onValue) {
          // used to get the current day earning
          myEarningsBloc.emitEvent(GetCompletedServicesListEvent(
            isLoading: true,
            context: context,
            selectedDate: DateUtils.dateUtilsInstance.convertDateTimeToString(
                dateTime: selectedDate ?? DateTime.now()),
          ));

          // used to get the current month earning
          currentMonthEarningsBloc.emitEvent(GetCurrentMonthCompletedServicesListEvent(
            isLoading: false,
            context: context,
            selectedDate: DateUtils.dateUtilsInstance.convertDateTimeToString(
                dateTime: selectedDate ?? DateTime.now()),
          ));
        }
      },
    );
  }
}
