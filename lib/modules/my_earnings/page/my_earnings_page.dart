import 'dart:io';
import 'package:date_picker_timeline/date_picker_widget.dart';
import 'package:flutter/material.dart';
import 'package:upmart_driver/localizations.dart';
import 'package:upmart_driver/modules/auth/auth_bloc/auth_bloc.dart';
import 'package:upmart_driver/modules/auth/auth_bloc/auth_state.dart';
import 'package:upmart_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_provider.dart';
import 'package:upmart_driver/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:upmart_driver/modules/common/app_config/app_config.dart';
import 'package:upmart_driver/modules/common/common_widget/common_image_with_text.dart';
import 'package:upmart_driver/modules/common/constants/color_constants.dart';
import 'package:upmart_driver/modules/common/constants/dimens_constants.dart';
import 'package:upmart_driver/modules/common/theme/app_themes.dart';
import 'package:upmart_driver/modules/common/utils/common_utils.dart';
import 'package:upmart_driver/modules/common/utils/firebase_messaging_utils.dart';
import 'package:upmart_driver/modules/common/utils/network_connectivity_utils.dart';
import 'package:upmart_driver/modules/common/utils/number_format_utils.dart';
import 'package:upmart_driver/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:upmart_driver/modules/my_earnings/bloc/current_month_credit_bloc/current_month_earnings_bloc.dart';
import 'package:upmart_driver/modules/my_earnings/bloc/current_month_credit_bloc/current_month_earnings_state.dart';
import 'package:upmart_driver/modules/my_earnings/bloc/my_earnings_bloc/my_earnings_bloc.dart';
import 'package:upmart_driver/modules/my_earnings/bloc/my_earnings_bloc/my_earnings_state.dart';
import 'package:upmart_driver/modules/my_earnings/manager/my_earnings_action_manager.dart';
import 'package:upmart_driver/modules/notification/widget/notification_unread_count_widget.dart';
import 'package:upmart_driver/modules/orders/api/model/order_listing_response_model.dart';
import 'package:upmart_driver/modules/orders/constant/image_constant.dart';

class MyEarningsPage extends StatefulWidget {
  BuildContext context;

  MyEarningsPage(this.context);

  @override
  _MyEarningsPageState createState() => _MyEarningsPageState();
}

class _MyEarningsPageState extends State<MyEarningsPage>
    implements PushReceived {
  MyEarningsActionManager _myEarningsActionManager = MyEarningsActionManager();

  DatePickerController _controller = DatePickerController();
  DateTime dateTime = DateTime.now().subtract(Duration(days: 90));

  //Declaration of scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  var _authBloc;
  var _myEarningsBloc = MyEarningsBloc();
  var _currentMonthEarningBloc = CurrentMonthEarningsBloc();
  bool _isCalled = false;
  var _animateToCurrentDate = false;

  @override
  void initState() {
    _authBloc = BlocProvider.of<AuthBloc>(widget.context);

    _myEarningsActionManager.context = widget.context;
    _myEarningsActionManager.authBloc = _authBloc;
    _myEarningsActionManager.myEarningsBloc = _myEarningsBloc;
    _myEarningsActionManager.currentMonthEarningsBloc =
        _currentMonthEarningBloc;

    FirebaseMessagingUtils.firebaseMessagingUtils
        .addCallback(pushReceived: this);

    super.initState();
  }

  @override
  void dispose() {
    FirebaseMessagingUtils.firebaseMessagingUtils
        .removeCallback(pushReceived: this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _myEarningsActionManager.context = context;
    return BlocEventStateBuilder<AuthState>(
      bloc: _authBloc,
      builder: (BuildContext context, AuthState authState) {
        _myEarningsActionManager.context = context;
        if (authState != null &&
            _myEarningsActionManager?.authState != authState) {
          _myEarningsActionManager?.authState = authState;
          if (_myEarningsActionManager.authState?.isLoading == false &&
              _isCalled == false) {
            _isCalled = true;
            _myEarningsActionManager.actionOnInit(
              scaffoldState: _scaffoldKey?.currentState,
              selectedDate: DateTime.now(),
            );
          }
        }
        return ModalProgressHUD(
            inAsyncCall: _myEarningsActionManager.authState?.isLoading ?? false,
            child: BlocEventStateBuilder<MyEarningsState>(
              bloc: _myEarningsBloc,
              builder: (BuildContext context, MyEarningsState myEarningsState) {
                _myEarningsActionManager.context = context;
                if (myEarningsState != null &&
                    _myEarningsActionManager?.myEarningsState !=
                        myEarningsState) {
                  _myEarningsActionManager?.myEarningsState = myEarningsState;
                  if (_animateToCurrentDate == false) {
                    if (_myEarningsActionManager?.myEarningsState?.isLoading ==
                        false) {
                      WidgetsBinding.instance.addPostFrameCallback((_) {
                        _animateToCurrentDate = true;
                        _controller.animateToDate(DateTime.now(),
                            duration: Duration(
                              milliseconds: 1,
                            ));
                      });
                    }
                  }
                }
                return ModalProgressHUD(
                    inAsyncCall:
                        _myEarningsActionManager.myEarningsState?.isLoading ??
                            false,
                    child: Scaffold(
                      appBar: _showAppBar(),
                      key: _scaffoldKey,
                      body: Column(
                        children: <Widget>[
                          // used for the current month earning
                          BlocEventStateBuilder<CurrentMonthEarningsState>(
                            bloc: _currentMonthEarningBloc,
                            builder: (BuildContext context,
                                CurrentMonthEarningsState
                                    currentMonthEarningsState) {
                              _myEarningsActionManager.context = context;
                              if (myEarningsState != null &&
                                  _myEarningsActionManager
                                          ?.currentMonthEarningsState !=
                                      currentMonthEarningsState) {
                                _myEarningsActionManager
                                        ?.currentMonthEarningsState =
                                    currentMonthEarningsState;
                              }
                              return ModalProgressHUD(
                                color: Colors.transparent,
                                inAsyncCall: _myEarningsActionManager
                                        .currentMonthEarningsState?.isLoading ??
                                    false,
                                child: // used for the total credits in wallet
                                    (_myEarningsActionManager
                                                    .currentMonthEarningsState
                                                    ?.isLoading ==
                                                false &&
                                            _myEarningsActionManager
                                                    .myEarningsState
                                                    ?.isLoading ==
                                                false)
                                        ? _getEarningWidget(
                                            text:
                                                "${AppLocalizations.of(_myEarningsActionManager.context).myEarnings.text.totalCreditsIn} ${_myEarningsActionManager?.currentMonthEarningsState?.monthName ?? "Month"}",
                                            width: SIZE_100,
                                            height: SIZE_20,
                                            isWallet: true,
                                            value: NumberFormatUtils
                                                .numberFormatUtilsInstance
                                                .formatPriceWithSymbol(
                                                    price: _myEarningsActionManager
                                                            .currentMonthEarningsState
                                                            ?.currentMonthTotalEarningOfDay ??
                                                        0))
                                        : _getEarningWidget(
                                            text:
                                                "${AppLocalizations.of(_myEarningsActionManager.context).myEarnings.text.totalCreditsIn} ${_myEarningsActionManager?.currentMonthEarningsState?.monthName ?? "Month"}",
                                            width: SIZE_100,
                                            height: SIZE_20,
                                            isWallet: true,
                                            value: AppLocalizations.of(
                                                    _myEarningsActionManager
                                                        .context)
                                                .myEarnings
                                                .text
                                                .calculating),
                              );
                            },
                          ),

                          // used for the total credits in wallet
                          (_myEarningsActionManager?.myEarningsState?.isLoading == false)
                              ? _getEarningWidget(
                                  text: AppLocalizations.of(_myEarningsActionManager.context)
                                      .myEarnings
                                      .text
                                      .totalEarningOfTheDay,
                                  width: SIZE_80,
                                  height: SIZE_20,
                                  isWallet: false,
                                  value: NumberFormatUtils
                                      .numberFormatUtilsInstance
                                      .formatPriceWithSymbol(
                                          price: _myEarningsActionManager
                                                  .myEarningsState
                                                  ?.totalEarningOfDay ??
                                              0))
                              : _getEarningWidget(
                                  text: AppLocalizations.of(
                                          _myEarningsActionManager.context)
                                      .myEarnings
                                      .text
                                      .totalEarningOfTheDay,
                                  width: SIZE_80,
                                  height: SIZE_20,
                                  isWallet: false,
                                  value: AppLocalizations.of(
                                          _myEarningsActionManager.context)
                                      .myEarnings
                                      .text
                                      .calculating),
                          _getCalendarView(),

                          (_myEarningsActionManager
                                      .myEarningsState?.orderList?.isNotEmpty ==
                                  true)
                              ? Expanded(
                                  child: ListView.builder(
                                      shrinkWrap: true,
                                      padding: EdgeInsets.all(SIZE_5),
                                      itemCount: _myEarningsActionManager
                                          .myEarningsState?.orderList?.length,
                                      itemBuilder: (context, index) {
                                        Order orderList =
                                            _myEarningsActionManager
                                                .myEarningsState
                                                ?.orderList[index];
                                        return _getListItem(
                                            orderModel: orderList);
                                      }),
                                )
                              : Visibility(
                                  visible: _myEarningsActionManager
                                          .myEarningsState?.isLoading ==
                                      false,
                                  child: Expanded(
                                    child: CommonImageWithTextWidget(
                                        context: context,
                                        localImagePath: NO_INTERNET_ICON,
                                        isSvgImage: false,
                                        imageHeight: SIZE_28,
                                        imageWidth: SIZE_50,
                                        textToShow: AppLocalizations.of(
                                                _myEarningsActionManager
                                                    .context)
                                            .orders
                                            .text
                                            .noOrderFound),
                                  ),
                                ),
                        ],
                      ),
                    ));
              },
            ));
      },
    );
  }

  // used for the list item
  Widget _getListItem({Order orderModel}) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(SIZE_5),
        child: Row(
          children: <Widget>[
            Expanded(
                child: Padding(
              padding: const EdgeInsets.all(SIZE_8),
              child: Column(
                children: [
                  Row(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            color: COLOR_GREEN, shape: BoxShape.circle),
                        height: SIZE_10,
                        width: SIZE_10,
                      ),
                      SizedBox(
                        width: SIZE_5,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "${AppLocalizations.of(_myEarningsActionManager.context).orders.text.orderId} - ${orderModel?.bookingCode}",
                            style: textStyleSize14WithLightBlueColor,
                          ),
                        ],
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            color: Colors.transparent, shape: BoxShape.circle),
                        height: SIZE_10,
                        width: SIZE_10,
                      ),
                      SizedBox(
                        width: SIZE_5,
                      ),
                      Expanded(
                        child: Text(
                          orderModel?.deliveryAddress?.formattedAddress??"",
                          style: textStyleSize14GreyColorFont500,
                        ),
                      ),
                      SizedBox(
                        height: SIZE_10,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: SIZE_10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            color: Colors.transparent, shape: BoxShape.circle),
                        height: SIZE_10,
                        width: SIZE_10,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'PickUp',
                            style:
                                AppConfig.of(_myEarningsActionManager.context)
                                    .themeData
                                    .primaryTextTheme
                                    .headline1,
                          ),
                          Text(
                            '${NumberFormatUtils.numberFormatUtilsInstance.formatPriceWithSymbol(price: ((orderModel?.amountPaidToDeliveryBoyOnOrderPickUp ?? 0.0)))}',
                            style:
                                AppConfig.of(_myEarningsActionManager.context)
                                    .themeData
                                    .primaryTextTheme
                                    .headline2,
                          ),
                        ],
                      ),
                      SizedBox(
                        width: SIZE_15,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Delivery',
                            style:
                                AppConfig.of(_myEarningsActionManager.context)
                                    .themeData
                                    .primaryTextTheme
                                    .headline1,
                          ),
                          Text(
                            '${NumberFormatUtils.numberFormatUtilsInstance.formatPriceWithSymbol(price: ((orderModel?.amountPaidToDeliveryBoyOnOrderDelivery ?? 0.0)))}',
                            style:
                                AppConfig.of(_myEarningsActionManager.context)
                                    .themeData
                                    .primaryTextTheme
                                    .headline2,
                          ),
                        ],
                      ),
                      SizedBox(
                        width: SIZE_15,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Delivered',
                            style:
                                AppConfig.of(_myEarningsActionManager.context)
                                    .themeData
                                    .primaryTextTheme
                                    .headline1,
                          ),
                          Text(
                            '${NumberFormatUtils.numberFormatUtilsInstance.formatPriceWithSymbol(price: ((orderModel?.deliveryChargeToDeliveryBoy ?? 0.0)))}',
                            style:
                                AppConfig.of(_myEarningsActionManager.context)
                                    .themeData
                                    .primaryTextTheme
                                    .headline2,
                          ),
                        ],
                      ),
                      SizedBox(
                        width: SIZE_15,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Tip',
                            style:
                                AppConfig.of(_myEarningsActionManager.context)
                                    .themeData
                                    .primaryTextTheme
                                    .headline1,
                          ),
                          Text(
                            '${NumberFormatUtils.numberFormatUtilsInstance.formatPriceWithSymbol(price: ((orderModel?.tipToDeliveryBoy ?? 0.0)))}',
                            style:
                                AppConfig.of(_myEarningsActionManager.context)
                                    .themeData
                                    .primaryTextTheme
                                    .headline2,
                          ),
                        ],
                      ),
                      SizedBox(
                        width: SIZE_15,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: SIZE_10,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            color: Colors.transparent, shape: BoxShape.circle),
                        height: SIZE_10,
                        width: SIZE_10,
                      ),
                      Text(
                        'Total',
                        style: AppConfig.of(_myEarningsActionManager.context)
                            .themeData
                            .primaryTextTheme
                            .headline6,
                      ),
                      SizedBox(
                        width: SIZE_10,
                      ),
                      Column(
                        children: [
                          Text(
                            NumberFormatUtils.numberFormatUtilsInstance.formatPriceWithSymbol(
                                price: ((orderModel
                                            ?.amountPaidToDeliveryBoyOnOrderPickUp ??
                                        0.0) +
                                    (orderModel
                                            ?.amountPaidToDeliveryBoyOnOrderDelivery ??
                                        0.0) +
                                    (orderModel?.deliveryChargeToDeliveryBoy ??
                                        0.0) +
                                    (orderModel?.tipToDeliveryBoy ?? 0.0))),
                            style: textStyleSize16WithPrimaryColor,
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            )),
            // Padding(
            //   padding: const EdgeInsets.all(SIZE_8),
            //   child: Text(
            //     NumberFormatUtils.numberFormatUtilsInstance
            //         .formatPriceWithSymbol(
            //             price:
            //                 ((orderModel?.deliveryChargeToDeliveryBoy ?? 0.0) +
            //                     (orderModel?.tipToDeliveryBoy ?? 0.0))),
            //     style: textStyleSize16WithBlueColor,
            //   ),
            // )
          ],
        ),
      ),
    );
  }

  //this method will return a app bar widget
  Widget _showAppBar() {
    return CommonUtils.commonUtilsInstance.getAppBar(
        context: _myEarningsActionManager?.context,
        elevation: ELEVATION_0,
        appBarTitle: AppLocalizations.of(_myEarningsActionManager.context)
            .myEarnings
            .text
            .myEarnings,
        defaultLeadingIcon: Icons.arrow_back,
        defaultLeadingIconColor: COLOR_BLACK,
        actionWidgets: [
          Padding(
            padding: const EdgeInsets.only(right: SIZE_16, top: SIZE_16),
            child: NotificationReadCountWidget(
              context: context,
            ),
          )
        ],
        appBarTitleStyle: AppConfig.of(_myEarningsActionManager?.context)
            .themeData
            .primaryTextTheme
            .headline3,
        backGroundColor: Colors.transparent);
  }

  // used to get the earning widget
  Widget _getEarningWidget(
      {num height, num width, String text, bool isWallet, String value}) {
    return Container(
      margin: EdgeInsets.only(left: SIZE_15, right: SIZE_15, top: SIZE_10),
      width: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _myEarningsActionManager.context,
          ofWidth: true,
          percentage: width),
      height: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _myEarningsActionManager.context, percentage: height),
      decoration: BoxDecoration(
          color: (isWallet == true) ? COLOR_BLUE_TEXT : COLOR_ACCENT,
          borderRadius: BorderRadius.all(Radius.circular(20))),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            text,
            style: TextStyle(
                fontSize: SIZE_13,
                color: (isWallet == true) ? Colors.white : Colors.white),
          ),
          SizedBox(
            height: SIZE_5,
          ),
          Text(
            value ?? "",
            style: TextStyle(
                fontSize: SIZE_24,
                color: (isWallet == true) ? Colors.white : Colors.white),
          ),
        ],
      ),
    );
  }

  // used to get the calendar view
  Widget _getCalendarView() {
    return Container(
      margin: EdgeInsets.only(left: SIZE_15, right: SIZE_15, top: SIZE_10),
      child: DatePicker(
        dateTime,
        width: SIZE_50,
        height: SIZE_80,
        controller: _controller,
        initialSelectedDate: DateTime.now(),
        selectionColor: COLOR_PRIMARY,
        selectedTextColor: Colors.white,
        monthTextStyle: textStyleSize10WithDarkGreyColor,
        dayTextStyle: textStyleSize10WithDarkGreyColor,
        dateTextStyle: textStyleSize16WithColorHintColor,
        onDateChange: (date) {
          NetworkConnectionUtils.networkConnectionUtilsInstance
              .getConnectivityStatus(context, showNetworkDialog: true)
              .then(
            (onValue) {
              if (onValue) {
                // New date selected
                _myEarningsActionManager.actionOnInit(
                  selectedDate: date,
                  scaffoldState: _scaffoldKey?.currentState,
                );
              }
            },
          );
        },
      ),
    );
  }

  @override
  onMessageReceived({NotificationPushModel notificationPushModel}) {
    print("NOTIFICATION_RECEIVED MY_PROFILE");
    _myEarningsActionManager.actionOnInit(
      scaffoldState: _scaffoldKey?.currentState,
    );
  }
}
