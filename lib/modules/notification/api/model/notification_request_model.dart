// To parse this JSON data, do
//
//     final notificationRequestModel = notificationRequestModelFromMap(jsonString);

import 'dart:convert';

NotificationRequestModel notificationRequestModelFromMap(String str) => NotificationRequestModel.fromMap(json.decode(str));

String notificationRequestModelToMap(NotificationRequestModel data) => json.encode(data.toMap());

class NotificationRequestModel {
  NotificationRequestModel({
    this.userId,
    this.page,
  });

  String userId;
  int page;

  factory NotificationRequestModel.fromMap(Map<String, dynamic> json) => NotificationRequestModel(
    userId: json["userId"] == null ? null : json["userId"],
    page: json["page"] == null ? null : json["page"],
  );

  Map<String, dynamic> toMap() => {
    "userId": userId == null ? null : userId,
    "page": page == null ? null : page,
  };
}
