// To parse this JSON data, do
//
// final notificationResponseModel = notificationResponseModelFromJson(jsonString);

import 'dart:convert';

NotificationResponseModel notificationResponseModelFromJson(String str) => NotificationResponseModel.fromJson(json.decode(str));

String notificationResponseModelToJson(NotificationResponseModel data) => json.encode(data.toJson());

class NotificationResponseModel {
  NotificationResponseModel({
    this.statusCode,
    this.message,
    this.data,
  });

  int statusCode;
  String message;
  List<NotificationModel> data;

  factory NotificationResponseModel.fromJson(Map<String, dynamic> json) => NotificationResponseModel(
    statusCode: json["status_code"] == null ? null : json["status_code"],
    message: json["message"] == null ? null : json["message"],
    data: json["data"] == null ? null : List<NotificationModel>.from(json["data"].map((x) => NotificationModel.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status_code": statusCode == null ? null : statusCode,
    "message": message == null ? null : message,
    "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class NotificationModel {
  NotificationModel({
    this.id,
    this.notificationType,
    this.title,
    this.description,
    this.isRead,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  int notificationType;
  String title;
  String description;
  int isRead;
  String createdAt;
  String updatedAt;

  factory NotificationModel.fromJson(Map<String, dynamic> json) => NotificationModel(
    id: json["id"] == null ? null : json["id"],
    notificationType: json["notification_type"] == null ? null : json["notification_type"],
    title: json["title"] == null ? null : json["title"],
    description: json["description"] == null ? null : json["description"],
    isRead: json["is_read"] == null ? null : json["is_read"],
    createdAt: json["created_at"] == null ? null : json["created_at"],
    updatedAt: json["updated_at"] == null ? null : json["updated_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "notification_type": notificationType == null ? null : notificationType,
    "title": title == null ? null : title,
    "description": description == null ? null : description,
    "is_read": isRead == null ? null : isRead,
    "created_at": createdAt == null ? null : createdAt,
    "updated_at": updatedAt == null ? null : updatedAt,
  };
}