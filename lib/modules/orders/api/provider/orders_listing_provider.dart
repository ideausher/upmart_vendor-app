import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/common/app_config/app_config.dart';

class OrdersListingProvider {
  //method to get customer order history
  Future<dynamic> getOrdersApiCall({
    BuildContext context,
    int page,
    int orderStatus,
  }) async {
    var _orderList = "booking/deliveryboy";

    var result = await AppConfig.of(context)
        .baseApi
        .getRequest(_orderList, context, queryParameters: {
      "page": page,
      "status": orderStatus,
      "limit": 10,
    });

    return result;
  }
}

// status : 1 => ORDER_ACCEPTED, 2 => ORDER_REJECTED, 3 => ORDER_COMPLETED, 4 => ORDER_INPROGRESS, 5 => PENDING_ORDERS
