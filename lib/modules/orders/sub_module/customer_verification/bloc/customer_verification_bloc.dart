import 'package:upmart_driver/localizations.dart';
import 'package:upmart_driver/modules/auth/api/sign_up/provider/sign_up_provider.dart';
import 'package:upmart_driver/modules/auth/api/verify_otp/provider/verify_otp_provider.dart';
import 'package:upmart_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:upmart_driver/modules/common/enum/enums.dart';
import 'package:upmart_driver/modules/common/model/common_response_model.dart';
import 'package:upmart_driver/modules/orders/api/model/order_listing_response_model.dart';
import 'package:upmart_driver/modules/orders/enum/order_enum.dart';
import 'package:upmart_driver/modules/orders/sub_module/order_details/api/provider/order_detail_provider.dart';

import 'customer_verification_event.dart';
import 'customer_verification_state.dart';

class CustomerVerificationBloc extends BlocEventStateBase<
    CustomerVerficationEvent, CustomerVerificationState> {
  CustomerVerificationBloc({bool initializing = true})
      : super(initialState: CustomerVerificationState.initiating());

  @override
  Stream<CustomerVerificationState> eventHandler(CustomerVerficationEvent event,
      CustomerVerificationState currentState) async* {
    // update home page UI

    if (event is CheckOtpValid) {
      CommonResponseModel commonResponseModel;
      String _message = "";
      yield CustomerVerificationState.updateCustomerVerification(
          isLoading: true,
          context: event?.context,
          message: "",
          order: event.order,
          showResendButton: event.showResendButton);

      var result = await VerifyOtpProvider().verifyOtpApiCall(
        context: event.context,
        verifyOtpRequestModel: event.verifyOtpRequestModel,
      );
      Order _order = event?.order;
      int _status;
      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          commonResponseModel = CommonResponseModel.fromJson(result);

          var _result = await OrderDetailProvider().updateOrderApiCall(
              context: event.context,
              id: event.order?.id,
              status: OrderStatus.deliveredProduct.value);

          if (_result != null) {
            // check result status
            if (_result[ApiStatusParams.Status.value] != null &&
                _result[ApiStatusParams.Status.value] ==
                    ApiStatus.Success.value) {
              _order = Order.fromMap(_result["data"]);
              _status = ApiStatus.Success.value;
            }
            // failure case
            else {
              _message = result[ApiStatusParams.Message.value];
            }
          } else {
            _message = AppLocalizations.of(event?.context)
                .common
                .error
                .somethingWentWrong;
          }
        }
        // failure case
        else {
          _message = result[ApiStatusParams.Message.value];
        }
      } else {
        _message =
            AppLocalizations.of(event?.context).common.error.somethingWentWrong;
      }
      yield CustomerVerificationState.updateCustomerVerification(
          isLoading: false,
          context: event?.context,
          message: _message,
          status: _status,
          order: _order,
          showResendButton: event.showResendButton);
    }

    //Use to hide and show resend button
    if (event is ShowHideResendButton) {
      yield CustomerVerificationState.updateCustomerVerification(
          isLoading: false,
          context: event?.context,
          message: "",
          order: event.order,
          showResendButton: event.showResendButton);
    }


    //   BLOCK
    if (event is ResendOtp) {
      CommonResponseModel commonResponseModel;
      String _message = "";
      yield CustomerVerificationState.updateCustomerVerification(
          isLoading: true,
          context: event?.context,
          message: "",
          order: event.order,
          showResendButton: event.showResendButton);

      var result = await SendOtpProvider().sendOtp(
        context: event.context,
        sendOtpRequestModel: event.sendOtpRequestModel,
      );

      if (result != null) {
// check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          commonResponseModel = CommonResponseModel.fromJson(result);
          _message = commonResponseModel?.message;
        }
// failure case
        else {
          _message = result[ApiStatusParams.Message.value];
        }
      } else {
        _message =
            AppLocalizations.of(event?.context).common.error.somethingWentWrong;
      }
      yield CustomerVerificationState.updateCustomerVerification(
          isLoading: false,
          context: event?.context,
          message: _message,
          order: event?.order,
          showResendButton: event.showResendButton);
    }
  }
}
