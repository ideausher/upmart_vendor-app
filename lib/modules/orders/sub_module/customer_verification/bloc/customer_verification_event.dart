import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/auth/api/sign_up/model/send_otp_request_model.dart';
import 'package:upmart_driver/modules/auth/api/verify_otp/model/verify_otp_request_model.dart';
import 'package:upmart_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:upmart_driver/modules/orders/api/model/order_listing_response_model.dart';

abstract class CustomerVerficationEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final Order order;
  final VerifyOtpRequestModel verifyOtpRequestModel;
  final SendOtpRequestModel sendOtpRequestModel;
  final bool showResendButton;

  CustomerVerficationEvent({
    this.isLoading: false,
    this.context,
    this.order,
    this.verifyOtpRequestModel,
    this.sendOtpRequestModel,
    this.showResendButton,
  });
}

//this event is used for updating the home page UI data
class CheckOtpValid extends CustomerVerficationEvent {
  CheckOtpValid(
      {bool isLoading,
      BuildContext context,
      Order order,
      VerifyOtpRequestModel verifyOtpRequestModel,
      bool showResendButton})
      : super(
            isLoading: isLoading,
            context: context,
            order: order,
            verifyOtpRequestModel: verifyOtpRequestModel,
            showResendButton: showResendButton);
}

//this event is used for updating the home page UI data
class ResendOtp extends CustomerVerficationEvent {
  ResendOtp(
      {bool isLoading,
      BuildContext context,
      Order order,
      SendOtpRequestModel sendOtpRequestModel,
      bool showResendButton})
      : super(
          isLoading: isLoading,
          context: context,
          order: order,
          sendOtpRequestModel: sendOtpRequestModel,
          showResendButton: showResendButton,
        );
}

//this event is used for updating the home page UI data
class ShowHideResendButton extends CustomerVerficationEvent {
  ShowHideResendButton({
    bool isLoading,
    BuildContext context,
    Order order,
    SendOtpRequestModel sendOtpRequestModel,
    bool showResendButton,
  }) : super(
          isLoading: isLoading,
          context: context,
          order: order,
          sendOtpRequestModel: sendOtpRequestModel,
          showResendButton: showResendButton,
        );
}
