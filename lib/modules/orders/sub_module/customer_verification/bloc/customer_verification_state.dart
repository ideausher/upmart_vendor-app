import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/auth/api/verify_otp/model/verify_otp_request_model.dart';
import 'package:upmart_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:upmart_driver/modules/orders/api/model/order_listing_response_model.dart';

class CustomerVerificationState extends BlocState {
  CustomerVerificationState(
      {this.status,
      this.isLoading: false,
      this.message,
      this.context,
      this.order,
      this.verifyOtpRequestModel,
      this.showResendButton})
      : super(isLoading);

  final bool isLoading;
  final String message;
  final BuildContext context;
  final Order order;
  final int status;
  final VerifyOtpRequestModel verifyOtpRequestModel;
  final bool showResendButton;

  // used for update home page state
  factory CustomerVerificationState.updateCustomerVerification({
    bool isLoading,
    String message,
    BuildContext context,
    Order order,
    int status,
    VerifyOtpRequestModel verifyOtpRequestModel,
    bool showResendButton,
  }) {
    return CustomerVerificationState(
      isLoading: isLoading,
      message: message,
      context: context,
      order: order,
      status: status,
      verifyOtpRequestModel: verifyOtpRequestModel,
      showResendButton: showResendButton,
    );
  }

  factory CustomerVerificationState.initiating() {
    return CustomerVerificationState(
      isLoading: false,
    );
  }
}
