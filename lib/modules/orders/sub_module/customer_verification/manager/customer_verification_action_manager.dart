import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/auth/api/sign_up/model/send_otp_request_model.dart';
import 'package:upmart_driver/modules/auth/api/verify_otp/model/verify_otp_request_model.dart';
import 'package:upmart_driver/modules/auth/enums/auth_enums.dart';
import 'package:upmart_driver/modules/auth/validator/auth_validator.dart';
import 'package:upmart_driver/modules/common/enum/enums.dart';
import 'package:upmart_driver/modules/common/utils/common_utils.dart';
import 'package:upmart_driver/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:upmart_driver/modules/common/utils/navigator_utils.dart';
import 'package:upmart_driver/modules/common/utils/network_connectivity_utils.dart';
import 'package:upmart_driver/modules/orders/api/model/order_listing_response_model.dart';
import 'package:upmart_driver/modules/orders/sub_module/customer_verification/bloc/customer_verification_bloc.dart';
import 'package:upmart_driver/modules/orders/sub_module/customer_verification/bloc/customer_verification_event.dart';
import 'package:upmart_driver/modules/orders/sub_module/customer_verification/bloc/customer_verification_state.dart';

class CustomerVerificationActionManager {
  BuildContext context;
  CustomerVerificationState customerVerificationState;
  CustomerVerificationBloc customerVerificationBloc;

  //Action on state change
  void actionOnStateChanged({ScaffoldState currentState}) {
    if (customerVerificationState?.isLoading == false) {
      WidgetsBinding.instance.addPostFrameCallback((_) async {
        if (customerVerificationState?.message?.trim()?.isNotEmpty == true) {
          DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
              context: context,
              message: customerVerificationState?.message?.trim(),
              scaffoldState: currentState);
        }

        if (customerVerificationState?.status == ApiStatus.Success.value) {
          NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
        }
      });
    }
  }

  //Action On complete Delivery Button Click
  void actionOnCompleteDeliveryButtonClick(
      {ScaffoldState scaffoldState, String otp, Order order}) {
    // check otp
    String result = AuthValidator.authValidatorInstance.validateVerifyOtpScreen(
      value: otp,
      context: context,
    );

    if (result.isEmpty) {
      NetworkConnectionUtils.networkConnectionUtilsInstance
          .getConnectivityStatus(context, showNetworkDialog: true)
          .then(
        (onValue) {
          if (onValue) {
            // used to get the current day earning
            customerVerificationBloc.emitEvent(
              CheckOtpValid(
                  isLoading: true,
                  context: context,
                  order: order,
                  verifyOtpRequestModel: new VerifyOtpRequestModel(
                      otp: otp,
                      phoneNumber: order?.userDetails?.phoneNumber,
                      countryCode: order?.userDetails?.countryCode,
                      action: "0",
                      type: LoginWithPhoneOrEmail.phone.value.toString())),
            );
          }
        },
      );
    } else {
      // show error
      DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
          context: context, scaffoldState: scaffoldState, message: result);
    }
  }

// for resend otp
  resendOtpCall({Order order, bool showResendButton}) {
// check internet connection
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) {
      if (onValue) {
        // hide keyboard
        CommonUtils.commonUtilsInstance.hideKeyboard(context: context);

        customerVerificationBloc?.emitEvent(ResendOtp(
            order: order,
            context: context,
            isLoading: true,
            showResendButton: showResendButton,
            sendOtpRequestModel: SendOtpRequestModel(
                type: "1",
                countryIsoCode: "${order?.userDetails?.countryISOCode}",
                phoneNumber: order?.userDetails?.phoneNumber,
                countryCode: "${order?.userDetails?.countryCode}")));
      }
    });
  }

  //Show Hide resend Button
  void showHideResendButton({bool showResendButton, Order order}) {
    customerVerificationBloc.emitEvent(ShowHideResendButton(
        isLoading: false,
        context: context,
        order: order,
        showResendButton: showResendButton));
  }
}
