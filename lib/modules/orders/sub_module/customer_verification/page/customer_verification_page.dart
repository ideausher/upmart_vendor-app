import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:upmart_driver/localizations.dart';
import 'package:upmart_driver/modules/auth/enums/auth_enums.dart';
import 'package:upmart_driver/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:upmart_driver/modules/common/app_config/app_config.dart';
import 'package:upmart_driver/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:upmart_driver/modules/common/common_widget/custom_raised_gradient_button.dart';
import 'package:upmart_driver/modules/common/constants/color_constants.dart';
import 'package:upmart_driver/modules/common/constants/dimens_constants.dart';
import 'package:upmart_driver/modules/common/theme/app_themes.dart';
import 'package:upmart_driver/modules/common/utils/common_utils.dart';
import 'package:upmart_driver/modules/common/utils/navigator_utils.dart';
import 'package:upmart_driver/modules/orders/api/model/order_listing_response_model.dart';
import 'package:upmart_driver/modules/orders/sub_module/customer_verification/bloc/customer_verification_bloc.dart';
import 'package:upmart_driver/modules/orders/sub_module/customer_verification/bloc/customer_verification_state.dart';
import 'package:upmart_driver/modules/orders/sub_module/customer_verification/manager/customer_verification_action_manager.dart';

import 'package:pin_code_fields/pin_code_fields.dart';

class CustomerVerificationPage extends StatefulWidget {
  BuildContext context;

  CustomerVerificationPage(BuildContext context) {
    this.context = context;
  }

  @override
  _CustomerVerificationPageState createState() =>
      _CustomerVerificationPageState();
}

class _CustomerVerificationPageState extends State<CustomerVerificationPage> {
  //Controllers for OTP
  final TextEditingController _verifyOtpController = TextEditingController();
  Order _order;

  //Declaration of scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  CustomerVerificationBloc _customerVerificationBloc;
  CustomerVerificationActionManager _customerVerificationActionManager =
      new CustomerVerificationActionManager();

  //Controllers for OTP
  StreamController<ErrorAnimationType> _errorController =
      StreamController<ErrorAnimationType>();

  int _endTime = DateTime.now()
      .add(Duration(minutes: Timer?.minute?.value))
      .millisecondsSinceEpoch;

  @override
  void initState() {
    super.initState();
    _customerVerificationBloc = new CustomerVerificationBloc();
    _customerVerificationActionManager?.context = widget.context;
    _customerVerificationActionManager?.customerVerificationBloc =
        _customerVerificationBloc;

    _order = ModalRoute.of(widget.context).settings.arguments;
    _customerVerificationActionManager?.resendOtpCall(
        order: _order, showResendButton: false);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _customerVerificationActionManager?.context = context;
    return BlocEventStateBuilder<CustomerVerificationState>(
      bloc: _customerVerificationActionManager?.customerVerificationBloc,
      builder: (BuildContext context,
          CustomerVerificationState customerVerificationState) {
        _customerVerificationActionManager?.context = context;
        if (customerVerificationState != null &&
            _customerVerificationActionManager?.customerVerificationState !=
                customerVerificationState) {
          _customerVerificationActionManager?.customerVerificationState =
              customerVerificationState;

          if (_customerVerificationActionManager
                  ?.customerVerificationState?.isLoading ==
              false) {
            _customerVerificationActionManager?.actionOnStateChanged(
                currentState: _scaffoldKey?.currentState);
          }
        }
        return ModalProgressHUD(
            inAsyncCall: _customerVerificationActionManager
                    ?.customerVerificationState?.isLoading ??
                false,
            child: Scaffold(
                key: _scaffoldKey,
                appBar: _showAppBar(),
                body: Center(
                  child: SingleChildScrollView(
                    padding: EdgeInsets.all(SIZE_18),
                    child: Wrap(
                      alignment: WrapAlignment.center,
                      children: <Widget>[
                        _verifyMobileNumberTitleView(),
                        _verifyMobileNumberSubtitleView(),
                        _enterOtpView(),
                        _showOTPTimerView(),
                        _completeDeliveryButton(),
                      ],
                    ),
                  ),
                )));
      },
    );
  }

  //this method is used to return the show app bar
  Widget _showAppBar() {
    return CommonUtils.commonUtilsInstance.getAppBar(
        context: _customerVerificationActionManager?.context,
        elevation: ELEVATION_0,
        defaultLeadIconPressed: () {
          NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
        },
        popScreenOnTapOfLeadingIcon: false,
        centerTitle: true,
        appBarTitle: AppLocalizations.of(_customerVerificationActionManager.context).customerVerification.text.customerVerification,
        defaultLeadingIconColor: Colors.black,
        appBarTitleStyle:
            AppConfig.of(_customerVerificationActionManager?.context)
                .themeData
                .primaryTextTheme
                .headline3,
        backGroundColor: Colors.transparent);
  }

  //method to show verify mobile number title view
  Widget _verifyMobileNumberTitleView() {
    return Text(
      AppLocalizations.of(_customerVerificationActionManager.context).customerVerification.text.customerVerification,
      style: AppConfig.of(_customerVerificationActionManager?.context)
          .themeData
          .textTheme
          .headline1,
    );
  }

  //method to show verify mobile number subtitle view
  Widget _verifyMobileNumberSubtitleView() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Expanded(
          child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              children: <TextSpan>[
                TextSpan(
                    text: AppLocalizations.of(_customerVerificationActionManager.context).customerVerification.text.enterTheOtpSentTo,
                    style: AppConfig.of(
                            _customerVerificationActionManager?.context)
                        .themeData
                        .primaryTextTheme
                        .bodyText1),
                TextSpan(
                    text:
                        ' (${_order?.userDetails?.countryCode}) ${_order?.userDetails?.phoneNumber}',
                    style: AppConfig.of(
                            _customerVerificationActionManager?.context)
                        .themeData
                        .primaryTextTheme
                        .caption),
              ],
            ),
            textScaleFactor: 0.5,
          ),
        )
      ],
    );
  }

  //This method will return a view to fill OTP
  Widget _enterOtpView() {
    return Padding(
      padding:
          const EdgeInsets.only(left: SIZE_30, right: SIZE_30, top: SIZE_22),
      child: PinCodeTextField(
        length: MaxLength.OTP.value,
        obsecureText: false,
        inputFormatters: <TextInputFormatter>[
          WhitelistingTextInputFormatter.digitsOnly
        ],
        textInputType: TextInputType.number,
        animationType: AnimationType.scale,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        pinTheme: PinTheme(
          shape: PinCodeFieldShape.box,
          borderRadius: BorderRadius.circular(SIZE_6),
          borderWidth: SIZE_1,
          fieldHeight: CommonUtils.commonUtilsInstance.getPercentageSize(
              context: _customerVerificationActionManager?.context,
              percentage: SIZE_6,
              ofWidth: false),
          activeColor: AppConfig.of(_customerVerificationActionManager?.context)
              .themeData
              .primaryColor,
          inactiveColor: COLOR_LIGHT_GREY,
          inactiveFillColor: COLOR_OFF_WHITE,
          selectedFillColor: Colors.white,
          selectedColor:
              AppConfig.of(_customerVerificationActionManager?.context)
                  .themeData
                  .primaryColor,
          fieldWidth: CommonUtils.commonUtilsInstance.getPercentageSize(
              context: _customerVerificationActionManager?.context,
              percentage: SIZE_12,
              ofWidth: true),
          activeFillColor: COLOR_OFF_WHITE,
        ),
        animationDuration: Duration(milliseconds: 300),
        enableActiveFill: true,
        textStyle: AppConfig.of(_customerVerificationActionManager?.context)
            .themeData
            .textTheme
            .bodyText1,
        autoDisposeControllers: false,
        errorAnimationController: _errorController,
        controller: _verifyOtpController,
        onCompleted: (v) {
          // _updateUiDataModel?.otpLength =
          //     widget?.verifyOtpController?.text?.length;
          // _callChangeButtonColorEvent();
          print('hii i am oncomplete');
        },
        onChanged: (value) {
          if (value.length <= MaxLength.OtpErrorLength.value) {
            // _updateUiDataModel?.otpLength =
            //     widget?.verifyOtpController?.text?.length;
            // _callChangeButtonColorEvent();
          }
        },
        beforeTextPaste: (text) {
          print("Allowing to paste $text");
          //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
          //but you can show anything you want here, like your pop up saying wrong paste format or etc
          return true;
        },
      ),
    );
  }

  //method to show otp timer widget view
  Widget _showOTPTimerView() {
    return _customerVerificationActionManager
                .customerVerificationState?.showResendButton ==
            true
        ? InkWell(
            onTap: () {
              _endTime = DateTime.now()
                  .add(Duration(minutes: Timer?.minute?.value))
                  .millisecondsSinceEpoch;

              _customerVerificationActionManager?.resendOtpCall(
                  order: _order, showResendButton: false);
            },
            child: Align(
              alignment: Alignment.centerRight,
              child: Text(
                AppLocalizations.of(_customerVerificationActionManager.context).customerVerification.text.resendOtp,
                style: textStyleSize16WithBlueColor,
                textAlign: TextAlign.center,
              ),
            ),
          )
        : CountdownTimer(
            endTime: _endTime,
            widgetBuilder: (_, CurrentRemainingTime time) {
              return (time != null)
                  ? Container(
                      padding: const EdgeInsets.only(right: SIZE_12),
                      alignment: Alignment.topRight,
                      width: MediaQuery.of(context).size.width,
                      child: Text(
                        '0${time?.min ?? "0"} : ${(time?.sec?.toString()?.length == Timer?.sec?.value) ? time?.sec : '0${time?.sec}' ?? "00:00"}',
                        style: AppConfig.of(
                                _customerVerificationActionManager?.context)
                            .themeData
                            .textTheme
                            .headline4,
                      ),
                    )
                  : Container(
                      padding: const EdgeInsets.only(right: SIZE_12),
                      width: MediaQuery.of(context).size.width,
                      alignment: Alignment.topRight,
                      child: Text(
                        "00:00",
                        style: AppConfig.of(
                                _customerVerificationActionManager?.context)
                            .themeData
                            .textTheme
                            .headline4,
                        textAlign: TextAlign.end,
                      ),
                    );
            },
            emptyWidget: Text(
              "00:00",
              style: AppConfig.of(_customerVerificationActionManager?.context)
                  .themeData
                  .textTheme
                  .headline4,
              textAlign: TextAlign.center,
            ),
            onEnd: () {
              _customerVerificationActionManager.showHideResendButton(
                  showResendButton: true, order: _order);
            },
            textStyle: AppConfig.of(_customerVerificationActionManager?.context)
                .themeData
                .textTheme
                .headline4,
          );
  }

  //Method to show continue Raised Button.
  Widget _completeDeliveryButton() {
    return Container(
      margin: EdgeInsets.only(top: SIZE_40),
      height: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _customerVerificationActionManager?.context,
          percentage: SIZE_6,
          ofWidth: false),
      width: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _customerVerificationActionManager?.context,
          percentage: SIZE_70,
          ofWidth: true),
      child: RaisedGradientButton(
        radious: SIZE_30,
        gradient: LinearGradient(
          begin: Alignment.bottomCenter,
          end: Alignment.topCenter,
          colors: <Color>[COLOR_PRIMARY, COLOR_ACCENT],
        ),
        onPressed: () {
          _customerVerificationActionManager
              ?.actionOnCompleteDeliveryButtonClick(
              scaffoldState: _scaffoldKey?.currentState,
              order: _order,
              otp: _verifyOtpController?.text);
        },
        child: Text(
            AppLocalizations.of(_customerVerificationActionManager.context).customerVerification.text.completeDelivery,
          textAlign: TextAlign.center,
          style: textStyleSize14WithWhiteColor,
        ),
      ),
    );
  }
}
