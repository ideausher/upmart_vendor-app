import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:upmart_driver/modules/orders/api/model/order_listing_response_model.dart';

abstract class OrderDetailsEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final Order order;
  final int status;

  OrderDetailsEvent({
    this.isLoading: false,
    this.context,
    this.status,
    this.order,
  });
}

//this event is used for getting order details
class GetOrderDetailsEvent extends OrderDetailsEvent {
  GetOrderDetailsEvent({
    bool isLoading,
    BuildContext context,
    Order order,
  }) : super(
          isLoading: isLoading,
          context: context,
          order: order,
        );
}

class UpdateOrderDetailEvent extends OrderDetailsEvent {
  UpdateOrderDetailEvent({
    bool isLoading,
    BuildContext context,
    Order order,
  }) : super(
          isLoading: isLoading,
          context: context,
          order: order,
        );
}

//this event is used for order cancel
class UpdateStatusEvent extends OrderDetailsEvent {
  UpdateStatusEvent({
    bool isLoading,
    BuildContext context,
    Order order,
    int status,
  }) : super(
          isLoading: isLoading,
          context: context,
          order: order,
          status: status,
        );
}
