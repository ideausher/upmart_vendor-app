import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:upmart_driver/modules/common/utils/navigator_utils.dart';
import 'package:upmart_driver/modules/common/utils/network_connectivity_utils.dart';
import 'package:upmart_driver/modules/orders/api/model/order_listing_response_model.dart';
import 'package:upmart_driver/modules/orders/sub_module/order_details/bloc/tracking/tracking_bloc.dart';
import 'package:upmart_driver/modules/orders/sub_module/order_details/bloc/tracking/tracking_event.dart';
import 'package:upmart_driver/modules/orders/sub_module/order_details/bloc/tracking/tracking_state.dart';

import '../../../../order_routes.dart';

class TrackingActionManagers {
  BuildContext context;
  TrackingBloc trackingBloc = new TrackingBloc();
  TackingState trackingState;

  //used to perform the action on the init to call delivery boy location and order details api
  actionOnInit({Order order}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then(
      (onValue) {
        if (onValue) {
          trackingBloc.emitEvent(GetOrderDetailsAndDeliveryBoyLocationEvent(
              isLoading: true, context: context, order: order));
        }
      },
    );
  }

  //action on tracking page state change
  actionOnOrderDetailsStateChange({
    ScaffoldState scaffoldState,
  }) {
    WidgetsBinding.instance.addPostFrameCallback(
      (_) {
        if (trackingState?.isLoading == false) {
          //error case
          if (trackingState?.message?.toString()?.trim()?.isNotEmpty == true) {
            DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                context: context,
                scaffoldState: scaffoldState,
                message: trackingState?.message);
            print("order details==> ${trackingState?.message}");
          }
        }
      },
    );
  }

  actionToUpdateCurrentLocation() {
    if (trackingState?.order != null) {
      trackingBloc.emitEvent(GetDeliveryBoyLocationEvent(
          isLoading: true,
          context: context,
          order: trackingState?.order,
          getDeliveryBoyLocationResponseModel:
              trackingState?.getDeliveryLocationResponseModel));
    }
  }

  // action to update order status
  actionOnUpdateStatusOrder({ScaffoldState scaffoldState, int Status}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then(
      (onValue) {
        if (onValue) {
          trackingBloc.emitEvent(UpdateOrderStatusEvent(
              isLoading: true,
              context: context,
              status: Status,
              order: trackingState?.order,
              getDeliveryBoyLocationResponseModel:
                  trackingState?.getDeliveryLocationResponseModel));
        }
      },
    );
  }
  // action to verify number
  actionToVerifyCustomerNumber() async {
    await NavigatorUtils.navigatorUtilsInstance.navigatorPushedNameResult(
        context, OrderRoutes.CUSTOMER_VERIFICATION_PAGE,
        dataToBeSend: trackingState?.order);
    actionOnInit(order: trackingState?.order);
  }
}
