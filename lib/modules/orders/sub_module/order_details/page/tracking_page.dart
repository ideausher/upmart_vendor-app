import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:upmart_driver/localizations.dart';
import 'package:upmart_driver/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:upmart_driver/modules/common/app_config/app_config.dart';
import 'package:upmart_driver/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:upmart_driver/modules/common/constants/color_constants.dart';
import 'package:upmart_driver/modules/common/constants/dimens_constants.dart';
import 'package:upmart_driver/modules/common/theme/app_themes.dart';
import 'package:upmart_driver/modules/common/utils/common_utils.dart';
import 'package:upmart_driver/modules/common/utils/firebase_messaging_utils.dart';
import 'package:upmart_driver/modules/common/utils/navigator_utils.dart';
import 'package:upmart_driver/modules/current_location_updator/manager/current_location_manager.dart';
import 'package:upmart_driver/modules/orders/api/model/order_listing_response_model.dart';
import 'package:upmart_driver/modules/orders/constant/image_constant.dart';
import 'package:upmart_driver/modules/orders/enum/order_enum.dart';
import 'package:upmart_driver/modules/orders/manager/orders_utils_manager.dart';
import 'package:upmart_driver/modules/orders/sub_module/order_details/bloc/tracking/tracking_state.dart';
import 'package:upmart_driver/modules/orders/sub_module/order_details/managers/order_details/order_details_widget_manager.dart';
import 'package:upmart_driver/modules/orders/sub_module/order_details/managers/tracking/tracking_action_manager.dart';
import 'package:upmart_driver/modules/polyline/api/polyline_points_api/model/poly_lat_long_model.dart';
import 'package:upmart_driver/modules/polyline/page/polyline_page.dart';
import 'package:timeline_tile/timeline_tile.dart';

class TrackingPage extends StatefulWidget {
  BuildContext context;

  TrackingPage(this.context);

  @override
  _TrackingPageState createState() => _TrackingPageState();
}

class _TrackingPageState extends State<TrackingPage>
    implements PushReceived, LocationChangeCallBack {
  Order _order;

  //Bloc Variables
  TrackingActionManagers _trackingActionManagers = TrackingActionManagers();

  //Declaration of scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    FirebaseMessagingUtils.firebaseMessagingUtils
        .addCallback(pushReceived: this);
    _order = ModalRoute.of(widget.context).settings.arguments;
    _trackingActionManagers?.context = widget?.context;
    _trackingActionManagers?.actionOnInit(order: _order);
    CurrentLocationManger.locationMangerInstance.addLocationCallback(this);
  }

  @override
  void dispose() {
    super.dispose();
    CurrentLocationManger.locationMangerInstance.removeLocationCallback(this);
    FirebaseMessagingUtils.firebaseMessagingUtils
        .removeCallback(pushReceived: this);
    _trackingActionManagers?.trackingBloc?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocEventStateBuilder<TackingState>(
      bloc: _trackingActionManagers?.trackingBloc,
      builder: (BuildContext context, TackingState trackingState) {
        _trackingActionManagers.context = context;
        List<PolyPointLatLngModel> _wayPoints = List<PolyPointLatLngModel>();
        if (trackingState != null &&
            _trackingActionManagers.trackingState != trackingState) {
          _trackingActionManagers.trackingState = trackingState;
          _trackingActionManagers.actionOnOrderDetailsStateChange(
            scaffoldState: _scaffoldKey?.currentState,
          );

          if (trackingState?.getDeliveryLocationResponseModel?.data?.lat !=
              null) {
            _wayPoints.add(PolyPointLatLngModel(
                longitude: num.parse(
                    trackingState?.getDeliveryLocationResponseModel?.data?.lng),
                latitude: num.parse(trackingState
                    ?.getDeliveryLocationResponseModel?.data?.lat)));

            WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
              PolyLineLatLongMapPage.updatePolyPointsCallback
                  ?.updatePolyDetails(wayPosPoints: _wayPoints);
            });
          }
        }
        return ModalProgressHUD(
            inAsyncCall: trackingState?.isLoading ?? false,
            child: WillPopScope(
              onWillPop: () {
                return NavigatorUtils.navigatorUtilsInstance
                    .navigatorPopScreen(context);
              },
              child: Scaffold(
                key: _scaffoldKey,
                appBar: CommonUtils.commonUtilsInstance.getAppBar(
                    context: context,
                    elevation: ELEVATION_0,
                    appBarTitle:
                        AppLocalizations.of(context).orders.text.trackOrder,
                    appBarTitleStyle: AppConfig.of(context)
                        .themeData
                        .primaryTextTheme
                        .headline3,
                    popScreenOnTapOfLeadingIcon: false,
                    defaultLeadingIcon: Icons.arrow_back,
                    defaultLeadingIconColor: COLOR_BLACK,
                    defaultLeadIconPressed: () {
                      return NavigatorUtils.navigatorUtilsInstance
                          .navigatorPopScreen(context);
                    },
                    actionWidgets: [],
                    backGroundColor: Colors.transparent),
                body: Stack(
                  alignment: Alignment.centerRight,
                  children: [
                    Column(
                      children: [
                        Expanded(
                            child: PolyLineLatLongMapPage(
                          destLatLong: PolyPointLatLngModel(
                              latitude: _trackingActionManagers?.trackingState
                                      ?.order?.deliveryAddress?.latitude ??
                                  _order?.deliveryAddress?.latitude,
                              longitude: _trackingActionManagers?.trackingState
                                      ?.order?.deliveryAddress?.longitude ??
                                  _order?.deliveryAddress?.longitude,
                              address: _trackingActionManagers
                                      ?.trackingState
                                      ?.order
                                      ?.deliveryAddress
                                      ?.formattedAddress ??
                                  _order?.deliveryAddress?.formattedAddress),
                          originLatLong: PolyPointLatLngModel(
                              latitude: double.parse(_trackingActionManagers
                                      ?.trackingState
                                      ?.order
                                      ?.shopDetails
                                      ?.latitude ??
                                  _order?.shopDetails?.latitude),
                              longitude: double.parse(
                                _trackingActionManagers?.trackingState?.order
                                        ?.shopDetails?.longitude ??
                                    _order?.shopDetails?.longitude,
                              ),
                              address: _trackingActionManagers?.trackingState
                                      ?.order?.shopDetails?.address ??
                                  _order?.shopDetails?.address),
                          wayPosPoints: _wayPoints,
                        )),
                        Padding(
                          padding: const EdgeInsets.all(SIZE_10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              _showDetailsView(
                                  title: AppLocalizations.of(context)
                                      .orders
                                      .text
                                      .orderId,
                                  data: (_trackingActionManagers
                                              ?.trackingState?.order ??
                                          _order)
                                      ?.bookingCode,
                                  showDivider: true),
                              _showDetailsView(
                                  title: AppLocalizations.of(context)
                                      .orders
                                      .text
                                      .pickUp,
                                  data: (_trackingActionManagers
                                              ?.trackingState?.order ??
                                          _order)
                                      ?.shopDetails
                                      ?.address,
                                  showDivider: true),
                              _showDetailsView(
                                  title: AppLocalizations.of(context)
                                      .orders
                                      .text
                                      .deliveryAddress,
                                  data: (_trackingActionManagers
                                              ?.trackingState?.order ??
                                          _order)
                                      ?.deliveryAddress
                                      ?.formattedAddress,
                                  showDivider: true),
                              OrderDetailsWidgetManager
                                  .orderDetailsWidgetManager
                                  .showOutletStoreWidget(
                                      context: context,
                                      order: _trackingActionManagers
                                              ?.trackingState?.order ??
                                          _order,
                                      onAccept: () {
                                        _trackingActionManagers
                                            ?.actionOnUpdateStatusOrder(
                                                scaffoldState:
                                                    _scaffoldKey?.currentState,
                                                Status: OrderStatus
                                                    .deliveryBoyAssigned.value);
                                      },
                                      onCancel: () {
                                        _trackingActionManagers
                                            ?.actionOnUpdateStatusOrder(
                                                scaffoldState:
                                                    _scaffoldKey?.currentState,
                                                Status: OrderStatus
                                                    .cancelledByDeliveryBoy
                                                    .value);
                                      },
                                      onStatusChange: () {
                                        if (OrdersUtilsManager
                                                .ordersUtilsManagerInstance
                                                .getStatusChangeMessage(
                                                    order:
                                                        _trackingActionManagers
                                                                ?.trackingState
                                                                ?.order ??
                                                            _order,
                                                    context: context)
                                                ?.item2 ==
                                            OrderStatus
                                                .deliveredProduct.value) {
                                          _trackingActionManagers
                                              ?.actionToVerifyCustomerNumber();
                                        } else {
                                          _trackingActionManagers
                                              ?.actionOnUpdateStatusOrder(
                                                  scaffoldState: _scaffoldKey
                                                      ?.currentState,
                                                  Status: OrdersUtilsManager
                                                      .ordersUtilsManagerInstance
                                                      .getStatusChangeMessage(
                                                          order: _order,
                                                          context: context)
                                                      ?.item2);
                                        }
                                      }),
                            ],
                          ),
                        )
                      ],
                    ),
                    Positioned(
                        right: 0,
                        bottom: CommonUtils.commonUtilsInstance.getPercentageSize(context: context,ofWidth: false,percentage: SIZE_42),
                        child: Container(
                          margin: EdgeInsets.only(right: SIZE_40),
                          child: Image.asset(
                            NAVIGATION_UP_ICON,
                            height: SIZE_60,
                          ),
                        ))
                  ],
                ),
              ),
            ));
      },
    );
  }

  Widget _showDetailsView(
      {String title, String data, bool showDivider = true}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: textStyleSize14LightGreyColor,
        ),
        Text(data),
        (showDivider)
            ? Divider(
                color: COLOR_BORDER_GREY,
              )
            : const SizedBox()
      ],
    );
  }

  @override
  onMessageReceived({NotificationPushModel notificationPushModel}) {
    // _trackingActionManagers?.actionOnInit(order: _order);
  }

  @override
  void onLocationChanged({Position currentLocation}) {
    print(
        "locationchanged --- Track ${PolyLineLatLongMapPage.updatePolyPointsCallback}");
    List<PolyPointLatLngModel> _wayPoints = List<PolyPointLatLngModel>();
    _wayPoints.add(PolyPointLatLngModel(
        longitude: currentLocation?.longitude,
        latitude: currentLocation?.latitude));
    PolyLineLatLongMapPage.updatePolyPointsCallback
        ?.updatePolyDetails(wayPosPoints: _wayPoints);
  }
}
