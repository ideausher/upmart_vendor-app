import 'package:upmart_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:upmart_driver/modules/common/enum/enums.dart';
import 'package:upmart_driver/modules/orders/api/model/order_listing_response_model.dart';
import 'package:upmart_driver/modules/orders/api/provider/orders_listing_provider.dart';
import 'package:upmart_driver/modules/orders/enum/order_enum.dart';

import '../../../../../localizations.dart';
import 'active_orders_event.dart';
import 'active_orders_state.dart';

class ActiveOrdersBloc
    extends BlocEventStateBase<ActiveOrdersEvent, ActiveOrdersState> {
  ActiveOrdersBloc({bool isLoading = false})
      : super(initialState: ActiveOrdersState.initiating(isLoading: isLoading));

  @override
  Stream<ActiveOrdersState> eventHandler(
      ActiveOrdersEvent event, ActiveOrdersState currentState) async* {
    // used for the select tab
    if (event is GetActiveOrdersEvent) {
      yield ActiveOrdersState.updateUi(
        isLoading: true,
        context: event.context,
        page: event.page,
        listOrders: event.listOrders,
      );

      var _result = await OrdersListingProvider().getOrdersApiCall(
          context: event.context,
          page: event.page,
          orderStatus: TabOrderStatus.ACTIVE.value);

      if (_result != null) {
        if (_result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          List<Order> _list;
          // parse result to list
          OrderListingResponseModel _orderListingResponseModel =
              OrderListingResponseModel.fromMap(_result);
          // check if the page is first and event.coupon list is null or empty
          if (event?.listOrders == null ||
              event?.listOrders?.isEmpty == true ||
              event.page == 1) {
            // pass response list t temp value
            _list = _orderListingResponseModel?.orders;
          } else {
            _list = event?.listOrders;

            _list.addAll(_orderListingResponseModel?.orders);
          }

          yield ActiveOrdersState.updateUi(
            isLoading: false,
            context: event.context,
            page: (_orderListingResponseModel?.orders?.isNotEmpty == true)
                ? event.page
                : (event.page > 1)
                    ? event.page - 1
                    : event.page,
            listOrders: _list,
          );
        } else {
          yield ActiveOrdersState.updateUi(
            isLoading: false,
            context: event.context,
            message: _result[ApiStatusParams.Message.value],
            page: (event.page > 1) ? event.page - 1 : event.page,
            listOrders: event.listOrders,
          );
        }
      } else {
        yield ActiveOrdersState.updateUi(
          isLoading: false,
          context: event.context,
          listOrders: event.listOrders,
          message:
              AppLocalizations.of(event.context).common.error.somthingWentWrong,
          page: (event.page > 1) ? event.page - 1 : event.page,
        );
      }
    }
  }
}
