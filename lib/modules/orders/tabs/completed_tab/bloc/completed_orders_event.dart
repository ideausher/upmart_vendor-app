import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:upmart_driver/modules/orders/api/model/order_listing_response_model.dart';

abstract class CompletedOrdersEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final int page;
  final List<Order> listOrders;

  CompletedOrdersEvent({
    this.isLoading: false,
    this.context,
    this.page,
    this.listOrders,
  });
}

// for get orders history
class GetCompletedOrdersEvent extends CompletedOrdersEvent {
  GetCompletedOrdersEvent({
    BuildContext context,
    bool isLoading,
    int page,
    List<Order> listOrders,
  }) : super(
          context: context,
          isLoading: isLoading,
          page: page,
          listOrders: listOrders,
        );
}

// for get orders history
class UpdateOrdersHistoryEvent extends CompletedOrdersEvent {
  UpdateOrdersHistoryEvent({
    BuildContext context,
    bool isLoading,
    int page,
    List<Order> listOrders,
  }) : super(
    context: context,
    isLoading: isLoading,
    page: page,
    listOrders: listOrders,
  );
}