import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:upmart_driver/modules/orders/api/model/order_listing_response_model.dart';


class CompletedOrdersState extends BlocState {
  final bool isLoading; // used to show loader
  final BuildContext context;
  final String message;
  final int page;
  final List<Order> listOrders;

  CompletedOrdersState({
    this.isLoading,
    this.context,
    this.message,
    this.page,
    this.listOrders,
  }) : super(isLoading);

  // not authenticated
  factory CompletedOrdersState.initiating({bool isLoading}) {
    return CompletedOrdersState(
      isLoading: isLoading,
    );
  }

  factory CompletedOrdersState.updateUi({
    bool isLoading,
    BuildContext context,
    String message,
    int page,
    List<Order> listOrders,
  }) {
    return CompletedOrdersState(
      isLoading: isLoading,
      context: context,
      message: message,
      page: page,
      listOrders: listOrders,
    );
  }
}
