import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:upmart_driver/modules/common/utils/navigator_utils.dart';
import 'package:upmart_driver/modules/common/utils/network_connectivity_utils.dart';
import 'package:upmart_driver/modules/orders/api/model/order_listing_response_model.dart';
import 'package:upmart_driver/modules/orders/tabs/completed_tab/bloc/completed_orders_bloc.dart';
import 'package:upmart_driver/modules/orders/tabs/completed_tab/bloc/completed_orders_event.dart';
import 'package:upmart_driver/modules/orders/tabs/completed_tab/bloc/completed_orders_state.dart';

import '../../../../../localizations.dart';
import '../../../../../routes.dart';
import '../../../order_routes.dart';

class CompletedOrdersActionManager {
  BuildContext context;
  CompletedOrdersBloc completedOrdersBloc;
  CompletedOrdersState completedOrdersState;

  //used to perform the action on the init
  void actionOnInit({
    ScaffoldState scaffoldState,
    int page,
  }) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) {
      if (onValue) {
        completedOrdersBloc.emitEvent(GetCompletedOrdersEvent(
          context: context,
          isLoading: true,
          page: page,
          listOrders: completedOrdersState?.listOrders,
        ));
      }
    });
  }

  // used to perform the action on the item tap
  void actionOnItemTap({Order order, ScaffoldState scaffoldState,bool raiseAQuery}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context, showNetworkDialog: true)
        .then((onValue) async {
      if (onValue) {
        if(raiseAQuery ){
         await NavigatorUtils.navigatorUtilsInstance.navigatorPushedNameResult(
              context, Routes.SEND_QUERY_PAGE,
              dataToBeSend: order);
        }else {
         await NavigatorUtils.navigatorUtilsInstance.navigatorPushedName(
            context,
            OrderRoutes.ORDER_DETAILS,
            dataToBeSend: order,
          );
        }
      }
    });
  }

  //used to perform the action on the state change
  void actionOnStateChanged({ScaffoldState scaffoldState}) {
    if (completedOrdersState?.isLoading == false) {
      if (completedOrdersState?.message
          ?.trim()
          ?.isNotEmpty == true) {
        WidgetsBinding.instance.addPostFrameCallback(
              (_) {
            DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                context: context,
                message: completedOrdersState?.message,
                scaffoldState: scaffoldState);
          },
        );
      }
    }
  }


}
