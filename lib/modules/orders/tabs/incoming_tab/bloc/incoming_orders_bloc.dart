import 'package:upmart_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:upmart_driver/modules/common/enum/enums.dart';
import 'package:upmart_driver/modules/orders/api/model/order_listing_response_model.dart';
import 'package:upmart_driver/modules/orders/api/provider/orders_listing_provider.dart';
import 'package:upmart_driver/modules/orders/enum/order_enum.dart';
import 'package:upmart_driver/modules/orders/sub_module/order_details/api/provider/order_detail_provider.dart';

import '../../../../../localizations.dart';
import 'incoming_orders_event.dart';
import 'incoming_orders_state.dart';

class IncomingOrdersBloc
    extends BlocEventStateBase<IncomingOrdersEvent, IncomingOrdersState> {
  IncomingOrdersBloc({bool isLoading = false})
      : super(
            initialState: IncomingOrdersState.initiating(isLoading: isLoading));

  @override
  Stream<IncomingOrdersState> eventHandler(
      IncomingOrdersEvent event, IncomingOrdersState currentState) async* {

    if (event is GetIncomingOrdersEvent) {
      yield IncomingOrdersState.updateUi(
        isLoading: true,
        context: event.context,
        page: event.page,
        listOrders: event.listOrders,
      );

      var _result = await OrdersListingProvider().getOrdersApiCall(
          context: event.context,
          page: event.page,
          orderStatus: TabOrderStatus.INCOMING.value);

      if (_result != null) {
        if (_result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          List<Order> _list;
          // parse result to list
          OrderListingResponseModel _orderListingResponseModel =
              OrderListingResponseModel.fromMap(_result);
          // check if the page is first and event.coupon list is null or empty
          if (event?.listOrders == null ||
              event?.listOrders?.isEmpty == true ||
              event.page == 1) {
            // pass response list t temp value
            _list = _orderListingResponseModel?.orders;
          } else {
            _list = event?.listOrders;

            _list.addAll(_orderListingResponseModel?.orders);
          }

          yield IncomingOrdersState.updateUi(
            isLoading: false,
            context: event.context,
            page: (_orderListingResponseModel?.orders?.isNotEmpty == true)
                ? event.page
                : (event.page > 1)
                    ? event.page - 1
                    : event.page,
            listOrders: _list,
          );
        } else {
          yield IncomingOrdersState.updateUi(
            isLoading: false,
            context: event.context,
            message: _result[ApiStatusParams.Message.value],
            page: (event.page > 1) ? event.page - 1 : event.page,
            listOrders: event.listOrders,
          );
        }
      } else {
        yield IncomingOrdersState.updateUi(
          isLoading: false,
          context: event.context,
          listOrders: event.listOrders,
          message:
              AppLocalizations.of(event.context).common.error.somthingWentWrong,
          page: (event.page > 1) ? event.page - 1 : event.page,
        );
      }
    }

    if (event is UpdateOrdersHistoryEvent) {
      yield IncomingOrdersState.updateUi(
        isLoading: event?.isLoading,
        context: event.context,
        page: event.page,
        listOrders: event.listOrders,
      );
    }

    if (event is AcceptOrRejectOrder) {
      yield IncomingOrdersState.updateUi(
        isLoading: true,
        context: event.context,
        page: event.page,
        listOrders: event.listOrders,
      );

      var _result = await OrderDetailProvider().updateOrderApiCall(
          context: event.context,
          id: event.order?.id,
          status: event?.accept
              ? OrderStatus.deliveryBoyAssigned.value
              : OrderStatus.cancelledByDeliveryBoy.value);

      if (_result != null) {
        // check result status
        if (_result[ApiStatusParams.Status.value] != null &&
            _result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {

          event.listOrders?.removeWhere((element) => element?.id ==  event.order?.id);
          yield IncomingOrdersState.updateUi(
            isLoading: false,
            context: event.context,
            page: event.page,
            listOrders: event.listOrders,
          );
        }
        // failure case
        else {
          yield IncomingOrdersState.updateUi(
            isLoading: false,
            context: event.context,
            page: event.page,
            listOrders: event.listOrders,
            message: _result[ApiStatusParams.Message.value],
          );
        }
      } else {
        yield IncomingOrdersState.updateUi(
          isLoading: false,
          context: event.context,
          page: event.page,
          listOrders: event.listOrders,
          message: AppLocalizations.of(event?.context)
              .common
              .error
              .somethingWentWrong,
        );
      }
    }
  }
}
