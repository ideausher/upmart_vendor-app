import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:upmart_driver/modules/orders/api/model/order_listing_response_model.dart';

abstract class IncomingOrdersEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final int page;
  final List<Order> listOrders;
  final Order order;
  final bool accept;

  IncomingOrdersEvent({
    this.isLoading: false,
    this.context,
    this.page,
    this.order,
    this.accept,
    this.listOrders,
  });
}

// for get orders history
class GetIncomingOrdersEvent extends IncomingOrdersEvent {
  GetIncomingOrdersEvent({
    BuildContext context,
    bool isLoading,
    int page,
    List<Order> listOrders,
  }) : super(
          context: context,
          isLoading: isLoading,
          page: page,
          listOrders: listOrders,
        );
}

// for get orders history
class UpdateOrdersHistoryEvent extends IncomingOrdersEvent {
  UpdateOrdersHistoryEvent({
    BuildContext context,
    bool isLoading,
    int page,
    List<Order> listOrders,
  }) : super(
          context: context,
          isLoading: isLoading,
          page: page,
          listOrders: listOrders,
        );
}

// for get orders history
class AcceptOrRejectOrder extends IncomingOrdersEvent {
  AcceptOrRejectOrder(
      {BuildContext context,
      bool isLoading,
      int page,
      List<Order> listOrders,
      Order order,
      bool accept})
      : super(
          context: context,
          isLoading: isLoading,
          page: page,
          listOrders: listOrders,
          accept: accept,
          order: order,
        );
}
