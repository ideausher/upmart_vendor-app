// To parse this JSON data, do
//
//     final polyPointLatLngModel = polyPointLatLngModelFromJson(jsonString);

import 'dart:convert';

PolyPointLatLngModel polyPointLatLngModelFromJson(String str) => PolyPointLatLngModel.fromJson(json.decode(str));

String polyPointLatLngModelToJson(PolyPointLatLngModel data) => json.encode(data.toJson());

class PolyPointLatLngModel {
  double latitude;
  double longitude;
  String address;

  PolyPointLatLngModel({
    this.latitude,
    this.longitude,
    this.address,
  });

  factory PolyPointLatLngModel.fromJson(Map<String, dynamic> json) => PolyPointLatLngModel(
    latitude: json["latitude"] == null ? null : json["latitude"].toDouble(),
    longitude: json["longitude"] == null ? null : json["longitude"].toDouble(),
    address: json["address"] == null ? null : json["address"],
  );

  Map<String, dynamic> toJson() => {
    "latitude": latitude == null ? null : latitude,
    "longitude": longitude == null ? null : longitude,
    "address": address == null ? null : address,
  };
}
