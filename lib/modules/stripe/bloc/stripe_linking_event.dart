import 'package:flutter/cupertino.dart';
import 'package:upmart_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:upmart_driver/modules/common/model/common_response_model.dart';


abstract class StripeLinkingEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final CommonResponseModel commonResponseModel;
  final bool isStripeTokenAvailableOnServer;
  final String code;

  StripeLinkingEvent({
    this.isLoading,
    this.context,
    this.commonResponseModel,
    this.isStripeTokenAvailableOnServer,
    this.code,
  });
}

// used to check authentication
class StripeLinkingPageLoaderEvent extends StripeLinkingEvent {
  StripeLinkingPageLoaderEvent({
    bool isLoading,
    BuildContext context,
    CommonResponseModel commonResponseModel,
    bool isStripeTokenAvailableOnServer,
  }) : super(
          isLoading: isLoading,
           context: context,
           isStripeTokenAvailableOnServer: isStripeTokenAvailableOnServer,
           commonResponseModel: commonResponseModel
        );
}

class RemoveStripeConnectAlertEvent extends StripeLinkingEvent {
  RemoveStripeConnectAlertEvent({
    bool isLoading,
    BuildContext context,
    CommonResponseModel commonResponseModel,
  }) : super(
          isLoading: isLoading,
          context: context,
        );
}

// used to check authentication
class CheckStripeTokenAvailableOnServerEvent extends StripeLinkingEvent {
  CheckStripeTokenAvailableOnServerEvent({
    bool isLoading,
    BuildContext context,
    bool isStripeTokenAvailableOnServer,
  }) : super(
          isLoading: isLoading,
          context: context,
          isStripeTokenAvailableOnServer: isStripeTokenAvailableOnServer,
        );
}

// used to check authentication
class SendStripeTokenToServerEvent extends StripeLinkingEvent {
  SendStripeTokenToServerEvent({
    bool isLoading,
    BuildContext context,
    bool isStripeTokenAvailableOnServer,
    String code,
  }) : super(
          isLoading: isLoading,
          context: context,
          isStripeTokenAvailableOnServer: isStripeTokenAvailableOnServer,
          code: code,
        );
}
