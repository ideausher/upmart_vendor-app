import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:upmart_driver/localizations.dart';
import 'package:upmart_driver/modules/auth/auth_routes.dart';
import 'package:upmart_driver/modules/common/enum/enums.dart';
import 'package:upmart_driver/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:upmart_driver/modules/common/utils/navigator_utils.dart';

import '../bloc/stripe_linking_state.dart';

class StripeLinkingManager {
  // action performed on state change
  actionPerformedOnStateChanged({BuildContext context, StripeLinkingState stripeState, ScaffoldState scaffoldState}) {
    WidgetsBinding.instance.addPostFrameCallback(
      (_) async {
        if(stripeState?.isLoading == false)
          {
            if (stripeState?.commonResponseModel?.status == ApiStatus.Success.value) {
              print("calling");

              WidgetsBinding.instance.addPostFrameCallback(
                      (_) async {
                    print("response ok called");
                    // when we got status 1 from api
                    //NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context, dataToBeSend: ApiStatus.Success.value);
                    NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);

                    /* DialogSnackBarUtils.dialogSnackBarUtilsInstance.showAlertDialog(
                context: context,
                title: stripeState?.commonResponseModel?.message,
                positiveButton: AppLocalizations.of(context).common.text.ok,
                onPositiveButtonTab: () {
                  NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(context);
                  NavigatorUtils.navigatorUtilsInstance.navigatorClearStack(context, AuthRoutes.HOME_SCREEN);

                });*/

                  });
            } else if (stripeState?.commonResponseModel?.status == ApiStatus.Failure.value) {
              // show snackbar in case of failure
              DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                  context: context, scaffoldState: scaffoldState, message: stripeState?.commonResponseModel?.message);
            }
          }

      },
    );
  }
}
