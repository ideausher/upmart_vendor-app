import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:upmart_driver/localizations.dart';
import 'package:upmart_driver/modules/auth/auth_routes.dart';
import 'package:upmart_driver/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:upmart_driver/modules/common/app_config/app_config.dart';
import 'package:upmart_driver/modules/common/constants/dimens_constants.dart';
import 'package:upmart_driver/modules/common/enum/enums.dart';
import 'package:upmart_driver/modules/common/model/common_response_model.dart';
import 'package:upmart_driver/modules/common/theme/app_themes.dart';
import 'package:upmart_driver/modules/common/utils/common_utils.dart';
import 'package:upmart_driver/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:upmart_driver/modules/common/utils/navigator_utils.dart';
import 'package:upmart_driver/modules/common/utils/network_connectivity_utils.dart';
import 'package:native_webview/native_webview.dart';
import '../bloc/stripe_linking_bloc.dart';
import '../bloc/stripe_linking_event.dart';
import '../bloc/stripe_linking_state.dart';

class StripeLinkingWebViewScreen extends StatefulWidget {
  BuildContext context;

  StripeLinkingWebViewScreen(this.context);

  @override
  _StripeLinkingWebViewScreenState createState() =>
      _StripeLinkingWebViewScreenState();
}

class _StripeLinkingWebViewScreenState
    extends State<StripeLinkingWebViewScreen> {
  Completer<WebViewController> _controller = Completer<WebViewController>();

  //Test
  String _clientId = "ca_Jth3RahEtdHUfcSOdDY01o8zOMwK7EaY";

  //Live
  //  String _clientId = "ca_Jth3RlDEiqYhTotzRU4YFXB2ZrRyX0Ct";

  var _stripeBloc = StripeLinkingBloc(isLoading: true);
  StripeLinkingState _stripeState;

  final _key = UniqueKey();
  var _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isLoading = true;

  // var _firstTimeLoad = true;
  var _context;
  var _webView;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _stripeBloc.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _context = context;

    return Scaffold(
      key: _scaffoldKey,
      appBar: CommonUtils.commonUtilsInstance.getAppBar(
        context: context,
        elevation: ELEVATION_0,
        appBarTitle: AppLocalizations.of(context).stripeAlert.text.appBar,
        appBarTitleStyle: textStyleSize16WithWhiteColor,
        backGroundColor: Colors.black,
        centerTitle: true,
        defaultLeadingIconColor: Colors.white,
      ),
      body: BlocEventStateBuilder<StripeLinkingState>(
        bloc: _stripeBloc,
        builder: (BuildContext context, StripeLinkingState stripeState) {
          if (_stripeState != stripeState) {
            _stripeState = stripeState;
            isLoading = _stripeState?.isLoading ?? isLoading;
            print('is loading value is $isLoading');

            WidgetsBinding.instance.addPostFrameCallback((_) async {
              // action performed when state changed

              if (_stripeState.isLoading == false) {
                if (stripeState?.commonResponseModel?.status ==
                    ApiStatus.Success.value) {
                  DialogSnackBarUtils.dialogSnackBarUtilsInstance
                      .showAlertDialog(
                          context: context,
                          title: AppLocalizations.of(context)
                              .stripe
                              .text
                              .stripeAccount,
                          titleTextStyle: AppConfig.of(_context)
                              .themeData
                              .textTheme
                              .bodyText2,
                          subTitle: stripeState?.commonResponseModel?.message,
                          positiveButton:
                              AppLocalizations.of(context).common.text.ok,
                          onPositiveButtonTab: () {
                            NavigatorUtils.navigatorUtilsInstance
                                .navigatorPopScreen(context);
                            NavigatorUtils.navigatorUtilsInstance
                                .navigatorClearStack(
                                    context, AuthRoutes.HOME_SCREEN_ROOT);
                          });
                } else if (stripeState?.commonResponseModel?.status ==
                    ApiStatus.Failure.value) {
                  // show snackbar in case of failure
                  DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
                      context: context,
                      scaffoldState: _scaffoldKey?.currentState,
                      message: stripeState?.commonResponseModel?.message);
                }
              }
            });
            /* _manager.actionPerformedOnStateChanged(
                context: context,
                scaffoldState: _scaffoldKey.currentState,
                stripeState: stripeState);*/

          }
          _createWebView(_context);

          return Container(
            child: _webView ??
                Container(
                  color: Colors.yellow,
                  height: double.infinity,
                  width: double.infinity,
                ),
            color: Colors.white,
            height: double.infinity,
            width: double.infinity,
          );
        },
      ),
    );
  }

  // used to create webview
  _createWebView(BuildContext context) {
    print("_createWebView");
    _webView = Stack(
      alignment: Alignment.center,
      children: [
        WebView(
          key: _key,
          initialUrl:
              'https://connect.stripe.com/oauth/authorize?response_type=code&'
              'client_id=$_clientId&scope=read_write&redirect_uri=http://iphoneapps.co.in:9074/upmart/admin/public/admin/stripe-connect/connect-stripe-app',
          onWebViewCreated: (WebViewController webViewController) {
            print('Calling on web view created');
            if (!_controller.isCompleted)
              _controller.complete(webViewController);
          },
          onProgressChanged: (controller, data) {
            print('onProgressChanged: $data');
          },
          onPageStarted: (WebViewController controller, String url) {
            print('Page started loading: $url');

            if (Platform.isIOS) {
              if (url.contains("&error_description=")) {
                // show dialog
                DialogSnackBarUtils.dialogSnackBarUtilsInstance.showAlertDialog(
                    context: context,
                    title: AppLocalizations.of(context)
                        .common
                        .error
                        .somethingWentWrong,
                    subTitle: url
                        .split("&error_description=")[1]
                        .replaceAll("+", " "),
                    positiveButton: AppLocalizations.of(context).common.text.ok,
                    onPositiveButtonTab: () {
                      NavigatorUtils.navigatorUtilsInstance
                          .navigatorMayBePopScreen(context);
                    });
                _stripeBloc.emitEvent(StripeLinkingPageLoaderEvent(
                    isLoading: false,
                    commonResponseModel: CommonResponseModel(),
                    isStripeTokenAvailableOnServer: false));
              } else if (url.contains("&code=")) {
                // need to call api

                NetworkConnectionUtils.networkConnectionUtilsInstance
                    .getConnectivityStatus(context, showNetworkDialog: true)
                    .then((value) {
                  if (value) {
                    String code = url.split("&code=")[1];
                    print("$url===${code}");
                    _stripeBloc.emitEvent(SendStripeTokenToServerEvent(
                        isLoading: false, context: widget.context, code: code));
                  }
                });
              }
            }
          },
          onPageFinished: (WebViewController controller, String url) {
            print('Page finished loading: $url');
            isLoading = false;

            if (url.contains("&error_description=")) {
              // show dialog
              DialogSnackBarUtils.dialogSnackBarUtilsInstance.showAlertDialog(
                  context: context,
                  title: AppLocalizations.of(context)
                      .common
                      .error
                      .somethingWentWrong,
                  subTitle:
                      url.split("&error_description=")[1].replaceAll("+", " "),
                  positiveButton: AppLocalizations.of(context).common.text.ok,
                  onPositiveButtonTab: () {
                    NavigatorUtils.navigatorUtilsInstance
                        .navigatorMayBePopScreen(context);
                  });
              _stripeBloc.emitEvent(StripeLinkingPageLoaderEvent(
                  isLoading: false,
                  commonResponseModel: CommonResponseModel(),
                  isStripeTokenAvailableOnServer: false));
            } else if (url.contains("&code=")) {
              // need to call api

              NetworkConnectionUtils.networkConnectionUtilsInstance
                  .getConnectivityStatus(context, showNetworkDialog: true)
                  .then((value) {
                if (value) {
                  String code = url.split("&code=")[1];
                  print("$url===${code}");
                  _stripeBloc.emitEvent(SendStripeTokenToServerEvent(
                      isLoading: false, context: widget.context, code: code));
                }
              });
            } else {
              _stripeBloc.emitEvent(StripeLinkingPageLoaderEvent(
                  isLoading: false,
                  commonResponseModel: CommonResponseModel(),
                  isStripeTokenAvailableOnServer: false));
            }
          },
          gestureNavigationEnabled: true,
          debuggingEnabled: true,
        ),
        isLoading == true
            ? Container(
                decoration:
                    BoxDecoration(color: Colors.transparent.withOpacity(0.5)),
                child: Center(
                  child: CircularProgressIndicator(),
                ))
            : SizedBox()
      ],
    );
  }
}
