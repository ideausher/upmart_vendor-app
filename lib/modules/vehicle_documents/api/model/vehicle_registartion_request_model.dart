// To parse this JSON data, do
//
//     final driverRegistrationRequestModel = driverRegistrationRequestModelFromJson(jsonString);

import 'dart:convert';

DriverRegistrationRequestModel driverRegistrationRequestModelFromJson(String str) => DriverRegistrationRequestModel.fromMap(json.decode(str));

String driverRegistrationRequestModelToJson(DriverRegistrationRequestModel data) => json.encode(data.toMap());

class DriverRegistrationRequestModel {
  DriverRegistrationRequestModel({
    this.vehicleRegisteration,
    this.drivingLicence,
    this.licenceNoPlate,
    this.vehicleInsurance,
    this.status

  });

  VehicleRegData vehicleRegisteration;
  DrivingLicenceData drivingLicence;
  NumberPlateData licenceNoPlate;
  VehicleInsuranceData vehicleInsurance;
  num status;

  factory DriverRegistrationRequestModel.fromMap(Map<String, dynamic> json) => DriverRegistrationRequestModel(
    vehicleRegisteration: json["vehicle_registeration"] == null ? null : VehicleRegData.fromMap(json["vehicle_registeration"]),
    drivingLicence: json["driving_licence"] == null ? null : DrivingLicenceData.fromMap(json["driving_licence"]),
    licenceNoPlate: json["licence_no_plate"] == null ? null : NumberPlateData.fromMap(json["licence_no_plate"]),
    vehicleInsurance: json["vehicle_insurance"] == null ? null : VehicleInsuranceData.fromMap(json["vehicle_insurance"]),
  );

  Map<String, dynamic> toMap() => {
    "vehicle_registeration": vehicleRegisteration == null ? null : vehicleRegisteration.toMap(),
    "driving_licence": drivingLicence == null ? null : drivingLicence.toMap(),
    "licence_no_plate": licenceNoPlate == null ? null : licenceNoPlate.toMap(),
    "vehicle_insurance": vehicleInsurance == null ? null : vehicleInsurance.toMap(),
  };
}

class DrivingLicenceData {
  DrivingLicenceData({
    this.drinvinglicenceImages,
    this.drivinglicenceNo,
    this.drivinglicenceExpiryDate,
  });

  String drinvinglicenceImages;
  String drivinglicenceNo;
  String drivinglicenceExpiryDate;

  factory DrivingLicenceData.fromMap(Map<String, dynamic> json) => DrivingLicenceData(
    drinvinglicenceImages: json["drivinglicence_images"] == null ? null : json["drivinglicence_images"],
    drivinglicenceNo: json["drivinglicence_no"] == null ? null : json["drivinglicence_no"],
    drivinglicenceExpiryDate: json["drivinglicence_expiry_date"] == null ? null : json["drivinglicence_expiry_date"]
  );

  Map<String, dynamic> toMap() => {
    "drivinglicence_images": drinvinglicenceImages == null ? null : drinvinglicenceImages,
    "drivinglicence_no": drivinglicenceNo == null ? null : drivinglicenceNo,
    "drivinglicence_expiry_date": drivinglicenceExpiryDate == null ? null :drivinglicenceExpiryDate,
  };
}


class NumberPlateData {
  NumberPlateData({
    this.licenceNoPlateImages,
    this.noPlate,
  });

  String licenceNoPlateImages;
  String noPlate;

  factory NumberPlateData.fromMap(Map<String, dynamic> json) => NumberPlateData(
    licenceNoPlateImages: json["licence_no_plate_images"] == null ? null : json["licence_no_plate_images"],
    noPlate: json["no_plate"] == null ? null : json["no_plate"],
  );

  Map<String, dynamic> toMap() => {
    "licence_no_plate_images": licenceNoPlateImages == null ? null : licenceNoPlateImages,
    "no_plate": noPlate == null ? null : noPlate,
  };
}

class VehicleInsuranceData {
  VehicleInsuranceData({
    this.vehicleInsurenceImage,
    this.policyNumber,
    this.validFrom,
    this.validTill
  });

  String vehicleInsurenceImage;
  String policyNumber;
  String validFrom;
  String validTill;

  factory VehicleInsuranceData.fromMap(Map<String, dynamic> json) => VehicleInsuranceData(
    vehicleInsurenceImage: json["vehicle_insurence_image"] == null ? null : json["vehicle_insurence_image"],
    policyNumber: json["policy_number"] == null ? null : json["policy_number"],
    validFrom: json["vehicle_insurence_valid_from"] == null ? null : json["vehicle_insurence_valid_from"],
    validTill: json["vehicle_insurence_valid_upto"] == null ? null : json["vehicle_insurence_valid_upto"],
  );

  Map<String, dynamic> toMap() => {
    "vehicle_insurence_image": vehicleInsurenceImage == null ? null : vehicleInsurenceImage,
    "policy_number": policyNumber == null ? null : policyNumber,
    "vehicle_insurence_valid_from": validFrom == null ? null : validFrom,
    "vehicle_insurence_valid_upto": validTill == null ? null : validTill,
  };
}

class VehicleRegData {
  VehicleRegData({
    this.registerationNumber,
    this.vehiclePicture,
    this.vehicleCompany,
    this.model,
    this.color,
    this.validUpto,
  });

  String registerationNumber;
  String vehiclePicture;
  String vehicleCompany;
  String model;
  String color;
  String validUpto;

  factory VehicleRegData.fromMap(Map<String, dynamic> json) => VehicleRegData(
    registerationNumber: json["registeration_number"] == null ? null : json["registeration_number"],
    vehiclePicture: json["vehicle_picture"] == null ? null : json["vehicle_picture"],
    vehicleCompany: json["vehicle_company"] == null ? null : json["vehicle_company"],
    model: json["model"] == null ? null : json["model"],
    color: json["color"] == null ? null : json["color"],
    validUpto: json["valid_upto"] == null ? null : json["valid_upto"],
  );

  Map<String, dynamic> toMap() => {
    "registeration_number": registerationNumber == null ? null : registerationNumber,
    "vehicle_picture": vehiclePicture == null ? null : vehiclePicture,
    "vehicle_company": vehicleCompany == null ? null : vehicleCompany,
    "model": model == null ? null : model,
    "color": color == null ? null : color,
    "valid_upto": validUpto == null ? null : validUpto,
  };
}
