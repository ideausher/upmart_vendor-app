import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/common/app_config/app_config.dart';
import 'package:upmart_driver/modules/common/utils/driver_vehicle_details_utils.dart';
import 'package:upmart_driver/modules/vehicle_documents/api/model/vehicle_registartion_request_model.dart';


class VehicleRegistrationProvider {
  Future<dynamic> registerDriverVehicle({BuildContext context,DriverRegistrationRequestModel driverRegistrationRequestModel}) async {
    var path = "deliveryboydoc";
    print('the request is ${driverRegistrationRequestModelToJson(driverRegistrationRequestModel)} ');

    var result = await AppConfig.of(context).baseApi.postRequest(
      path,
      context,
      data: driverRegistrationRequestModelToJson(driverRegistrationRequestModel),
    );
    return result;
  }


  Future<dynamic> getDriverDocStatus({BuildContext context,DriverRegistrationRequestModel driverRegistrationRequestModel}) async {
    var path = "getdocument";

    var result = await AppConfig.of(context).baseApi.getRequest(
      path,
      context,);
    return result;
  }

  // upload profile pic
  Future<dynamic> uploadImage({BuildContext context, File file}) async {
    FormData formData = FormData.fromMap({"image": await MultipartFile.fromFile(file.path)});
    var path = "upload-image";
    var result = await AppConfig
        .of(context)
        .baseApi
        .postRequest(path, context, data: formData);
    return result;
  }
}