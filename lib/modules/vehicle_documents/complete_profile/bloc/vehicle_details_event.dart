import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:upmart_driver/modules/vehicle_documents/api/model/vehicle_registration_response_model.dart';

abstract class GetVehicleDetailsEvent extends BlocEvent {
  final bool isLoading;
  final BuildContext context;
  final VehicleRegistrationResponseModel driverRegistrationResponseModel;

  GetVehicleDetailsEvent({this.isLoading: false, this.context, this.driverRegistrationResponseModel});
}

//this event is used for getting current location data
class VehicleDetailsEvent extends GetVehicleDetailsEvent {
  VehicleDetailsEvent({
    bool isLoading,
    BuildContext context,
    VehicleRegistrationResponseModel driverRegistrationResponseModel,
  }) : super(
          isLoading: isLoading,
          context: context,
          driverRegistrationResponseModel: driverRegistrationResponseModel,
        );
}
