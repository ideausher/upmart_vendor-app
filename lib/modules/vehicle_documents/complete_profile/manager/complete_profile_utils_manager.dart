import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:upmart_driver/localizations.dart';
import 'package:upmart_driver/modules/auth/enums/auth_enums.dart';
import 'package:upmart_driver/modules/common/constants/color_constants.dart';
import 'package:upmart_driver/modules/vehicle_documents/api/model/vehicle_registration_response_model.dart';
import 'package:upmart_driver/modules/vehicle_documents/complete_profile/bloc/vehicle_details_state.dart';

class CompleteProfileUtilsManager {
  BuildContext context;

  CompleteProfileUtilsManager({this.context});

  //method to show date picke
  Future<String> getDatePicker() async {
    var arr;
    final picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime.now(),
        lastDate: DateTime(2500),
        builder: (BuildContext context, Widget child) {
          return Theme(
            data: ThemeData.light().copyWith(
              primaryColor: COLOR_PRIMARY,
              accentColor: COLOR_PRIMARY,
              colorScheme: ColorScheme.light(primary: COLOR_PRIMARY),
              dialogBackgroundColor: Colors.white,
            ),
            child: child,
          );
        });
    if (picked != null && picked != DateTime.now()) {
      arr = picked.toString().split(' ');
    }
    return arr[0];
  }

  //set data to vehicle registration controllers
  setDataToVehicleRegistrationController(
      {TextEditingController registrationController,
      // VehicleRegistrationActionManagers vehicleRegistrationActionManagers,
      TextEditingController modelController,
      ScaffoldState scaffoldState,
      // VehicleRegistrationState vehicleRegistrationState,
      TextEditingController vehicleController,
      TextEditingController validController,
      TextEditingController colorController,
      VehicleRegistrationResponseModel driverRegistrationResponseModel}) {
    registrationController.text = driverRegistrationResponseModel
            ?.data?.vehicleRegisteration?.registerationNumber ??
        "";
    vehicleController.text = driverRegistrationResponseModel
            ?.data?.vehicleRegisteration?.vehicleCompany ??
        "";
    modelController.text =
        driverRegistrationResponseModel?.data?.vehicleRegisteration?.model ??
            "";
    colorController.text =
        driverRegistrationResponseModel?.data?.vehicleRegisteration?.color ??
            "";
    validController.text = driverRegistrationResponseModel
            ?.data?.vehicleRegisteration?.validUpto ??
        "";
  }

  //set data to licence details controllers
  setDataToVehicleLicenceControllers(
      {TextEditingController licenceContorller,
      TextEditingController expiryController,
      VehicleRegistrationResponseModel driverRegistrationResponseModel}) {
    licenceContorller.text = driverRegistrationResponseModel
            ?.data?.drivingLicence?.drivingLicenceNo ??
        "";
    expiryController.text = driverRegistrationResponseModel
            ?.data?.drivingLicence?.drivingLicenceExpiryDate ??
        "";
  }

  //method to get vehicle registration heading text
  String getHeadingText({GetVehicleDetailsState getVehicleDetailsState}) {
    if (getVehicleDetailsState
            ?.driverRegistrationResponseModel?.data?.action?.status ==
        DocumentStatus?.Pending?.value) {
      return AppLocalizations.of(context)
          .completeProfile
          .error
          .verificationPending;
    } else if (getVehicleDetailsState
            ?.driverRegistrationResponseModel?.data?.action?.status ==
        DocumentStatus?.Rejected?.value) {
      return AppLocalizations.of(context)
          .completeProfile
          .error
          .documentRejected;
    } else {
      return AppLocalizations.of(context)
          .completeProfile
          .text
          .completeProfileToGetOrders;
    }
  }

  //method to get vehicle registration sub heading text
  String getSubtitleText({GetVehicleDetailsState getVehicleDetailsState}) {
    if (getVehicleDetailsState
            ?.driverRegistrationResponseModel?.data?.action?.status ==
        DocumentStatus?.Pending?.value) {
      return AppLocalizations.of(context)
          .completeProfile
          .text
          .waitFordocumentVerification;
    } else if (getVehicleDetailsState
            ?.driverRegistrationResponseModel?.data?.action?.status ==
        DocumentStatus?.Rejected?.value) {
      return AppLocalizations.of(context).completeProfile.text.uploadDocAgain;
    } else {
      return AppLocalizations.of(context).completeProfile.text.receiveOrderText;
    }
  }
}
