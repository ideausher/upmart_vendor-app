import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:upmart_driver/localizations.dart';
import 'package:upmart_driver/modules/auth/auth_routes.dart';
import 'package:upmart_driver/modules/auth/constants/image_constant.dart';
import 'package:upmart_driver/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:upmart_driver/modules/common/app_config/app_config.dart';
import 'package:upmart_driver/modules/common/common_widget/common_image_with_text.dart';
import 'package:upmart_driver/modules/common/common_widget/custom_raised_gradient_button.dart';
import 'package:upmart_driver/modules/common/common_widget/step_bar_indicator.dart';
import 'package:upmart_driver/modules/common/constants/color_constants.dart';
import 'package:upmart_driver/modules/common/constants/dimens_constants.dart';
import 'package:upmart_driver/modules/common/theme/app_themes.dart';
import 'package:upmart_driver/modules/common/utils/common_utils.dart';
import 'package:upmart_driver/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:upmart_driver/modules/common/utils/navigator_utils.dart';
import 'package:upmart_driver/modules/notification/widget/notification_unread_count_widget.dart';
import 'package:upmart_driver/modules/orders/constant/image_constant.dart';

import 'package:upmart_driver/modules/vehicle_documents/api/model/vehicle_registartion_request_model.dart';
import 'package:upmart_driver/modules/vehicle_documents/complete_profile/bloc/vehicle_details_bloc.dart';
import 'package:upmart_driver/modules/vehicle_documents/complete_profile/bloc/vehicle_details_state.dart';
import 'package:upmart_driver/modules/vehicle_documents/complete_profile/manager/complete_profile_action_manager.dart';
import 'package:upmart_driver/modules/vehicle_documents/complete_profile/manager/complete_profile_utils_manager.dart';

import 'package:upmart_driver/modules/vehicle_documents/constants/image_constants.dart';
import 'package:upmart_driver/modules/vehicle_documents/constants/string_constant.dart';

import 'package:timeline_tile/timeline_tile.dart';

class CompleteProfilePage extends StatefulWidget {
  BuildContext context;
  String isComingFrom;

  CompleteProfilePage(this.context) {
    isComingFrom = ModalRoute.of(context).settings.arguments;
  }

  @override
  _CompleteProfilePageState createState() => _CompleteProfilePageState();
}

class _CompleteProfilePageState extends State<CompleteProfilePage> {
  //Bloc Declaration
  GetVehicleDetailsBloc _getVehicleDetailsBloc;

  //Managers
  CompleteProfileActionManager _vehicleRegistrationActionManagers =
      new CompleteProfileActionManager();
  CompleteProfileUtilsManager _vehicleRegistrationUtilsManager;
  DriverRegistrationRequestModel driverRegistrationRequestModel;

  //Declaration of scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  bool _networkAvailable = true;

  @override
  void dispose() {
    super.dispose();
    _vehicleRegistrationActionManagers?.getVehicleDetailsBloc?.dispose();
  }

  @override
  void initState() {
    super.initState();
    _getVehicleDetailsBloc = new GetVehicleDetailsBloc();
    _vehicleRegistrationActionManagers.context = widget?.context;
    _vehicleRegistrationActionManagers.getVehicleDetailsBloc =
        _getVehicleDetailsBloc;

    _vehicleRegistrationUtilsManager =
        new CompleteProfileUtilsManager(context: widget?.context);

    _vehicleRegistrationActionManagers.actionOnInit().then((value) {
      if (value == false) {
        _networkAvailable = false;
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    _vehicleRegistrationActionManagers.context = context;
    return BlocEventStateBuilder<GetVehicleDetailsState>(
      bloc: _vehicleRegistrationActionManagers?.getVehicleDetailsBloc,
      builder: (BuildContext context, GetVehicleDetailsState getVehicleState) {
        _vehicleRegistrationActionManagers.context = context;
        if (getVehicleState != null &&
            _vehicleRegistrationActionManagers?.getVehicleDetailsState !=
                getVehicleState) {
          _vehicleRegistrationActionManagers?.getVehicleDetailsState =
              getVehicleState;

          _vehicleRegistrationActionManagers?.actionVehicleDetailsState(
              context: _vehicleRegistrationActionManagers.context,
              scaffoldState: _scaffoldKey?.currentState,
              getVehicleDetailsState:
                  _vehicleRegistrationActionManagers?.getVehicleDetailsState,
              getVehicleDetailsBloc:
                  _vehicleRegistrationActionManagers?.getVehicleDetailsBloc);
        }
        return ModalProgressHUD(
          inAsyncCall: getVehicleState?.isLoading ?? false,
          child: Scaffold(
            key: _scaffoldKey,
            appBar: _showAppBar(),
            body: Visibility(
              visible: getVehicleState?.isLoading == false &&
                  _networkAvailable == true,
              child: Padding(
                padding: const EdgeInsets.all(SIZE_10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // _showTitleText(),
                    _showDivider(),
                    _showSpace(SIZE_10),
                    _completeProfileText(),
                    _completeToReceiveOrder(),
                    _showSpace(SIZE_20),
                    _showDivider(),
                    _showSpace(SIZE_20),
                    _showStepBar(),
                    _showContinueButton()
                  ],
                ),
              ),
              replacement: _networkAvailable == true
                  ? const SizedBox()
                  : Center(
                      child: CommonImageWithTextWidget(
                        context: context,
                        localImagePath: NO_INTERNET_ICON,
                        isSvgImage: false,
                        imageHeight: SIZE_28,
                        imageWidth: SIZE_50,
                        textToShow: AppLocalizations.of(
                                _vehicleRegistrationActionManagers.context)
                            .completeProfile
                            .text
                            .youAreOffline,
                      ),
                    ),
            ),
          ),
        );
      },
    );
  }

  // //method to show title text
  // Widget _showTitleText() {
  //   return Text(
  //       AppLocalizations.of(_vehicleRegistrationActionManagers.context)
  //           .completeProfile
  //           .text
  //           .title,
  //       style: AppConfig.of(context).themeData.textTheme.headline6);
  // }

//method to show complete profile text
  Widget _completeProfileText() {
    return Text(
      _getHeadingText() ?? "",
      style: AppConfig.of(_vehicleRegistrationActionManagers.context)
          .themeData
          .primaryTextTheme
          .headline6,
    );
  }

  //method to show receive order text
  Widget _completeToReceiveOrder() {
    return Text(_getSubtitleText() ?? "",
        style: AppConfig.of(_vehicleRegistrationActionManagers.context)
            .themeData
            .primaryTextTheme
            .headline1);
  }

  //method to show divider text
  Widget _showDivider() {
    return Divider(
      color: COLOR_BORDER_GREY,
    );
  }

  //method to show app bar
  Widget _showAppBar() {
    return CommonUtils.commonUtilsInstance.getAppBar(
        context: context,
        elevation: 0,
        backGroundColor: Colors.white,
        popScreenOnTapOfLeadingIcon: true,
        actionWidgets: [
          Padding(
            padding: const EdgeInsets.only(right: SIZE_16, top: SIZE_16),
            child: NotificationReadCountWidget(
              context: context,
            ),
          )
        ],
        appBarTitle: AppLocalizations.of(
            _vehicleRegistrationActionManagers.context)
            .completeProfile
            .text
            .profileCompletion,
        centerTitle: false,
        appBarTitleStyle:
            AppConfig.of(_vehicleRegistrationActionManagers.context)
                .themeData
                .primaryTextTheme
                .headline3,
        leadingWidget: Container(
          padding: EdgeInsets.all(SIZE_10),
          child: InkWell(
              onTap: () {
                NavigatorUtils.navigatorUtilsInstance.navigatorPushedName(
                    context, AuthRoutes.PROFILE_SCREEN_ROOT);
              },
              child: Padding(
                padding: const EdgeInsets.all(SIZE_8),
                child: Image.asset(
                  DRAWER_ICON,
                  height: SIZE_20,
                  width: SIZE_20,
                ),
              )),
        ));
  }

  //complete profile button
  Widget _showContinueButton() {
    return Expanded(
      flex: 50,
      child: Align(
        alignment: Alignment.center,
        child: Container(
          height: CommonUtils.commonUtilsInstance.getPercentageSize(
              context: context, percentage: SIZE_6, ofWidth: false),
          width: CommonUtils.commonUtilsInstance.getPercentageSize(
              context: context, percentage: SIZE_70, ofWidth: true),
          child: RaisedGradientButton(
            radious: SIZE_30,
            gradient: LinearGradient(
              begin: Alignment.bottomCenter,
              end: Alignment.topCenter,
              colors: <Color>[COLOR_PRIMARY, COLOR_ACCENT],
            ),
            onPressed: () {
              _navigateUserOnTheBasisOfStatus();
            },
            child: Text(
              AppLocalizations.of(
                  _vehicleRegistrationActionManagers.context)
                  .completeProfile
                  .button
                  .completeProfile,
              textAlign: TextAlign.center,
              style: textStyleSize14WithWhiteColor,
            ),
          ),

        ),
      ),
    );
  }

  //show step bar widget
  Widget _showStepBar() {
    return Container(
      margin: EdgeInsets.only(left: SIZE_20, top: SIZE_10),
      alignment: Alignment.centerLeft,
      child: Column(
        children: <Widget>[
          (widget?.isComingFrom == HOME)
              ? TimelineTile(
                  alignment: TimelineAlign.start,
                  isFirst: true,
                  indicatorStyle: const IndicatorStyle(
                      width: SIZE_20,
                      drawGap: true,
                      color: COLOR_LIGHT_GREY,
                      indicator: IndicatorExample(color: COLOR_BLACK)),
                  beforeLineStyle: const LineStyle(
                    color: COLOR_LIGHT_GREY,
                    thickness: 1,
                  ),
                  endChild: Padding(
                    padding: const EdgeInsets.only(left: SIZE_10),
                    child: Text(AppLocalizations.of(
                            _vehicleRegistrationActionManagers.context)
                        .driverDetails
                        .text
                        .driverDetails),
                  ))
              : TimelineTile(
                  alignment: TimelineAlign.start,
                  isFirst: true,
                  indicatorStyle: const IndicatorStyle(
                    width: SIZE_20,
                    drawGap: true,
                    color: COLOR_LIGHT_GREY,
                  ),
                  beforeLineStyle: const LineStyle(
                    color: COLOR_LIGHT_GREY,
                    thickness: 1,
                  ),
                  endChild: Padding(
                    padding: const EdgeInsets.only(left: SIZE_10),
                    child: Text(AppLocalizations.of(
                            _vehicleRegistrationActionManagers.context)
                        .completeProfile
                        .text
                        .detailsOfDriver),
                  )),
          (widget?.isComingFrom == HOME)
              ? TimelineTile(
                  isLast: true,
                  alignment: TimelineAlign.start,
                  beforeLineStyle: const LineStyle(
                    color: COLOR_LIGHT_GREY,
                    thickness: 1,
                  ),
                  indicatorStyle: const IndicatorStyle(
                      width: SIZE_20,
                      color: COLOR_LIGHT_GREY,
                      drawGap: true,
                      indicator: IndicatorExample(color: COLOR_LIGHT_GREY)),
                  endChild: Container(
                    padding: EdgeInsets.only(left: SIZE_10),
                    alignment: Alignment.centerLeft,
                    height: 180,
                    child: Text(
                      AppLocalizations.of(
                              _vehicleRegistrationActionManagers.context)
                          .completeProfile
                          .text
                          .bankAccountDetails,
                      style: (widget?.isComingFrom == HOME)
                          ? TextStyle(color: COLOR_BORDER_GREY)
                          : TextStyle(color: COLOR_BORDER_GREY),
                    ),
                  ))
              : TimelineTile(
                  isLast: true,
                  alignment: TimelineAlign.start,
                  beforeLineStyle: const LineStyle(
                    color: COLOR_LIGHT_GREY,
                    thickness: 1,
                  ),
                  indicatorStyle: const IndicatorStyle(
                      width: SIZE_20,
                      color: COLOR_LIGHT_GREY,
                      drawGap: true,
                      indicator: IndicatorExample(color: COLOR_LIGHT_GREY)),
                  endChild: Container(
                    padding: EdgeInsets.only(left: SIZE_10),
                    alignment: Alignment.centerLeft,
                    height: 180,
                    child: Text(
                      AppLocalizations.of(
                              _vehicleRegistrationActionManagers.context)
                          .completeProfile
                          .text
                          .bankAccountDetails,
                      style: (widget?.isComingFrom == HOME)
                          ? TextStyle(color: COLOR_BORDER_GREY)
                          : TextStyle(color: COLOR_BORDER_GREY),
                    ),
                  )),
        ],
      ),
    );
  }

  //show space
  Widget _showSpace(space) {
    return SizedBox(
      height: space,
    );
  }

  //navigate to next screen
  void _navigateUserOnTheBasisOfStatus() {
    _vehicleRegistrationActionManagers?.navigateUserToNextScreen(
        comingFrom: widget?.isComingFrom,
        driverRegistrationRequestModel: driverRegistrationRequestModel,
        getVehicleDetailsState:
            _vehicleRegistrationActionManagers?.getVehicleDetailsState,
        scaffoldState: _scaffoldKey?.currentState);
  }

  //method to get heading text
  String _getHeadingText() {
    return _vehicleRegistrationUtilsManager?.getHeadingText(
        getVehicleDetailsState:
            _vehicleRegistrationActionManagers?.getVehicleDetailsState);
  }

  //method to get sub heading text
  String _getSubtitleText() {
    return _vehicleRegistrationUtilsManager?.getSubtitleText(
        getVehicleDetailsState:
            _vehicleRegistrationActionManagers?.getVehicleDetailsState);
  }
}
