import 'package:upmart_driver/localizations.dart';
import 'package:upmart_driver/modules/auth/enums/auth_enums.dart';
import 'package:upmart_driver/modules/common/enum/enums.dart';
import 'package:upmart_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:upmart_driver/modules/vehicle_documents/api/model/vehicle_registration_response_model.dart';
import 'package:upmart_driver/modules/vehicle_documents/api/provider/vehicle_registration_api_provider.dart';
import 'package:upmart_driver/modules/vehicle_documents/driver_details/bloc/driver_details_event.dart';
import 'package:upmart_driver/modules/vehicle_documents/driver_details/bloc/driver_details_state.dart';

class MyVehicleDetailsBloc
    extends BlocEventStateBase<MyVehicleDetailsEvent, MyVehicleDetailsState> {
  MyVehicleDetailsBloc({bool initializing = true})
      : super(initialState: MyVehicleDetailsState.initiating());

  @override
  Stream<MyVehicleDetailsState> eventHandler(
      MyVehicleDetailsEvent event, MyVehicleDetailsState currentState) async* {
    if (event is DetailsEvent) {
      String message = "";
      VehicleRegistrationResponseModel driverRegistrationResponseModel;

      yield MyVehicleDetailsState.getVehicleDetails(
        isLoading: event?.isLoading,
        context: event?.context,
        message: message,
      );

      //<-----------------api calling vehicle registration-------------->
      var result = await VehicleRegistrationProvider().getDriverDocStatus(
        context: event.context,
      );

      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          // parse value
          driverRegistrationResponseModel =
              VehicleRegistrationResponseModel.fromMap(result);
        }
        // failure case
        else {
          if (result[ApiStatusParams.Status.value] != null &&
              result[ApiStatusParams.Status.value] ==
                  ApiStatus.NotFound.value) {
            driverRegistrationResponseModel =
                new VehicleRegistrationResponseModel(
                    data: new Data(
                        vehicleInsurance: VehicleInsurance(
                            vehicleInsurenceStatus:
                                DocumentStatus.NotUploaded.value),
                        licenceNoPlate: LicenceNoPlate(
                            licenceNoPlateStatus:
                                DocumentStatus.NotUploaded.value),
                        drivingLicence: DrivingLicence(
                            drivingLicenceStatus:
                                DocumentStatus.NotUploaded.value),
                        vehicleRegisteration: VehicleRegisteration(
                            vehicleStatus: DocumentStatus.NotUploaded.value)));
          }
          message = result[ApiStatusParams.Message.value];
        }
      } else {
        message =
            AppLocalizations.of(event?.context).common.error.somethingWentWrong;
      }
      yield MyVehicleDetailsState.getVehicleDetails(
          isLoading: false,
          context: event?.context,
          message: message,
          driverRegistrationResponseModel: driverRegistrationResponseModel);
    }
  }
}
