enum DriverDocumentTypeEnum {
  vehicleRegisteration,
  drivingLicence,
  vehicleInsurance,
  licenceNumberPlate
}

extension DriverDocumentTypeExtension on DriverDocumentTypeEnum {
  int get value {
    switch (this) {
      case DriverDocumentTypeEnum.vehicleRegisteration:
        return 0;
      case DriverDocumentTypeEnum.drivingLicence:
        return 1;
      case DriverDocumentTypeEnum.licenceNumberPlate:
        return 2;
      case DriverDocumentTypeEnum.vehicleInsurance:
        return 3;
      default:
        return null;
    }
  }
}
