import 'package:flutter/material.dart';
import 'package:upmart_driver/localizations.dart';
import 'package:upmart_driver/modules/auth/enums/auth_enums.dart';
import 'package:upmart_driver/modules/common/theme/app_themes.dart';
import 'package:upmart_driver/modules/vehicle_documents/constants/string_constant.dart';
import 'package:upmart_driver/modules/vehicle_documents/driver_details/enums/driver_document_type_enums.dart';

import 'driver_detail_action_manager.dart';

class DriverDetailUtillManager {

  static DriverDetailUtillManager _driverDetailUtillManager = DriverDetailUtillManager();

  static DriverDetailUtillManager get driverDetailUtillInstance => _driverDetailUtillManager;
  //To get the document status (not uploaded, pending, completed or rejected)
  TextSpan getStatusValue({
    int index,
    DriverDetailActionManagers driverDetailActionManagers,
  }) {
    //Vehicle Registration
    if (index == DriverDocumentTypeEnum?.vehicleRegisteration?.value) {
      if (driverDetailActionManagers
              ?.myVehicleDetailsState
              ?.vehicleRegistrationResponseModel
              ?.data
              ?.vehicleRegisteration
              ?.vehicleStatus ==
          DocumentStatus?.Pending?.value) {
        return TextSpan(
          text: ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.pending})',
          style: textStyleSize14WithRED500,
        );
      } else if (driverDetailActionManagers
              ?.myVehicleDetailsState
              ?.vehicleRegistrationResponseModel
              ?.data
              ?.vehicleRegisteration
              ?.vehicleStatus ==
          DocumentStatus?.Approved?.value) {
        return TextSpan(
          text: ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.completed})',
          style: textStyleSize14GREEN500,
        );
      } else if (driverDetailActionManagers
              ?.myVehicleDetailsState
              ?.vehicleRegistrationResponseModel
              ?.data
              ?.vehicleRegisteration
              ?.vehicleStatus ==
          DocumentStatus?.Rejected?.value) {
        return TextSpan(
          text: ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.rejected})',
          style: textStyleSize14WithRED500,
        );
      } else {
        return TextSpan(
          text: ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.notUploaded})',
          style: textStyleSize14WithRED500,
        );
      }
    }
    //Driving Licence
    else if (index == DriverDocumentTypeEnum?.drivingLicence?.value) {
      if (driverDetailActionManagers
              ?.myVehicleDetailsState
              ?.vehicleRegistrationResponseModel
              ?.data
              ?.drivingLicence
              ?.drivingLicenceStatus ==
          DocumentStatus?.Pending?.value) {
        return TextSpan(
          text: ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.pending})',
          style: textStyleSize14WithRED500,
        );
      } else if (driverDetailActionManagers
              ?.myVehicleDetailsState
              ?.vehicleRegistrationResponseModel
              ?.data
              ?.drivingLicence
              ?.drivingLicenceStatus ==
          DocumentStatus?.Approved?.value) {
        return TextSpan(
          text: ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.completed})',
          style: textStyleSize14GREEN500,
        );
      } else if (driverDetailActionManagers
              ?.myVehicleDetailsState
              ?.vehicleRegistrationResponseModel
              ?.data
              ?.drivingLicence
              ?.drivingLicenceStatus ==
          DocumentStatus?.Rejected?.value) {
        return TextSpan(
          text: ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.rejected})',
          style: textStyleSize14WithRED500,
        );
      } else {
        return TextSpan(
          text: ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.notUploaded})',
          style: textStyleSize14WithRED500,
        );
      }
    }
    //Licence number plate
    else if (index == DriverDocumentTypeEnum?.licenceNumberPlate?.value) {
      if (driverDetailActionManagers
              ?.myVehicleDetailsState
              ?.vehicleRegistrationResponseModel
              ?.data
              ?.licenceNoPlate
              ?.licenceNoPlateStatus ==
          DocumentStatus?.Pending?.value) {
        return TextSpan(
          text: ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.pending})',
          style: textStyleSize14WithRED500,
        );
      } else if (driverDetailActionManagers
              ?.myVehicleDetailsState
              ?.vehicleRegistrationResponseModel
              ?.data
              ?.licenceNoPlate
              ?.licenceNoPlateStatus ==
          DocumentStatus?.Approved?.value) {
        return TextSpan(
          text: ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.completed})',
          style: textStyleSize14GREEN500,
        );
      } else if (driverDetailActionManagers
              ?.myVehicleDetailsState
              ?.vehicleRegistrationResponseModel
              ?.data
              ?.licenceNoPlate
              ?.licenceNoPlateStatus ==
          DocumentStatus?.Rejected?.value) {
        return TextSpan(
          text: ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.rejected})',
          style: textStyleSize14WithRED500,
        );
      } else {
        return TextSpan(
          text: ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.notUploaded})',
          style: textStyleSize14WithRED500,
        );
      }
    }
    //Vehicle Insurance
    else {
      if (driverDetailActionManagers
              ?.myVehicleDetailsState
              ?.vehicleRegistrationResponseModel
              ?.data
              ?.vehicleInsurance
              ?.vehicleInsurenceStatus ==
          DocumentStatus?.Pending?.value) {
        return TextSpan(
          text: ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.pending})',
          style: textStyleSize14WithRED500,
        );
      } else if (driverDetailActionManagers
              ?.myVehicleDetailsState
              ?.vehicleRegistrationResponseModel
              ?.data
              ?.vehicleInsurance
              ?.vehicleInsurenceStatus ==
          DocumentStatus?.Approved?.value) {
        return TextSpan(
          text: ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.completed})',
          style: textStyleSize14GREEN500,
        );
      } else if (driverDetailActionManagers
              ?.myVehicleDetailsState
              ?.vehicleRegistrationResponseModel
              ?.data
              ?.vehicleInsurance
              ?.vehicleInsurenceStatus ==
          DocumentStatus?.Rejected?.value) {
        return TextSpan(
          text: ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.rejected})',
          style: textStyleSize14WithRED500,
        );
      } else {
        return TextSpan(
          text: ' (${AppLocalizations.of(driverDetailActionManagers.context).driverDetails.text.notUploaded})',
          style: textStyleSize14WithRED500,
        );
      }
    }
  }

  //Check need to show proceed button or not
  bool showProceedButton(
      {DriverDetailActionManagers driverDetailActionManagers,
      String comeFrom}) {
    bool showProceedButton = false;
    if (comeFrom == PROFILE) {
      showProceedButton = false;
    } else {
      if (driverDetailActionManagers
                  ?.myVehicleDetailsState
                  ?.vehicleRegistrationResponseModel
                  ?.data
                  ?.vehicleRegisteration ==
              null ||
          driverDetailActionManagers?.myVehicleDetailsState
                  ?.vehicleRegistrationResponseModel?.data?.drivingLicence ==
              null ||
          driverDetailActionManagers?.myVehicleDetailsState
                  ?.vehicleRegistrationResponseModel?.data?.licenceNoPlate ==
              null ||
          driverDetailActionManagers?.myVehicleDetailsState
                  ?.vehicleRegistrationResponseModel?.data?.vehicleInsurance ==
              null) {
        showProceedButton = true;
      } else if (((driverDetailActionManagers?.myVehicleDetailsState?.vehicleRegistrationResponseModel?.data?.vehicleRegisteration?.vehicleStatus == DocumentStatus?.NotUploaded?.value ||
                  driverDetailActionManagers
                          ?.myVehicleDetailsState
                          ?.vehicleRegistrationResponseModel
                          ?.data
                          ?.drivingLicence
                          ?.drivingLicenceStatus ==
                      DocumentStatus?.NotUploaded?.value) ||
              (driverDetailActionManagers?.myVehicleDetailsState?.vehicleRegistrationResponseModel?.data?.licenceNoPlate?.licenceNoPlateStatus == DocumentStatus?.NotUploaded?.value ||
                  driverDetailActionManagers
                          ?.myVehicleDetailsState
                          ?.vehicleRegistrationResponseModel
                          ?.data
                          ?.vehicleInsurance
                          ?.vehicleInsurenceStatus ==
                      DocumentStatus?.NotUploaded?.value)) ||
          ((driverDetailActionManagers?.myVehicleDetailsState?.vehicleRegistrationResponseModel?.data?.vehicleRegisteration?.vehicleStatus == DocumentStatus?.Approved?.value &&
                  driverDetailActionManagers
                          ?.myVehicleDetailsState
                          ?.vehicleRegistrationResponseModel
                          ?.data
                          ?.drivingLicence
                          ?.drivingLicenceStatus ==
                      DocumentStatus?.Approved?.value) &&
              (driverDetailActionManagers?.myVehicleDetailsState?.vehicleRegistrationResponseModel?.data?.licenceNoPlate?.licenceNoPlateStatus == DocumentStatus?.Approved?.value &&
                  driverDetailActionManagers?.myVehicleDetailsState?.vehicleRegistrationResponseModel?.data?.vehicleInsurance?.vehicleInsurenceStatus == DocumentStatus?.Approved?.value))) {
        showProceedButton = true;
      }
    }
    return showProceedButton;
  }


  //Use to check all document approved or not
  bool checkAllDocumentApprovedOrNot(
      {DriverDetailActionManagers driverDetailActionManagers,
      String comeFrom}) {
    bool allDoucmentApproved = false;
    if (driverDetailActionManagers
                ?.myVehicleDetailsState
                ?.vehicleRegistrationResponseModel
                ?.data
                ?.vehicleRegisteration
                ?.vehicleStatus ==
            DocumentStatus.Approved.value &&
        driverDetailActionManagers
                ?.myVehicleDetailsState
                ?.vehicleRegistrationResponseModel
                ?.data
                ?.drivingLicence
                ?.drivingLicenceStatus ==
            DocumentStatus.Approved.value &&
        driverDetailActionManagers
                ?.myVehicleDetailsState
                ?.vehicleRegistrationResponseModel
                ?.data
                ?.licenceNoPlate
                ?.licenceNoPlateStatus ==
            DocumentStatus.Approved.value &&
        driverDetailActionManagers
                ?.myVehicleDetailsState
                ?.vehicleRegistrationResponseModel
                ?.data
                ?.vehicleInsurance
                ?.vehicleInsurenceStatus ==
            DocumentStatus.Approved.value) {
      allDoucmentApproved = true;
    }
    return allDoucmentApproved;
  }
}
