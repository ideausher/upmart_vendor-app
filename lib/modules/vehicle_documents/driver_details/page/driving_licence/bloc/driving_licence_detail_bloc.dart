import 'package:upmart_driver/localizations.dart';
import 'package:upmart_driver/modules/common/enum/enums.dart';


import 'package:upmart_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:upmart_driver/modules/vehicle_documents/api/model/upload_image_response_model.dart';
import 'package:upmart_driver/modules/vehicle_documents/api/model/vehicle_registration_response_model.dart';
import 'package:upmart_driver/modules/vehicle_documents/api/provider/upload_image_provider.dart';
import 'package:upmart_driver/modules/vehicle_documents/api/provider/vehicle_registration_api_provider.dart';


import 'driver_licence_detail_state.dart';
import 'driving_licence_detail_event.dart';

class DrivingLicenceDetailBloc extends BlocEventStateBase<
    DrivingLicenceDetailsEvent, DrivingLicenceDetailsState> {
  DrivingLicenceDetailBloc({bool initializing = true})
      : super(initialState: DrivingLicenceDetailsState.initiating());

  @override
  Stream<DrivingLicenceDetailsState> eventHandler(
      DrivingLicenceDetailsEvent event,
      DrivingLicenceDetailsState currentState) async* {
    //To update driving licence
    if (event is UpdateDrivingLicenceEvent) {
      yield DrivingLicenceDetailsState.getVehicleDetails(
          isLoading: false,
          context: event?.context,
          vehicleRegistrationResponseModel:
              event?.vehicleRegistrationResponseModel);
    }

    //Upload driving licence Data
    if (event is UploadDrivingLicenceEvent) {
      String message = "";
      int status;
      VehicleRegistrationResponseModel vehicleRegistrationResponseModel;

      yield DrivingLicenceDetailsState.getVehicleDetails(
        isLoading: event?.isLoading,
        context: event?.context,
        message: message,
        vehicleRegistrationResponseModel: event.vehicleRegistrationResponseModel
      );

      //<-----------------api calling driving licence detail update-------------->
      var result = await VehicleRegistrationProvider().registerDriverVehicle(
          context: event.context,
          driverRegistrationRequestModel: event.driverRegistrationRequestModel);

      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          // parse value
          vehicleRegistrationResponseModel =
              VehicleRegistrationResponseModel.fromMap(result);

          status = ApiStatus.Success.value;
        }
        // failure case
        else {
          message = result[ApiStatusParams.Message.value];
        }
      } else {
        message =
            AppLocalizations.of(event?.context).common.error.somethingWentWrong;
      }
      yield DrivingLicenceDetailsState.getVehicleDetails(
          isLoading: false,
          context: event?.context,
          message: message,
          status: status,
          vehicleRegistrationResponseModel: vehicleRegistrationResponseModel);
    }

    //Upload Image
    if (event is UploadImageDrivingLicenceEvent) {
      String message = "";

      yield DrivingLicenceDetailsState.getVehicleDetails(
        isLoading: event?.isLoading,
        context: event?.context,
        message: message,
      );

      var result = await UploadImageProvider()
          .uploadDocumentImage(context: event?.context, file: event.file);
      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          var _uploadImageResponseModel =
              UploadImageResponseModel.fromMap(result);

          event.vehicleRegistrationResponseModel?.data?.drivingLicence
                  ?.drivingLicenceImages =
              _uploadImageResponseModel?.data?.imagePath ?? "";
        }
        // failure case
        else {
          message = result[ApiStatusParams.Message.value];
        }
      } else {
        message =
            AppLocalizations.of(event.context).common.error.somethingWentWrong;
      }

      yield DrivingLicenceDetailsState.getVehicleDetails(
          isLoading: false,
          context: event?.context,
          message: message,
          vehicleRegistrationResponseModel:
              event?.vehicleRegistrationResponseModel);
    }
  }
}
