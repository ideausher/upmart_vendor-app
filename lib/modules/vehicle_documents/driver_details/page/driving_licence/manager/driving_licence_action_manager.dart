import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/src/material/scaffold.dart';
import 'package:upmart_driver/modules/auth/enums/auth_enums.dart';
import 'package:upmart_driver/modules/common/enum/enums.dart';
import 'package:upmart_driver/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:upmart_driver/modules/common/utils/navigator_utils.dart';
import 'package:upmart_driver/modules/common/utils/network_connectivity_utils.dart';
import 'package:upmart_driver/modules/common/model/common_pass_data_model.dart';
import 'package:upmart_driver/modules/file_picker/dialog/single_file_picker_dialog.dart';
import 'package:upmart_driver/modules/file_picker/enums/file_picker_enums.dart';
import 'package:upmart_driver/modules/file_picker/model/file_picker_model.dart';
import 'package:upmart_driver/modules/vehicle_documents/api/model/vehicle_registartion_request_model.dart';
import 'package:upmart_driver/modules/vehicle_documents/api/model/vehicle_registration_response_model.dart';
import 'package:upmart_driver/modules/vehicle_documents/driver_details/page/driving_licence/bloc/driver_licence_detail_state.dart';
import 'package:upmart_driver/modules/vehicle_documents/driver_details/page/driving_licence/bloc/driving_licence_detail_bloc.dart';
import 'package:upmart_driver/modules/vehicle_documents/driver_details/page/driving_licence/bloc/driving_licence_detail_event.dart';
import 'package:upmart_driver/modules/vehicle_documents/validator/vehicle_registration_validator.dart';

import '../../../../registration_routes.dart';

class DrivingLicenceActionManager {
  BuildContext context;

  DrivingLicenceDetailBloc drivingLicenceDetailBloc;
  DrivingLicenceDetailsState drivingLicenceDetailsState;
  CommonPassDataModel commonPassDataModel;

  //Action on init
  void actionOnInit({ScaffoldState currentState}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context,  showNetworkDialog: true)
        .then((onValue) {
      if (onValue) {
        drivingLicenceDetailBloc?.emitEvent(UpdateDrivingLicenceEvent(
            context: context,
            isLoading: false,
            vehicleRegistrationResponseModel:
                commonPassDataModel?.vehicleRegistrationResponseModel));
      }
    });
  }

  //Action on state change
  void actionOnStateChange({ScaffoldState currentState}) {
    if (drivingLicenceDetailsState?.isLoading == false) {
      WidgetsBinding.instance.addPostFrameCallback((_) async {
        if (drivingLicenceDetailsState?.message?.trim()?.isNotEmpty == true) {
          DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
              context: context,
              message: drivingLicenceDetailsState?.message?.trim(),
              scaffoldState: currentState);
        }

        if (drivingLicenceDetailsState?.status == ApiStatus.Success.value) {
          if (drivingLicenceDetailsState?.vehicleRegistrationResponseModel?.data
                  ?.licenceNoPlate?.licenceNoPlateStatus ==
              DocumentStatus?.NotUploaded?.value) {
            NavigatorUtils.navigatorUtilsInstance.navigatorPopAndPushNamed(
                context, RegistrationRoutes.LICENCE_NUMBER_PLATE,
                dataToBeSend: commonPassDataModel);
          } else if (drivingLicenceDetailsState
                  ?.vehicleRegistrationResponseModel
                  ?.data
                  ?.vehicleInsurance
                  ?.vehicleInsurenceStatus ==
              DocumentStatus?.NotUploaded?.value) {
            NavigatorUtils.navigatorUtilsInstance.navigatorPopAndPushNamed(
                context, RegistrationRoutes.INSURANCE_DETAILS,
                dataToBeSend: commonPassDataModel);
          } else if (drivingLicenceDetailsState
                  ?.vehicleRegistrationResponseModel
                  ?.data
                  ?.vehicleRegisteration
                  ?.vehicleStatus ==
              DocumentStatus?.NotUploaded?.value) {
            NavigatorUtils.navigatorUtilsInstance.navigatorPopAndPushNamed(
                context, RegistrationRoutes.VEHICLE_REG,
                dataToBeSend: commonPassDataModel);
          } else {
            if (commonPassDataModel.screenPopCallBack != null) {
              commonPassDataModel.screenPopCallBack.onScreenPop();
            }
            NavigatorUtils.navigatorUtilsInstance
                .navigatorPopScreen(context, dataToBeSend: true);
          }
        }
      });
    }
  }

  //Use to upload image
  void actionOnUploadImage({ScaffoldState currentState}) {
    SingleFilePickerDialog.singleFilePickerDialogInstance.showFilePickerDialog(
        filePickerModel: FilePickerModel(
          pickerType: FilePickerTypeEnum.PickerTypeImage,
          pickFrom: FilePickFromEnum.PickFromBoth,
        ),
        context: context,
        fileData: (value) {
          var _image = File(value);
          if (_image != null) {
            NetworkConnectionUtils.networkConnectionUtilsInstance
                .getConnectivityStatus(context,  showNetworkDialog: true)
                .then((onValue) async {
              if (onValue) {
                drivingLicenceDetailBloc?.emitEvent(
                    UploadImageDrivingLicenceEvent(
                        context: context,
                        isLoading: true,
                        file: _image,
                        vehicleRegistrationResponseModel: commonPassDataModel
                            ?.vehicleRegistrationResponseModel));
              }
            });
            // _imageFile = _image;
            // _callApi();
          }
        });
  }

  //Action Update Driving Licence detail
  actionUpdateDrivingLicenceDetail(
      {ScaffoldState currentState,
      DriverRegistrationRequestModel driverRegistrationRequestModel,
      VehicleRegistrationResponseModel vehicleRegistrationResponseModel}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context,  showNetworkDialog: true)
        .then((onValue) {
      if (onValue) {
        drivingLicenceDetailBloc?.emitEvent(UploadDrivingLicenceEvent(
            context: context,
            isLoading: true,
            driverRegistrationRequestModel: driverRegistrationRequestModel,
            vehicleRegistrationResponseModel:
                vehicleRegistrationResponseModel));
      }
    });
  }

  //Action on click on next button
  void actionOnClickNextButton(
      {ScaffoldState currentState,
      TextEditingController licenceNumberController,
      TextEditingController expiryController}) {
    //Validations on controllers
    String result = VehicleRegistrationValidator.vehicleRegistrationInstance
        .validateDrivingLicenceScreen(
            context: context,
            licenceNumber: licenceNumberController?.text,
            expiryDate: expiryController?.text,
            file: drivingLicenceDetailsState?.vehicleRegistrationResponseModel
                    ?.data?.vehicleRegisteration?.vehiclePicture ??
                "");

    // if its not empty
    if (result.isEmpty == true) {
      NetworkConnectionUtils.networkConnectionUtilsInstance
          .getConnectivityStatus(context,  showNetworkDialog: true)
          .then((onValue) {
        if (onValue) {
          //Create request model
          DriverRegistrationRequestModel _driverRegistrationRequestModel =
              new DriverRegistrationRequestModel(
                  drivingLicence: DrivingLicenceData(
            drivinglicenceNo: licenceNumberController?.text,
            drinvinglicenceImages: drivingLicenceDetailsState
                    ?.vehicleRegistrationResponseModel
                    ?.data
                    ?.drivingLicence
                    ?.drivingLicenceImages ??
                "",
            drivinglicenceExpiryDate: expiryController?.text,
          ));

          //Event to update driving licence details
          actionUpdateDrivingLicenceDetail(
              currentState: currentState,
              driverRegistrationRequestModel: _driverRegistrationRequestModel,
              vehicleRegistrationResponseModel:
                  drivingLicenceDetailsState?.vehicleRegistrationResponseModel);
        }
      });
    } else {
      // show error in fields
      DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
          context: context, scaffoldState: currentState, message: result);
    }
  }
}
