import 'dart:io';

import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/auth/enums/auth_enums.dart';
import 'package:upmart_driver/modules/common/enum/enums.dart';
import 'package:upmart_driver/modules/common/model/common_pass_data_model.dart';
import 'package:upmart_driver/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:upmart_driver/modules/common/utils/navigator_utils.dart';
import 'package:upmart_driver/modules/common/utils/network_connectivity_utils.dart';
import 'package:upmart_driver/modules/vehicle_documents/api/model/vehicle_registartion_request_model.dart';
import 'package:upmart_driver/modules/vehicle_documents/api/model/vehicle_registration_response_model.dart';
import 'package:upmart_driver/modules/vehicle_documents/driver_details/page/licence_number_plate/bloc/licence_number_plate_bloc.dart';
import 'package:upmart_driver/modules/vehicle_documents/driver_details/page/licence_number_plate/bloc/licence_number_plate_event.dart';
import 'package:upmart_driver/modules/vehicle_documents/driver_details/page/licence_number_plate/bloc/licence_number_plate_state.dart';
import 'package:upmart_driver/modules/file_picker/dialog/single_file_picker_dialog.dart';
import 'package:upmart_driver/modules/file_picker/enums/file_picker_enums.dart';
import 'package:upmart_driver/modules/file_picker/model/file_picker_model.dart';
import 'package:upmart_driver/modules/vehicle_documents/validator/vehicle_registration_validator.dart';

import '../../../../registration_routes.dart';

class LicenceNumberPlateActionManager {
  BuildContext context;
  LicenceNumberPlateBloc licenceNumberPlateBloc;
  LicenceNumberPlateState licenceNumberPlateState;
  CommonPassDataModel commonPassDataModel;

  //Action on init
  void actionOnInit({ScaffoldState currentState}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context,  showNetworkDialog: true)
        .then((onValue) {
      if (onValue) {
        licenceNumberPlateBloc?.emitEvent(UpdateLicenceNumberPlateEvent(
            context: context,
            isLoading: false,
            vehicleRegistrationResponseModel:
                commonPassDataModel?.vehicleRegistrationResponseModel));
      }
    });
  }

  //Use to upload image
  void actionOnUploadImage({ScaffoldState currentState}) {
    SingleFilePickerDialog.singleFilePickerDialogInstance.showFilePickerDialog(
        filePickerModel: FilePickerModel(
          pickerType: FilePickerTypeEnum.PickerTypeImage,
          pickFrom: FilePickFromEnum.PickFromBoth,
        ),
        context: context,
        fileData: (value) {
          var _image = File(value);
          if (_image != null) {
            NetworkConnectionUtils.networkConnectionUtilsInstance
                .getConnectivityStatus(context, showNetworkDialog: true)
                .then((onValue) async {
              if (onValue) {
                licenceNumberPlateBloc?.emitEvent(
                    UploadImageLicenceNumberPlateEvent(
                        context: context,
                        isLoading: true,
                        file: _image,
                        vehicleRegistrationResponseModel: commonPassDataModel
                            ?.vehicleRegistrationResponseModel));
              }
            });
            // _imageFile = _image;
            // _callApi();
          }
        });
  }

  //Action on state change
  void actionOnStateChange({ScaffoldState currentState}) {
    if (licenceNumberPlateState?.isLoading == false) {
      WidgetsBinding.instance.addPostFrameCallback((_) async {
        if (licenceNumberPlateState?.message?.trim()?.isNotEmpty == true) {
          DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
              context: context,
              message: licenceNumberPlateState?.message?.trim(),
              scaffoldState: currentState);
        }

        if (licenceNumberPlateState?.status == ApiStatus.Success.value) {
          if (licenceNumberPlateState?.vehicleRegistrationResponseModel?.data
                  ?.vehicleInsurance?.vehicleInsurenceStatus ==
              DocumentStatus?.NotUploaded?.value) {
            NavigatorUtils.navigatorUtilsInstance.navigatorPopAndPushNamed(
                context, RegistrationRoutes.INSURANCE_DETAILS,
                dataToBeSend: commonPassDataModel);
          } else if (licenceNumberPlateState?.vehicleRegistrationResponseModel
                  ?.data?.vehicleRegisteration?.vehicleStatus ==
              DocumentStatus?.NotUploaded?.value) {
            NavigatorUtils.navigatorUtilsInstance.navigatorPopAndPushNamed(
                context, RegistrationRoutes.VEHICLE_REG,
                dataToBeSend: commonPassDataModel);
          } else if (licenceNumberPlateState?.vehicleRegistrationResponseModel
                  ?.data?.drivingLicence?.drivingLicenceStatus ==
              DocumentStatus?.NotUploaded?.value) {
            NavigatorUtils.navigatorUtilsInstance.navigatorPopAndPushNamed(
                context, RegistrationRoutes.LICENCE_DETAILS,
                dataToBeSend: commonPassDataModel);
          } else {
            if (commonPassDataModel.screenPopCallBack != null) {
              commonPassDataModel.screenPopCallBack.onScreenPop();
            }
            NavigatorUtils.navigatorUtilsInstance
                .navigatorPopScreen(context, dataToBeSend: true);
          }
        }
      });
    }
  }

  //Action click on next button
  void actionOnClickNextButton(
      {ScaffoldState scaffoldState,
      TextEditingController numberPlateController}) {
    // check name field is empty or not
    String result = VehicleRegistrationValidator.vehicleRegistrationInstance
        .validateNumberPlateScreen(
            context: context,
            numberPlate: numberPlateController.text,
            file: licenceNumberPlateState?.vehicleRegistrationResponseModel
                    ?.data?.vehicleRegisteration?.vehiclePicture ??
                "");

    // if its not empty
    if (result.isEmpty) {
      DriverRegistrationRequestModel driverRegistrationRequestModel =
          new DriverRegistrationRequestModel(
              licenceNoPlate: NumberPlateData(
        noPlate: numberPlateController.text,
        licenceNoPlateImages: licenceNumberPlateState
                ?.vehicleRegistrationResponseModel
                ?.data
                ?.licenceNoPlate
                ?.licenceNoPlateImages ??
            "",
      ));

      //Check internet connection avaibale or not
      NetworkConnectionUtils.networkConnectionUtilsInstance
          .getConnectivityStatus(context,  showNetworkDialog: true)
          .then((onValue) {
        if (onValue) {
          licenceNumberPlateBloc?.emitEvent(UploadLicenceNumberPlateEvent(
              context: context,
              isLoading: true,
              driverRegistrationRequestModel: driverRegistrationRequestModel,
              vehicleRegistrationResponseModel:
                  licenceNumberPlateState?.vehicleRegistrationResponseModel));
        }
      });
    } else {
      // show error in fields
      DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
          context: context, scaffoldState: scaffoldState, message: result);
    }
  }
}
