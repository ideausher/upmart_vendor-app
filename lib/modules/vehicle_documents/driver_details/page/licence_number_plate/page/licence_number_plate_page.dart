import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:upmart_driver/localizations.dart';
import 'package:upmart_driver/modules/auth/constants/image_constant.dart';
import 'package:upmart_driver/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:upmart_driver/modules/common/app_config/app_config.dart';
import 'package:upmart_driver/modules/common/common_widget/custom_raised_gradient_button.dart';
import 'package:upmart_driver/modules/common/constants/color_constants.dart';
import 'package:upmart_driver/modules/common/constants/dimens_constants.dart';
import 'package:upmart_driver/modules/common/model/common_pass_data_model.dart';
import 'package:upmart_driver/modules/common/theme/app_themes.dart';
import 'package:upmart_driver/modules/common/utils/common_utils.dart';
import 'package:upmart_driver/modules/common/utils/image_utils.dart';
import 'package:upmart_driver/modules/common/utils/navigator_utils.dart';
import 'package:upmart_driver/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:upmart_driver/modules/notification/widget/notification_unread_count_widget.dart';
import 'package:upmart_driver/modules/vehicle_documents/api/model/vehicle_registration_response_model.dart';
import 'package:upmart_driver/modules/vehicle_documents/constants/image_constants.dart';
import 'package:upmart_driver/modules/vehicle_documents/driver_details/page/licence_number_plate/bloc/licence_number_plate_bloc.dart';
import 'package:upmart_driver/modules/vehicle_documents/driver_details/page/licence_number_plate/bloc/licence_number_plate_state.dart';
import 'package:upmart_driver/modules/vehicle_documents/driver_details/page/licence_number_plate/manager/licence_number_plate_action_manager.dart';

class LicenceNumberPlatePage extends StatefulWidget {
  VehicleRegistrationResponseModel driverRegistrationResponseModel;
  CommonPassDataModel commonPassDataModel;
  BuildContext context;

  LicenceNumberPlatePage(this.context) {
    commonPassDataModel = ModalRoute.of(context).settings.arguments;
    driverRegistrationResponseModel =
        commonPassDataModel?.vehicleRegistrationResponseModel;
  }

  @override
  _LicenceNumberPlatePageState createState() => _LicenceNumberPlatePageState();
}

class _LicenceNumberPlatePageState extends State<LicenceNumberPlatePage> {
  // text editing controllers
  TextEditingController _numberPlateController = new TextEditingController();

  //for vehicle licence image
  File imageFile;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  //Manager
  LicenceNumberPlateActionManager _licenceNumberPlateActionManager;
  LicenceNumberPlateBloc _licenceNumberPlateBloc = new LicenceNumberPlateBloc();

  @override
  void initState() {
    super.initState();

    _licenceNumberPlateActionManager = new LicenceNumberPlateActionManager();
    _licenceNumberPlateActionManager.context = widget.context;
    _licenceNumberPlateActionManager.commonPassDataModel =
        widget.commonPassDataModel;
    _licenceNumberPlateActionManager.licenceNumberPlateBloc =
        _licenceNumberPlateBloc;

    _licenceNumberPlateActionManager.actionOnInit();
  }

  @override
  void dispose() {
    super.dispose();
    _numberPlateController.dispose();
    _licenceNumberPlateActionManager?.licenceNumberPlateBloc?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _licenceNumberPlateActionManager.context = context;
    return BlocEventStateBuilder<LicenceNumberPlateState>(
      bloc: _licenceNumberPlateActionManager?.licenceNumberPlateBloc,
      builder: (BuildContext context,
          LicenceNumberPlateState licenceNumberPlateState) {
        _licenceNumberPlateActionManager?.context = context;
        if (licenceNumberPlateState != null &&
            _licenceNumberPlateActionManager?.licenceNumberPlateState !=
                licenceNumberPlateState) {
          _licenceNumberPlateActionManager?.licenceNumberPlateState =
              licenceNumberPlateState;

          if (_licenceNumberPlateActionManager
                  ?.licenceNumberPlateState?.isLoading ==
              false) {
            _setDataToControllers();
            _licenceNumberPlateActionManager?.actionOnStateChange(
                currentState: _scaffoldKey?.currentState);
          }
        }
        return ModalProgressHUD(
            inAsyncCall: _licenceNumberPlateActionManager
                    ?.licenceNumberPlateState?.isLoading ??
                false,
            child: Scaffold(
              key: _scaffoldKey,
              appBar: _showAppBar(),
              body: WillPopScope(
                onWillPop: () {
                  if (widget.commonPassDataModel.screenPopCallBack != null) {
                    widget.commonPassDataModel.screenPopCallBack.onScreenPop();
                  }
                  return NavigatorUtils.navigatorUtilsInstance
                      .navigatorPopScreen(
                          _licenceNumberPlateActionManager.context,
                          dataToBeSend: true);
                },
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: SIZE_15, right: SIZE_15, bottom: SIZE_15),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        _showDivider(),
                        _showVehicleRegistrationTitle(),
                        SizedBox(
                          height: SIZE_15,
                        ),
                        _showUploadPhotoView(),
                        SizedBox(
                          height: SIZE_20,
                        ),
                        _showVehicleDetailsForm()
                      ],
                    ),
                  ),
                ),
              ),
            ));
      },
    );
  }

  //method to show app bar
  Widget _showAppBar() {
    return CommonUtils.commonUtilsInstance.getAppBar(
        context: _licenceNumberPlateActionManager.context,
        elevation: ELEVATION_0,
        centerTitle: false,
        actionWidgets: [
          Padding(
            padding: const EdgeInsets.only(right: SIZE_16, top: SIZE_16),
            child: NotificationReadCountWidget(
              context: context,
            ),
          )
        ],
        appBarTitle:
            AppLocalizations.of(_licenceNumberPlateActionManager.context)
                .driverDetails
                .text
                .driverDetails,
        popScreenOnTapOfLeadingIcon: false,
        defaultLeadIconPressed: () {
          if (widget.commonPassDataModel.screenPopCallBack != null) {
            widget.commonPassDataModel.screenPopCallBack.onScreenPop();
          }
          NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(
              _licenceNumberPlateActionManager.context,
              dataToBeSend: true);
        },
        defaultLeadingIcon: Icons.arrow_back,
        defaultLeadingIconColor: COLOR_BLACK,
        appBarTitleStyle: AppConfig.of(_licenceNumberPlateActionManager.context)
            .themeData
            .primaryTextTheme
            .headline3,
        backGroundColor: Colors.transparent);
  }

  //method to show vehicle registration title
  Widget _showVehicleRegistrationTitle() {
    return Text(
        AppLocalizations.of(_licenceNumberPlateActionManager.context)
            .numberPlate
            .text
            .title,
        style: AppConfig.of(_licenceNumberPlateActionManager.context)
            .themeData
            .primaryTextTheme
            .headline5);
  }

  //method to show upload photo view
  Widget _showUploadPhotoView() {
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(SIZE_15))),
      elevation: ELEVATION_05,
      clipBehavior: Clip.antiAlias,
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              COLOR_CARD_GARDIENT_1,
              COLOR_CARD_GARDIENT_2,
            ],
            begin: const FractionalOffset(0.0, 2.2),
            end: const FractionalOffset(1.5, 1.0),
            stops: [0.0, 1.0],
            tileMode: TileMode.clamp,
          ),
        ),
        child: Column(
          children: [
            _getImageView(),
            InkWell(
              onTap: () {
                _licenceNumberPlateActionManager?.actionOnUploadImage(
                    currentState: _scaffoldKey?.currentState);
              },
              child: Container(
                decoration: new BoxDecoration(
                  color: COLOR_WHITE,
                  borderRadius: new BorderRadius.only(
                      bottomRight: Radius.circular(SIZE_15),
                      bottomLeft: Radius.circular(SIZE_15)),
                  shape: BoxShape.rectangle,
                ),
                height: SIZE_40,
                alignment: Alignment.center,
                child: Text(
                  AppLocalizations.of(_licenceNumberPlateActionManager?.context)
                      .common
                      .text
                      .uploadPhoto,
                  style: TextStyle(color: Colors.black),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  //show vehicle details form
  Widget _showVehicleDetailsForm() {
    return Wrap(
      runSpacing: SIZE_10,
      children: [_showRegistrationNumber(), _showNextButton()],
    );
  }

  Widget _textFieldForm({
    TextEditingController controller,
    TextInputType keyboardType,
    IconData icon,
    String prefixIcon,
    String hint,
    bool isInputFormater,
    double elevation,
    FormFieldValidator<String> validator,
    bool enable,
  }) {
    return Material(
      borderRadius: BorderRadius.circular(SIZE_5),
      color: Colors.white,
      child: Padding(
        padding: EdgeInsets.only(left: SIZE_0),
        child: TextFormField(
          validator: validator,
          style: AppConfig.of(_licenceNumberPlateActionManager.context)
              .themeData
              .textTheme
              .headline2,
          enabled: enable,
          controller: controller,
          keyboardType: keyboardType,
          textCapitalization: TextCapitalization.words,
          decoration: InputDecoration(
            hintText: hint,
            hintStyle: TextStyle(color: COLOR_BORDER_GREY),
            contentPadding: EdgeInsets.all(SIZE_10),
            border: OutlineInputBorder(borderSide: BorderSide.none),
          ),
        ),
      ),
    );
  }

  //registration number
  Widget _showRegistrationNumber() {
    return Container(
      padding: EdgeInsets.only(left: SIZE_5, right: SIZE_5),
      decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          border: Border.all(color: COLOR_BORDER_GREY, width: SIZE_1),
          borderRadius: BorderRadius.circular(SIZE_10)),
      child: _textFieldForm(
          controller: _numberPlateController,
          icon: null,
          isInputFormater: false,
          hint: AppLocalizations.of(_licenceNumberPlateActionManager.context)
              .numberPlate
              .text
              .plateNumber,
          enable: true),
    );
  }

  //method to show next button
  Widget _showNextButton() {
    return Align(
      alignment: Alignment.center,
      child: Container(
        margin: EdgeInsets.only(top: SIZE_20, bottom: SIZE_20),
        height: CommonUtils.commonUtilsInstance.getPercentageSize(
            context: context, percentage: SIZE_7, ofWidth: false),
        width: CommonUtils.commonUtilsInstance.getPercentageSize(
            context: context, percentage: SIZE_80, ofWidth: true),
        child: RaisedGradientButton(
          radious: SIZE_30,
          gradient: LinearGradient(
            begin: Alignment.bottomCenter,
            end: Alignment.topCenter,
            colors: <Color>[COLOR_PRIMARY, COLOR_ACCENT],
          ),
          onPressed: () {
            _licenceNumberPlateActionManager?.actionOnClickNextButton(
                scaffoldState: _scaffoldKey.currentState,
                numberPlateController: _numberPlateController);
          },
          child: Text(
            AppLocalizations.of(_licenceNumberPlateActionManager?.context)
                .common
                .text
                .next,
            textAlign: TextAlign.center,
            style: textStyleSize14WithWhiteColor,
          ),
        ),

      ),
    );
  }

  //method to set data in controllers
  void _setDataToControllers() {
    _numberPlateController?.text =
        _numberPlateController?.text?.trim()?.isNotEmpty == true
            ? _numberPlateController?.text
            : _licenceNumberPlateActionManager
                    ?.licenceNumberPlateState
                    ?.vehicleRegistrationResponseModel
                    ?.data
                    ?.licenceNoPlate
                    ?.noPlate ??
                "";
  }

  //method to show view of image uploaded by user or empty image view
  Widget _getImageView() {
    if ((widget?.driverRegistrationResponseModel?.data?.licenceNoPlate
                ?.licenceNoPlateImages !=
            null &&
        widget?.driverRegistrationResponseModel?.data?.licenceNoPlate
                ?.licenceNoPlateImages?.isNotEmpty ==
            true)) {
      //this will execute in the case of edit licence doc
      return _getWidget(
          url: widget?.driverRegistrationResponseModel?.data?.licenceNoPlate
              ?.licenceNoPlateImages);
    } else if (_licenceNumberPlateActionManager
            ?.licenceNumberPlateState
            ?.vehicleRegistrationResponseModel
            ?.data
            ?.licenceNoPlate
            ?.licenceNoPlateImages
            ?.isNotEmpty ==
        true) {
      //this will execute when user upload image or edit image
      return _getWidget(
          url: _licenceNumberPlateActionManager
              ?.licenceNumberPlateState
              ?.vehicleRegistrationResponseModel
              ?.data
              ?.licenceNoPlate
              ?.licenceNoPlateImages);
    }
    //this is default condition when no image uploaded
    else {
      return Row(
        children: [
          Expanded(
              flex: 2,
              child: Align(
                alignment: Alignment.topLeft,
                child: Padding(
                  padding: const EdgeInsets.only(left: SIZE_10),
                  child: SvgPicture.asset(
                    NUMBER_ICON,
                    color: Colors.white,
                    height: CommonUtils.commonUtilsInstance.getPercentageSize(
                        context: _licenceNumberPlateActionManager?.context,
                        percentage: SIZE_10,
                        ofWidth: false),
                    width: CommonUtils.commonUtilsInstance.getPercentageSize(
                        context: _licenceNumberPlateActionManager?.context,
                        percentage: SIZE_10,
                        ofWidth: true),
                  ),
                ),
              )),
          Expanded(
              flex: 3,
              child: Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.only(left: SIZE_25, right: SIZE_25),
                  height: CommonUtils.commonUtilsInstance.getPercentageSize(
                      context: _licenceNumberPlateActionManager?.context,
                      percentage: SIZE_20,
                      ofWidth: false),
                  child: Wrap(
                    runSpacing: SIZE_10,
                    children: [
                      Container(
                        height: SIZE_45,
                        decoration: new BoxDecoration(
                          color: COLOR_LIGHT_PINK,
                          borderRadius: new BorderRadius.all(
                            Radius.circular(SIZE_5),
                          ),
                          shape: BoxShape.rectangle,
                        ),
                      ),
                      Container(
                        height: SIZE_15,
                        decoration: new BoxDecoration(
                          color: COLOR_LIGHT_PINK,
                          borderRadius: new BorderRadius.all(
                            Radius.circular(SIZE_20),
                          ),
                          shape: BoxShape.rectangle,
                        ),
                      ),
                      Container(
                        height: SIZE_15,
                        decoration: new BoxDecoration(
                          color: COLOR_LIGHT_PINK,
                          borderRadius: new BorderRadius.all(
                            Radius.circular(SIZE_20),
                          ),
                          shape: BoxShape.rectangle,
                        ),
                      )
                    ],
                  )))
        ],
      );
    }
  }

  //this widget will return a cache network image
  Widget _showCacheNetworkImage({String imageUrl}) {
    return ImageUtils.imageUtilsInstance.showCacheNetworkImage(
      context: context,
      showProgressBarInPlaceHolder: true,
      shape: BoxShape.rectangle,
      loaderHeight: CommonUtils?.commonUtilsInstance?.getPercentageSize(
          context: _licenceNumberPlateActionManager.context,
          percentage: SIZE_40,
          ofWidth: false),
      loaderWidth: CommonUtils?.commonUtilsInstance?.getPercentageSize(
          context: _licenceNumberPlateActionManager.context,
          percentage: SIZE_40,
          ofWidth: true),
      height: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _licenceNumberPlateActionManager.context,
          percentage: SIZE_24,
          ofWidth: false),
      width: double.infinity,
      url: imageUrl ?? "",
    );
  }

  //get widget which will show image
  Widget _getWidget({String url}) {
    return Container(
        height: CommonUtils.commonUtilsInstance.getPercentageSize(
            context: _licenceNumberPlateActionManager.context,
            percentage: SIZE_24,
            ofWidth: false),
        width: MediaQuery.of(context).size.width,
        child: _showCacheNetworkImage(imageUrl: url));
  }

//method to show divider text
  Widget _showDivider() {
    return Divider(
      color: COLOR_BORDER_GREY,
    );
  }
}
