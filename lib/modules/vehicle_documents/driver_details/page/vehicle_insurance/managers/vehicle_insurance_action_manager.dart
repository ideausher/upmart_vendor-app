import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/auth/enums/auth_enums.dart';
import 'package:upmart_driver/modules/common/enum/enums.dart';
import 'package:upmart_driver/modules/common/utils/dialog_snackbar_utils.dart';
import 'package:upmart_driver/modules/common/utils/navigator_utils.dart';
import 'package:upmart_driver/modules/common/utils/network_connectivity_utils.dart';
import 'package:upmart_driver/modules/vehicle_documents/api/model/vehicle_registartion_request_model.dart';
import 'package:upmart_driver/modules/vehicle_documents/driver_details/page/vehicle_insurance/bloc/vehicle_insurance_detail_bloc.dart';
import 'package:upmart_driver/modules/vehicle_documents/driver_details/page/vehicle_insurance/bloc/vehicle_insurance_detail_event.dart';
import 'package:upmart_driver/modules/vehicle_documents/driver_details/page/vehicle_insurance/bloc/vehicle_insurance_detail_state.dart';
import 'package:upmart_driver/modules/vehicle_documents/validator/vehicle_registration_validator.dart';
import 'package:upmart_driver/modules/common/model/common_pass_data_model.dart';
import 'package:upmart_driver/modules/file_picker/dialog/single_file_picker_dialog.dart';
import 'package:upmart_driver/modules/file_picker/enums/file_picker_enums.dart';
import 'package:upmart_driver/modules/file_picker/model/file_picker_model.dart';

import '../../../../registration_routes.dart';

class VehicleInsuranceActionManager {
  BuildContext context;
  VehicleInsuranceDetailsState vehicleInsuranceDetailsState;
  VehicleInsuranceDetailBloc vehicleInsuranceDetailBloc;

  CommonPassDataModel commonPassDataModel;

  //Action on init
  void actionOnInit({ScaffoldState currentState}) {
    NetworkConnectionUtils.networkConnectionUtilsInstance
        .getConnectivityStatus(context,  showNetworkDialog: true)
        .then((onValue) {
      if (onValue) {
        vehicleInsuranceDetailBloc?.emitEvent(UpdateVehicleInsuranceEvent(
            context: context,
            isLoading: false,
            vehicleRegistrationResponseModel:
                commonPassDataModel?.vehicleRegistrationResponseModel));
      }
    });
  }

  //Action on state change
  void actionOnStateChange({ScaffoldState currentState}) {
    if (vehicleInsuranceDetailsState?.isLoading == false) {
      WidgetsBinding.instance.addPostFrameCallback((_) async {
        if (vehicleInsuranceDetailsState?.message?.trim()?.isNotEmpty == true) {
          DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
              context: context,
              message: vehicleInsuranceDetailsState?.message?.trim(),
              scaffoldState: currentState);
        }

        if (vehicleInsuranceDetailsState?.status == ApiStatus.Success.value) {
          if (vehicleInsuranceDetailsState?.vehicleRegistrationResponseModel
                  ?.data?.vehicleInsurance?.vehicleInsurenceStatus ==
              DocumentStatus?.NotUploaded?.value) {
            NavigatorUtils.navigatorUtilsInstance.navigatorPopAndPushNamed(
                context, RegistrationRoutes.VEHICLE_REG,
                dataToBeSend: commonPassDataModel);
          } else if (vehicleInsuranceDetailsState
                  ?.vehicleRegistrationResponseModel
                  ?.data
                  ?.drivingLicence
                  ?.drivingLicenceStatus ==
              DocumentStatus?.NotUploaded?.value) {
            NavigatorUtils.navigatorUtilsInstance.navigatorPopAndPushNamed(
                context, RegistrationRoutes.LICENCE_DETAILS,
                dataToBeSend: commonPassDataModel);
          } else if (vehicleInsuranceDetailsState
                  ?.vehicleRegistrationResponseModel
                  ?.data
                  ?.licenceNoPlate
                  ?.licenceNoPlateStatus ==
              DocumentStatus?.NotUploaded?.value) {
            NavigatorUtils.navigatorUtilsInstance.navigatorPopAndPushNamed(
                context, RegistrationRoutes.LICENCE_NUMBER_PLATE,
                dataToBeSend: commonPassDataModel);
          } else {
            if (commonPassDataModel.screenPopCallBack != null) {
              commonPassDataModel.screenPopCallBack.onScreenPop();
            }
            NavigatorUtils.navigatorUtilsInstance
                .navigatorPopScreen(context, dataToBeSend: true);
          }
        }
      });
    }
  }

  //Use to upload image
  void actionOnUploadInsuranceImage({ScaffoldState currentState}) {
    SingleFilePickerDialog.singleFilePickerDialogInstance.showFilePickerDialog(
        filePickerModel: FilePickerModel(
          pickerType: FilePickerTypeEnum.PickerTypeImage,
          pickFrom: FilePickFromEnum.PickFromBoth,
        ),
        context: context,
        fileData: (value) {
          var _image = File(value);
          if (_image != null) {
            NetworkConnectionUtils.networkConnectionUtilsInstance
                .getConnectivityStatus(context,  showNetworkDialog: true)
                .then((onValue) async {
              if (onValue) {
                vehicleInsuranceDetailBloc?.emitEvent(
                    UploadImageVehicleInsuranceEvent(
                        context: context,
                        isLoading: true,
                        file: _image,
                        vehicleRegistrationResponseModel: commonPassDataModel
                            ?.vehicleRegistrationResponseModel));
              }
            });
          }
        });
  }

  //Action on click Next Button
  void actionOnclickNextButton(
      {ScaffoldState currentState,
      TextEditingController policyNumberController,
      TextEditingController validFromController,
      TextEditingController validTillController}) {
    // check name field is empty or not
    String result = VehicleRegistrationValidator.vehicleRegistrationInstance
        .validateVehicleInsuranceScreen(
            context: context,
            policyNumber: policyNumberController?.text,
            file: vehicleInsuranceDetailsState?.vehicleRegistrationResponseModel
                ?.data?.vehicleInsurance?.vehicleInsurenceImage,
            validFrom: validFromController?.text,
            validTill: validTillController?.text);

    // if its not empty
    if (result.isEmpty) {
      VehicleInsuranceData vehicleInsuranceData = new VehicleInsuranceData();
      vehicleInsuranceData?.policyNumber = policyNumberController?.text;
      vehicleInsuranceData?.vehicleInsurenceImage = vehicleInsuranceDetailsState
          ?.vehicleRegistrationResponseModel
          ?.data
          ?.vehicleInsurance
          ?.vehicleInsurenceImage;
      vehicleInsuranceData?.validFrom = validFromController?.text;
      vehicleInsuranceData?.validTill = validTillController?.text;

      DriverRegistrationRequestModel driverRegistrationRequestModel =
          new DriverRegistrationRequestModel(
              vehicleInsurance: vehicleInsuranceData);

      NetworkConnectionUtils.networkConnectionUtilsInstance
          .getConnectivityStatus(context,  showNetworkDialog: true)
          .then((onValue) {
        if (onValue) {
          vehicleInsuranceDetailBloc?.emitEvent(UploadVehicleInsuranceEvent(
              context: context,
              isLoading: true,
              driverRegistrationRequestModel: driverRegistrationRequestModel,
              vehicleRegistrationResponseModel: vehicleInsuranceDetailsState
                  ?.vehicleRegistrationResponseModel));
        }
      });
    } else {
      // show error in fields
      DialogSnackBarUtils.dialogSnackBarUtilsInstance.showSnackbar(
          context: context, scaffoldState: currentState, message: result);
    }
  }
}
