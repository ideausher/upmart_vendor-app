import 'package:upmart_driver/localizations.dart';
import 'package:upmart_driver/modules/common/enum/enums.dart';
import 'package:upmart_driver/modules/vehicle_documents/api/model/upload_image_response_model.dart';
import 'package:upmart_driver/modules/vehicle_documents/api/model/vehicle_registration_response_model.dart';
import 'package:upmart_driver/modules/vehicle_documents/api/provider/upload_image_provider.dart';
import 'package:upmart_driver/modules/vehicle_documents/api/provider/vehicle_registration_api_provider.dart';
import 'package:upmart_driver/modules/common/app_bloc_utilities/bloc_helpers/bloc_event_state.dart';
import 'package:upmart_driver/modules/vehicle_documents/driver_details/page/vehicle_registration/bloc/vehicle_register_detail_event.dart';
import 'package:upmart_driver/modules/vehicle_documents/driver_details/page/vehicle_registration/bloc/vehicle_register_detail_state.dart';

class VehicleRegisterDetailBloc extends BlocEventStateBase<
    VehicleRegisterDetailsEvent, VehicleRegisterDetailsState> {
  VehicleRegisterDetailBloc({bool initializing = true})
      : super(initialState: VehicleRegisterDetailsState.initiating());

  @override
  Stream<VehicleRegisterDetailsState> eventHandler(
      VehicleRegisterDetailsEvent event,
      VehicleRegisterDetailsState currentState) async* {
    //To update driving licence
    if (event is UpdateVehicleRegisterEvent) {
      yield VehicleRegisterDetailsState.getVehicleDetails(
          isLoading: false,
          context: event?.context,
          vehicleRegistrationResponseModel:
              event?.vehicleRegistrationResponseModel);
    }

    //Upload driving licence Data
    if (event is UploadVehicleRegisterEvent) {
      String message = "";
      int status;
      VehicleRegistrationResponseModel vehicleRegistrationResponseModel;

      yield VehicleRegisterDetailsState.getVehicleDetails(
          isLoading: event?.isLoading,
          context: event?.context,
          message: message,
          vehicleRegistrationResponseModel:
              event.vehicleRegistrationResponseModel);

      //<-----------------api calling driving licence detail update-------------->
      var result = await VehicleRegistrationProvider().registerDriverVehicle(
          context: event.context,
          driverRegistrationRequestModel: event.driverRegistrationRequestModel);

      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          // parse value
          vehicleRegistrationResponseModel =
              VehicleRegistrationResponseModel.fromMap(result);

          status = ApiStatus.Success.value;
        }
        // failure case
        else {
          message = result[ApiStatusParams.Message.value];
        }
      } else {
        message =
            AppLocalizations.of(event?.context).common.error.somethingWentWrong;
      }
      yield VehicleRegisterDetailsState.getVehicleDetails(
          isLoading: false,
          context: event?.context,
          message: message,
          status: status,
          vehicleRegistrationResponseModel: vehicleRegistrationResponseModel);
    }

    //Upload Image
    if (event is UploadImageVehicleRegisterEvent) {
      String message = "";

      yield VehicleRegisterDetailsState.getVehicleDetails(
        isLoading: event?.isLoading,
        context: event?.context,
        message: message,
      );

      var result = await UploadImageProvider()
          .uploadDocumentImage(context: event?.context, file: event.file);
      if (result != null) {
        // check result status
        if (result[ApiStatusParams.Status.value] != null &&
            result[ApiStatusParams.Status.value] == ApiStatus.Success.value) {
          var _uploadImageResponseModel =
              UploadImageResponseModel.fromMap(result);

          event.vehicleRegistrationResponseModel?.data?.vehicleRegisteration
                  ?.vehiclePicture =
              _uploadImageResponseModel?.data?.imagePath ?? "";
        }
        // failure case
        else {
          message = result[ApiStatusParams.Message.value];
        }
      } else {
        message =
            AppLocalizations.of(event.context).common.error.somethingWentWrong;
      }

      yield VehicleRegisterDetailsState.getVehicleDetails(
          isLoading: false,
          context: event?.context,
          message: message,
          vehicleRegistrationResponseModel:
              event?.vehicleRegistrationResponseModel);
    }
  }
}
