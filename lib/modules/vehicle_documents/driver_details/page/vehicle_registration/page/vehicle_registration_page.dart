import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:upmart_driver/localizations.dart';
import 'package:upmart_driver/modules/common/app_bloc_utilities/bloc_widgets/bloc_state_builder.dart';
import 'package:upmart_driver/modules/common/app_config/app_config.dart';
import 'package:upmart_driver/modules/common/common_widget/custom_raised_gradient_button.dart';
import 'package:upmart_driver/modules/common/constants/color_constants.dart';
import 'package:upmart_driver/modules/common/constants/dimens_constants.dart';
import 'package:upmart_driver/modules/common/model/common_pass_data_model.dart';
import 'package:upmart_driver/modules/common/theme/app_themes.dart';
import 'package:upmart_driver/modules/common/utils/common_utils.dart';
import 'package:upmart_driver/modules/common/utils/date_picker_utils.dart';
import 'package:upmart_driver/modules/common/utils/date_utils.dart';
import 'package:upmart_driver/modules/common/utils/image_utils.dart';
import 'package:upmart_driver/modules/common/utils/navigator_utils.dart';
import 'package:upmart_driver/modules/common/common_widget/async_call_parent_widget.dart';
import 'package:upmart_driver/modules/notification/widget/notification_unread_count_widget.dart';
import 'package:upmart_driver/modules/vehicle_documents/api/model/vehicle_registration_response_model.dart';
import 'package:upmart_driver/modules/vehicle_documents/constants/image_constants.dart';
import 'package:upmart_driver/modules/vehicle_documents/api/model/vehicle_registartion_request_model.dart';
import 'package:upmart_driver/modules/vehicle_documents/driver_details/page/vehicle_registration/bloc/vehicle_register_detail_bloc.dart';
import 'package:upmart_driver/modules/vehicle_documents/driver_details/page/vehicle_registration/bloc/vehicle_register_detail_state.dart';
import 'package:upmart_driver/modules/vehicle_documents/driver_details/page/vehicle_registration/manager/vehicle_register_action_manager.dart';

class VehicleRegistrationPage extends StatefulWidget {
  BuildContext context;
  VehicleRegistrationResponseModel driverRegistrationResponseModel;
  CommonPassDataModel commonPassDataModel;

  VehicleRegistrationPage(this.context) {
    commonPassDataModel = ModalRoute.of(context).settings.arguments;
    driverRegistrationResponseModel =
        commonPassDataModel?.vehicleRegistrationResponseModel;
  }

  @override
  _VehicleRegistrationPageState createState() =>
      _VehicleRegistrationPageState();
}

class _VehicleRegistrationPageState extends State<VehicleRegistrationPage> {
  // text editing controllers
  final TextEditingController _registrationNumberController =
      new TextEditingController();
  final TextEditingController _companyController = new TextEditingController();
  final TextEditingController _modelController = new TextEditingController();
  final TextEditingController _colorController = new TextEditingController();
  final TextEditingController _validController = new TextEditingController();

  //Model Declaration
  DriverRegistrationRequestModel _driverRegistrationRequestModel;

  //Bloc Variables
  VehicleRegisterDetailBloc _vehicleRegisterDetailBloc;

  //managers
  VehicleRegisterActionManager _vehicleRegisterActionManager =
      new VehicleRegisterActionManager();

  // VehicleRegistrationUtilsManager _vehicleRegistrationUtilsManager;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();

    _vehicleRegisterDetailBloc = new VehicleRegisterDetailBloc();

    _vehicleRegisterActionManager?.context = widget?.context;

    _vehicleRegisterActionManager?.vehicleRegisterDetailBloc =
        _vehicleRegisterDetailBloc;

    _vehicleRegisterActionManager?.commonPassDataModel =
        widget?.commonPassDataModel;

    _vehicleRegisterActionManager?.actionOnInit(
        currentState: _scaffoldKey?.currentState);
  }

  @override
  void dispose() {
    super.dispose();
    _registrationNumberController?.dispose();
    _companyController?.dispose();
    _modelController?.dispose();
    _colorController?.dispose();
    _validController?.dispose();
    _vehicleRegisterActionManager?.vehicleRegisterDetailBloc?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _vehicleRegisterActionManager?.context = context;
    return Scaffold(
      key: _scaffoldKey,
      appBar: _showAppBar(),
      body: BlocEventStateBuilder<VehicleRegisterDetailsState>(
        bloc: _vehicleRegisterActionManager?.vehicleRegisterDetailBloc,
        builder: (BuildContext context,
            VehicleRegisterDetailsState vehicleRegisterDetailsState) {
          _vehicleRegisterActionManager?.context = context;
          if (vehicleRegisterDetailsState != null &&
              _vehicleRegisterActionManager?.vehicleRegisterDetailsState !=
                  vehicleRegisterDetailsState) {
            _vehicleRegisterActionManager?.vehicleRegisterDetailsState =
                vehicleRegisterDetailsState;

            if (_vehicleRegisterActionManager
                    ?.vehicleRegisterDetailsState?.isLoading ==
                false) {
              //Set old data on controller
              _setDataToControllers();

              //Action on state change
              _vehicleRegisterActionManager?.actionOnStateChange(
                  currentState: _scaffoldKey?.currentState);
            }
          }
          return ModalProgressHUD(
            inAsyncCall: _vehicleRegisterActionManager
                    ?.vehicleRegisterDetailsState?.isLoading ??
                false,
            child: WillPopScope(
              onWillPop: () {
                if (widget.commonPassDataModel.screenPopCallBack != null) {
                  widget.commonPassDataModel.screenPopCallBack.onScreenPop();
                }
                return NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(
                    _vehicleRegisterActionManager?.context,
                    dataToBeSend: true);
              },
              child: Padding(
                padding: const EdgeInsets.only(
                    left: SIZE_15, right: SIZE_15, bottom: SIZE_15),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      _showDivider(),
                      _showVehicleRegistrationTitle(),
                      _showSpace(SIZE_10),
                      _showUploadPhotoView(),
                      _showSpace(SIZE_20),
                      _showVehicleDetailsForm()
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  //method to show app bar
  Widget _showAppBar() {
    return CommonUtils.commonUtilsInstance.getAppBar(
        context: _vehicleRegisterActionManager?.context,
        elevation: ELEVATION_0,
        centerTitle: false,
        actionWidgets: [
          Padding(
            padding: const EdgeInsets.only(right: SIZE_16, top: SIZE_16),
            child: NotificationReadCountWidget(
              context: context,
            ),
          )
        ],
        appBarTitle:
            AppLocalizations.of(context).driverDetails.text.driverDetails,
        popScreenOnTapOfLeadingIcon: false,
        defaultLeadIconPressed: () {
          if (widget.commonPassDataModel.screenPopCallBack != null) {
            widget.commonPassDataModel.screenPopCallBack.onScreenPop();
          }
          NavigatorUtils.navigatorUtilsInstance.navigatorPopScreen(
              _vehicleRegisterActionManager?.context,
              dataToBeSend: true);
        },
        defaultLeadingIcon: Icons.arrow_back,
        defaultLeadingIconColor: COLOR_BLACK,
        appBarTitleStyle: AppConfig.of(_vehicleRegisterActionManager?.context)
            .themeData
            .primaryTextTheme
            .headline3,
        backGroundColor: Colors.transparent);
  }

  //method to show vehicle registration title
  Widget _showVehicleRegistrationTitle() {
    return Text(
        AppLocalizations.of(_vehicleRegisterActionManager?.context)
            .vehicleRegistration
            .text
            .title,
        style: AppConfig.of(_vehicleRegisterActionManager?.context)
            .themeData
            .primaryTextTheme
            .headline5);
  }

  //method to show upload photo view
  Widget _showUploadPhotoView() {
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(SIZE_15))),
      elevation: ELEVATION_05,
      clipBehavior: Clip.antiAlias,
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              COLOR_CARD_GARDIENT_1,
              COLOR_CARD_GARDIENT_2,
            ],
            begin: const FractionalOffset(0.0, 2.2),
            end: const FractionalOffset(1.5, 1.0),
            stops: [0.0, 1.0],
            tileMode: TileMode.clamp,
          ),
        ),
        child: Column(
          children: [
            _showImageView(),
            _showUploadPhotoWidget(),
          ],
        ),
      ),
    );
  }

  //method to vehicle details form
  Widget _showVehicleDetailsForm() {
    return Wrap(
      runSpacing: SIZE_10,
      children: [
        _showRegistrationNumber(),
        _showVehicleCompany(),
        _showModelCompany(),
        _showColorCompany(),
        _showValidUpto(),
        _showNextButton()
      ],
    );
  }

  //text form field widget
  Widget _textFieldForm({
    TextEditingController controller,
    TextInputType keyboardType,
    Widget icon,
    String hint,
    bool isInputFormater,
    FormFieldValidator<String> validator,
    bool enable,
  }) {
    return Material(
      borderRadius: BorderRadius.circular(SIZE_5),
      color: Colors.white,
      child: Padding(
        padding: EdgeInsets.only(left: SIZE_10),
        child: TextFormField(
          validator: validator,
          style: AppConfig.of(_vehicleRegisterActionManager?.context)
              .themeData
              .textTheme
              .headline2,
          enabled: enable,
          controller: controller,
          keyboardType: keyboardType,
          textCapitalization: TextCapitalization.words,
          decoration: InputDecoration(
            hintText: hint,
            hintStyle: TextStyle(color: COLOR_BORDER_GREY),
            suffixIcon: icon ?? const SizedBox(),
            contentPadding: EdgeInsets.all(SIZE_0),
            border: OutlineInputBorder(borderSide: BorderSide.none),
          ),
        ),
      ),
    );
  }

  //registration number
  Widget _showRegistrationNumber() {
    return Container(
      padding: EdgeInsets.only(left: SIZE_5, right: SIZE_5),
      decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          border: Border.all(color: COLOR_BORDER_GREY, width: SIZE_1),
          borderRadius: BorderRadius.circular(SIZE_10)),
      child: _textFieldForm(
          controller: _registrationNumberController,
          icon: null,
          isInputFormater: false,
          hint: AppLocalizations.of(_vehicleRegisterActionManager?.context)
              .vehicleRegistration
              .hint
              .regno,
          enable: true),
    );
  }

  //vehicle company
  Widget _showVehicleCompany() {
    return Container(
      padding: EdgeInsets.only(left: SIZE_5, right: SIZE_5),
      decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          border: Border.all(color: COLOR_BORDER_GREY, width: SIZE_1),
          borderRadius: BorderRadius.circular(SIZE_10)),
      child: _textFieldForm(
          controller: _companyController,
          icon: null,
          isInputFormater: false,
          hint: AppLocalizations.of(_vehicleRegisterActionManager?.context)
              .vehicleRegistration
              .hint
              .company,
          enable: true),
    );
  }

  //vehicle model number
  Widget _showModelCompany() {
    return Container(
      padding: EdgeInsets.only(left: SIZE_5, right: SIZE_5),
      decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          border: Border.all(color: COLOR_BORDER_GREY, width: SIZE_1),
          borderRadius: BorderRadius.circular(SIZE_10)),
      child: _textFieldForm(
          controller: _modelController,
          icon: null,
          isInputFormater: false,
          hint: AppLocalizations.of(_vehicleRegisterActionManager?.context)
              .vehicleRegistration
              .hint
              .model,
          enable: true),
    );
  }

  //color of vehicle
  Widget _showColorCompany() {
    return Container(
      padding: EdgeInsets.only(left: SIZE_5, right: SIZE_5),
      decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          border: Border.all(color: COLOR_BORDER_GREY, width: SIZE_1),
          borderRadius: BorderRadius.circular(SIZE_10)),
      child: _textFieldForm(
          controller: _colorController,
          icon: null,
          isInputFormater: false,
          hint: AppLocalizations.of(_vehicleRegisterActionManager?.context)
              .vehicleRegistration
              .hint
              .color,
          enable: true),
    );
  }

  //vehicle registration valid upto
  Widget _showValidUpto() {
    return Container(
      padding: EdgeInsets.only(left: SIZE_5, right: SIZE_5),
      decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          border: Border.all(color: COLOR_BORDER_GREY, width: SIZE_1),
          borderRadius: BorderRadius.circular(SIZE_10)),
      child: InkWell(
        onTap: () {
          _showDatePicker(_vehicleRegisterActionManager?.context);
        },
        child: _textFieldForm(
            controller: _validController,
            icon: Icon(
              Icons.calendar_today,
              color: COLOR_BLACK,
            ),
            isInputFormater: false,
            hint: AppLocalizations.of(_vehicleRegisterActionManager?.context)
                .vehicleRegistration
                .hint
                .validUpto,
            enable: false),
      ),
    );
  }

  //show date picker dialog
  _showDatePicker(
    BuildContext context,
  ) async {
    var picked = await DatePickerUtils.datePickerUtilsInstance.getDatePicker(
        context: context,
        selectedDate: _validController.text ?? "",
        firstDate: DateTime.now());

    _validController.text = picked.isEmpty
        ? DateUtils.dateUtilsInstance.convertDateToYYYYMMDD(
            _vehicleRegisterActionManager
                    ?.vehicleRegisterDetailsState
                    ?.vehicleRegistrationResponseModel
                    ?.data
                    ?.vehicleRegisteration
                    ?.validUpto ??
                "")
        : picked;
    ;
  }

  //method to next button widget
  Widget _showNextButton() {
    return Align(
      alignment: Alignment.center,
      child: Container(
        margin: EdgeInsets.only(top: SIZE_20, bottom: SIZE_20),
        height: CommonUtils.commonUtilsInstance.getPercentageSize(
            context: context, percentage: SIZE_7, ofWidth: false),
        width: CommonUtils.commonUtilsInstance.getPercentageSize(
            context: context, percentage: SIZE_80, ofWidth: true),
        child: RaisedGradientButton(
          radious: SIZE_30,
          gradient: LinearGradient(
            begin: Alignment.bottomCenter,
            end: Alignment.topCenter,
            colors: <Color>[COLOR_PRIMARY, COLOR_ACCENT],
          ),
          onPressed: () {
            _vehicleRegisterActionManager?.actionOnClickNextButton(
                currentState: _scaffoldKey?.currentState,
                registrationNumberController: _registrationNumberController,
                colorController: _colorController,
                companyController: _companyController,
                modelController: _modelController,
                validController: _validController);
          },
          child: Text(
            AppLocalizations.of(_vehicleRegisterActionManager?.context)
                .common
                .text
                .next,
            textAlign: TextAlign.center,
            style: textStyleSize14WithWhiteColor,
          ),
        ),
      ),
    );
  }

  //show space btw the widgets
  Widget _showSpace(double size) {
    return SizedBox(
      height: size,
    );
  }

  //method to show image view
  Widget _showImageView() {
    return _getImageView();
  }

  //method to show upload photo widget
  Widget _showUploadPhotoWidget() {
    return InkWell(
      onTap: () {
        _vehicleRegisterActionManager?.actionOnUploadImage(
            currentState: _scaffoldKey?.currentState);
      },
      child: Container(
        decoration: new BoxDecoration(
          color: COLOR_WHITE,
          borderRadius: new BorderRadius.only(
              bottomRight: Radius.circular(SIZE_15),
              bottomLeft: Radius.circular(SIZE_15)),
          shape: BoxShape.rectangle,
        ),
        height: SIZE_40,
        alignment: Alignment.center,
        child: Text(
          AppLocalizations.of(_vehicleRegisterActionManager?.context)
              .common
              .text
              .uploadPhoto,
          style: TextStyle(color: Colors.black),
        ),
      ),
    );
  }

  //This will set data to controllers while updating the data
  void _setDataToControllers() {
    _registrationNumberController?.text =
        _registrationNumberController?.text?.trim()?.isNotEmpty == true
            ? _registrationNumberController?.text
            : _vehicleRegisterActionManager
                    ?.vehicleRegisterDetailsState
                    ?.vehicleRegistrationResponseModel
                    ?.data
                    ?.vehicleRegisteration
                    ?.registerationNumber ??
                "";

    _companyController?.text =
        _companyController?.text?.trim()?.isNotEmpty == true
            ? _companyController?.text
            : _vehicleRegisterActionManager
                    ?.vehicleRegisterDetailsState
                    ?.vehicleRegistrationResponseModel
                    ?.data
                    ?.vehicleRegisteration
                    ?.vehicleCompany ??
                "";

    _modelController?.text = _modelController?.text?.trim()?.isNotEmpty == true
        ? _modelController?.text
        : _vehicleRegisterActionManager
                ?.vehicleRegisterDetailsState
                ?.vehicleRegistrationResponseModel
                ?.data
                ?.vehicleRegisteration
                ?.model ??
            "";

    _colorController?.text = _colorController?.text?.trim()?.isNotEmpty == true
        ? _colorController?.text
        : _vehicleRegisterActionManager
                ?.vehicleRegisterDetailsState
                ?.vehicleRegistrationResponseModel
                ?.data
                ?.vehicleRegisteration
                ?.color ??
            "";

    _validController?.text = _validController?.text?.trim()?.isNotEmpty == true
        ? _validController?.text
        : DateUtils.dateUtilsInstance.convertDateToYYYYMMDD(
                _vehicleRegisterActionManager
                        ?.vehicleRegisterDetailsState
                        ?.vehicleRegistrationResponseModel
                        ?.data
                        ?.vehicleRegisteration
                        ?.validUpto ??
                    "") ??
            "";
  }

  //method to show view of image uploaded by user or empty image view
  Widget _getImageView() {
    //in the case of edit this condition will execute
    if (widget?.driverRegistrationResponseModel?.data?.vehicleRegisteration !=
            null &&
        widget?.driverRegistrationResponseModel?.data?.vehicleRegisteration
                ?.vehiclePicture?.isNotEmpty ==
            true) {
      return _getWidget(
          url: widget?.driverRegistrationResponseModel?.data
                  ?.vehicleRegisteration?.vehiclePicture ??
              '');
    }
    //in this case when user upload document for the first time
    else if (_vehicleRegisterActionManager
            ?.vehicleRegisterDetailsState
            ?.vehicleRegistrationResponseModel
            ?.data
            ?.vehicleRegisteration
            ?.vehiclePicture
            ?.isNotEmpty ==
        true) {
      return _getWidget(
          url: _vehicleRegisterActionManager
                  ?.vehicleRegisterDetailsState
                  ?.vehicleRegistrationResponseModel
                  ?.data
                  ?.vehicleRegisteration
                  ?.vehiclePicture ??
              "");
    }
    //this is the default case when no image is uploaded
    else {
      return Row(
        children: [
          Expanded(
              flex: 2,
              child: Align(
                alignment: Alignment.topLeft,
                child: Padding(
                  padding: const EdgeInsets.only(left: SIZE_10),
                  child: SvgPicture.asset(
                    VEHICLE_ICON,
                    color: Colors.white,
                    height: CommonUtils.commonUtilsInstance.getPercentageSize(
                        context: _vehicleRegisterActionManager?.context,
                        percentage: SIZE_10,
                        ofWidth: false),
                    width: CommonUtils.commonUtilsInstance.getPercentageSize(
                        context: _vehicleRegisterActionManager?.context,
                        percentage: SIZE_10,
                        ofWidth: true),
                  ),
                ),
              )),
          Expanded(
              flex: 3,
              child: Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.only(left: SIZE_25, right: SIZE_25),
                  height: CommonUtils.commonUtilsInstance.getPercentageSize(
                      context: _vehicleRegisterActionManager?.context,
                      percentage: SIZE_20,
                      ofWidth: false),
                  child: Wrap(
                    runSpacing: SIZE_10,
                    children: [
                      Container(
                        height: SIZE_45,
                        decoration: new BoxDecoration(
                          color: COLOR_LIGHT_PINK,
                          borderRadius: new BorderRadius.all(
                            Radius.circular(SIZE_5),
                          ),
                          shape: BoxShape.rectangle,
                        ),
                      ),
                      Container(
                        height: SIZE_15,
                        decoration: new BoxDecoration(
                          color: COLOR_LIGHT_PINK,
                          borderRadius: new BorderRadius.all(
                            Radius.circular(SIZE_20),
                          ),
                          shape: BoxShape.rectangle,
                        ),
                      ),
                      Container(
                        height: SIZE_15,
                        decoration: new BoxDecoration(
                          color: COLOR_LIGHT_PINK,
                          borderRadius: new BorderRadius.all(
                            Radius.circular(SIZE_20),
                          ),
                          shape: BoxShape.rectangle,
                        ),
                      )
                    ],
                  )))
        ],
      );
    }
  }

  //this widget will return a cache network image
  Widget _showCacheNetworkImage({String imageUrl}) {
    return ImageUtils.imageUtilsInstance.showCacheNetworkImage(
      context: context,
      showProgressBarInPlaceHolder: true,
      shape: BoxShape.rectangle,
      loaderHeight: CommonUtils?.commonUtilsInstance?.getPercentageSize(
          context: _vehicleRegisterActionManager?.context,
          percentage: SIZE_40,
          ofWidth: false),
      loaderWidth: CommonUtils?.commonUtilsInstance?.getPercentageSize(
          context: _vehicleRegisterActionManager?.context,
          percentage: SIZE_40,
          ofWidth: true),
      height: CommonUtils.commonUtilsInstance.getPercentageSize(
          context: _vehicleRegisterActionManager?.context,
          percentage: SIZE_24,
          ofWidth: false),
      width: double.infinity,
      url: imageUrl ?? "",
    );
  }

  //get widget which will show image
  Widget _getWidget({String url}) {
    return Container(
        height: CommonUtils.commonUtilsInstance.getPercentageSize(
            context: _vehicleRegisterActionManager?.context,
            percentage: SIZE_24,
            ofWidth: false),
        width: MediaQuery.of(context).size.width,
        child: _showCacheNetworkImage(imageUrl: url));
  }

//method to show divider text
  Widget _showDivider() {
    return Divider(
      color: COLOR_BORDER_GREY,
    );
  }
}
