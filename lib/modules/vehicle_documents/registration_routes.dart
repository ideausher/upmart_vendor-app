import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/my_earnings/page/my_earnings_page.dart';
import 'complete_profile/page/complete_profile_page.dart';
import 'driver_details/page/driver_details_page.dart';
import 'driver_details/page/driving_licence/page/driving_licence_page.dart';
import 'driver_details/page/licence_number_plate/page/licence_number_plate_page.dart';
import 'driver_details/page/vehicle_insurance/page/insurance_details_page.dart';
import 'driver_details/page/vehicle_registration/page/vehicle_registration_page.dart';

class RegistrationRoutes {
  static const String COMPLETE_PROFILE = '/COMPLETE_PROFILE';
  static const String DRIVER_DETAILS = '/DRIVER_DETAILS';
  static const String LICENCE_DETAILS = '/LICENCE_DETAILS';
  static const String LICENCE_NUMBER_PLATE = '/NUMBER_PLATE_DETAILS';
  static const String INSURANCE_DETAILS = '/INSURANCE_DETAILS';
  static const String VEHICLE_REG = '/VEHICLE_REG';
  static const String MY_EARNING_PAGE = '/MY_EARNING_PAGE';


  static Map<String, WidgetBuilder> routes() {
    Map<String, WidgetBuilder> route = {
      // When we navigate to the "/" route, build the FirstScreen Widget
      COMPLETE_PROFILE: (context) => CompleteProfilePage(context),
      DRIVER_DETAILS: (context) => DriverDetailsPage(context),
      LICENCE_DETAILS: (context) => DrivingLicencePage(context),
      LICENCE_NUMBER_PLATE: (context) => LicenceNumberPlatePage(context),
      INSURANCE_DETAILS: (context) => InsuranceDetailsPage(context),
      VEHICLE_REG: (context) => VehicleRegistrationPage(context),
      MY_EARNING_PAGE: (context) => MyEarningsPage(context),

    };

    return route;
  }
}
