import 'dart:io';

import 'package:flutter/material.dart';
import 'package:upmart_driver/localizations.dart';
import 'package:upmart_driver/modules/common/utils/date_utils.dart';

class VehicleRegistrationValidator {
  static VehicleRegistrationValidator _vehicleRegistrationValidator =
      VehicleRegistrationValidator();

  static VehicleRegistrationValidator get vehicleRegistrationInstance =>
      _vehicleRegistrationValidator;

  // used to check empty field
  bool checkEmptyField({String value}) {
    return value?.trim()?.isEmpty;
  }

  // validate vehicle registration screen
  String validateVehicleRegistrationScreen(
      {String regNumber,
      String company,
      String model,
      BuildContext context,
      String color,
      String validUpto,
      String file}) {
    String result = "";
    if (checkEmptyField(value: regNumber) == true)
      return result =
          AppLocalizations.of(context).verification.error.emptyRegNumber;
    if (checkEmptyField(value: company) == true)
      return result =
          AppLocalizations.of(context).verification.error.emptyVehicleCmpny;
    if (checkEmptyField(value: model) == true)
      return result =
          AppLocalizations.of(context).verification.error.emptyVehicleModel;
    if (checkEmptyField(value: color) == true)
      return result =
          AppLocalizations.of(context).verification.error.emptyVehicleColor;
    if (checkEmptyField(value: validUpto) == true)
      return result =
          AppLocalizations.of(context).verification.error.emptyValidity;
    if (checkEmptyFileField(imageFile: file) == true)
      return result =
          AppLocalizations.of(context).verification.error.emptyImage;

    return result;
  }

  // validate vehicle driving licence screen
  String validateDrivingLicenceScreen(
      {String licenceNumber,
      String expiryDate,
      BuildContext context,
      String file}) {
    String result = "";
    if (checkEmptyField(value: licenceNumber) == true)
      return result =
          AppLocalizations.of(context).verification.error.emptyLicenceNumber;
    if (checkEmptyField(value: expiryDate) == true)
      return result =
          AppLocalizations.of(context).verification.error.emptyValidity;
    if (checkEmptyFileField(imageFile: file) == true)
      return result =
          AppLocalizations.of(context).verification.error.emptyImage;
    return result;
  }

  // validate vehicle number plate screen
  String validateNumberPlateScreen(
      {String numberPlate, BuildContext context, String file}) {
    String result = "";
    if (checkEmptyField(value: numberPlate) == true)
      return result =
          AppLocalizations.of(context).verification.error.emptyNumberPlate;
    if (checkEmptyFileField(imageFile: file) == true)
      return result =
          AppLocalizations.of(context).verification.error.emptyImage;
    return result;
  }

  // validate vehicle number plate screen
  String validateVehicleInsuranceScreen(
      {String policyNumber,
      BuildContext context,
      String file,
      String validFrom,
      String validTill}) {
    String result = "";
    if (checkEmptyField(value: policyNumber) == true)
      return result =
          AppLocalizations.of(context).verification.error.emptyPolicyNumber;
    if (checkEmptyFileField(imageFile: file) == true)
      return result =
          AppLocalizations.of(context).verification.error.emptyImage;
    if (checkEmptyField(value: validFrom) == true)
      return result = AppLocalizations.of(context)
          .vehicleInsurance
          .error
          .plsEnterInsuranceValidFrom;
    if (checkEmptyField(value: validTill) == true)
      return result = AppLocalizations.of(context)
          .vehicleInsurance
          .error
          .plsEnterInsuranceValidTill;

    return result = checkSelectedDateValidOrNot(
        validFrom: validFrom, validTill: validTill, context: context);

    return result;
  }

  bool checkEmptyFileField({String imageFile}) {
    if (imageFile?.isNotEmpty == true)
      return false;
    else
      return true;
  }

  String checkSelectedDateValidOrNot(
      {String validFrom, String validTill, BuildContext context}) {
    String result = "";
    DateTime validFromDateTime =
        DateUtils.dateUtilsInstance.convertStringToDate(dateTime: validFrom);

    DateTime validTillDateTime =
        DateUtils.dateUtilsInstance.convertStringToDate(dateTime: validTill);

    if (validFromDateTime == validTillDateTime) {
      result = AppLocalizations.of(context)
          .vehicleInsurance
          .error
          .validFromAndValidTillMustBeDiff;
    } else if (validTillDateTime.isBefore(validFromDateTime)) {
      result = AppLocalizations.of(context)
          .vehicleInsurance
          .error
          .validTillGreaterThanValidFrom;
    }

    return result;
  }
}
