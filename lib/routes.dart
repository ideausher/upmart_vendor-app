import 'package:flutter/material.dart';
import 'package:upmart_driver/modules/orders/order_routes.dart';
import 'package:upmart_driver/modules/vehicle_documents/registration_routes.dart';
import 'package:upmart_driver/modules/intro/screens/start_screen_page.dart';
import 'modules/auth/auth_routes.dart';
import 'modules/help_and_support/queries/screens/query_listing_page.dart';
import 'modules/help_and_support/start_new_query/screens/send_query_page.dart';
import 'modules/notification/screens/notification/page/notification_page.dart';
import 'modules/stripe/page/stripe_linking_web_view_page.dart';
import 'modules/stripe/widget/stripe_alert_widget.dart';

class Routes {
  static const String DASH_BOARD = '/DASH_BOARD';
  static const String FEEDBACK = '/FEEDDBACK';
  static const String NOTIFICATION = '/NOTIFICATION';
  static const String START_SCREENS = '/START_SCREENS';
  static const String CART_SCREEN = '/CART_SCREEN';
  static const String STRIPE_LINKING_SCREEN = '/STRIPE_LINKING_SCREEN';
  static const String STRIPE_ALERT = '/STRIPE_ALERT';
  static const String QUERY_LISTING_PAGE = '/QUERY_LISTING_PAGE';
  static const String SEND_QUERY_PAGE = '/SEND_QUERY_PAGE';

  static Map<String, WidgetBuilder> routes() {
    Map<String, WidgetBuilder> route = {
      // When we navigate to the "/" route, build the FirstScreen Widget
      START_SCREENS: (context) => StartScreenPage(context),
      STRIPE_LINKING_SCREEN: (context) => StripeLinkingWebViewScreen(context),
      STRIPE_ALERT: (context) => StripeAlertWidget(context),
      QUERY_LISTING_PAGE: (context) => QueriesListingPage(context),
      SEND_QUERY_PAGE: (context) => SendQueryPage(context),
      NOTIFICATION: (context) => NotificationPage(context)

    };

    route.addAll(AuthRoutes.routes());
    route.addAll(RegistrationRoutes.routes());
    route.addAll(OrderRoutes.routes());

    return route;
  }
}
